package pl.oakfusion.sst.data.util;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TypesTest {

    @Test
    public void should_count_as_integer() {
        //given
        String integerToParse = "324";
        //when
        boolean isInteger = Types.isInteger(integerToParse);
        //then
        assertThat(isInteger).isTrue();
    }

    @Test
    public void should_count_as_negative_integer() {
        //given
        String integerToParse = "-10078";
        //when
        boolean isInteger = Types.isInteger(integerToParse);
        //then
        assertThat(isInteger).isTrue();
    }

    @Test
    public void should_not_count_as_integer() {
        //given
        String notIntegerToParse = "1000.203";
        //when
        boolean isInteger = Types.isInteger(notIntegerToParse);
        //then
        assertThat(isInteger).isFalse();
    }

    @Test
    public void should_count_as_double() {
        //given
        String doubleToParse = "324.6574";
        //when
        boolean isDouble = Types.isDouble(doubleToParse);
        //then
        assertThat(isDouble).isTrue();
    }

    @Test
    public void should_count_integer_as_double() {
        //given
        String doubleToParse = "10008";
        //when
        boolean isDouble = Types.isDouble(doubleToParse);
        //then
        assertThat(isDouble).isTrue();
    }

    @Test
    public void should_count_as_negative_double() {
        //given
        String doubleToParse = "-10078.234";
        //when
        boolean isDouble = Types.isDouble(doubleToParse);
        //then
        assertThat(isDouble).isTrue();
    }

    @Test
    public void should_not_count_as_double() {
        //given
        String notDoubleToParse = "1000ss10";
        //when
        boolean isDouble = Types.isDouble(notDoubleToParse);
        //then
        assertThat(isDouble).isFalse();
    }
}
