package pl.oakfusion.sst.data.util;

import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class AbbreviationFactoryTest {

    @Test
    public void should_return_name() {
        //when:
        List<String> abbreviations = AbbreviationFactory.getAbbreviations("abcdefg", "");
        //then:
        assertThat(abbreviations).containsExactly("ABCDEFG");
    }

    @Test
    public void should_return_all_abbreviations() {
        //when:
        List<String> abbreviations = AbbreviationFactory.getAbbreviations("supertrek", "s");
		//then
        assertThat(abbreviations).containsExactly("S", "SU", "SUP", "SUPE", "SUPER", "SUPERT", "SUPERTR", "SUPERTRE", "SUPERTREK");
    }

    @Test
    public void should_return_limited_abbreviations() {
        //when:
        List<String> abbreviations = AbbreviationFactory.getAbbreviations("project-trek", "project");
        //then:
        assertThat(abbreviations).containsExactly("PROJECT", "PROJECT-", "PROJECT-T", "PROJECT-TR", "PROJECT-TRE", "PROJECT-TREK");
    }

    @Test
    public void should_return_no_abbreviations() {
        //when:
        List<String> abbreviations = AbbreviationFactory.getAbbreviations("hellothere123", "123");
        //then:
        assertThat(abbreviations.size()).isEqualTo(0);
    }
}
