package pl.oakfusion.sst.data.util;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class RectangleTest {

    @Test
    public void should_not_allow_negative_x() {
        //given
        int negativeX = -1;
        //when
        Throwable throwable = catchThrowable(() -> new Rectangle(negativeX, 1, 1, 1));
        //then
        assertThat(throwable).isInstanceOf(IllegalArgumentException.class)
                             .hasMessage("negative x position: %s", negativeX);
    }

    @Test
    public void should_not_allow_negative_y() {
        //given
        int negativeY = -1;
        //when
        Throwable throwable = catchThrowable(() -> new Rectangle(1, negativeY, 1, 1));
        //then
        assertThat(throwable).isInstanceOf(IllegalArgumentException.class)
                             .hasMessage("negative y position: %s", negativeY);
    }

    @Test
    public void should_not_allow_negative_width() {
        //given
        int negativeWidth = -1;
        //when
        Throwable throwable = catchThrowable(() -> new Rectangle(1, 1, negativeWidth, 1));
        //then
        assertThat(throwable).isInstanceOf(IllegalArgumentException.class)
                             .hasMessage("negative width: %s", negativeWidth);
    }

    @Test
    public void should_not_allow_negative_height() {
        //given
        int negativeHeight = -1;
        //when
        Throwable throwable = catchThrowable(() -> new Rectangle(1, 1, 1, negativeHeight));
        //then
        assertThat(throwable).isInstanceOf(IllegalArgumentException.class)
                             .hasMessage("negative height: %s", negativeHeight);
    }

    @Test
    public void should_create_rectangle_with_positive_arguments() {
        //when
        Throwable throwable = catchThrowable(() -> new Rectangle(1, 0, 1, 0));
        //then
        assertThat(throwable).isNull();
    }

    @Test
    public void should_assign_values_to_rectangle() {
        //given
        int x = 5;
        int y = 6;
        int w = 3;
        int h = 10;
        //when
        Rectangle rectangle = new Rectangle(x, y, w, h);
        //then
        assertThat(rectangle.getX()).isEqualTo(x);
        assertThat(rectangle.getY()).isEqualTo(y);
        assertThat(rectangle.getWidth()).isEqualTo(w);
        assertThat(rectangle.getHeight()).isEqualTo(h);
    }

    @Test
    public void should_fill_rectangle_with_default_values() {
        //when
        Rectangle rectangle = new Rectangle();
        //then
        assertThat(rectangle.getX()).isEqualTo(0);
        assertThat(rectangle.getY()).isEqualTo(0);
        assertThat(rectangle.getWidth()).isEqualTo(0);
        assertThat(rectangle.getHeight()).isEqualTo(0);
    }
}
