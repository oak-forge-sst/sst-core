package pl.oakfusion.sst.data.util;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class ViewportTest {

    @Test
    public void should_render_joined_viewports() {

        // given
        Viewport testAboveView = new Viewport(3, 3, '.');
        Viewport testBelowView = new Viewport(4, 2, '_');

        String testString = "... \n" +
                            "... \n" +
                            "... \n" +
                            "    \n" +
                            "    \n" +
                            "____\n" +
                            "____";

        // when
        Viewport joinedViewport = testAboveView.joinAboveOf(testBelowView, 2);

        // then
        assertThat(joinedViewport.toString()).isEqualTo(testString);

    }

    @Test
    public void should_not_allow_negative_width() {
        // given
        int negativeWidth = -1;

        // when
        Throwable testThrowable = catchThrowable(() -> new Viewport(negativeWidth, 3, '.'));

        // then
        assertThat(testThrowable).isInstanceOf(IllegalArgumentException.class)
                                 .hasMessage("negative or 0 width value: %s", negativeWidth);
    }

    @Test
    public void should_not_allow_negative_height() {
        // given
        int negativeHeight = -14;

        // when
        Throwable testThrowable = catchThrowable(() -> new Viewport(3, negativeHeight, '.'));

        // then
        assertThat(testThrowable).isInstanceOf(IllegalArgumentException.class)
                                 .hasMessage("negative or 0 height value: %s", negativeHeight);
    }

    @Test
    public void should_not_allow_negative_spacing() {
        // given
        int negativeSpacing = -4;
        Viewport testAboveView = new Viewport(3, 3, '.');
        Viewport testBelowView = new Viewport(4, 2, '_');

        // when
        Throwable testThrowable = catchThrowable(() -> testAboveView.joinAboveOf(testBelowView, negativeSpacing));

        // then
        assertThat(testThrowable).isInstanceOf(IllegalArgumentException.class)
                                 .hasMessage("negative value of spacing: %s", negativeSpacing);
    }

    @Test
    public void should_not_allow_zero_width() {
        // given
        int zeroWidth = 0;

        // when
        Throwable testThrowable = catchThrowable(() -> new Viewport(zeroWidth, 3, '.'));

        // then
        assertThat(testThrowable).isInstanceOf(IllegalArgumentException.class)
                                 .hasMessage("negative or 0 width value: %s", zeroWidth);
    }

    @Test
    public void should_not_allow_zero_height() {
        // given
        int zeroHeight = 0;

        // when
        Throwable testThrowable = catchThrowable(() -> new Viewport(3, zeroHeight, '.'));

        // then
        assertThat(testThrowable).isInstanceOf(IllegalArgumentException.class)
                                 .hasMessage("negative or 0 height value: %s", zeroHeight);
    }

    @Test
    public void should_render_joinOnLeftOf() {
        //given
        Viewport testAboveView = new Viewport(3, 3, '.');
        Viewport testRightView = new Viewport(2, 3, ',');
        String testString = "... ,,\n" +
                            "... ,,\n" +
                            "... ,,";
        //when
        Viewport joinedViewport = testAboveView.joinLeftOf(testRightView, 1);
        //then
        assertThat(joinedViewport.toString()).isEqualTo(testString);
    }

    @Test
    public void should_render_joinOnLeftOf_without_spacing() {
        //given
        Viewport testAboveView = new Viewport(3, 3, '.');
        Viewport testRightView = new Viewport(2, 3, ',');
        String testString = "...,,\n" +
                            "...,,\n" +
                            "...,,";
        //when
        Viewport joinedViewport = testAboveView.joinLeftOf(testRightView);
        //then
        assertThat(joinedViewport.toString()).isEqualTo(testString);
    }


    @Test
    public void should_render_joinOnRightOf() {
        //given
        Viewport testAboveView = new Viewport(3, 3, '.');
        Viewport testLeftView = new Viewport(2, 3, ',');
        String testString = ",, ...\n" +
                            ",, ...\n" +
                            ",, ...";
        //when
        Viewport joinedViewport = testAboveView.joinRightOf(testLeftView, 1);
        //then
        assertThat(joinedViewport.toString()).isEqualTo(testString);
    }


    @Test
    public void should_render_joinOnRightOf_without_spacing() {
        //given
        Viewport testAboveView = new Viewport(3, 3, '.');
        Viewport testLeftView = new Viewport(2, 3, ',');
        String testString = ",,...\n" +
                            ",,...\n" +
                            ",,...";
        //when
        Viewport joinedViewport = testAboveView.joinRightOf(testLeftView);
        //then
        assertThat(joinedViewport.toString()).isEqualTo(testString);
    }


    @Test
    public void should_render_joined_viewports_without_spacing() {

        // given
        Viewport testAboveView = new Viewport(3, 3, '.');
        Viewport testBelowView = new Viewport(4, 2, '_');

        String testString = "... \n" +
                            "... \n" +
                            "... \n" +
                            "____\n" +
                            "____";

        // when
        Viewport joinedViewport = testAboveView.joinAboveOf(testBelowView);

        // then
        assertThat(joinedViewport.toString()).isEqualTo(testString);

    }

    @Test
    public void should_render_joined_viewports_joinBelow() {

        // given
        Viewport testAboveView = new Viewport(3, 3, '.');
        Viewport testBelowView = new Viewport(4, 2, '_');

        String testString = "... \n" +
                            "... \n" +
                            "... \n" +
                            "    \n" +
                            "    \n" +
                            "    \n" +
                            "____\n" +
                            "____";

        // when
        Viewport joinedViewport = testBelowView.joinBelowOf(testAboveView, 3);

        // then
        assertThat(joinedViewport.toString()).isEqualTo(testString);

    }

    @Test
    public void should_render_joined_viewports_joinBelow_without_spacing() {

        // given
        Viewport testAboveView = new Viewport(3, 3, '.');
        Viewport testBelowView = new Viewport(4, 2, '_');

        String testString = "... \n" +
                            "... \n" +
                            "... \n" +
                            "____\n" +
                            "____";

        // when
        Viewport joinedViewport = testBelowView.joinBelowOf(testAboveView);

        // then
        assertThat(joinedViewport.toString()).isEqualTo(testString);

    }

    @Test
    public void should_ExtendBelow() {
        //given
        Viewport testAboveView = new Viewport(3, 3, '.');
        String testString = "...\n" +
                            "...\n" +
                            "...\n" +
                            "   ";
        //when
        Viewport joinedViewport = testAboveView.extendBelow(1);
        //then
        assertThat(joinedViewport.toString()).isEqualTo(testString);
    }

    @Test
    public void should_ExtendAbove() {
        //given
        Viewport testAboveView = new Viewport(3, 3, '.');
        String testString = "   \n" +
                            "...\n" +
                            "...\n" +
                            "...";
        //when
        Viewport joinedViewport = testAboveView.extendAbove(1);
        //then
        assertThat(joinedViewport.toString()).isEqualTo(testString);
    }

    @Test
    public void should_ExtendOnRight() {
        //given
        Viewport testAboveView = new Viewport(3, 3, '.');
        String testString = "... \n" +
                            "... \n" +
                            "... ";
        //when
        Viewport joinedViewport = testAboveView.extendRight(1);
        //then
        assertThat(joinedViewport.toString()).isEqualTo(testString);
    }

    @Test
    public void should_ExtendOnLeft() {
        //given
        Viewport testAboveView = new Viewport(3, 3, '.');
        String testString = " ...\n" +
                            " ...\n" +
                            " ...";
        //when
        Viewport joinedViewport = testAboveView.extendLeft(1);
        //then
        assertThat(joinedViewport.toString()).isEqualTo(testString);
    }


    @Test
    public void should_generate_viewport_when_width_is_0() {
        //given
        int width = 0;

        //when
        Throwable testThrowable = catchThrowable(() -> new Viewport(width, 3, ' '));

        //then
        assertThat(testThrowable).isInstanceOf(IllegalArgumentException.class)
                                 .hasMessage("negative or 0 width value: %s", width);
    }

    @Test
    public void should_generate_viewport_when_height_is_0() {
        //given
        int height = 0;

        //when
        Throwable testThrowable = catchThrowable(() -> new Viewport(3, height, ' '));

        //then
        assertThat(testThrowable).isInstanceOf(IllegalArgumentException.class)
                                 .hasMessage("negative or 0 height value: %s", height);
    }


    @Test
    public void should_generate_viewport_when_character_is_null() {
        //given
        char array[][] = null;

        char testFillCharacter = ' ';

        //when
        Throwable testThrowable = catchThrowable(() -> new Viewport(array, ' '));
        //then
        assertThat(testThrowable).isInstanceOf(IllegalArgumentException.class)
                                 .hasMessage("input characters array cannot be null", testFillCharacter);
    }


}
