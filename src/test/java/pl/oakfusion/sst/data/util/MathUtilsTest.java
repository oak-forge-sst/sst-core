package pl.oakfusion.sst.data.util;

import org.junit.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class MathUtilsTest {

    @Test
    public void should_get_max_value_from_parameters() {
        //given
        int max = 6;
        //when
        Optional<Integer> result = MathUtils.max(max - 3, max, max - 1, max - 2, max - 3);
        //then
        assertThat(result).isNotEmpty().contains(max);
    }

    @Test
    public void should_get_min_value_from_parameters() {
        //given
        int min = 1;
        //when
        Optional<Integer> result = MathUtils.min(min + 3, min, min + 1, min + 2);
        //then
        assertThat(result).isNotEmpty().contains(min);
    }

    @Test
    public void should_return_empty_optional_on_no_parameters_passed_to_max() {
        //when
        Optional<?> result = MathUtils.max();
        //then
        assertThat(result).isEmpty();
    }

    @Test
    public void should_return_empty_optional_on_no_parameters_passed_to_min() {
        //when
        Optional<?> result = MathUtils.min();
        //then
        assertThat(result).isEmpty();
    }
}
