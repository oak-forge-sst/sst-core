package pl.oakfusion.sst.data.util;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.Collection;
import java.util.Random;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnitParamsRunner.class)
public class RandomGeneratorTest {

    private RandomGenerator random = new RandomGenerator();

    private Object[] testCase(int min, int max) {
        return new Object[]{min, max};
    }

    private Object[] passingParams() {
        return new Object[]{
                testCase(1, 10),
                testCase(2, 5),
                testCase(1, 1),
                testCase(10, 10)
        };
    }

    @Test
    @Parameters(method = "passingParams")
    public void should_generate_numbers_in_range(int min, int max) {
        // when
        int randomIntInRange = random.nextIntInRange(min, max);
        // then
        assertThat(randomIntInRange).isBetween(min, max);
    }

    @Test
    public void should_return_empty_list_on_shuffling_empty_list() {
        //given
        Collection<Integer> emptyList = emptyList();

        //when
        Collection<Integer> shuffledList = random.shuffle(emptyList);

        //then
        assertThat(shuffledList).isNotNull().isEmpty();
    }

    @Test
    public void should_return_empty_list_on_shuffling_null() {
        //when
        Collection<Object> shuffledList = random.shuffle(null);

        //then
        assertThat(shuffledList).isNotNull().isEmpty();
    }

    @Test
    public void should_return_same_list_as_input_list() {
        //given
        Random random = mock(Random.class);
        RandomGenerator randomGenerator = new RandomGenerator(random);

        Collection<Character> chars = Arrays.asList('r', 'a', 'n', 'd', 'o', 'm', 'i', 'z', 'e', 'd');

        when(random.nextInt(anyInt())).thenReturn(0);

        //when
        Collection<Character> shuffledChars = randomGenerator.shuffle(chars);

        //then
        assertThat(shuffledChars).isNotNull()
                .hasSize(10)
                .containsExactly('r', 'a', 'n', 'd', 'o', 'm', 'i', 'z', 'e', 'd');
    }

    @Test
    public void should_reverse_list() {
        //given
        Random random = mock(Random.class);
        RandomGenerator randomGenerator = new RandomGenerator(random);

        Collection<Character> chars = Arrays.asList('r', 'a', 'n', 'd', 'o', 'm', 'i', 'z', 'e', 'd');

        when(random.nextInt(anyInt())).thenReturn(9, 8, 7, 6, 5, 4, 3, 2, 1, 0);

        //when
        Collection<Character> shuffledChars = randomGenerator.shuffle(chars);

        //then
        assertThat(shuffledChars).isNotNull()
                .hasSize(10)
                .containsExactly('d', 'e', 'z', 'i', 'm', 'o', 'd', 'n', 'a', 'r');
    }

    @Test
    public void should_shuffle_list_according_to_random() {
        //given
        Random random = mock(Random.class);
        RandomGenerator randomGenerator = new RandomGenerator(random);

        Collection<Character> chars = Arrays.asList('r', 'a', 'n', 'd', 'o', 'm', 'i', 'z', 'e', 'd');

        when(random.nextInt(anyInt())).thenReturn(3, 8, 5, 2, 0, 1, 2, 1, 0, 0);

        //when
        Collection<Character> shuffledChars = randomGenerator.shuffle(chars);

        //then
        assertThat(shuffledChars).isNotNull()
                .hasSize(10)
                .containsExactly('d', 'd', 'i', 'n', 'r', 'o', 'z', 'm', 'a', 'e');
    }
}
