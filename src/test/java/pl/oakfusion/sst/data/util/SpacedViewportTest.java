package pl.oakfusion.sst.data.util;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

public class SpacedViewportTest {

    @Test
    public void should_not_allow_negative_spacing() {
        //given
        int spacing = -1;
        //when
        Throwable throwable = catchThrowable(() -> new SpacedViewport(10, 10, spacing));
        //then
        assertThat(throwable).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void should_correctly_assign_dimensions() {
        //given
        int width = 10;
        int height = 7;
        int spacingX = 2;
        int spacingY = 1;
        int totalWidth = 28;
        int totalHeight = 13;
        //when
        SpacedViewport spacedViewport = new SpacedViewport(width, height, spacingX, spacingY);
        //then
        assertThat(spacedViewport.getWidth()).isEqualTo(totalWidth);
        assertThat(spacedViewport.getHeight()).isEqualTo(totalHeight);
    }

    @Test
    public void should_correctly_assign_values_via_set_method() {
        //given
        int x = 1;
        int y = 1;
        char character = 'C';
        SpacedViewport spacedViewport = new SpacedViewport(10, 10, 3);
        //when
        spacedViewport.set(x, y, character);
        //then
        assertThat(spacedViewport.get(x, y)).isEqualTo(character);
    }

    @Test
    public void should_correctly_convert_viewport_to_string_representation() {
        //given
        SpacedViewport spacedViewport = new SpacedViewport(3, 2, 3, 2);
        spacedViewport.set(0, 0, 'A');
        spacedViewport.set(1, 0, 'B');
        spacedViewport.set(2, 0, 'C');
        spacedViewport.set(0, 1, 'D');
        spacedViewport.set(1, 1, 'E');
        spacedViewport.set(2, 1, 'F');
        String expectedRepresentation = "A   B   C\n" +
                                        "         \n" +
                                        "         \n" +
                                        "D   E   F";
        //when
        String viewportStringRepresentation = spacedViewport.toString();
        //then
        assertThat(viewportStringRepresentation).isEqualTo(expectedRepresentation);
    }


    // composition not working!
    @Test
    public void should_correctly_join_two_spaced_viewports() {
        //given
        SpacedViewport aboveViewport = new SpacedViewport(3, 4, '#', 1);
        SpacedViewport belowViewport = new SpacedViewport(4, 2, '$', 2, 1);
        String expectedRepresentation = "#####     \n" +
                                        "#####     \n" +
                                        "#####     \n" +
                                        "#####     \n" +
                                        "#####     \n" +
                                        "#####     \n" +
                                        "#####     \n" +
                                        "$$$$$$$$$$\n" +
                                        "$$$$$$$$$$\n" +
                                        "$$$$$$$$$$";

        //when
        Viewport joinedViewport = aboveViewport.joinAboveOf(belowViewport);
        //then
        assertThat(joinedViewport.toString()).isEqualTo(expectedRepresentation);
    }

    @Test
    public void should_correctly_assign_values_after_joining_viewports() {
        //given
        SpacedViewport leftViewport = new SpacedViewport(3, 2, 2, 0);
        SpacedViewport rightViewport = new SpacedViewport(2, 3, 1, 2);

        leftViewport.set(1, 1, 'A');
        leftViewport.set(2, 1, 'B');

        rightViewport.set(1, 0, 'E');
        rightViewport.set(0, 1, 'C');
        rightViewport.set(1, 2, 'D');

        String expected = "          E\n" +
                          "   A  B    \n" +
                          "           \n" +
                          "        C  \n" +
                          "           \n" +
                          "           \n" +
                          "          D";
        //when
        Viewport joinedViewport = leftViewport.joinLeftOf(rightViewport, 1);
        //then
        assertThat(joinedViewport.toString()).isEqualTo(expected);

    }
}
