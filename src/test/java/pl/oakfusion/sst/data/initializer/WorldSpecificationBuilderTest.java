package pl.oakfusion.sst.data.initializer;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import pl.oakfusion.sst.data.parameter.GameDifficulty;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static pl.oakfusion.sst.data.initializer.WorldSpecification.WorldSpecificationBuilder.*;

@RunWith(JUnitParamsRunner.class)
public class WorldSpecificationBuilderTest {

    private WorldSpecification.WorldSpecificationBuilder builder;

    @Before
    public void setupBuilder() {
        builder = new WorldSpecification.WorldSpecificationBuilder().widthInQuadrants(10)
                                                 .heightInQuadrants(10)
                                                 .quadrantSize(10);
    }

    @Test
    public void should_not_allow_negative_width() {
        //given
        int width = -1;
        //when
        builder.widthInQuadrants(width);
        Throwable throwable = catchThrowable(() -> builder.build());
        //then
        assertThat(throwable).isInstanceOf(IllegalArgumentException.class)
                             .hasMessage("World width must be positive.");
    }

    @Test
    public void should_not_allow_zero_width() {
        //given
        int width = 0;
        //when
        builder.widthInQuadrants(width);
        Throwable throwable = catchThrowable(() -> builder.build());
        //then
        assertThat(throwable).isInstanceOf(IllegalArgumentException.class)
                             .hasMessage("World width must be positive.");
    }

    @Test
    public void should_not_allow_negative_height() {
        //given
        int height = -1;
        //when
        builder.heightInQuadrants(height);
        Throwable throwable = catchThrowable(() -> builder.build());
        //then
        assertThat(throwable).isInstanceOf(IllegalArgumentException.class)
                             .hasMessage("World height must be positive.");
    }

    @Test
    public void should_not_allow_zero_height() {
        //given
        int height = 0;
        //when
        builder.heightInQuadrants(height);
        Throwable throwable = catchThrowable(() -> builder.build());
        //then
        assertThat(throwable).isInstanceOf(IllegalArgumentException.class)
                             .hasMessage("World height must be positive.");
    }

    @Test
    public void should_ensure_correct_quadrant_size() {
        //given
        int quadrantSize = MIN_QUADRANT_SIZE - 1;
        //when
        builder.quadrantSize(quadrantSize);
        Throwable throwable = catchThrowable(() -> builder.build());
        //then
        assertThat(throwable).isInstanceOf(IllegalArgumentException.class)
                             .hasMessageStartingWith("Quadrant size must be bigger or equal to")
                             .hasMessageEndingWith(String.valueOf(MIN_QUADRANT_SIZE));
    }

    @Test
    public void should_ensure_small_enough_star_quantity() {
        //given
        int starQuantity = (int) (10000 * MAX_STAR_PER_QUADRANT_RATIO) + 1;
        //when
        builder.starQuantity(starQuantity);
        Throwable throwable = catchThrowable(() -> builder.build());
        //then
        assertThat(throwable).isInstanceOf(IllegalArgumentException.class)
                             .hasMessageStartingWith("Star ratio per quadrant must be less or equal to")
                             .hasMessageEndingWith(String.valueOf(MAX_STAR_PER_QUADRANT_RATIO));
    }

    @Test
    public void should_ensure_small_enough_planet_quantity() {
        //given
        int planetQuantity = (int) (10000 * MAX_PLANET_PER_QUADRANT_RATIO) + 1;
        //when
        builder.planetQuantity(planetQuantity);
        Throwable throwable = catchThrowable(() -> builder.build());
        //then
        assertThat(throwable).isInstanceOf(IllegalArgumentException.class)
                             .hasMessageStartingWith("Planet ratio per quadrant must be less or equal to")
                             .hasMessageEndingWith(String.valueOf(MAX_PLANET_PER_QUADRANT_RATIO));
    }

    @Test
    public void should_ensure_small_enough_black_hole_quantity() {
        //given
        int blackHoleQuantity = (int) (10000 * MAX_BLACK_HOLE_PER_QUADRANT_RATIO) + 1;
        //when
        builder.blackHoleQuantity(blackHoleQuantity);
        Throwable throwable = catchThrowable(() -> builder.build());
        //then
        assertThat(throwable).isInstanceOf(IllegalArgumentException.class)
                             .hasMessageStartingWith("Black hole ratio per quadrant must be less or equal to")
                             .hasMessageEndingWith(String.valueOf(MAX_BLACK_HOLE_PER_QUADRANT_RATIO));
    }

    @Test
    public void should_ensure_small_enough_star_base_quantity() {
        //given
        int starbaseQuantity = (int) (100 * MAX_STARBASE_RATIO) + 1;
        //when
        builder.starbaseQuantity(starbaseQuantity);
        Throwable throwable = catchThrowable(() -> builder.build());
        //then
        assertThat(throwable).isInstanceOf(IllegalArgumentException.class)
                             .hasMessageStartingWith("Starbase ratio must be less or equal to")
                             .hasMessageEndingWith(String.valueOf(MAX_STARBASE_RATIO));
    }


    @Test
    public void should_create_specification() {
        //given
        int width = 10;
        int height = 10;
        int quadrantSize = 10;
        int starQuantity = 100;
        int planetQuantity = 50;
		int blackHoleQuantity = 10;
		GameDifficulty gameDifficulty = GameDifficulty.NOVICE;
		int starbaseQuantity = 5;
        //when
        WorldSpecification specification = builder.widthInQuadrants(width)
                                                  .heightInQuadrants(height)
                                                  .quadrantSize(quadrantSize)
                                                  .starQuantity(starQuantity)
                                                  .planetQuantity(planetQuantity)
                                                  .blackHoleQuantity(blackHoleQuantity)
												  .gameDifficulty(gameDifficulty)
                                                  .starbaseQuantity(starbaseQuantity)
                                                  .build();
        //then
        assertThat(specification.getWidthInQuadrants()).isEqualTo(width);
        assertThat(specification.getHeightInQuadrants()).isEqualTo(height);
        assertThat(specification.getQuadrantSize()).isEqualTo(quadrantSize);
        assertThat(specification.getStarQuantity()).isEqualTo(starQuantity);
        assertThat(specification.getPlanetQuantity()).isEqualTo(planetQuantity);
        assertThat(specification.getBlackHoleQuantity()).isEqualTo(blackHoleQuantity);
        assertThat(specification.getStarbaseQuantity()).isEqualTo(starbaseQuantity);
    }

    private Object[] quadrantTestCase(int width, int height, int quadrantSize, double ratio, int maxObjectQuantity) {
        return new Object[]{width, height, quadrantSize, ratio, maxObjectQuantity};
    }

    private Object[] absoluteTestCase(int width, int height, double ratio, int maxObjectQuantity) {
        return new Object[]{width, height, ratio, maxObjectQuantity};
    }

    private Object[] objectPerQuadrantQuantityParams() {
        return new Object[]{
                quadrantTestCase(10, 10, 10, 1, 10000),
                quadrantTestCase(3, 3, 5, 1, 225),
                quadrantTestCase(10, 3, 5, 1, 750),
                quadrantTestCase(5, 5, 5, 1, 625),
                quadrantTestCase(10, 5, 5, 0.5, 625),
                quadrantTestCase(10, 5, 5, 0.1, 125),
                quadrantTestCase(10, 10, 10, 0.05, 500)
        };
    }

    private Object[] objectAbsoluteQuantityParams() {
        return new Object[]{
                absoluteTestCase(10, 10, 1, 100),
                absoluteTestCase(10, 10, 0.1, 10),
                absoluteTestCase(3, 25, 0.4, 31),
                absoluteTestCase(3, 17, 0.15, 8),
                absoluteTestCase(49, 91, 0.074, 330),
        };
    }

    @Test
    @Parameters(method = "objectPerQuadrantQuantityParams")
    public void should_correctly_assign_max_object_quantity_by_quadrant_ratio(int width,
                                                                              int height,
                                                                              int quadrantSize,
                                                                              double ratio,
                                                                              int maxObjectQuantity) {
        //given
        builder.widthInQuadrants(width);
        builder.heightInQuadrants(height);
        builder.quadrantSize(quadrantSize);
        //when
        double actualMaxQuantity = builder.maxObjectQuantityByQuadrantRatio(ratio);
        //then
        assertThat(actualMaxQuantity).isEqualTo(maxObjectQuantity);
    }

    @Test
    @Parameters(method = "objectAbsoluteQuantityParams")
    public void should_correctly_assign_max_objects_quantity_by_absolute_ratio(int width,
                                                                               int height,
                                                                               double ratio,
                                                                               int maxObjectQuantity) {
        //given
        builder.widthInQuadrants(width);
        builder.heightInQuadrants(height);
        //when
        double actualMaxQuantity = builder.maxObjectQuantityByAbsoluteRatio(ratio);
        //then
        assertThat(actualMaxQuantity).isEqualTo(maxObjectQuantity);
    }
}
