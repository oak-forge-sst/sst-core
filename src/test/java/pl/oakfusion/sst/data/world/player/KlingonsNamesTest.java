package pl.oakfusion.sst.data.world.player;

import org.junit.Test;
import pl.oakfusion.sst.data.world.player.KlingonsNames;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class KlingonsNamesTest {

    @Test
    public void get_klingon_names_list() {
        //given
        KlingonsNames klingonsNames = new KlingonsNames();

        //when
        List<String> names = klingonsNames.newName();

        //then
        assertThat(names).hasSizeGreaterThan(1);
    }

    @Test
    public void get_first_klingon_name() {
        //given
        KlingonsNames klingonsNames = new KlingonsNames();

        //when
        List<String> names = klingonsNames.newName();

        //then
        assertThat(names.get(0)).isEqualTo("T'aban Qonzorgh");
    }

}
