package pl.oakfusion.sst.data.world;

import org.junit.Test;
import pl.oakfusion.sst.data.world.gameobject.GameObject;

import static org.assertj.core.api.Assertions.assertThat;

public class QuadrantTest {

    private GameObject gameObject() {
        return new GameObject(0, 0);
    }

    @Test
    public void should_add_static_object() {
        // given
        Quadrant q = new Quadrant();
        GameObject go = gameObject();

        // when
        q.addStaticObject(go);

        // then
        assertThat(q.getStaticObjects()).contains(go);
    }

    @Test
    public void should_remove_static_object() {
        // given
        Quadrant q = new Quadrant();
        GameObject go = gameObject();
        q.addStaticObject(go);

        // when
        q.removeStaticObject(go);

        // then
        assertThat(q.getStaticObjects()).doesNotContain(go);
    }

    @Test
    public void should_add_dynamic_object() {
        // given
        Quadrant q = new Quadrant();
        GameObject go = gameObject();

        // when
        q.addDynamicObject(go);

        // then
        assertThat(q.getDynamicObjects()).contains(go);
    }

    @Test
    public void should_remove_dynamic_object() {
        // given
        Quadrant q = new Quadrant();
        GameObject go = gameObject();
        q.addDynamicObject(go);

        // when
        q.removeDynamicObject(go);

        // then
        assertThat(q.getDynamicObjects()).doesNotContain(go);
    }

}
