package pl.oakfusion.sst.data.world.gameobject;

import org.junit.Test;
import pl.oakfusion.sst.data.world.gameobject.GameObject;
import pl.oakfusion.testutils.TestGameObject;

import static org.assertj.core.api.Assertions.assertThat;

public class GameObjectTest {


    @Test
    public void should_spawn_game_object_within_valid_space() {
        // when
        GameObject go = new TestGameObject(4, 5);

        // then
        assertThat(go.getX()).isEqualTo(4);
        assertThat(go.getY()).isEqualTo(5);
    }

    @Test
    public void should_spawn_game_object_in_origin() {
        // when
        GameObject go = new TestGameObject(0, 0);

        // then
        assertThat(go.getX()).isEqualTo(0);
        assertThat(go.getY()).isEqualTo(0);
    }



}
