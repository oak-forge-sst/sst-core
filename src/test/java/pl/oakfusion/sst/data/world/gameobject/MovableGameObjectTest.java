package pl.oakfusion.sst.data.world.gameobject;

import org.junit.Test;
import pl.oakfusion.sst.data.world.gameobject.MovableGameObject;

import static org.assertj.core.api.Assertions.assertThat;

public class MovableGameObjectTest {

    @Test
    public void should_move_game_object() {
        // given
        MovableGameObject go = new TestMovableGameObject(4, 5);

        // when
        go.moveTo(1, 2);

        // then
        assertThat(go.getX()).isEqualTo(1);
        assertThat(go.getY()).isEqualTo(2);
    }

    @Test
    public void should_move_game_object_with_delta() {
        // given
        MovableGameObject go = new TestMovableGameObject(4, 5);

        // when
        go.moveDelta(1, -2);

        // then
        assertThat(go.getX()).isEqualTo(5);
        assertThat(go.getY()).isEqualTo(3);
    }

    private static class TestMovableGameObject extends MovableGameObject {
        private TestMovableGameObject(int x, int y) {
            super(x, y);
        }
    }
}
