package pl.oakfusion.sst.data.world.component;

import org.junit.Before;
import org.junit.Test;
import pl.oakfusion.sst.data.world.civilizations.federationcivilization.Enterprise;
import pl.oakfusion.sst.data.world.civilizations.federationcivilization.Federation;
import pl.oakfusion.sst.data.world.gameobject.GameObject;
import pl.oakfusion.sst.data.initializer.ComponentSpecification;
import pl.oakfusion.sst.data.initializer.ComponentSystemSpecification;
import pl.oakfusion.sst.data.util.RandomGenerator;
import pl.oakfusion.sst.data.world.civilizations.Unit;
import pl.oakfusion.sst.data.world.civilizations.klingoncivilization.Klingon;
import pl.oakfusion.sst.data.world.civilizations.klingoncivilization.KlingonUnitsSpecification;
import pl.oakfusion.sst.data.world.civilizations.UnitSpecification;
import pl.oakfusion.sst.data.world.gameobject.Star;
import pl.oakfusion.sst.data.world.gameobject.Starbase;
import pl.oakfusion.sst.data.world.player.CivilizationType;
import pl.oakfusion.sst.data.world.player.HumanPlayer;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Collections.singleton;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

public class QuadrantScanTest  {
Unit unit;
RandomGenerator random = new RandomGenerator();

	@Before
	public void init() {
		UUID playerUuid = UUID.randomUUID();
		HumanPlayer humanPlayer = new HumanPlayer(CivilizationType.FEDERATION, "qwe", playerUuid, true);
		KlingonUnitsSpecification klingonUnitsSpecification = new KlingonUnitsSpecification();
		klingonUnitsSpecification.setKlingonSpecification();
		Federation federation =new Federation( new Enterprise(10, 10, playerUuid, generateComponents(ComponentSystemSpecification.ENTERPRISE_COMPONENT_REGULAR_SPECIFICATION), new UnitSpecification(50.0, Condition.GREEN)));
		unit = federation.getEnterprise();
	}
	public ComponentSystem generateComponents(ComponentSystemSpecification componentSpecification) {
		Map<ComponentId, Component> components = new HashMap<>();
		componentSpecification.getSpecifications().values().forEach(specification -> {
			components.put(specification.getId(), specification.createComponent());
		});
		return new ComponentSystem(components);
	}

	private Klingon createKlingon(int x, int y, ComponentSystem componentSystem) {
		return new Klingon(x, y, UUID.randomUUID(), componentSystem, new UnitSpecification(50.0, Condition.GREEN));
	}

	@Test
	public void should_have_nothing_scanned_after_creation() {
		//given
		QuadrantScan scan = new QuadrantScan();

		//then
		assertThat(scan.getScannedObjects()).isEmpty();
		assertThat(scan.getScannedKlingons(unit)).isEqualTo(0);
		assertThat(scan.getScannedStarbases()).isEqualTo(0);
		assertThat(scan.getScannedPlayer(unit)).isEqualTo(0);
	}

	@Test
	public void should_indicate_scanned_klingons() {
		//given
		QuadrantScan scan = new QuadrantScan();
		Set<GameObject> klingonSet = singleton(createKlingon(1, 1, generateComponents(ComponentSystemSpecification.KLINGON_REGULAR_SPECIFICATION)));

		//when
		scan.setScannedObjects(klingonSet);
		//then
		assertThat(scan.getScannedObjects()).containsAll(klingonSet);
		assertThat(scan.getScannedKlingons(unit)).isEqualTo(100);
	}

	@Test
	public void should_indicate_scanned_starbases() {
		//given
		QuadrantScan scan = new QuadrantScan();
		Set<GameObject> starbaseSet = singleton(new Starbase(1, 1));

		//when
		scan.setScannedObjects(starbaseSet);
		//then
		assertThat(scan.getScannedObjects()).containsAll(starbaseSet);
		assertThat(scan.getScannedStarbases()).isEqualTo(10);
	}

	@Test
	public void should_indicate_scanned_player() {
		//given
		QuadrantScan scan = new QuadrantScan();

		Set<GameObject> playerSet = singleton(unit);
		//when
		scan.setScannedObjects(playerSet);
		//then
		assertThat(scan.getScannedObjects()).containsAll(playerSet);
		assertThat(scan.getScannedPlayer(unit)).isEqualTo(1);
	}

	@Test
	public void should_indicate_correct_number_of_game_objects() {
		//given
		QuadrantScan scan = new QuadrantScan();

		Set<GameObject> objects = Stream.of(
				unit,
				new Starbase(1, 1),
				new Starbase(1, 1),
				createKlingon(1, 1, generateComponents(ComponentSystemSpecification.KLINGON_REGULAR_SPECIFICATION)),
				createKlingon(1, 1, generateComponents(ComponentSystemSpecification.KLINGON_REGULAR_SPECIFICATION)),
				createKlingon(1, 1, generateComponents(ComponentSystemSpecification.KLINGON_REGULAR_SPECIFICATION))
		).collect(Collectors.toSet());
		//when
		scan.setScannedObjects(objects);
		//then
		assertThat(scan.getScannedObjects()).containsAll(objects);
		assertThat(scan.getScannedPlayer(unit)).isEqualTo(1);
		assertThat(scan.getScannedStarbases()).isEqualTo(20);
		assertThat(scan.getScannedKlingons(unit)).isEqualTo(300);
	}

	@Test
	public void should_overwrite_old_data_on_new_scan() {
		//given
		QuadrantScan scan = new QuadrantScan();

		Set<GameObject> oldData = Stream.of(
				new Star(1, 1),
				new Starbase(1, 1)
		).collect(Collectors.toSet());

		Set<GameObject> newData = Stream.of(
				new Starbase(1, 1),
				createKlingon(1, 1, generateComponents(ComponentSystemSpecification.KLINGON_REGULAR_SPECIFICATION)),
				createKlingon(1, 1, generateComponents(ComponentSystemSpecification.KLINGON_REGULAR_SPECIFICATION)),
				createKlingon(1, 1, generateComponents(ComponentSystemSpecification.KLINGON_REGULAR_SPECIFICATION))
		).collect(Collectors.toSet());

		//when
		scan.setScannedObjects(oldData);
		scan.setScannedObjects(newData);
		//then
		assertThat(scan.getScannedObjects()).containsAll(newData).doesNotContainAnyElementsOf(oldData);
	}

	@Test
	public void should_keep_indication_of_special_game_objects_after_new_scan() {
		//given
		QuadrantScan scan = new QuadrantScan();

		Set<GameObject> oldData = Stream.of(
				unit,
				new Starbase(1, 1)
		).collect(Collectors.toSet());


		Set<GameObject> newData = singleton(unit);

		//when
		scan.setScannedObjects(oldData);
		scan.setScannedObjects(newData);
		//then
		assertThat(scan.getScannedObjects()).containsAll(newData);
		assertThat(scan.getScannedPlayer(unit)).isEqualTo(1);
		assertThat(scan.getScannedStarbases()).isEqualTo(0);
		assertThat(scan.getScannedKlingons(unit)).isEqualTo(0);

	}
}
