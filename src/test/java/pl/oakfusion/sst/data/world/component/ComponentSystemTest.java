package pl.oakfusion.sst.data.world.component;

import org.junit.Test;
import pl.oakfusion.sst.data.initializer.ComponentSpecification;
import pl.oakfusion.sst.data.initializer.ComponentSystemSpecification;
import pl.oakfusion.sst.data.initializer.ComponentSystemSpecificationBuilder;
import pl.oakfusion.sst.data.util.RandomGenerator;
import pl.oakfusion.sst.data.world.component.strategy.damage.OverflowNormalHitDamageReceivingStrategy;
import pl.oakfusion.sst.data.world.component.strategy.repair.MostBrokenFirstRepairingStrategy;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static pl.oakfusion.sst.data.world.component.ShieldsStatus.DOWN;
import static pl.oakfusion.sst.data.world.component.ShieldsStatus.UP;


public class ComponentSystemTest {

    @Test
    public void should_damage_only_shields_when_up() {
        //given
        RandomGenerator randomGenerator = new RandomGenerator();
        OverflowNormalHitDamageReceivingStrategy strategy = new OverflowNormalHitDamageReceivingStrategy(randomGenerator);
        Shields shields = new Shields(100,100, 100, 100, 100, UP);
	    Map<ComponentId, Component> components = new HashMap<>() {{
			put(ComponentId.SHIELDS, shields);
			put(ComponentId.HULL, new TestComponent(100));
		}};
        ComponentSystem packet = new ComponentSystem(components);

        //when
        packet.receiveDamage(strategy, 80);

        //then
        assertThat(shields.getEnergyContainer().getEnergy()).isEqualTo(20);
        assertThat(components.values()).allMatch(component -> component.calculateHealthPercentage() == 100);
    }

    @Test
    public void should_damage_components_when_no_shield_provided() {
        //given
        RandomGenerator randomGenerator = new RandomGenerator();
        OverflowNormalHitDamageReceivingStrategy strategy = new OverflowNormalHitDamageReceivingStrategy(randomGenerator);
	    Map<ComponentId, Component> components = new HashMap<>() {{
			put(ComponentId.SHIELDS, new Shields(100, 100, 100, 100, 100, DOWN));
			put(ComponentId.HULL, new TestComponent(100));
		}};
        ComponentSystem packet = new ComponentSystem(components);

        //when
        packet.receiveDamage(strategy, 80);

        //then
        assertThat(components.get(ComponentId.HULL).calculateHealthPercentage()).isEqualTo(20);
    }

    @Test
    public void should_damage_both_the_shields_and_components_when_shields_are_up() {
        //given
        RandomGenerator randomGenerator = new RandomGenerator();
        OverflowNormalHitDamageReceivingStrategy strategy = new OverflowNormalHitDamageReceivingStrategy(randomGenerator);
        Shields shields = new Shields(100, 100, 100, 100, 100, UP);
	    Map<ComponentId, Component> components = new HashMap<>() {{
			put(ComponentId.SHIELDS, shields);
			put(ComponentId.HULL, new TestComponent(100, 0));
		}};
	    ComponentSystem packet = new ComponentSystem(components);

        //when
        packet.receiveDamage(strategy, 150);

        //then
        assertThat(shields.getEnergyContainer().getEnergy()).isEqualTo(0);
        components.remove(ComponentId.SHIELDS);
        assertThat(components.values()).allMatch(component -> component.calculateHealthPercentage() == 50.0);
    }

    @Test
    public void should_damage_only_component_when_shields_are_down() {
        //given
        RandomGenerator randomGenerator = new RandomGenerator();
        OverflowNormalHitDamageReceivingStrategy strategy = new OverflowNormalHitDamageReceivingStrategy(randomGenerator);
		Shields shields = new Shields(100, 100, 100, 100, 100, UP);
        shields.setShieldsStatus(DOWN);
	    Map<ComponentId, Component> components = new HashMap<>() {{
			put(ComponentId.SHIELDS, shields);
			put(ComponentId.HULL, new TestComponent(100, 0));
		}};
        ComponentSystem componentSystem = new ComponentSystem(components);

        //when
        componentSystem.receiveDamage(strategy, 200);

        //then
        assertThat(shields.getEnergyContainer().getEnergy()).isEqualTo(100);
        components.remove(ComponentId.SHIELDS);
        assertThat(components.values()).allMatch(component -> component.calculateHealthPercentage() == 0);
    }

    @Test
    public void should_repair_only_components_when_shields_are_not_provided() {
        //given
        MostBrokenFirstRepairingStrategy strategy = new MostBrokenFirstRepairingStrategy();
        Map<ComponentId, Component> components = new HashMap<>() {{
			put(ComponentId.SHIELDS, new Shields(100, 0));
			put(ComponentId.HULL, new TestComponent(100, 0));
		}};
        ComponentSystem componentSystem = new ComponentSystem(components);

        //when
        componentSystem.repair(strategy, 200);

        //then
        assertThat(components.values()).allMatch(component -> component.calculateHealthPercentage() == 100);
    }

    @Test
    public void should_repair_both_shields_and_other_components() {
        //given
        MostBrokenFirstRepairingStrategy strategy = new MostBrokenFirstRepairingStrategy();
        Shields shields = new Shields(100, 100);
        // TODO: 8/14/18 temporary: need to change shields structure to match components idea
        shields.receiveDamageAndGetOverflow(100);
	    Map<ComponentId, Component> components = new HashMap<>() {{
			put(ComponentId.SHIELDS, shields);
			put(ComponentId.HULL, new TestComponent(100, 50, 1));
		}};
        ComponentSystem componentSystem = new ComponentSystem(components);

        //when
        componentSystem.repair(strategy, 300);

        //then
        assertThat(shields.calculateHealthPercentage()).isEqualTo(100);
        assertThat(components.values()).allMatch(component -> component.calculateHealthPercentage() == 100);
    }

    @Test
    public void should_return_correct_components_average_value() {
        // given
        double expectedValue = 90.0;

        ComponentSystemSpecification testSpecification = new ComponentSystemSpecificationBuilder()
                .specifyComponent(ComponentId.SHIELDS, 200, 10, Shields::new)
                .specifyComponent(ComponentId.HULL, 100, 15, Hull::new)
                .specifyComponent(ComponentId.ENERGY_CONTROLLER, 200, 20, EnergyController::new)
                .specifyComponent(ComponentId.LIFE_SUPPORT, 100, 30, LifeSupport::new)
                .build();

        Map<ComponentId, Component> components = new HashMap<>();
        testSpecification.getSpecifications().forEach((componentId, specification) -> {
            components.put(componentId, specification.createComponent());
        });

        ComponentSystem componentsMap = new ComponentSystem(components);

        // when
        componentsMap.getComponent(ComponentId.SHIELDS).receiveDamageAndGetOverflow(0.1 * 200);
        componentsMap.getComponent(ComponentId.HULL).receiveDamageAndGetOverflow(0.1 * 100);
        componentsMap.getComponent(ComponentId.ENERGY_CONTROLLER).receiveDamageAndGetOverflow(20);
        componentsMap.getComponent(ComponentId.LIFE_SUPPORT).receiveDamageAndGetOverflow(10);

        // then
        assertThat(componentsMap.getComponentSystemHealthStatus() == expectedValue);
    }

	final static class TestComponent extends Component {

		TestComponent(double maxHealth) {
			super(maxHealth, maxHealth);
		}

		TestComponent(double maxHealth, double repairRatio) {
			super(maxHealth, maxHealth, repairRatio);
		}

		TestComponent(double maxHealth, double currentHealth, double repairRatio) {
			super(maxHealth, currentHealth, repairRatio);
		}
	}
}

