package pl.oakfusion.sst.data.world.component.strategy.repair;

import org.junit.Test;
import pl.oakfusion.sst.data.util.RandomGenerator;
import pl.oakfusion.sst.data.world.component.Component;
import pl.oakfusion.sst.data.world.component.strategy.damage.OverflowNormalHitDamageReceivingStrategy;
import pl.oakfusion.sst.data.world.component.strategy.repair.MostBrokenFirstRepairingStrategy;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MostBrokenFirstRepairingStrategyTest {
    @Test
    public void should_repairing_most_broken_components() {

        //given
        RandomGenerator random = mock(RandomGenerator.class);
        OverflowNormalHitDamageReceivingStrategy TestNormalHit = new OverflowNormalHitDamageReceivingStrategy(random);

        Component testComponent1 = new Component(1500){};
        Component testComponent2 = new Component(1000){};
        MostBrokenFirstRepairingStrategy TestStrategy = new MostBrokenFirstRepairingStrategy();
        List<Component> components = Arrays.asList(testComponent1, testComponent2);

        when(random.shuffle(anyListOf(Component.class))).thenReturn(components);

        //when
        TestNormalHit.applyDamage(components,1800);
        TestStrategy.repair(components,110);

        //then
        assertThat(components.get(0).calculateHealthPercentage()).isEqualTo(100.0);
        assertThat(components.get(1).calculateHealthPercentage()).isEqualTo(85.0);
    }
    @Test
    public void should_repairing_most_broken_components_two_equal_HP() {
        //given


        Component testComponent1 = new Component(1000,500, 15){};
        Component testComponent2 = new Component(1000,500, 15){};
        MostBrokenFirstRepairingStrategy TestStrategy = new MostBrokenFirstRepairingStrategy();
        List<Component> components = Arrays.asList(testComponent1, testComponent2);
        //when

        TestStrategy.repair(components,0);


        //then

        assertThat(components.get(1).calculateHealthPercentage()).isEqualTo(50.0);
        assertThat(components.get(0).calculateHealthPercentage()).isEqualTo(50.0);
    }
    @Test
    public void should_repairing_most_broken_components_two_equal_HP_repair2() {
        //given


        Component testComponent1 = new Component(1000,500, 15){};
        Component testComponent2 = new Component(1000,500, 15){};
        MostBrokenFirstRepairingStrategy TestStrategy = new MostBrokenFirstRepairingStrategy();
        List<Component> components = Arrays.asList(testComponent1, testComponent2);
        //when

        TestStrategy.repair(components,150);

        //then

        assertThat(components.get(0).calculateHealthPercentage()).isEqualTo(100.0);
        assertThat(components.get(1).calculateHealthPercentage()).isEqualTo(100.0);
    }
    @Test
    public void should_repairing_most_broken_components_repair_two_components() {
        //given


        Component testComponent1 = new Component(1000,500, 15){};
        Component testComponent2 = new Component(1000,400, 15){};
        MostBrokenFirstRepairingStrategy TestStrategy = new MostBrokenFirstRepairingStrategy();
        List<Component> components = Arrays.asList(testComponent1, testComponent2);
        //when

        TestStrategy.repair(components,700);

        //then

        assertThat(components.get(1).calculateHealthPercentage()).isEqualTo(100.0);
        assertThat(components.get(0).calculateHealthPercentage()).isEqualTo(100.0);
    }
    @Test
    public void should_repairing_most_broken_components_repair_two_components_different_max_HP() {
        //given


        Component testComponent1 = new Component(500,150, 15){};
        Component testComponent2 = new Component(1000,200, 15){};
        MostBrokenFirstRepairingStrategy TestStrategy = new MostBrokenFirstRepairingStrategy();
        List<Component> components = Arrays.asList(testComponent1, testComponent2);
        //when

        TestStrategy.repair(components,700);

        //then

        assertThat(components.get(1).calculateHealthPercentage()).isEqualTo(100.0);
        assertThat(components.get(0).calculateHealthPercentage()).isEqualTo(100.0);
    } @Test
    public void should_repairing_most_broken_components_repair_two_components_different_max_HP_same_percent() {
        //given


        Component testComponent1 = new Component(500,250, 15){};
        Component testComponent2 = new Component(1000,500, 15){};
        MostBrokenFirstRepairingStrategy TestStrategy = new MostBrokenFirstRepairingStrategy();
        List<Component> components = Arrays.asList(testComponent1, testComponent2);
        //when

        TestStrategy.repair(components,700);

        //then

        assertThat(components.get(0).calculateHealthPercentage()).isEqualTo(100.0);
        assertThat(components.get(1).calculateHealthPercentage()).isEqualTo(100.0);
    }

}
