package pl.oakfusion.sst.data.world;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class StarDateTest {

    @Test
    public void should_create_stardate() {


        // when
        StarDate stardate = new StarDate(2500, 2);

        // then
        assertThat(stardate.toString()).isEqualTo("2500.00");
    }

    @Test
    public void should_not_create_stardate_for_incorrect_precision() {
        //then for negative precision
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> new StarDate(1, -1))
                .withMessage("negative precision: -1");

        //then for to big precision
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> new StarDate(1, 7))
                .withMessage("too big precision: 7");
    }

    @Test
    public void should_not_create_stardate_for_negative_to_increment() {
        // given
        StarDate starDate = new StarDate(1, 1);

        //then
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> starDate.increment(-1))
                .withMessage("negative toIncrement: -1.0");
    }

    @Test
    public void should_format_stardate_with_given_precision() {
        // then
        assertThat(new StarDate(2018, 1).toString()).isEqualTo("2018.0");
        assertThat(new StarDate(2018, 2).toString()).isEqualTo("2018.00");
        assertThat(new StarDate(2018, 3).toString()).isEqualTo("2018.000");
        assertThat(new StarDate(2018.123, 1).toString()).isEqualTo("2018.1");
        assertThat(new StarDate(2018.123, 2).toString()).isEqualTo("2018.12");
        assertThat(new StarDate(2018.123, 3).toString()).isEqualTo("2018.123");
    }

    @Test
    public void should_set_precision() {
        // given
        StarDate starDate = new StarDate(1, 1);

        // when
        starDate.setPrecision(3);

        // then
        assertThat(starDate.toString()).isEqualTo("1.000");
    }

    @Test
    public void should_not_change_precision_to_incorrect_value() {
        // given
        StarDate starDate = new StarDate(1, 1);

        // then
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> starDate.setPrecision(0));
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> starDate.setPrecision(5));
    }
}
