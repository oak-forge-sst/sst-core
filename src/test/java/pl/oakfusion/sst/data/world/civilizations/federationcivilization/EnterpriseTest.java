package pl.oakfusion.sst.data.world.civilizations.federationcivilization;

import org.junit.Test;
import pl.oakfusion.sst.data.initializer.ComponentSystemSpecification;
import pl.oakfusion.sst.data.util.RandomGenerator;
import pl.oakfusion.sst.data.world.civilizations.federationcivilization.Enterprise;
import pl.oakfusion.sst.data.world.civilizations.UnitSpecification;
import pl.oakfusion.sst.data.world.component.Component;
import pl.oakfusion.sst.data.world.component.ComponentSystem;
import pl.oakfusion.sst.data.world.component.Condition;
import pl.oakfusion.sst.data.world.component.strategy.damage.OverflowNormalHitDamageReceivingStrategy;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static pl.oakfusion.sst.data.world.component.ComponentSystem.DOCKED_REPAIR_RATIO;

public class EnterpriseTest {

	private final RandomGenerator random = mock(RandomGenerator.class);
	private final OverflowNormalHitDamageReceivingStrategy strategy = new OverflowNormalHitDamageReceivingStrategy(random);
	private final ComponentSystemSpecification enterpriseComponentSpecification = ComponentSystemSpecification.ENTERPRISE_COMPONENT_REGULAR_SPECIFICATION;
	private final ComponentSystem components = new ComponentSystem(enterpriseComponentSpecification.build());
	private final List<Component> componentsList = new ArrayList<>(components.getComponents().values());
	private final Enterprise enterprise = new Enterprise(0, 0, UUID.randomUUID(), components, new UnitSpecification(50.0, Condition.GREEN));

	@Test
	public void should_return_green_status() {
		//given
		when(random.shuffle(anyListOf(Component.class))).thenReturn(componentsList);
		strategy.applyDamage(componentsList, 200);
		//when
		enterprise.getCondition();
		//then
		assertThat(enterprise.getCondition()).isEqualByComparingTo(Condition.GREEN);
	}

	@Test
	public void should_return_yellow_status() {
		//given
		when(random.shuffle(anyListOf(Component.class))).thenReturn(componentsList);
		strategy.applyDamage(componentsList, components.getComponentSystemHealthSum() * 0.5);
		//when
		enterprise.getCondition();
		//then
		assertThat(enterprise.getCondition()).isEqualByComparingTo(Condition.YELLOW);
	}

	@Test
	public void should_return_red_status() {
		//given
		when(random.shuffle(anyListOf(Component.class))).thenReturn(componentsList);
		strategy.applyDamage(componentsList, 1600);
		//when
		enterprise.getCondition();
		//then
		assertThat(enterprise.getCondition()).isEqualByComparingTo(Condition.RED);
	}

	@Test
	public void should_change_state_to_dock_for_moveTo() {
		//given
		enterprise.dock();
		//when
		enterprise.moveTo(3, 5);
		//then
		assertThat(enterprise.getCondition()).isNotEqualByComparingTo(Condition.DOCKED);

	}

	@Test
	public void should_change_state_to_dock_for_moveDelta() {
		//given
		enterprise.dock();
		//when
		enterprise.moveDelta(3, 5);
		//then
		assertThat(enterprise.getCondition()).isNotEqualByComparingTo(Condition.DOCKED);
	}

	@Test
	public void should_have_full_energy_after_dock() {
		//given
		enterprise.getComponentSystem().getEnergyContainer().setEnergy(1000);
		//when
		enterprise.dock();
		//then
		assertThat(enterprise.getComponentSystem().getEnergyContainer().getEnergy()).isEqualTo(5000);
	}

	@Test
	public void should_change_repair_ratio_after_dock() {
		//when
		enterprise.dock();
		//then
		assertThat(enterprise.getComponentSystem().getShields().getRepairRatio()).isEqualTo(DOCKED_REPAIR_RATIO);
	}

	@Test
	public void should_fully_repair_shields_after_dock() {
		//given
		when(random.shuffle(anyListOf(Component.class))).thenReturn(componentsList);
		strategy.applyDamage(componentsList, 600);
		//when
		enterprise.dock();
		//then
		assertThat(enterprise.getComponentSystem().getShields().calculateHealthPercentage()).isEqualTo(100.0);
	}
}
