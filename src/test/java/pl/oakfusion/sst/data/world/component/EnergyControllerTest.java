package pl.oakfusion.sst.data.world.component;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class EnergyControllerTest {

	private static final int MAX_HEALTH = 200;
	private static final int REPAIR_RATIO = 17;

	@Test
    public void should_transfer_specified_amount_of_energy() {

        // given
        double energyToTransfer = 100;
        EnergyContainer testSource = new EnergyContainer(200, 150, 100, 10);
        EnergyContainer testDestination = new EnergyContainer(200, 100, 100, 10);
        EnergyController energyController = new EnergyController(MAX_HEALTH, REPAIR_RATIO);
        double expectedEnergyAmount = 200;
        double expectedSourceEnergyAmount = testSource.getEnergy() - energyToTransfer;

        // when
        energyController.transferEnergy(testSource, testDestination, energyToTransfer);

        // then
        assertThat(testDestination.getEnergy()).isEqualTo(expectedEnergyAmount);
        assertThat(testSource.getEnergy()).isEqualTo(expectedSourceEnergyAmount);
    }

    @Test
    public void should_transfer_energy_to_fill_destination_capacity() {

        // given
        double energyToTransfer = 100;
        EnergyContainer testSource = new EnergyContainer(200, 150, 100, 10);
        EnergyContainer testDestination = new EnergyContainer(200, 150, 100, 10);
        EnergyController energyController = new EnergyController(MAX_HEALTH, REPAIR_RATIO);
        double expectedEnergyAmount = 200;
        double expectedSourceEnergyAmount = 100;

        // when
        energyController.transferEnergy(testSource, testDestination, energyToTransfer);

        // then
        assertThat(testDestination.getEnergy()).isEqualTo(expectedEnergyAmount);
        assertThat(testSource.getEnergy()).isEqualTo(expectedSourceEnergyAmount);
    }

    @Test
    public void should_transfer_sources_energy_to_destination_instead_of_given() {

        // given
        double energyToTransfer = 100;
        EnergyContainer testSource = new EnergyContainer(200, 50, 100, 10);
        EnergyContainer testDestination = new EnergyContainer(200, 100, 100, 10);
        EnergyController energyController = new EnergyController(MAX_HEALTH, REPAIR_RATIO);
        double expectedEnergyAmount = 150;
        double expectedSourceEnergyAmount = 0;

        // when
        energyController.transferEnergy(testSource, testDestination, energyToTransfer);

        // then
        assertThat(testDestination.getEnergy()).isEqualTo(expectedEnergyAmount);
        assertThat(testSource.getEnergy()).isEqualTo(expectedSourceEnergyAmount);
    }
}
