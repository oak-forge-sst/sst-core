package pl.oakfusion.sst.data.world.component.strategy.damage;

import org.junit.Test;
import pl.oakfusion.sst.data.util.RandomGenerator;
import pl.oakfusion.sst.data.world.component.Component;
import pl.oakfusion.sst.data.world.component.Shields;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class OverflowNormalHitDamageReceivingStrategyTest {


    @Test
    public void should_apply_damage_one_component() {
        //given
        RandomGenerator random = mock(RandomGenerator.class);

        Component testComponent1 = new Component(1000){};
        Component testComponent2 = new Component(1000){};

        OverflowNormalHitDamageReceivingStrategy strategy = new OverflowNormalHitDamageReceivingStrategy(random);

        List<Component> components = Arrays.asList(testComponent1, testComponent2);

        when(random.shuffle(anyListOf(Component.class))).thenReturn(components);
        //when
        strategy.applyDamage(components, 600);

        //then
        assertThat(components.get(0).calculateHealthPercentage()).isEqualTo(40.0);
        assertThat(components.get(1).calculateHealthPercentage()).isEqualTo(100.0);

    }

    @Test
    public void should_apply_damage_two_component() {
        //given
        RandomGenerator random = mock(RandomGenerator.class);

        Component testComponent1 = new Component(1000){};
        Component testComponent2 = new Component(1000){};

        OverflowNormalHitDamageReceivingStrategy strategy = new OverflowNormalHitDamageReceivingStrategy(random);

        List<Component> components = Arrays.asList(testComponent1, testComponent2);

        when(random.shuffle(anyListOf(Component.class))).thenReturn(components);
        //when
        strategy.applyDamage(components, 1200);

        //then
        assertThat(components.get(0).calculateHealthPercentage()).isEqualTo(0.0);
        assertThat(components.get(1).calculateHealthPercentage()).isEqualTo(80.0);
    }

    @Test
    public void should_apply_damage_more_component() {
        //given
        RandomGenerator random = mock(RandomGenerator.class);
        Component testComponent1 = new Component(1000){};
        Component testComponent2 = new Component(1000){};
        Component testComponent3 = new Component(1000){};
        OverflowNormalHitDamageReceivingStrategy strategy = new OverflowNormalHitDamageReceivingStrategy(random);
        List<Component> components = Arrays.asList(testComponent1, testComponent2,testComponent3);
        when(random.shuffle(anyListOf(Component.class))).thenReturn(components);

        //when
        strategy.applyDamage(components, 3000);

        //then
        assertThat(components.get(0).calculateHealthPercentage()).isEqualTo(0.0);
        assertThat(components.get(1).calculateHealthPercentage()).isEqualTo(0.0);
        assertThat(components.get(2).calculateHealthPercentage()).isEqualTo(0.0);
    }

    @Test
    public void should_apply_damage_shields_and_one_component() {
        //given
        RandomGenerator random = mock(RandomGenerator.class);

        Component testComponent1 = new Component(1000){};
        Component testComponent2 = new Component(1000){};

        OverflowNormalHitDamageReceivingStrategy strategy = new OverflowNormalHitDamageReceivingStrategy(random);

        List<Component> components = Arrays.asList(testComponent1, testComponent2);

        when(random.shuffle(anyListOf(Component.class))).thenReturn(components);
        Shields shields = new Shields(500,15,500);

        //when
        strategy.applyDamage(shields, components, 1000);

        //then
        assertThat(components.get(0).calculateHealthPercentage()).isEqualTo(50.0);
        assertThat(components.get(1).calculateHealthPercentage()).isEqualTo(100.0);
    }

    @Test
    public void should_apply_damage_shields_and_two_component() {
        //given
        RandomGenerator random = mock(RandomGenerator.class);

        Component testComponent1 = new Component(1000){};
        Component testComponent2 = new Component(1000){};

        OverflowNormalHitDamageReceivingStrategy strategy = new OverflowNormalHitDamageReceivingStrategy(random);

        List<Component> components = Arrays.asList(testComponent1, testComponent2);

        when(random.shuffle(anyListOf(Component.class))).thenReturn(components);
        Shields shields = new Shields(500,15,500);

        //when
        strategy.applyDamage(shields, components, 1700);

        //then
        assertThat(components.get(0).calculateHealthPercentage()).isEqualTo(0.0);
        assertThat(components.get(1).calculateHealthPercentage()).isEqualTo(80.0);
    }
}
