package pl.oakfusion.sst.data.world.component.strategy.damage;

import org.junit.Test;
import pl.oakfusion.sst.data.util.RandomGenerator;
import pl.oakfusion.sst.data.world.component.Component;
import pl.oakfusion.sst.data.world.component.Shields;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CriticalHitDamageReceivingStrategyTest {

    @Test
    public void should_apply_damage_like_normal_hit_when_no_shields_are_passed() {
        //given
        RandomGenerator randomGenerator = mock(RandomGenerator.class);

        CriticalHitDamageReceivingStrategy strategy = new CriticalHitDamageReceivingStrategy(randomGenerator);

        List<Component> components = Arrays.asList(
                new Component(100){},
                new Component(100){},
                new Component(100){}
        );

        when(randomGenerator.shuffle(anyListOf(Component.class))).thenReturn(components);

        //when
        strategy.applyDamage(components, 150);

        //then
        assertThat(components.get(0).calculateHealthPercentage()).isEqualTo(0);
        assertThat(components.get(1).calculateHealthPercentage()).isEqualTo(50.0);
        assertThat(components.get(2).calculateHealthPercentage()).isEqualTo(100.0);
    }

    @Test
    public void should_partially_apply_damage_to_components_and_rest_to_shields() {
        //given
        RandomGenerator randomGenerator = mock(RandomGenerator.class);
        CriticalHitDamageReceivingStrategy strategy = new CriticalHitDamageReceivingStrategy(randomGenerator);

        Shields shields = new Shields(200,10,200);

        List<Component> components = Arrays.asList(
                new Component(100){},
                new Component(100){},
                new Component(100){}
        );

        when(randomGenerator.nextDoubleInRange(anyDouble(),anyDouble())).thenReturn(0.5);
        when(randomGenerator.shuffle(anyListOf(Component.class))).thenReturn(components);

        //when
        strategy.applyDamage(shields, components, 300);

        //then
        assertThat(shields.getEnergyContainer().getEnergy()).isEqualTo(50);
        assertThat(components.get(0).calculateHealthPercentage()).isEqualTo(0);
        assertThat(components.get(1).calculateHealthPercentage()).isEqualTo(50.0);
        assertThat(components.get(2).calculateHealthPercentage()).isEqualTo(100.0);
    }

    @Test
    public void should_bypass_shields_and_damage_only_components() {
        //given
        RandomGenerator randomGenerator = mock(RandomGenerator.class);
        CriticalHitDamageReceivingStrategy strategy = new CriticalHitDamageReceivingStrategy(randomGenerator);

        Shields shields = new Shields(200,10,200);

        List<Component> components = Arrays.asList(
                new Component(100){},
                new Component(100){},
                new Component(100){}
        );

        when(randomGenerator.nextDoubleInRange(anyDouble(),anyDouble())).thenReturn(0.0);
        when(randomGenerator.shuffle(anyListOf(Component.class))).thenReturn(components);

        //when
        strategy.applyDamage(shields, components, 300);

        //then
        assertThat(shields.getEnergyContainer().getEnergy()).isEqualTo(200);
        assertThat(components.get(0).calculateHealthPercentage()).isEqualTo(0);
        assertThat(components.get(1).calculateHealthPercentage()).isEqualTo(0);
        assertThat(components.get(2).calculateHealthPercentage()).isEqualTo(0);
    }

    @Test
    public void should_apply_damage_only_to_shields() {
        //given
        RandomGenerator randomGenerator = mock(RandomGenerator.class);
        CriticalHitDamageReceivingStrategy strategy = new CriticalHitDamageReceivingStrategy(randomGenerator);

        Shields shields = new Shields(200,10,200);

        List<Component> components = Arrays.asList(
                new Component(100){},
                new Component(100){},
                new Component(100){}
        );


        when(randomGenerator.nextDoubleInRange(anyDouble(),anyDouble())).thenReturn(1.0);

        //when
        strategy.applyDamage(shields, components, 200);

        //then
        assertThat(shields.getEnergyContainer().getEnergy()).isEqualTo(0);
        assertThat(components.get(0).calculateHealthPercentage()).isEqualTo(100.0);
        assertThat(components.get(1).calculateHealthPercentage()).isEqualTo(100.0);
        assertThat(components.get(2).calculateHealthPercentage()).isEqualTo(100.0);
    }

    @Test
    public void should_apply_all_damage_to_shields_and_remaining_damage_to_components() {
        //given
        RandomGenerator randomGenerator = mock(RandomGenerator.class);
        CriticalHitDamageReceivingStrategy strategy = new CriticalHitDamageReceivingStrategy(randomGenerator);

        Shields shields = new Shields(200,10,200);

        List<Component> components = Arrays.asList(
                new Component(100){},
                new Component(100){},
                new Component(100){}
        );

        when(randomGenerator.nextDoubleInRange(anyDouble(),anyDouble())).thenReturn(1.0);
        when(randomGenerator.shuffle(anyListOf(Component.class))).thenReturn(components);

        //when
        strategy.applyDamage(shields, components, 450);

        //then
        assertThat(shields.getEnergyContainer().getEnergy()).isEqualTo(0);
        assertThat(components.get(0).calculateHealthPercentage()).isEqualTo(0);
        assertThat(components.get(1).calculateHealthPercentage()).isEqualTo(0);
        assertThat(components.get(2).calculateHealthPercentage()).isEqualTo(50.0);
    }
    @Test
    public void should_apply_all_damage_to_shields_and_remaining_damage_to_components_all_damage_absorbed() {
        //given
        RandomGenerator randomGenerator = mock(RandomGenerator.class);
        CriticalHitDamageReceivingStrategy strategy = new CriticalHitDamageReceivingStrategy(randomGenerator);

        Shields shields = new Shields(200,10,200);

        List<Component> components = Arrays.asList(
                new Component(100){},
                new Component(100){},
                new Component(100){}
        );

        when(randomGenerator.nextDoubleInRange(anyDouble(),anyDouble())).thenReturn(0.5);
        when(randomGenerator.shuffle(anyListOf(Component.class))).thenReturn(components);

        //when
        strategy.applyDamage(shields, components, 450);

        //then
        assertThat(shields.getEnergyContainer().getEnergy()).isEqualTo(0);
        assertThat(components.get(0).calculateHealthPercentage()).isEqualTo(0);
        assertThat(components.get(1).calculateHealthPercentage()).isEqualTo(0);
        assertThat(components.get(2).calculateHealthPercentage()).isEqualTo(50.0);
    } @Test
    public void should_apply_all_damage_to_shields_and_remaining_damage_to_components_all_components_destroyed() {
        //given
        RandomGenerator randomGenerator = mock(RandomGenerator.class);
        CriticalHitDamageReceivingStrategy strategy = new CriticalHitDamageReceivingStrategy(randomGenerator);

        Shields shields = new Shields(200,10,200);

        List<Component> components = Arrays.asList(
                new Component(100){},
                new Component(100){},
                new Component(100){}
        );

        when(randomGenerator.nextDoubleInRange(anyDouble(),anyDouble())).thenReturn(0.5);
        when(randomGenerator.shuffle(anyListOf(Component.class))).thenReturn(components);

        //when
        strategy.applyDamage(shields, components, 1000);

        //then
        assertThat(shields.getEnergyContainer().getEnergy()).isEqualTo(0);
        assertThat(components.get(0).calculateHealthPercentage()).isEqualTo(0);
        assertThat(components.get(1).calculateHealthPercentage()).isEqualTo(0);
        assertThat(components.get(2).calculateHealthPercentage()).isEqualTo(0.0);
    }

    @Test
    public void should_apply_all_damage_to_shields_and_remaining_damage_to_components_little_damage() {
        //given
        RandomGenerator randomGenerator = mock(RandomGenerator.class);
        CriticalHitDamageReceivingStrategy strategy = new CriticalHitDamageReceivingStrategy(randomGenerator);

        Shields shields = new Shields(200,10,200);

        List<Component> components = Arrays.asList(
                new Component(100){},
                new Component(100){},
                new Component(100){}
        );

        when(randomGenerator.nextDoubleInRange(anyDouble(),anyDouble())).thenReturn(0.5);
        when(randomGenerator.shuffle(anyListOf(Component.class))).thenReturn(components);

        //when
        strategy.applyDamage(shields, components, 1);

        //then
        assertThat(shields.getEnergyContainer().getEnergy()).isEqualTo(199.5);
        assertThat(components.get(0).calculateHealthPercentage()).isEqualTo(99.5);
        assertThat(components.get(1).calculateHealthPercentage()).isEqualTo(100.0);
        assertThat(components.get(2).calculateHealthPercentage()).isEqualTo(100.0);
    }
}
