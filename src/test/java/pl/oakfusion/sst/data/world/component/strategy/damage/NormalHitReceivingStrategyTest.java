package pl.oakfusion.sst.data.world.component.strategy.damage;

import org.junit.Test;
import org.mockito.Mockito;
import pl.oakfusion.sst.data.util.RandomGenerator;
import pl.oakfusion.sst.data.world.component.ShieldsStatus;
import pl.oakfusion.sst.data.world.component.Component;
import pl.oakfusion.sst.data.world.component.Shields;

import java.util.Collection;


import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.Mockito.mock;
import static pl.oakfusion.sst.data.initializer.ComponentSystemSpecification.ENTERPRISE_COMPONENT_REGULAR_SPECIFICATION;

public class NormalHitReceivingStrategyTest {

	@Test
	public void should_receive_proper_amount_of_damage() {
		double damage = 303;
		RandomGenerator randomGenerator = mock(RandomGenerator.class);
		Mockito.when(randomGenerator.nextDoubleInRange(0, 1)).thenReturn(1.0);
		NormalHitReceivingStrategy strategy = new NormalHitReceivingStrategy(randomGenerator);
		Collection<Component> componentCollection = ENTERPRISE_COMPONENT_REGULAR_SPECIFICATION.build().values();
		double healthBefore =  componentCollection.stream().map(Component::getCurrentHealth).reduce(0.0, Double::sum);
		strategy.applyDamage(componentCollection, damage);
		assertThat(componentCollection.stream().map(Component::getCurrentHealth).reduce(0.0, Double::sum)).isEqualTo(healthBefore-damage);
	}

	@Test
	public void should_receive_proper_amount_of_damage_with_shields() {
		double damage = 400;
		RandomGenerator randomGenerator = mock(RandomGenerator.class);
		Mockito.when(randomGenerator.nextDoubleInRange(0, 1)).thenReturn(1.0);
		NormalHitReceivingStrategy strategy = new NormalHitReceivingStrategy(randomGenerator);
		Collection<Component> componentCollection = ENTERPRISE_COMPONENT_REGULAR_SPECIFICATION.build().values();
		double healthBefore =  componentCollection.stream().map(Component::getCurrentHealth).reduce(0.0, Double::sum);
		Shields shields = new Shields(100, 25, 20, 100, 100, ShieldsStatus.UP);
		strategy.applyDamage(shields, componentCollection, damage);
		assertThat(componentCollection.stream().map(Component::getCurrentHealth).reduce(0.0, Double::sum)).isEqualTo(healthBefore-damage*0.75);
	}
}
