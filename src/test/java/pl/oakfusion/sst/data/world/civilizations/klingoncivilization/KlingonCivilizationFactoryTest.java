package pl.oakfusion.sst.data.world.civilizations.klingoncivilization;

import org.junit.Test;
import pl.oakfusion.sst.data.world.civilizations.klingoncivilization.factories.KlingonCivilizationFactory;
import pl.oakfusion.sst.data.world.gameobject.GameObject;
import pl.oakfusion.sst.data.world.gameobject.GameObjectFactory;
import pl.oakfusion.sst.data.parameter.GameDifficulty;
import pl.oakfusion.sst.data.util.RandomGenerator;
import pl.oakfusion.sst.data.world.StarDate;
import pl.oakfusion.sst.data.world.World;
import pl.oakfusion.sst.data.world.civilizations.UnitFactory;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class KlingonCivilizationFactoryTest {

	World world;
	RandomGenerator random = new RandomGenerator();

	@Test
	public void should_spawn_klingon_civilization() {
		//when
		int klingonQuantity = 10;
		int commanderQuantity = 4;
		int superCommanderQuantity = 1;
		world = new World(5, 5, 10, new StarDate(203.7, 1));
		KlingonSpecification klingonSpecification = new KlingonSpecification(klingonQuantity, commanderQuantity, superCommanderQuantity);

		KlingonCivilizationFactory klingonCivilizationFactory = new KlingonCivilizationFactory(klingonSpecification, new UnitFactory(GameDifficulty.NOVICE), UUID.randomUUID(), this::spawnObject);

		KlingonCivilization klingonCivilization = (KlingonCivilization) klingonCivilizationFactory.createCivilization();

		//then
		assertThat(klingonCivilization.klingonList.size()).isEqualTo(klingonQuantity);
		assertThat(klingonCivilization.commandersList.size()).isEqualTo(commanderQuantity);
		assertThat(klingonCivilization.superCommanderList.size()).isEqualTo(superCommanderQuantity);

		klingonCivilization.klingonList.forEach(k -> assertThat(world.getGameObject(k.getX(), k.getY()).isPresent()).isTrue());
		klingonCivilization.commandersList.forEach(k -> assertThat(world.getGameObject(k.getX(), k.getY()).isPresent()).isTrue());
		klingonCivilization.superCommanderList.forEach(k -> assertThat(world.getGameObject(k.getX(), k.getY()).isPresent()).isTrue());
	}

	private <G extends GameObject> G spawnObject(GameObjectFactory<G> factory, int forceFieldRange) {
		G spawnedObject = null;
		while (spawnedObject == null){
			int x = random.nextInt(world.getAbsoluteWidth());
			int y = random.nextInt(world.getAbsoluteHeight());

			if (world.getGameObject(x, y).isEmpty()) {
				spawnedObject = factory.create(x, y);
				world.add(spawnedObject);
			}
		}
		return spawnedObject;
	}

}
