package pl.oakfusion.sst.data.world.component;

import junitparams.JUnitParamsRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(JUnitParamsRunner.class)
public class EnergyContainerTest {

    private Object[] testCase(double sourceCapacity,
                              double sourceEnergy,
                              double targetCapacity,
                              double targetEnergy,
                              ComponentCondition sourceCondition,
                              double energyToTransfer,
                              double actualTransferredEnergy,
                              double sourceEnergyAfter,
                              double targetEnergyAfter) {
        return new Object[]{
                sourceCapacity, sourceEnergy, targetCapacity, targetEnergy, sourceCondition, energyToTransfer, actualTransferredEnergy, sourceEnergyAfter, targetEnergyAfter
        };
    }

    @Test
    public void should_create_container_with_energy_equal_to_capacity() {
        //given
        int capacity = 1000;
        //when
        EnergyContainer energyContainer = new EnergyContainer(capacity, 100, 10);
        //then
        assertThat(energyContainer.getEnergy()).isEqualTo(energyContainer.getCapacity())
                                               .isEqualTo(capacity);
    }

}
