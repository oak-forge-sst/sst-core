package pl.oakfusion.sst.data.world;


import org.junit.Test;
import pl.oakfusion.sst.data.world.gameobject.GameObject;
import pl.oakfusion.testutils.TestGameObject;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class WorldTest {

    @Test
    public void should_create_world() {
        // when
        StarDate starDate = new StarDate(2018.231, 3);
        World world = new World(1, 2, 10, starDate);

        // then
        assertThat(world.getCurrentStarDate()).isEqualTo("2018.231");
        assertThat(world.getWidthInQuadrants()).isEqualTo(1);
        assertThat(world.getHeightInQuadrants()).isEqualTo(2);
        assertThat(world.getQuadrantSize()).isEqualTo(10);
        assertThat(world.getAbsoluteWidth()).isEqualTo(10);
        assertThat(world.getAbsoluteHeight()).isEqualTo(20);
    }

    @Test
    public void should_add_game_object() {
        // given
        StarDate starDate = new StarDate(2018.231, 3);

        World world = new World(2, 1, 10, starDate);
        GameObject go = new TestGameObject(0, 0);

        // when
        world.add(go);

        // then
        assertThat(world.getGameObject(0, 0)).contains(go);
    }

    @Test
    public void should_remove_game_object() {
        // given
        StarDate starDate = new StarDate(2018.231, 3);
        World world = new World(2, 1, 10, starDate);
        GameObject go = new TestGameObject(0, 0);
        world.add(go);

        // when
        world.remove(go);

        // then
        assertThat(world.getGameObject(0, 0)).isEmpty();
    }

    @Test
    public void should_not_create_world_with_incorrect_size() {
    	//given
        StarDate starDate = new StarDate(2018.231, 3);

        // then
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> new World(0, 1, 1, starDate))
                .withMessage("World width must be positive.");
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> new World(1, 0, 1, starDate))
                .withMessage("World height must be positive.");
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> new World(1, 1, 0, starDate))
                .withMessage("Quadrant size must be positive.");
    }

    @Test
    public void should_return_game_objects_in_range() {
        //given
        int sizeX = 10;
        int sizeY = 10;
        int quadrantSize = 10;

        int enterpriseX = 15;
        int enterpriseY = 15;
        int range = 10;

        World world = new World(sizeX, sizeY, quadrantSize, new StarDate(2018.231, 3));

        TestGameObject test1 = new TestGameObject(20, 16);
        TestGameObject test2 = new TestGameObject(13, 17);
        TestGameObject test3 = new TestGameObject(53, 21);

        world.add(test1);
        world.add(test2);
        world.add(test3);
        //when
        List<GameObject> gameObjectsInRange = world.getGameObjectsInRange(enterpriseX, enterpriseY, range);
        //then
        assertThat(gameObjectsInRange).contains(test1, test2).doesNotContain(test3).hasSize(2);
    }

    @Test
	public void should_see_an_object_of_given_type_in_range() {
    	//given
		int sizeX = 10;
		int sizeY = 10;
		int quadrantSize = 10;

		int enterpriseX = 15;
		int enterpriseY = 15;
		int range = 1;

		World world = new World(sizeX, sizeY, quadrantSize, new StarDate(2018.231, 3));
		TestGameObject test1 = new TestGameObject(16, 16);
		AnotherTestGameObject test2 = new AnotherTestGameObject(15, 16);
		//when
		world.add(test1);
		world.add(test2);
		//then
		assertThat(world.isObjectOfClassInRange(TestGameObject.class, enterpriseX, enterpriseY, range)).isTrue();

	}

	@Test
	public void should_not_see_an_object_of_given_type_in_range() {
    	//given
		int sizeX = 10;
		int sizeY = 10;
		int quadrantSize = 10;

		int enterpriseX = 15;
		int enterpriseY = 15;
		int range = 1;

		World world = new World(sizeX, sizeY, quadrantSize, new StarDate(2018.231, 3));
		TestGameObject test1 = new TestGameObject(11, 12);
		AnotherTestGameObject test2 = new AnotherTestGameObject(15, 16);
		//when
		world.add(test1);
		world.add(test2);
		//then
		assertThat(world.isObjectOfClassInRange(TestGameObject.class, enterpriseX, enterpriseY, range)).isFalse();

	}

	private class AnotherTestGameObject extends GameObject{
		public AnotherTestGameObject(int x, int y) {
			super(x, y);
		}
	}
}
