package pl.oakfusion.testutils;

import com.google.common.eventbus.EventBus;
import lombok.Getter;
import pl.oakfusion.bus.guava.GuavaMessageBus;
import pl.oakfusion.sst.core.gamedata.GameSessionData;
import pl.oakfusion.sst.core.gamedata.PlayersRepository;
import pl.oakfusion.sst.core.aggregates.HelpResolver;
import pl.oakfusion.sst.core.gamedata.GameSessionDataRepository;
import pl.oakfusion.sst.core.gamedata.InMemoryGameSessionDataRepository;
import pl.oakfusion.sst.core.generator.WorldGenerator;
import pl.oakfusion.sst.data.world.civilizations.federationcivilization.Federation;
import pl.oakfusion.sst.data.parameter.GameType;
import pl.oakfusion.sst.data.initializer.WorldSpecification;
import pl.oakfusion.sst.data.parameter.GameDifficulty;
import pl.oakfusion.sst.data.parameter.GameLength;
import pl.oakfusion.sst.data.util.RandomGenerator;
import pl.oakfusion.sst.data.world.*;
import pl.oakfusion.sst.data.world.civilizations.AbstractUnitFactory;
import pl.oakfusion.sst.data.world.civilizations.UnitFactory;
import pl.oakfusion.sst.data.world.civilizations.klingoncivilization.*;
import pl.oakfusion.sst.data.world.player.AIPlayer;
import pl.oakfusion.sst.data.world.player.CivilizationType;
import pl.oakfusion.sst.data.world.player.HumanPlayer;
import pl.oakfusion.sst.data.world.civilizations.klingoncivilization.factories.UnitsKlingonsNamesFactory;

import java.util.*;

@SuppressWarnings("UnstableApiUsage")
@Getter
public class SstTestUtils {

	protected GuavaMessageBus guavaMessageBus = new GuavaMessageBus(new EventBus());

	protected RandomGenerator random = new RandomGenerator();
	protected HelpResolver helpResolver = new HelpResolver();
	KlingonUnitsSpecification klingonUnitsSpecification = new KlingonUnitsSpecification();
	UnitsKlingonsNamesFactory unitsKlingonsNamesFactory = new UnitsKlingonsNamesFactory();

	private void setKlingonUnitsSpecification() {
		klingonUnitsSpecification.setKlingonSpecification();
	}

	protected WorldSpecification specification = WorldSpecification.builder()
			.widthInQuadrants(10)
			.heightInQuadrants(10)
			.quadrantSize(10)
			.starQuantity(100)
			.starbaseQuantity(10)
			.planetQuantity(100)
			.blackHoleQuantity(10)
			.gameLength(GameLength.SHORT)
			.gameDifficulty(GameDifficulty.NOVICE)
			.build();

	protected WorldSpecification emptySpecification = WorldSpecification.builder()
			.widthInQuadrants(10)
			.heightInQuadrants(10)
			.quadrantSize(10)
			.starQuantity(0)
			.starbaseQuantity(0)
			.planetQuantity(0)
			.blackHoleQuantity(0)
			.build();

	@Getter
	protected WorldSpecification smallWorldSpecification = WorldSpecification.builder()
			.widthInQuadrants(1)
			.heightInQuadrants(1)
			.quadrantSize(35)
			.starQuantity(10)
			.starbaseQuantity(1)
			.planetQuantity(10)
			.blackHoleQuantity(2)
			.gameDifficulty(GameDifficulty.NOVICE)
			.gameLength(GameLength.SHORT)
			.build();

	CivilizationType opponentsCivilization = CivilizationType.KLINGON_EMPIRE;
	CivilizationType userCivilization = CivilizationType.FEDERATION;

	PlayersRepository playersRepository = new PlayersRepository();
	HumanPlayer humanPlayer = new HumanPlayer(CivilizationType.FEDERATION, "qwe", new UUID(11111,22222), true);
	AIPlayer aIPlayer = new AIPlayer(CivilizationType.KLINGON_EMPIRE, "ewq", new UUID(33333,44444));

	AbstractUnitFactory unitFactory = new UnitFactory(specification.getGameDifficulty());
	Klingon klingon = unitFactory.createKlingon(aIPlayer.getUuid()).create(1,10);
	Commander commander = unitFactory.createCommander(aIPlayer.getUuid()).create(1,4);
	SuperCommander superCommander = unitFactory.createSuperCommander(aIPlayer.getUuid()).create(1,3);

	KlingonStrategy klingonStrategy;
	List<Klingon> klingonList = List.of(klingon);
	List<Commander> commanderslist = List.of(commander);
	List<SuperCommander> superCommanderList = List.of(superCommander);

	private final KlingonCivilization klingonCivilization = new KlingonCivilization(klingonStrategy, klingonList,
			commanderslist, superCommanderList);


	Federation federation = new Federation(unitFactory.createEnterprise(humanPlayer.getUuid()).create(10,10));

	GameSessionData gameSessionData =
			new GameSessionData(new World(2, 2, 10, new StarDate(2, 2)),
					getPlayersRepository(), GameType.SINGLEPLAYER);

	UUID gameSessionUUID = UUID.randomUUID();

	GameSessionDataRepository gameSessionDataRepository = new InMemoryGameSessionDataRepository();

	public GameSessionData getGameData() {
		gameSessionData.setActivePlayer(playersRepository.getPlayerList().get(0));
		//TODO: Uncomment after fixing setUpPlayerForNewTurnMethod
		//gameSessionData.getActivePlayer().setUpPlayerForNewTurn();
		return gameSessionData;
	}

	public Federation getFederation() {
		return federation;
	}

	public CivilizationType getOpponentsCivilization() {
		return opponentsCivilization;
	}

	public CivilizationType getUserCivilization() {
		return userCivilization;
	}

	public PlayersRepository getPlayersRepository() {
		playersRepository.clearList();
		playersRepository.addPlayersList(getHumanPlayer(), getAIPlayer());
		playersRepository.getPlayerList().get(0).setCivilization(federation);
		playersRepository.getPlayerList().get(1).setCivilization(klingonCivilization);
		playersRepository.getPlayerList().get(0).getCivilization().setActiveUnits(playersRepository.getPlayerList().get(0).getCivilization().getUnits());
		playersRepository.getPlayerList().get(1).getCivilization().setActiveUnits(playersRepository.getPlayerList().get(1).getCivilization().getUnits());
		return playersRepository;
	}

	public HumanPlayer getHumanPlayer() {
		return humanPlayer;
	}

	public AIPlayer getAIPlayer() {
		return aIPlayer;
	}

	public KlingonCivilization getKlingonCivilization() {
		return klingonCivilization;
	}

	public GuavaMessageBus getMessageBus() {
		return guavaMessageBus;
	}

	public WorldSpecification getSpecification() {
		return specification;
	}

	public GameSessionDataRepository getGameSessionDataRepo(){
		gameSessionDataRepository.save(gameSessionUUID, getGameData());
		return gameSessionDataRepository;
	}

	public GameSessionDataRepository getGameSessionDataRepoWithSmallWorld(){
		PlayersRepository playersRepository = getPlayersRepository();

		World world  = new WorldGenerator(new RandomGenerator())
				.generateWorld(smallWorldSpecification,
						playersRepository,
						GameType.SINGLEPLAYER);

		GameSessionData gameSessionData = new GameSessionData(world,
				playersRepository, GameType.SINGLEPLAYER);
		gameSessionData.setActivePlayer(playersRepository.getPlayerList().get(0));

		GameSessionDataRepository repo = new InMemoryGameSessionDataRepository();
		repo.save(gameSessionUUID, gameSessionData);

		return repo;
	}
}
