package pl.oakfusion.sst.data.game

import pl.oakfusion.sst.core.gamedata.GameSessionData
import pl.oakfusion.sst.data.game.MoveEstimation
import pl.oakfusion.sst.data.game.UnitMoveDestination
import pl.oakfusion.sst.data.game.UnitsMoveDestinations
import pl.oakfusion.sst.data.util.Position
import pl.oakfusion.sst.data.world.World
import pl.oakfusion.sst.data.world.civilizations.Unit
import pl.oakfusion.sst.data.world.component.Computer
import pl.oakfusion.testutils.SstTestUtils
import spock.lang.Specification

class UnitsMoveDestinationsTest extends Specification {
	UnitsMoveDestinations unitsMoveDestinations = new UnitsMoveDestinations()
	SstTestUtils testUtils = new SstTestUtils()
	GameSessionData gameData = testUtils.getGameData()
	Unit unit1 = gameData.getPlayersRepository().getPlayerList().get(0).getCivilization().getUnits().get(0)
	Unit unit2 = gameData.getPlayersRepository().getPlayerList().get(1).getCivilization().getUnits().get(0)
	Computer unit1Computer = unit1.getComponentSystem().getComputer()
	Computer unit2Computer = unit2.getComponentSystem().getComputer()
	World world = gameData.getWorld()

	def "should not add destination when same destination is already saved"(){
		given:
		Position destination = new Position(5, 5)
		List<Position> path1 = world.findShortestPathWarp(unit1.getPosition(), destination)
		MoveEstimation estimation1 = unit1Computer.estimateMove(world, unit1, path1, unit1.getPosition(), destination)
		unitsMoveDestinations.addDestination(new UnitMoveDestination(unit1, destination, estimation1, true))
		List<Position> path2 = world.findShortestPathWarp(unit2.getPosition(), destination)
		MoveEstimation estimation2 = unit2Computer.estimateMove(world, unit2, path2, unit2.getPosition(), destination)

		when:
		unitsMoveDestinations.addDestination(new UnitMoveDestination(unit2, destination, estimation2, true))

		then:
		unitsMoveDestinations.getDestinationByUnit(unit2).isEmpty()
	}

	def "should not add destination when destination for same unit is already save"(){
		given:
		Position destination1 = new Position(5, 5)
		Position destination2 = new Position(3, 3)
		List<Position> path1 = world.findShortestPathWarp(unit1.getPosition(), destination1)
		MoveEstimation estimation1 = unit1Computer.estimateMove(world, unit1, path1, unit1.getPosition(), destination1)
		unitsMoveDestinations.addDestination(new UnitMoveDestination(unit1, destination1, estimation1, true))
		List<Position> path2 = world.findShortestPathWarp(unit1.getPosition(), destination2)
		MoveEstimation estimation2 = unit1Computer.estimateMove(world, unit1, path2, unit1.getPosition(), destination2)

		when:
		unitsMoveDestinations.addDestination(new UnitMoveDestination(unit1, destination2, estimation2, true))

		then:
		unitsMoveDestinations.getDestinationsList().size() == 1
		unitsMoveDestinations.getDestinationByUnit(unit1).orElseThrow().destination.getX() == destination1.getX()
		unitsMoveDestinations.getDestinationByUnit(unit1).orElseThrow().destination.getY() == destination1.getY()
	}

	def "should return empty optional when no destination was added for unit"(){
		when:
		Optional<UnitMoveDestination> destination = unitsMoveDestinations.getDestinationByUnit(unit1)

		then:
		destination.isEmpty()
	}

	def "should return not empty optional when destination was added for unit"(){
		given:
		Position destination = new Position(5, 5)
		List<Position> path1 = world.findShortestPathWarp(unit1.getPosition(), destination)
		MoveEstimation estimation1 = unit1Computer.estimateMove(world, unit1, path1, unit1.getPosition(), destination)
		unitsMoveDestinations.addDestination(new UnitMoveDestination(unit1, destination, estimation1, true))

		when:
		Optional<UnitMoveDestination> destinationFound = unitsMoveDestinations.getDestinationByUnit(unit1)

		then:
		!destinationFound.isEmpty()
	}

	def "should return false when list of destinations is empty"(){
		when:
		boolean ifAllUnitsCanReachDestinations = unitsMoveDestinations.canAllUnitsReachDestinations()

		then:
		!ifAllUnitsCanReachDestinations
	}

	def "should return true when all units can reach destinations"(){
		given:
		Position destination1 = new Position(5, 5)
		Position destination2 = new Position(3, 3)
		List<Position> path1 = world.findShortestPathWarp(unit1.getPosition(), destination1)
		MoveEstimation estimation1 = unit1Computer.estimateMove(world, unit1, path1, unit1.getPosition(), destination1)
		List<Position> path2 = world.findShortestPathWarp(unit2.getPosition(), destination2)
		MoveEstimation estimation2 = unit2Computer.estimateMove(world, unit2, path2, unit2.getPosition(), destination2)


		when:
		unitsMoveDestinations.addDestination(new UnitMoveDestination(unit1, destination1, estimation1, true))
		unitsMoveDestinations.addDestination(new UnitMoveDestination(unit2, destination2, estimation2, true))

		then:
		unitsMoveDestinations.destinationsList.size() == 2
		unitsMoveDestinations.canAllUnitsReachDestinations()
	}

	def "should return false when any of units cannot reach destinations"(){
		given:
		Position destination1 = new Position(5, 5)
		Position destination2 = new Position(3, 3)
		List<Position> path1 = world.findShortestPathWarp(unit1.getPosition(), destination1)
		MoveEstimation estimation1 = unit1Computer.estimateMove(world, unit1, path1, unit1.getPosition(), destination1)
		List<Position> path2 = world.findShortestPathWarp(unit2.getPosition(), destination2)
		MoveEstimation estimation2 = unit2Computer.estimateMove(world, unit2, path2, unit2.getPosition(), destination2)


		when:
		unitsMoveDestinations.addDestination(new UnitMoveDestination(unit1, destination1, estimation1, true))
		unitsMoveDestinations.addDestination(new UnitMoveDestination(unit2, destination2, estimation2, false))

		then:
		unitsMoveDestinations.destinationsList.size() == 2
		!unitsMoveDestinations.canAllUnitsReachDestinations()
	}

	def "both should return true when contains destination or unit"(){
		given:
		Position destination = new Position(5, 5)
		List<Position> path1 = world.findShortestPathWarp(unit1.getPosition(), destination)
		MoveEstimation estimation1 = unit1Computer.estimateMove(world, unit1, path1, unit1.getPosition(), destination)
		unitsMoveDestinations.addDestination(new UnitMoveDestination(unit1, destination, estimation1, true))

		when:
		boolean containsUnit = unitsMoveDestinations.containsUnit(unit1)
		boolean containsDestination = unitsMoveDestinations.containsDestination(destination)

		then:
		containsUnit
		containsDestination
	}

	def "both should return false when not containing destination or unit"(){
		when:
		Position destination = new Position(5, 5)
		boolean containsUnit = unitsMoveDestinations.containsUnit(unit1)
		boolean containsDestination = unitsMoveDestinations.containsDestination(destination)

		then:
		!containsUnit
		!containsDestination
	}
}
