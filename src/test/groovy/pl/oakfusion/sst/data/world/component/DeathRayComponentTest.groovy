package pl.oakfusion.sst.data.world.component

import spock.lang.Specification

class DeathRayComponentTest extends Specification {

	def "should return proper status depending on health points"() {
		given:
		def deathRayComponent = new DeathRayComponent(100, currentHealth, 15)

		when:
		def broken = deathRayComponent.isBroken()

		then:
		broken == expectedStatus

		where:
		currentHealth | expectedStatus
		100           | false
		25            | true
		30            | false
	}

	def "should set current health to 0"(){
		given:
		def deathRayComponent = new DeathRayComponent()

		when:
		deathRayComponent.used()

		then:
		deathRayComponent.getCurrentHealth() == 0
	}
}
