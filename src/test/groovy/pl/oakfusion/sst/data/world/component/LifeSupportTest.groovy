package pl.oakfusion.sst.data.world.component


import spock.lang.Specification

class LifeSupportTest extends Specification {

	def "should LifeSupportStatus be ACTIVE"() {

		when:
		instance

		then:
		instance.getLifeSupportStatus() == expectedStatus

		where:
		instance                           | expectedStatus
		new LifeSupport()                  | LifeSupportStatus.ACTIVE
		new LifeSupport(100.0, 100.0)      | LifeSupportStatus.ACTIVE
		new LifeSupport(100.0, 100.0, 100) | LifeSupportStatus.ACTIVE
	}

	def "should lifeReserved equal to expected value depending on the entry data"() {

		given:
		def lifeSupport = new LifeSupport(maxHealth, currentHealth, 100)

		when:
		lifeSupport.lifeReserve(crew)

		then:
		lifeSupport.getRemainingTime() == expected

		where:
		maxHealth | currentHealth | crew | expected
		100       | 100           | 10   | 10
		100       | 10            | 10   | 111
		100       | 1             | 1000 | 0
	}

	def "should throw IllegalArgumentException when crew is 0"() {
		given:
		def lifeSupport = new LifeSupport(100, 100, 100)

		when:
		lifeSupport.lifeReserve(0)

		then:
		IllegalArgumentException exception = thrown()
		exception.getMessage() == "Crew can't be equal to 0"
	}

	def "should return proper status depending on the entry data"() {

		given:
		def lifeSupport = new LifeSupport(maxHealth, currentHealth, 100)

		when:
		lifeSupport.checkStatus()

		then:
		lifeSupport.getLifeSupportStatus() == expected

		where:
		maxHealth | currentHealth | expected
		100       | 100           | LifeSupportStatus.ACTIVE
		100       | 99            | LifeSupportStatus.DAMAGED
		100       | 0             | LifeSupportStatus.DAMAGED
		100       | 101           | LifeSupportStatus.DAMAGED

		//TODO Napisać zabezpieczenie przeciwko currentHealth > maxHealth
	}
}
