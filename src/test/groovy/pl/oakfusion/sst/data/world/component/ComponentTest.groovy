package pl.oakfusion.sst.data.world.component

import pl.oakfusion.sst.data.world.component.ComponentCondition
import pl.oakfusion.sst.data.world.component.Component
import spock.lang.Specification

class ComponentTest extends Specification {


	Component component = new Component(100, 70, 30) {
		@Override
		void setCurrentHealth(double currentHealth) {
			super.setCurrentHealth(currentHealth)
		}

		@Override
		double getCurrentHealth() {
			return super.getCurrentHealth()
		}

		@Override
		void fullRepair() {
			super.fullRepair()
		}

		@Override
		double calculateHealthPercentage() {
			return super.calculateHealthPercentage()
		}
	}


	def "should repair Health Percentage"() {
		given:
		component.currentHealth = 70
		component.repairRatio = 30
		when:
		double healthPercentage = component.calculateHealthPercentage()

		then:
		healthPercentage == 70
	}

	def "Should return repair time"() {
		given:
		component.currentHealth = 70
		component.repairRatio = 30
		when:
		double repairTime = component.calculateRepairTime()

		then:
		repairTime == 1.0
	}

	def "should update component condition to damaged"() {
		given:
		component.currentHealth = 30

		when:
		def cc = component.getComponentCondition()

		then:
		cc == ComponentCondition.DAMAGED
	}

	def "should update component condition to functional"() {
		given:
		component.currentHealth = 70

		when:
		def cc = component.getComponentCondition()

		then:
		cc == ComponentCondition.FUNCTIONAL
	}

	def "should recive repair set ComponentCondition to functional and return 0 "() {
		given:
		component.currentHealth = 30
		double stardatesToRepair = 15
		double repairTime = 30

		when:
		double result = component.receiveRepairAndGetOverflow(stardatesToRepair, repairTime)

		then:
		result == 0
		component.currentHealth == 65
		component.getComponentCondition() == ComponentCondition.FUNCTIONAL
	}

	def "should recive repair set ComponentCondition to Damaged and return 0 "() {
		given:
		component.currentHealth = 20
		double stardatesToRepair = 10
		double repairTime = 40

		when:
		double result = component.receiveRepairAndGetOverflow(stardatesToRepair, repairTime)

		then:
		result == 0
		component.currentHealth == 40
		component.getComponentCondition() == ComponentCondition.DAMAGED
	}


	def "should repair component to max health set ComponentCondition to Functional"() {
		given:
		component.currentHealth = 20
		double stardatesToRepair = 100
		double repairTime = 40

		when:
		double result = component.receiveRepairAndGetOverflow(stardatesToRepair, repairTime)

		then:
		result == 60
		component.currentHealth == 100
		component.getComponentCondition() == ComponentCondition.FUNCTIONAL
	}

	def " should full repair component"() {
		given:
		component.currentHealth = 10

		when:
		component.fullRepair()

		then:
		component.currentHealth == 100
		component.getComponentCondition() == ComponentCondition.FUNCTIONAL
	}

	def "Should reset repair ratio "() {
		given:
		component.repairRatio = 20

		when:
		component.resetRepairRatio()

		then:
		component.repairRatio == 15
	}

	def "Component should recive damage Enterprise is alive "() {
		given:
		double damage = 30
		component.currentHealth = 40

		when:
		double result = component.receiveDamageAndGetOverflow(damage)

		then:
		component.currentHealth == 10
		component.componentCondition == ComponentCondition.DAMAGED
		result == 0

	}

	def "Component should recive damage Enterprise is not alive "() {
		given:
		double damage = 50
		component.currentHealth = 40

		when:
		double result = component.receiveDamageAndGetOverflow(damage)

		then:
		component.currentHealth == 0
		component.componentCondition == ComponentCondition.DAMAGED
		result == 10
	}
}
