package pl.oakfusion.sst.data.world.component

import spock.lang.Specification

class WarpEnginesTest extends Specification {
	def warpEngines

	def setup() {
		warpEngines = new WarpEngines(100,30,2,true)
	}

	def "should not throw exception when when warp factor is in range 1-10"() {
		when:
		WarpEngines warpEngines = new WarpEngines(100.0, 1.0, 5, true)

		then:
		noExceptionThrown()
	}


	def "if ifWarpFactorMutable=true then conditionalSetWarpFactor() will change value of warpFactor"() {
		when:
		warpEngines.setWarpFactor(1)

		then:
		warpEngines.getWarpFactor()==1
	}

	def "if ifWarpFactorMutable=false then conditionalSetWarpFactor() will not change value of warpFactor"() {
		given:
		warpEngines.setIfWarpFactorMutable(false)

		when:
		warpEngines.setWarpFactor(1)

		then:
		warpEngines.getWarpFactor()==2
	}

	def "getMaxWarpFactor"() {
		when:
		warpEngines.setCurrentHealth(currentHealth)
		def maxWarpFactor = warpEngines.getMaxWarpFactor(levelOfFunctionality)

		then:
		maxWarpFactor == result

		where:
		currentHealth	|levelOfFunctionality	|result
		99				|0.99					|9
		70          	|0						|7
		30				|1						|10
		100             |0.7					|10
	}
}
