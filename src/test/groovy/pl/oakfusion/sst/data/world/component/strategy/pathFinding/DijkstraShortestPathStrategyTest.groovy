package pl.oakfusion.sst.data.world.component.strategy.pathFinding

import pl.oakfusion.sst.data.util.Position
import spock.lang.Specification

 class DijkstraShortestPathStrategyTest extends Specification {

	 def "should find path if exists"() {
		 given:
		 def matrix = createMatrixOfZeroes(4)
		 matrix[0][2] = -1;
		 matrix[1][2] = -1;
		 matrix[2][2] = -1;
		 def strategy = new DijkstraShortestPathStrategy();

		 when:
		 def path = strategy.findShortestPath(matrix, new Position(1,1), new Position(1,3))

		 then:
		 path.size() == 4
	 }

	 def "should find path if exists2"() {
		 given:
		 def matrix = createMatrixOfZeroes(4)
		 matrix[2][0] = -1;
		 matrix[2][1] = -1;
		 matrix[2][2] = -1;

		 def strategy = new DijkstraShortestPathStrategy();
		 when:
		 def path = strategy.findShortestPath(matrix, new Position(1,1), new Position(3,1))

		 then:
		 path.size() == 4


	 }

	 def "should not find path if doesn't exist"() {
		 given:
		 def matrix = createMatrixOfZeroes(4)
		 matrix[2][0] = -1;
		 matrix[2][1] = -1;
		 matrix[2][2] = -1;
		 matrix[0][2] = -1;
		 matrix[1][2] = -1;
		 def strategy = new DijkstraShortestPathStrategy();
		 when:
		 def path = strategy.findShortestPath(matrix, new Position(1,1), new Position(1,3))

		 then:
		 path.size() == 0
	 }

	 def "path should be empty if source == target"() {
		 given:
		 def matrix = createMatrixOfZeroes(4)
		 def strategy = new DijkstraShortestPathStrategy();
		 when:
		 def path = strategy.findShortestPath(matrix, new Position(1,1), new Position(1,1))

		 then:
		 path.size() == 0
	 }

	 def createMatrixOfZeroes(int n){
		 Integer[][] matrix = new Integer[n][n];
		 for (int i = 0; i < n; i++){
			 for (int j = 0; j < n; j++){
				 matrix[i][j] = 0;
			 }
		 }
		 return matrix
	 }
}
