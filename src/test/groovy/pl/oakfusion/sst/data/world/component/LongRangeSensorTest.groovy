package pl.oakfusion.sst.data.world.component

import spock.lang.Specification

class LongRangeSensorTest extends Specification{

	LongRangeSensor longRangeSensor = new LongRangeSensor(100, 30,2)

	def "getQuadrantKey should return quadrantY * quadrantSize + quadrantX"() {
		when:
		int quadrantKey = LongRangeSensor.getQuadrantKey(1,2,3)

		then:
		quadrantKey == 7
	}

	def "if levelOfFunctionality = 1 or CurrentHealth is maxHealth getViewRangeTest should return scanRange value"() {
		given:
		longRangeSensor.setCurrentHealth(currentHealth)

		when:
		int viewRange = longRangeSensor.getViewRange(levelOfFunctionality)

		then:
		viewRange == 2

		where:
		currentHealth  |levelOfFunctionality
		70          |1
		10          |1
		100             |0.5
		100             |0
	}

	def "if levelOfFunctionality <1 && CurrentHealth < maxHealth getViewRangeTest should return lower value than scanRange"() {
		given:
		longRangeSensor.setCurrentHealth(currentHealth)

		when:
		int viewRange = longRangeSensor.getViewRange(levelOfFunctionality)

		then:
		viewRange < 2

		where:
		currentHealth  |levelOfFunctionality
		70          |0.5
		10          |0.9
		99          |0.1
		0           |0
	}
}
