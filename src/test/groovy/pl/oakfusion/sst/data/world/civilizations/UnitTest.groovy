package pl.oakfusion.sst.data.world.civilizations

import pl.oakfusion.sst.data.game.commands.rest.Rest
import pl.oakfusion.sst.data.game.commands.torpedo.Torpedo
import pl.oakfusion.sst.data.parameter.GameDifficulty
import pl.oakfusion.sst.data.world.component.Condition
import pl.oakfusion.sst.data.world.component.ShieldsStatus
import pl.oakfusion.sst.data.world.component.strategy.repair.MostBrokenFirstRepairingStrategy
import spock.lang.Specification

class UnitTest extends Specification {
	AbstractUnitFactory unitFactory = new UnitFactory(GameDifficulty.GOOD)
	Unit unit = unitFactory.createEnterprise(UUID.randomUUID()).create(0,0)

	def "MoveToTest"() {
		when:
		unit.moveTo(2,2)

		then:
		assert unit.x == 2
		assert unit.y == 2
	}

	def "MoveDeltaTest"() {
		when:
		unit.moveTo(3,1)

		then:
		assert unit.x == 3
		assert unit.y == 1

	}

	def "UpdateConditionTest"() {
		when:
		unit.receiveHit(1000000)
		then:
		assert unit.condition == Condition.RED
	}

	def "GetHealthTest"() {
		when:
		double health = unit.getHealth()
		then:
		assert health > 0
	}

	def "GetHealthWithShieldsTest"() {
		when:
		double health = unit.getHealth()
		then:
		assert health > 0
	}

	def "GetPercentageHealthTest"() {
		when:
		double health = unit.getHealthAsFraction()
		then:
		assert health >= 0 && health <= 1
	}

	def "ReceiveHitTest"() {
		given:
		var oldHealth = unit.getHealthAsFraction()
		unit.componentSystem.getShields().setShieldsStatus(ShieldsStatus.DOWN)

		when:
		unit.receiveHit(1000)

		then:
		assert unit.getHealthAsFraction() < oldHealth
		unit.getCondition() == Condition.RED


	}

	def "GetTypeTest"() {
		assert unit.getType() == UnitType.ENTERPRISE
	}

	def "UsedActionPoints"() {
		when:
		unit.actionPoints = 3
		unit.decreaseActionPoints(1.0, new MostBrokenFirstRepairingStrategy())

		then:
		unit.actionPoints == 2
	}

	def "CanPerform command requiring technology"() {
		expect:
		assert unit.canPerformCommand(Torpedo.class)
	}

	def "Can perform command not requiring technology"() {
		expect:
		assert unit.canPerformCommand(Rest.class)
	}

	def "Dock"() {
		given:
		unit.receiveHit(3500)
		var oldHealth = unit.getHealth()

		when:

		unit.dock()

		then:
		println(oldHealth + " " + unit.getHealth())
		assert oldHealth < unit.getHealth()
		assert unit.getCondition() == Condition.YELLOW
	}

}
