package pl.oakfusion.sst.data.world.component


import spock.lang.Specification

class TorpedoesDeviceTest extends Specification {

	def torpedoesDevice

	def setup() {
		torpedoesDevice = new TorpedoesDevice()
	}

	def "should throw IllegalArgumentException when torpedoes less than 0"() {

		when:
		new TorpedoesDevice(100, 100, -1, 0)

		then:
		IllegalArgumentException exception = thrown()
		exception.getMessage() == "Torpedoes or Max Torpedoes can't be less than 0"
	}

	def "should throw IllegalArgumentException when maxTorpedoes less than 0"() {

		when:
		new TorpedoesDevice(100, 100, 0, -1)

		then:
		IllegalArgumentException exception = thrown()
		exception.getMessage() == "Torpedoes or Max Torpedoes can't be less than 0"
	}

	def "when reset torpedoes then torpedoes should equal to maxTorpedoes"() {
		given:
		torpedoesDevice.setTorpedoes(5)
		torpedoesDevice.setMaxTorpedoes(7)

		when:
		torpedoesDevice.resetTorpedoes()

		then:
		torpedoesDevice.getTorpedoes() == torpedoesDevice.getMaxTorpedoes()
	}



	def "shouldn throw IllegalArgumentException when maxTorpedoes less than 0"() {
		when:
		torpedoesDevice.setMaxTorpedoes(-1)

		then:
		IllegalArgumentException e = thrown()
		e.message == "Torpedoes or Max Torpedoes can't be less than 0"
	}

	def "shouldn throw IllegalArgumentException when torpedoes less than 0"() {
		when:
		torpedoesDevice.setTorpedoes(-1)

		then:
		IllegalArgumentException e = thrown()
		e.message == "Torpedoes or Max Torpedoes can't be less than 0"
	}
}

