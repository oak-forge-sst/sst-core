package pl.oakfusion.sst.data.world.component

import pl.oakfusion.sst.data.game.commands.shields.ChangeShieldsStatus
import pl.oakfusion.sst.data.game.events.shields.ShieldStatusChangeFailed
import pl.oakfusion.sst.data.game.events.shields.ShieldsLowered
import pl.oakfusion.sst.data.game.events.shields.ShieldsRisen
import spock.lang.Specification

class ShieldsTest extends Specification {

	def "should set up energyCapacity and energy on 2500 and 2500"() {

		when:
		def shields = new Shields(100, 100)

		then:
		shields.getEnergyContainer().getCapacity() == 2500
		shields.getEnergyContainer().getEnergy() == 2500
	}

	def "should set up energyCapacity and energy to the same level"() {

		when:
		def shields = new Shields(100, 100, 2000)

		then:
		shields.getEnergyContainer().getCapacity() == 2000
		shields.getEnergyContainer().getEnergy() == 2000
	}

	def "should invoke proper class when shieldStatus is properly configured"() {

		given:
		def shields = new Shields(maxHealth, currentHealth, repairRatio, energyCapacity, energy, currertShieldStatus)

		def changeShieldStatus = Mock(ChangeShieldsStatus.class)
		changeShieldStatus.getShieldsStatus() >> shieldStatus

		when:
		def status = shields.changeShieldStatus(changeShieldStatus)

		then:
		status.getClass() == expectedInstanceClass

		where:
		maxHealth | currentHealth | repairRatio | energyCapacity | energy | currertShieldStatus | shieldStatus       | expectedInstanceClass
		100       | 100           | 100         | 2500           | 2500   | ShieldsStatus.DOWN  | ShieldsStatus.UP   | ShieldsRisen.class
		100       | 100           | 100         | 2500           | 2500   | ShieldsStatus.UP    | ShieldsStatus.DOWN | ShieldsLowered.class
		100       | 25            | 100         | 2500           | 2500   | ShieldsStatus.UP    | ShieldsStatus.DOWN | ShieldStatusChangeFailed.class
		100       | 25            | 100         | 2500           | 2500   | ShieldsStatus.DOWN  | ShieldsStatus.UP   | ShieldStatusChangeFailed.class
	}
}
