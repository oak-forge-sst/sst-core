package pl.oakfusion.sst.data.world.component

import pl.oakfusion.sst.data.game.commands.deathRay.DeathRay
import pl.oakfusion.sst.data.game.commands.mayday.Mayday
import pl.oakfusion.sst.data.game.commands.move.Move
import pl.oakfusion.sst.data.game.commands.phasers.AskForOpponentsInRange
import pl.oakfusion.sst.data.game.commands.phasers.Phasers
import pl.oakfusion.sst.data.game.commands.shields.ChangeShieldsStatus
import pl.oakfusion.sst.data.game.commands.shields.ShieldsCommand
import pl.oakfusion.sst.data.game.commands.shields.TransferShieldsEnergy
import pl.oakfusion.sst.data.game.commands.torpedo.Torpedo
import pl.oakfusion.sst.data.game.commands.tractorbeam.TractorBeam
import pl.oakfusion.sst.data.game.commands.warp.ChangeWarpFactor
import spock.lang.Specification

class CommandToComponentIdChangerTest extends Specification {

	def "should return proper component id assigned to command"(){
		when:
		ComponentId componentId = CommandToComponentIdChanger.getComponentIdByCommand(command).orElseThrow()

		then:
		componentId == expectedId

		where:
		command                      |	expectedId
		ChangeShieldsStatus.class    |	ComponentId.SHIELDS
		ShieldsCommand.class         |	ComponentId.SHIELDS
		TransferShieldsEnergy.class  |	ComponentId.SHIELDS
		Torpedo.class                |	ComponentId.TORPEDOES_DEVICE
		TractorBeam.class            |	ComponentId.TRACTOR_BEAM_DEVICE
		ChangeWarpFactor.class       |	ComponentId.WARP_ENGINES
		Move.class                   |	ComponentId.WARP_ENGINES
		AskForOpponentsInRange.class |	ComponentId.PHASERS_DEVICE
		Phasers.class                |	ComponentId.PHASERS_DEVICE
		DeathRay.class               |	ComponentId.DEATH_RAY_COMPONENT
		Mayday.class                 |	ComponentId.SUBSPACE_RADIO
	}
}
