package pl.oakfusion.sst.data.world.component.strategy.pathFinding

import pl.oakfusion.sst.data.util.Position
import spock.lang.Specification

class EmptyWorldPathFinderStrategyTest extends Specification {

	def "should find path"() {
		given:
		def matrix = createMatrixOfZeroes(4)
		def strategy = new EmptyWorldPathFinderStrategy();
		when:
		def path = strategy.findShortestPath(matrix, new Position(1,1), new Position(1,3))

		then:
		path.size() == 2


	}

	def "should find path2"() {
		given:
		def matrix = createMatrixOfZeroes(4)
		def strategy = new EmptyWorldPathFinderStrategy();
		when:
		def path = strategy.findShortestPath(matrix, new Position(1,1), new Position(3,1))

		then:
		path.size() == 2


	}

	def "path should be empty if source == target"() {
		given:
		def matrix = createMatrixOfZeroes(4)
		def strategy = new EmptyWorldPathFinderStrategy();
		when:
		def path = strategy.findShortestPath(matrix, new Position(1,1), new Position(1,1))

		then:
		path.size() == 0
	}


	def createMatrixOfZeroes(int n){
		Integer[][] matrix = new Integer[n][n];
		for (int i = 0; i < n; i++){
			for (int j = 0; j < n; j++){
				matrix[i][j] = 0;
			}
		}
		return matrix
	}

}
