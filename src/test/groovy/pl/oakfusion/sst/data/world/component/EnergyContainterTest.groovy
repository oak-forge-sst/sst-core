package pl.oakfusion.sst.data.world.component

import pl.oakfusion.sst.data.world.component.EnergyContainer
import spock.lang.Specification

class EnergyContainterTest extends Specification {


	def energyContainer

	def "should return shield energy after got damage"() {
		given:
		energyContainer = new EnergyContainer(1000, 100, 100, 100)

		when:
		energyContainer.damageShields(50)

		then:
		energyContainer.energy == 50

	}

	def "shield is getting damage bigger then shield "() {
		given:
		energyContainer = new EnergyContainer(1000, 100, 100, 100)

		when:
		double overdamage = energyContainer.damageShields(101)

		then:
		overdamage == 1
		energyContainer.energy == 0
	}


	def "setting energy to max"() {
		given:
		energyContainer = new EnergyContainer(1000, 100, 100, 100)

		when:
		energyContainer.setEnergyToMax()

		then:
		energyContainer.energy == energyContainer.capacity
	}


}
