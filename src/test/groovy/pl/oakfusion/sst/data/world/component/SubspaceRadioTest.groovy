package pl.oakfusion.sst.data.world.component

import pl.oakfusion.sst.data.world.component.SubspaceRadio
import spock.lang.Specification

class SubspaceRadioTest extends Specification {

	def subspaceradio

	def "SubspaceRadio is not broken return false"() {
		given:
		subspaceradio = new SubspaceRadio(100, 100)
		subspaceradio.setCurrentHealth(100)

		when:
		def isBroken = subspaceradio.isBroken()

		then:
		!isBroken
	}

	def "SubspaceRadio is broken returns true"() {
		given:
		subspaceradio = new SubspaceRadio(100, 100)
		subspaceradio.setCurrentHealth(0)
		when:
		def isBroken = subspaceradio.isBroken()

		then:
		isBroken
	}

}
