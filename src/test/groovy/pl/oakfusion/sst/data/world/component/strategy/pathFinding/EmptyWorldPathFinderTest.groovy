package pl.oakfusion.sst.data.world.component.strategy.pathFinding

import pl.oakfusion.sst.data.util.Position
import spock.lang.Specification

class EmptyWorldPathFinderTest extends Specification {
	EmptyWorldPathFinder emptyWorldPathFinder = new EmptyWorldPathFinder()

	def "should return path in straight line"(){
		given:
		Position source = new Position(1, 1)
		Position target = new Position(xt, yt)

		when:
		List<Position> path = emptyWorldPathFinder.findPathInEmptyWorld(source, target)

		then:
		path.size() == 9
		path.containsAll(List.of(
				new Position(x1, y1),
				new Position(x2, y2),
				new Position(x3, y3),
				new Position(x4, y4),
				new Position(x5, y5),
				new Position(x6, y6),
				new Position(x7, y7),
				new Position(x8, y8),
				new Position(x9, y9),
		))

		where:
		xt	|	yt	|	x1	|	x2	|	x3	|	x4	|	x5	|	x6	|	x7	|	x8	|	x9	|	y1	|	y2	|	y3	|	y4	|	y5	|	y6	|	y7	|	y8	|	y9
		1	|	10	|	1	|	1	|	1	|	1	|	1	|	1	|	1	|	1	|	1	|	2	|	3	|	4	|	5	|	6	|	7	|	8	|	9	|	10
		10	|	10	|	2	|	3	|	4	|	5	|	6	|	7	|	8	|	9	|	10	|	2	|	3	|	4	|	5	|	6	|	7	|	8	|	9	|	10
		1	|	-8	|	1	|	1	|	1	|	1	|	1	|	1	|	1	|	1	|	1	|	0	|	-1	|	-2	|	-3	|	-4	|	-5	|	-6	|	-7	|	-8
		-8	|	-8	|	0	|	-1	|	-2	|	-3	|	-4	|	-5	|	-6	|	-7	|	-8	|	0	|	-1	|	-2	|	-3	|	-4	|	-5	|	-6	|	-7	|	-8
	}
}
