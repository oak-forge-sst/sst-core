package pl.oakfusion.sst.data.world.component

import pl.oakfusion.sst.data.game.MoveEstimation
import pl.oakfusion.sst.data.parameter.GameDifficulty
import pl.oakfusion.sst.data.util.Position
import pl.oakfusion.sst.data.world.*
import pl.oakfusion.sst.data.world.civilizations.Unit
import pl.oakfusion.sst.data.world.civilizations.UnitFactory
import pl.oakfusion.sst.data.world.gameobject.Planet
import pl.oakfusion.sst.data.world.gameobject.Starbase
import pl.oakfusion.testutils.SstTestUtils
import spock.lang.Specification

class ComputerTest extends Specification {
	private Computer computer = new Computer(100, 1)
	private SstTestUtils testUtils = new SstTestUtils()

	def "should compute distance without enterprise position"() {
		given:
		int x = 7
		int y = 4
		when:
		double distance = computer.computeDistance(x, y)
		double expectedDistance = 8.06225774829855
		then:
		distance == expectedDistance
	}

	def "should compute distance with enterprise position"() {
		given:
		int x = 15
		int y = 17
		int eX = 37
		int eY = 23
		when:
		double distance = computer.computeDistance(x, y, eX, eY)
		double expectedDistance = 22.80350850198276
		then:
		distance == expectedDistance
	}

	def "should compute energy cost at warp one"() {
		given:
		double distance = 13.41
		int warpFactor = 1
		when:
		double energyCost = computer.computeEnergy(warpFactor, distance, ShieldsStatus.DOWN)
		double expectedEnergyCost = 1.3410000000000002
		then:
		energyCost == expectedEnergyCost
	}

	def "should return energy -1 when warp factor or distance not in range cost at warp one"() {
		when:
		double energyCost = computer.computeEnergy(warpFactor, distance, ShieldsStatus.DOWN)

		then:
		energyCost == -1

		where:
		warpFactor	|	distance
		0			|	1
		11			|	1
		1			|	-1
		0			|	-1
		11			|	-1
	}

	def "should compute energy cost at warp ten"() {
		given:
		double distance = 13.41
		int warpFactor = 10
		when:
		double energyCost = computer.computeEnergy(warpFactor, distance, ShieldsStatus.DOWN)
		double expectedEnergyCost = 1341
		then:
		energyCost == expectedEnergyCost
	}

	def "should compute energy cost at warp ten when shields UP"() {
		given:
		double distance = 13.41
		int warpFactor = 10
		when:
		double energyCost = computer.computeEnergy(warpFactor, distance, ShieldsStatus.UP)
		double expectedEnergyCost = 1341*2
		then:
		energyCost == expectedEnergyCost
	}

	def "should compute time cost at warp one"() {
		given:
		double distance = 1
		int warpFactor = 1
		when:
		double timeCost = computer.computeTime(warpFactor, distance)
		double expectedTimeCost = 0.3
		then:
		timeCost == expectedTimeCost
	}

	def "should compute time cost at warp ten"() {
		given:
		double distance = 4.23
		int warpFactor = 10
		when:
		double timeCost = computer.computeTime(warpFactor, distance)
		double expectedTimeCost = 0.042300000000000004
		then:
		timeCost == expectedTimeCost
	}

	def "should return -1 time when warp factor or distance not in range cost at warp one"() {
		when:
		double timeCost = computer.computeTime(warpFactor, distance)

		then:
		timeCost == -1

		where:
		warpFactor	|	distance
		0			|	1
		11			|	1
		1			|	-1
		0			|	-1
		11			|	-1
	}


	def "should compute resources and return true"() {
		given:
		double timeCost = 124.6
		double energyCost = 326.8
		Unit enterprise = new UnitFactory(GameDifficulty.GOOD).createEnterprise(UUID.randomUUID()).create(0, 0)
		enterprise.getComponentSystem().getEnergyContainer().setEnergy(500)
		LifeSupport lifeSupport = enterprise.getComponentSystem().getComponent(ComponentId.LIFE_SUPPORT)
		lifeSupport.setLifeSupportStatus(LifeSupportStatus.DAMAGED)
		lifeSupport.setRemainingTime(10000)

		when:
		boolean isEnoughResources = computer.isEnoughResources(timeCost, energyCost, enterprise)

		then:
		isEnoughResources
	}

	def "should compute resources and return false when not enough reserve"() {
		given:
		double timeCost = 124.6
		double energyCost = 726.8
		Unit enterprise = new UnitFactory(GameDifficulty.GOOD).createEnterprise(UUID.randomUUID()).create(0, 0)
		enterprise.getComponentSystem().getWarpEngines().setWarpFactor(5)
		((EnergyContainer) enterprise.getComponentSystem().getComponent(ComponentId.ENERGY_CONTAINER)).setEnergy(800)
		LifeSupport lifeSupport = enterprise.getComponentSystem().getComponent(ComponentId.LIFE_SUPPORT)
		lifeSupport.setLifeSupportStatus(LifeSupportStatus.DAMAGED)
		lifeSupport.setRemainingTime(10)

		when:
		boolean isEnoughResources = computer.isEnoughResources(timeCost, energyCost, enterprise)

		then:
		!isEnoughResources
	}

	def "should compute resources and return false when not enough energy"() {
		given:
		double timeCost = 124.6
		double energyCost = 726.8
		Unit enterprise = new UnitFactory(GameDifficulty.GOOD).createEnterprise(UUID.randomUUID()).create(0, 0)
		enterprise.getComponentSystem().getWarpEngines().setWarpFactor(5)
		((EnergyContainer) enterprise.getComponentSystem().getComponent(ComponentId.ENERGY_CONTAINER)).setEnergy(500)
		LifeSupport lifeSupport = enterprise.getComponentSystem().getComponent(ComponentId.LIFE_SUPPORT)
		lifeSupport.setLifeSupportStatus(LifeSupportStatus.DAMAGED)
		lifeSupport.setRemainingTime(10000)

		when:
		boolean isEnoughResources = computer.isEnoughResources(timeCost, energyCost, enterprise)

		then:
		!isEnoughResources
	}

	def "should compute resources and return true when not enough reserve and active lifeSupport"() {
		given:
		double timeCost = 124.6
		double energyCost = 726.8
		Unit enterprise = new UnitFactory(GameDifficulty.GOOD).createEnterprise(UUID.randomUUID()).create(0, 0)
		LifeSupport lifeSupport = enterprise.getComponentSystem().getComponent(ComponentId.LIFE_SUPPORT)
		lifeSupport.setLifeSupportStatus(LifeSupportStatus.ACTIVE)
		lifeSupport.setRemainingTime(10)

		when:
		boolean isEnoughResources = computer.isEnoughResources(timeCost, energyCost, enterprise)

		then:
		isEnoughResources
	}

	def "should compute resources and return false when not enough energy and active lifeSupport"() {
		given:
		double timeCost = 124.6
		double energyCost = 726.8
		Unit enterprise = new UnitFactory(GameDifficulty.GOOD).createEnterprise(UUID.randomUUID()).create(0, 0)
		LifeSupport lifeSupport = enterprise.getComponentSystem().getComponent(ComponentId.LIFE_SUPPORT)
		lifeSupport.setLifeSupportStatus(LifeSupportStatus.ACTIVE)
		lifeSupport.setRemainingTime(10)
		enterprise.getComponentSystem().getEnergyContainer().setEnergy(500)

		when:
		boolean isEnoughResources = computer.isEnoughResources(timeCost, energyCost, enterprise)

		then:
		!isEnoughResources
	}

	def "should compute time and energy for autoMode to specified location"() {
		given:
		int x = 5
		int y = 5
		int warpFactor = 5
		World world = new World(10, 10, 10, new StarDate(2000, 3))

		Unit enterprise = new UnitFactory(GameDifficulty.GOOD).createEnterprise(UUID.randomUUID()).create(x, y)
		enterprise.getComponentSystem().getWarpEngines().setWarpFactor(warpFactor)
		enterprise.getComponentSystem().getShields().setShieldsStatus(ShieldsStatus.DOWN)

		Starbase starbase = new Starbase(10, 10)
		double expectedTime = 0.2
		double expectedEnergy = 62.5
		List<Position> path = world.findShortestPathWarp(enterprise.getPosition(), new Position(starbase.getX(), starbase.getY()))

		when:
		MoveEstimation moveEstimation = computer.estimateMove(world, enterprise, path, enterprise.getPosition(), new Position(starbase.getX(), starbase.getY()))

		then:
		moveEstimation.getTimeCostToLocation() == expectedTime
		moveEstimation.getEnergyCostToLocation() == expectedEnergy
	}

	def "should compute time and energy for autoMode to specified location shields UP"() {
		given:
		int x = 5
		int y = 5
		int warpFactor = 5
		World world = new World(10, 10, 10, new StarDate(2000, 3))
		Unit enterprise = new UnitFactory(GameDifficulty.GOOD).createEnterprise(UUID.randomUUID()).create(x, y)
		enterprise.getComponentSystem().getWarpEngines().setWarpFactor(warpFactor)
		enterprise.getComponentSystem().getShields().setShieldsStatus(ShieldsStatus.UP)

		Starbase starbase = new Starbase(10, 10)
		double expectedTime = 0.2
		double expectedEnergy = 125.0
		List<Position> path = world.findShortestPathWarp(enterprise.getPosition(), new Position(starbase.getX(), starbase.getY()))

		when:
		MoveEstimation moveEstimation = computer.estimateMove(world, enterprise, path, enterprise.getPosition(), new Position(starbase.getX(), starbase.getY()))

		then:
		moveEstimation.getTimeCostToLocation() == expectedTime
		moveEstimation.getEnergyCostToLocation() == expectedEnergy
	}

	def "should return closest starbase"() {
		given:
		StarDate starDate = new StarDate(2018.231, 3)
		World world = new World(1, 1, 10, starDate)
		Starbase starbase = new Starbase(1, 1)
		Starbase starbase2 = new Starbase(5, 5)
		world.add(starbase)
		world.add(starbase2)

		when:
		Starbase closestStarbase = computer.getClosestStarbase(world, 4, 4)

		then:
		closestStarbase == starbase2
	}

	def "should return null when no starbase in the world"() {
		given:
		StarDate starDate = new StarDate(2018.231, 3)
		World world = new World(1, 1, 10, starDate)

		when:
		Starbase closestStarbase = computer.getClosestStarbase(world, 4, 4)

		then:
		closestStarbase == null
	}

	def "should return max double value when no starbase in the world"() {
		given:
		StarDate starDate = new StarDate(2018.231, 3)
		World world = new World(1, 1, 10, starDate)

		when:
		double distance = computer.getDistanceToClosestStarbase(world, 2, 1)
		double expectedDistance = Double.MAX_VALUE

		then:
		distance == expectedDistance
	}

	def "should compute distance to starbase"() {
		given:
		StarDate starDate = new StarDate(2018.231, 3)
		World world = new World(1, 1, 10, starDate)
		Starbase starbase = new Starbase(1, 1)
		Starbase starbase2 = new Starbase(5, 5)
		world.add(starbase)
		world.add(starbase2)

		when:
		double distance = computer.getDistanceToClosestStarbase(world, 2, 1)
		double expectedDistance = 1.0

		then:
		distance == expectedDistance
	}

	def "should compute distance to starbase2"() {
		given:
		StarDate starDate = new StarDate(2018.231, 3)
		World world = new World(1, 1, 10, starDate)
		Starbase starbase = new Starbase(1, 1)
		Starbase starbase2 = new Starbase(5, 5)
		world.add(starbase)
		world.add(starbase2)

		when:
		double distance = computer.getDistanceToClosestStarbase(world, 4, 4)
		double expectedDistance = 1.4142135623730951

		then:
		distance == expectedDistance
	}

	def "should return estimation with empty path when not enough resources and all spaces on path are occupied" (){
		given:
		World world = new World(2, 2, 10, new StarDate(StarDate.MIN_STARDATE, 1))
		for(int i = 1; i <= 3; i++){
			world.add(new Planet(0, i))
		}
		Unit enterprise = testUtils.getGameData().getActivePlayer().getCivilization().getActiveUnits().get(0)
		enterprise.getComponentSystem().getWarpEngines().setWarpFactor(5)
		enterprise.setX(0)
		enterprise.setY(0)
		//Energy == 20 should be enough to move by one sector, but it is occupied by a planet, so path will be empty
		enterprise.getComponentSystem().getEnergyContainer().setEnergy(20)
		Position target = new Position(0, 4)
		List<Position> path = world.findShortestPathWarp(enterprise.getPosition(), target)
		Computer enterpriseComputer = enterprise.getComponentSystem().getComputer()

		when:
		MoveEstimation estimation = enterpriseComputer.estimateForcedMove(world, enterprise, path, enterprise.getPosition(), target, List.of())

		then:
		estimation.getPath().isEmpty()
	}

	def "should return estimation with shorter path when not enough resources to reach target" (){
		given:
		World world = new World(2, 2, 10, new StarDate(StarDate.MIN_STARDATE, 1))
		for(int i = 2; i <= 3; i++){
			world.add(new Planet(0, i))
		}
		Unit enterprise = testUtils.getGameData().getActivePlayer().getCivilization().getActiveUnits().get(0)
		enterprise.getComponentSystem().getShields().setShieldsStatus(ShieldsStatus.DOWN)
		enterprise.getComponentSystem().getWarpEngines().setWarpFactor(5)
		enterprise.setX(0)
		enterprise.setY(0)
		//Energy == 20 should be enough to move by one sector, but it is occupied by a planet, so path will be empty
		enterprise.getComponentSystem().getEnergyContainer().setEnergy(20)
		Position target = new Position(0, 4)
		List<Position> path = world.findShortestPathWarp(enterprise.getPosition(), target)
		Computer enterpriseComputer = enterprise.getComponentSystem().getComputer()

		when:
		MoveEstimation estimation = enterpriseComputer.estimateForcedMove(world, enterprise, path, enterprise.getPosition(), target, List.of())

		then:
		estimation.getPath().size() == 1
	}

	def "should return estimation with empty path when not enough resources to reach target and positions passed in parameter are occupied" (){
		given:
		World world = new World(2, 2, 10, new StarDate(StarDate.MIN_STARDATE, 1))
		Unit enterprise = testUtils.getGameData().getActivePlayer().getCivilization().getActiveUnits().get(0)
		enterprise.getComponentSystem().getWarpEngines().setWarpFactor(5)
		enterprise.setX(0)
		enterprise.setY(0)
		//Energy == 20 should be enough to move by one sector, but it is occupied, so path will be empty
		enterprise.getComponentSystem().getEnergyContainer().setEnergy(20)
		Position target = new Position(0, 4)
		List<Position> path = world.findShortestPathWarp(enterprise.getPosition(), target)
		Computer enterpriseComputer = enterprise.getComponentSystem().getComputer()

		when:
		MoveEstimation estimation = enterpriseComputer.estimateForcedMove(world, enterprise, path, enterprise.getPosition(), target,
				List.of(
						new Position(0, 1),
						new Position(0, 2),
						new Position(0, 3)
				))

		then:
		estimation.getPath().isEmpty()
	}

	def "should return estimation with shorter path when not enough resources to reach target and positions passed in parameter are occupied" (){
		given:
		World world = new World(2, 2, 10, new StarDate(StarDate.MIN_STARDATE, 1))
		Unit enterprise = testUtils.getGameData().getActivePlayer().getCivilization().getActiveUnits().get(0)
		enterprise.getComponentSystem().getShields().setShieldsStatus(ShieldsStatus.DOWN)
		enterprise.getComponentSystem().getWarpEngines().setWarpFactor(5)
		enterprise.setX(0)
		enterprise.setY(0)
		enterprise.getComponentSystem().getEnergyContainer().setEnergy(20)
		Position target = new Position(0, 4)
		List<Position> path = world.findShortestPathWarp(enterprise.getPosition(), target)
		Computer enterpriseComputer = enterprise.getComponentSystem().getComputer()

		when:
		MoveEstimation estimation = enterpriseComputer.estimateForcedMove(world, enterprise, path, enterprise.getPosition(), target,
				List.of(
						new Position(0, 2),
						new Position(0, 3)
				))

		then:
		estimation.getPath().size() == 1
	}

	def "should return estimation with shorter path when target is occupied" (){
		given:
		World world = new World(2, 2, 10, new StarDate(StarDate.MIN_STARDATE, 1))
		for(int i = 2; i <= 4; i++){
			world.add(new Planet(0, i))
		}
		Unit enterprise = testUtils.getGameData().getActivePlayer().getCivilization().getActiveUnits().get(0)
		enterprise.getComponentSystem().getWarpEngines().setWarpFactor(5)
		enterprise.setX(0)
		enterprise.setY(0)
		enterprise.getComponentSystem().getEnergyContainer().setEnergy(4000)
		Position target = new Position(0, 4)
		List<Position> path = world.findShortestPathWarp(enterprise.getPosition(), target)
		Computer enterpriseComputer = enterprise.getComponentSystem().getComputer()

		when:
		MoveEstimation estimation = enterpriseComputer.estimateForcedMove(world, enterprise, path, enterprise.getPosition(), target, List.of())

		then:
		estimation.getPath().size() == 1
	}

	def "should return estimation with normal path when target is not occupied and unit has enough resources" (){
		given:
		World world = new World(2, 2, 10, new StarDate(StarDate.MIN_STARDATE, 1))
		for(int i = 2; i <= 3; i++){
			world.add(new Planet(0, i))
		}
		Unit enterprise = testUtils.getGameData().getActivePlayer().getCivilization().getActiveUnits().get(0)
		enterprise.getComponentSystem().getWarpEngines().setWarpFactor(5)
		enterprise.setX(0)
		enterprise.setY(0)
		enterprise.getComponentSystem().getEnergyContainer().setEnergy(4000)
		Position target = new Position(0, 4)
		List<Position> path = world.findShortestPathWarp(enterprise.getPosition(), target)
		Computer enterpriseComputer = enterprise.getComponentSystem().getComputer()

		when:
		MoveEstimation estimation = enterpriseComputer.estimateForcedMove(world, enterprise, path, enterprise.getPosition(), target, List.of())

		then:
		estimation.getPath().size() == path.size()
	}


}
