package pl.oakfusion.sst.data.util.repository

import pl.oakfusion.sst.data.world.gameobject.GameObject
import pl.oakfusion.sst.data.parameter.GameDifficulty
import pl.oakfusion.sst.data.util.Position
import pl.oakfusion.sst.data.world.gameobject.Planet
import pl.oakfusion.sst.data.world.civilizations.AbstractUnitFactory
import pl.oakfusion.sst.data.world.civilizations.Unit
import pl.oakfusion.sst.data.world.civilizations.UnitFactory
import spock.lang.Specification

class GridTest extends Specification {

	def "should add object to NODE"() {
		given:
		Planet planet1 = new Planet(3, 3)
		Grid testGrid = new Grid(50, 50, 5)

		when:
		testGrid.add(planet1)

		then:
		testGrid.get(3, 3).orElseThrow() == planet1

	}

	def "should return exception space occupied"() {
		given:
		Planet planet1 = new Planet(15, 15)
		Planet planet2 = new Planet(15, 15)
		Planet planet3 = new Planet(13, 13)
		Grid testGrid = new Grid(50, 50, 5)
		testGrid.add(planet1)
		testGrid.add(planet3)

		when:
		testGrid.add(planet2)

		then:
		def ex = thrown(SpaceOccupiedException)
		ex.message == "This position: 15, 15 is already occupied by another object"
	}

	def "should return exception out of bound"() {
		given:
		Planet planet1 = new Planet(52, 3)
		Grid testGrid = new Grid(50, 50, 5)

		when:
		testGrid.add(planet1)

		then:
		def ex = thrown(IndexOutOfBoundsException)
		ex.message == "Object cant be added outside of the world: 52, 3, WorldSize: 50, 50"
	}

	def "should remove object in NODE by coordinate"() {
		given:
		Planet planet1 = new Planet(3, 3)
		Planet planet2 = new Planet(6, 3)
		Grid testGrid = new Grid(50, 50, 5)
		testGrid.add(planet1)
		testGrid.add(planet2)

		when:
		testGrid.remove(3, 3)

		then:
		testGrid.get(3, 3).isEmpty()
	}

	def "should remove object in NODE"() {
		given:
		Planet planet1 = new Planet(3, 3)
		Planet planet2 = new Planet(6, 3)
		Grid testGrid = new Grid(50, 50, 5)
		testGrid.add(planet1)
		testGrid.add(planet2)

		when:
		testGrid.remove(planet2)

		then:
		testGrid.get(6, 3).isEmpty()

	}

	def "should return array with nodes in range of enterprise NODE"() {
		given:
		int x = 2
		int y = 2
		int range = 5
		Planet planet = new Planet(3, 3)
		Grid testGrid = new Grid(50, 50, 5)
		testGrid.add(planet)

		when:
		List<GameObject> list = testGrid.getInRange(x, y, range)

		then:
		list.contains(planet)
	}

	def "should return array with nodes in range of enterprise NODE upLeftCorner"() {
		given:
		int x = 2
		int y = 2
		int range = 5
		Planet planet1 = new Planet(3, 3)
		Planet planet2 = new Planet(6, 3)
		Planet planet3 = new Planet(12, 3)
		Planet planet4 = new Planet(2, 7)
		Planet planet5 = new Planet(5, 7)
		Planet planet6 = new Planet(13, 7)
		Planet planet7 = new Planet(4, 13)
		Planet planet8 = new Planet(8, 13)
		Planet planet9 = new Planet(11, 13)
		Planet planet10 = new Planet(9, 9)
		Planet planet11 = new Planet(8, 8)
		Planet planet12 = new Planet(2, 8)
		Planet planet13 = new Planet(8, 2)

		Grid testGrid = new Grid(50, 50, 5)
		testGrid.add(planet1)
		testGrid.add(planet2)
		testGrid.add(planet3)
		testGrid.add(planet4)
		testGrid.add(planet5)
		testGrid.add(planet6)
		testGrid.add(planet7)
		testGrid.add(planet8)
		testGrid.add(planet9)
		testGrid.add(planet10)
		testGrid.add(planet11)
		testGrid.add(planet12)
		testGrid.add(planet13)


		when:
		List<GameObject> list = testGrid.getInRange(x, y, range)

		then:
		list.containsAll(List.of(planet1, planet2, planet4, planet5))
		!list.contains(planet3)
		!list.contains(planet6)
		!list.contains(planet7)
		!list.contains(planet8)
		!list.contains(planet9)
		!list.contains(planet10)
		!list.contains(planet11)
		!list.contains(planet12)
		!list.contains(planet13)
	}

	def "should return array with nodes in range of enterprise NODE upRightCorner"() {
		given:
		int x = 48
		int y = 2
		int range = 5
		Planet planet1 = new Planet(38, 3)
		Planet planet2 = new Planet(43, 3)
		Planet planet3 = new Planet(47, 3)
		Planet planet4 = new Planet(39, 7)
		Planet planet5 = new Planet(44, 7)
		Planet planet6 = new Planet(47, 7)
		Planet planet7 = new Planet(37, 13)
		Planet planet8 = new Planet(42, 13)
		Planet planet9 = new Planet(49, 13)
		Planet planet10 = new Planet(42, 2)
		Planet planet11 = new Planet(8, 48)

		Grid testGrid = new Grid(50, 50, 5)
		testGrid.add(planet1)
		testGrid.add(planet2)
		testGrid.add(planet3)
		testGrid.add(planet4)
		testGrid.add(planet5)
		testGrid.add(planet6)
		testGrid.add(planet7)
		testGrid.add(planet8)
		testGrid.add(planet9)
		testGrid.add(planet10)
		testGrid.add(planet11)

		when:
		List<GameObject> list = testGrid.getInRange(x, y, range)

		then:
		list.containsAll(List.of(planet2, planet3, planet5, planet6))
		!list.contains(planet1)
		!list.contains(planet4)
		!list.contains(planet7)
		!list.contains(planet8)
		!list.contains(planet9)
		!list.contains(planet10)
		!list.contains(planet11)
	}

	def "should return array with nodes in range of enterprise NODE bottomRightCorner"() {
		given:
		int x = 48
		int y = 48
		int range = 5
		Planet planet1 = new Planet(44, 44)
		Planet planet2 = new Planet(38, 44)
		Planet planet3 = new Planet(44, 38)
		Planet planet4 = new Planet(38, 38)
		Planet planet5 = new Planet(43, 43)
		Planet planet6 = new Planet(42, 42)
		Planet planet7 = new Planet(43, 38)
		Planet planet8 = new Planet(38, 43)
		Planet planet9 = new Planet(38, 42)
		Planet planet10 = new Planet(49, 49)
		Planet planet11 = new Planet(43, 48)
		Planet planet12 = new Planet(48, 43)
		Grid testGrid = new Grid(50, 50, 5)
		testGrid.add(planet1)
		testGrid.add(planet2)
		testGrid.add(planet3)
		testGrid.add(planet4)
		testGrid.add(planet5)
		testGrid.add(planet6)
		testGrid.add(planet7)
		testGrid.add(planet8)
		testGrid.add(planet9)
		testGrid.add(planet10)
		testGrid.add(planet11)
		testGrid.add(planet12)

		when:
		List<GameObject> list = testGrid.getInRange(x, y, range)

		then:
		list.containsAll(List.of(planet1, planet5, planet10, planet11, planet12))
	}


	def "should return array with nodes in range of enterprise NODE downLeftCorner"() {
		given:
		int x = 0
		int y = 19
		int range = 5
		Planet planet1 = new Planet(1, 7)
		Planet planet2 = new Planet(7, 7)
		Planet planet3 = new Planet(12, 7)
		Planet planet4 = new Planet(1, 12)
		Planet planet5 = new Planet(7, 12)
		Planet planet6 = new Planet(12, 12)
		Planet planet7 = new Planet(1, 17)
		Planet planet8 = new Planet(4, 17)
		Planet planet9 = new Planet(12, 17)
		Grid testGrid = new Grid(20, 20, 5)
		testGrid.add(planet1)
		testGrid.add(planet2)
		testGrid.add(planet3)
		testGrid.add(planet4)
		testGrid.add(planet5)
		testGrid.add(planet6)
		testGrid.add(planet7)
		testGrid.add(planet8)
		testGrid.add(planet9)

		when:
		List<GameObject> list = testGrid.getInRange(x, y, range)

		then:
		list.containsAll(List.of(planet7, planet8))
		!list.contains(planet1)
		!list.contains(planet2)
		!list.contains(planet3)
		!list.contains(planet4)
		!list.contains(planet5)
		!list.contains(planet6)
		!list.contains(planet9)
	}

	def "should return nodes array in range of specified line"() {
		given:
		int startX = 14
		int startY = 14
		int endX = 20
		int endY = 20
		int range = 5
		Planet planet1 = new Planet(15, 14)
		Planet planet2 = new Planet(16, 16)
		Planet planet3 = new Planet(18, 18)
		Planet planet4 = new Planet(11, 11)
		Planet planet5 = new Planet(23, 23)
		Planet planet6 = new Planet(25, 25)
		Planet planet7 = new Planet(20, 25)
		Planet planet8 = new Planet(8, 8)
		Planet planet9 = new Planet(3, 3)
		Planet planet10 = new Planet(31, 43)
		Grid testGrid = new Grid(50, 50, 5)
		testGrid.add(planet1)
		testGrid.add(planet2)
		testGrid.add(planet3)
		testGrid.add(planet4)
		testGrid.add(planet5)
		testGrid.add(planet6)
		testGrid.add(planet7)
		testGrid.add(planet8)
		testGrid.add(planet9)
		testGrid.add(planet10)

		when:
		List<GameObject> list = testGrid.getInLine(startX, startY, endX, endY, range)

		then:
		list.containsAll(List.of(planet1, planet2, planet3, planet4, planet5, planet6, planet7))
		!list.contains(planet8)
		!list.contains(planet9)
		!list.contains(planet10)
	}

	def "should return nodes array in range of specified line perpendicularly"() {
		given:
		int startX = 10
		int startY = 10
		int endX = 10
		int endY = 40
		int range = 5
		Planet planet1 = new Planet(10, 12)
		Planet planet2 = new Planet(12, 13)
		Planet planet3 = new Planet(12, 14)
		Planet planet4 = new Planet(13, 15)
		Planet planet5 = new Planet(12, 20)
		Planet planet6 = new Planet(13, 11)
		Planet planet7 = new Planet(10, 21)
		Planet planet8 = new Planet(10, 30)
		Planet planet9 = new Planet(10, 19)
		Planet planet10 = new Planet(2, 39)
		Grid testGrid = new Grid(50, 50, 5)
		testGrid.add(planet1)
		testGrid.add(planet2)
		testGrid.add(planet3)
		testGrid.add(planet4)
		testGrid.add(planet5)
		testGrid.add(planet6)
		testGrid.add(planet7)
		testGrid.add(planet8)
		testGrid.add(planet9)
		testGrid.add(planet10)

		when:
		List<GameObject> list = testGrid.getInLine(startX, startY, endX, endY, range)

		then:
		list.containsAll(List.of(
				planet1,
				planet2,
				planet3,
				planet4,
				planet5,
				planet6,
				planet7,
				planet8,
				planet9
		))
	}

	def "should return nodes array in range of specified line horizontally"() {
		given:
		int startX = 10
		int startY = 10
		int endX = 40
		int endY = 10
		int range = 5
		Planet planet1 = new Planet(10, 12)
		Planet planet2 = new Planet(12, 13)
		Planet planet3 = new Planet(15, 9)
		Planet planet4 = new Planet(13, 8)
		Planet planet5 = new Planet(21, 12)
		Planet planet6 = new Planet(24, 10)
		Planet planet7 = new Planet(29, 11)
		Planet planet8 = new Planet(32, 8)
		Planet planet9 = new Planet(41, 7)
		Planet planet10 = new Planet(2, 39)
		Grid testGrid = new Grid(50, 50, 5)
		testGrid.add(planet1)
		testGrid.add(planet2)
		testGrid.add(planet3)
		testGrid.add(planet4)
		testGrid.add(planet5)
		testGrid.add(planet6)
		testGrid.add(planet7)
		testGrid.add(planet8)
		testGrid.add(planet9)
		testGrid.add(planet10)

		when:
		List<GameObject> list = testGrid.getInLine(startX, startY, endX, endY, range)

		then:

		list.containsAll(List.of(
				planet1,
				planet2,
				planet3,
				planet4,
				planet5,
				planet6,
				planet7,
				planet8,
				planet9
		))
	}

	def "should return nodes array in range of specified line horizontally near bottom border"() {
		given:
		int startX = 10
		int startY = 49
		int endX = 40
		int endY = 49
		int range = 5
		Planet planet1 = new Planet(10, 48)
		Planet planet2 = new Planet(12, 47)
		Planet planet3 = new Planet(15, 49)
		Planet planet4 = new Planet(13, 48)
		Planet planet5 = new Planet(21, 47)
		Planet planet6 = new Planet(24, 49)
		Planet planet7 = new Planet(29, 48)
		Planet planet8 = new Planet(32, 48)
		Planet planet9 = new Planet(41, 47)
		Planet planet10 = new Planet(2, 49)
		Grid testGrid = new Grid(50, 50, 5)
		testGrid.add(planet1)
		testGrid.add(planet2)
		testGrid.add(planet3)
		testGrid.add(planet4)
		testGrid.add(planet5)
		testGrid.add(planet6)
		testGrid.add(planet7)
		testGrid.add(planet8)
		testGrid.add(planet9)
		testGrid.add(planet10)

		when:
		List<GameObject> list = testGrid.getInLine(startX, startY, endX, endY, range)

		then:
		list.containsAll(List.of(
				planet1,
				planet2,
				planet3,
				planet4,
				planet5,
				planet6,
				planet7,
				planet8,
				planet9
		))
	}

	def "should return nodes array in range of specified line horizontally near up border"() {
		given:
		int startX = 10
		int startY = 0
		int endX = 40
		int endY = 0
		int range = 5
		Planet planet1 = new Planet(10, 1)
		Planet planet2 = new Planet(12, 2)
		Planet planet3 = new Planet(15, 3)
		Planet planet4 = new Planet(13, 4)
		Planet planet5 = new Planet(21, 0)
		Planet planet6 = new Planet(24, 1)
		Planet planet7 = new Planet(29, 2)
		Planet planet8 = new Planet(32, 3)
		Planet planet9 = new Planet(41, 3)
		Planet planet10 = new Planet(2, 39)
		Grid testGrid = new Grid(50, 50, 5)
		testGrid.add(planet1)
		testGrid.add(planet2)
		testGrid.add(planet3)
		testGrid.add(planet4)
		testGrid.add(planet5)
		testGrid.add(planet6)
		testGrid.add(planet7)
		testGrid.add(planet8)
		testGrid.add(planet9)
		testGrid.add(planet10)

		when:
		List<GameObject> list = testGrid.getInLine(startX, startY, endX, endY, range)

		then:
		list.containsAll(List.of(
				planet1,
				planet2,
				planet3,
				planet4,
				planet5,
				planet6,
				planet7,
				planet8,
				planet9
		))
	}

	def "should return nodes array in range of specified line horizontally from corner to corner"() {
		given:
		int startX = 0
		int startY = 0
		int endX = 49
		int endY = 49
		int range = 5
		Planet planet1 = new Planet(2, 1)
		Planet planet2 = new Planet(5, 7)
		Planet planet3 = new Planet(7, 6)
		Planet planet4 = new Planet(14, 14)
		Planet planet5 = new Planet(21, 23)
		Planet planet6 = new Planet(25, 23)
		Planet planet7 = new Planet(29, 31)
		Planet planet8 = new Planet(32, 34)
		Planet planet9 = new Planet(41, 49)

		Planet planet10 = new Planet(25, 36)
		Planet planet11 = new Planet(49, 2)

		Grid testGrid = new Grid(50, 50, 5)
		testGrid.add(planet1)
		testGrid.add(planet2)
		testGrid.add(planet3)
		testGrid.add(planet4)
		testGrid.add(planet5)
		testGrid.add(planet6)
		testGrid.add(planet7)
		testGrid.add(planet8)
		testGrid.add(planet9)
		testGrid.add(planet10)
		testGrid.add(planet11)
		when:
		List<GameObject> list = testGrid.getInLine(startX, startY, endX, endY, range)

		then:
		list.containsAll(List.of(
				planet1,
				planet2,
				planet3,
				planet4,
				planet5,
				planet6,
				planet7,
				planet8,
				planet9
		))
		!list.contains(planet10)
		!list.contains(planet11)
	}

	def "should return path list near the border"() {
		given:
		int startX = 0
		int startY = 0
		int endX = 5
		int endY = 5
		int range = 5
		//in range
		Planet planet1 = new Planet(5, 0)
		Planet planet2 = new Planet(0, 5)
		Planet planet3 = new Planet(1, 6)
		Planet planet4 = new Planet(6, 1)
		Planet planet5 = new Planet(2, 7)
		Planet planet6 = new Planet(7, 2)
		Planet planet7 = new Planet(8, 3)
		Planet planet8 = new Planet(3, 8)
		Planet planet9 = new Planet(4, 9)
		Planet planet10 = new Planet(9, 4)
		Planet planet11 = new Planet(7, 9)
		Planet planet12 = new Planet(9, 7)
		//out of range
		Planet planet13 = new Planet(11, 5)
		Planet planet14 = new Planet(5, 11)
		Planet planet15 = new Planet(0, 11)
		Planet planet16 = new Planet(11, 0)

		Grid testGrid = new Grid(15, 15, 5)
		testGrid.add(planet1)
		testGrid.add(planet2)
		testGrid.add(planet3)
		testGrid.add(planet4)
		testGrid.add(planet5)
		testGrid.add(planet6)
		testGrid.add(planet7)
		testGrid.add(planet8)
		testGrid.add(planet9)
		testGrid.add(planet10)
		testGrid.add(planet11)
		testGrid.add(planet12)
		testGrid.add(planet13)
		testGrid.add(planet14)
		testGrid.add(planet15)
		testGrid.add(planet16)

		when:
		List<GameObject> list = testGrid.getInLine(startX, startY, endX, endY, range)

		then:
		list.containsAll(List.of(
				planet1,
				planet2,
				planet3,
				planet4,
				planet5,
				planet6,
				planet7,
				planet8,
				planet9,
				planet10,
				planet11,
				planet12
		))
		!list.contains(planet13)
		!list.contains(planet14)
		!list.contains(planet15)
		!list.contains(planet16)
	}

	def "should return path list one step in one direction"() {
		given:
		int startX = 1
		int startY = 0
		int endX = 2
		int endY = 9
		int range = 5
		Planet planet1 = new Planet(6, 0)
		Planet planet2 = new Planet(6, 5)
		Planet planet3 = new Planet(6, 6)
		Planet planet4 = new Planet(6, 7)
		Planet planet5 = new Planet(7, 9)
		Planet planet6 = new Planet(7, 14)

		Planet planet7 = new Planet(7, 15)
		Planet planet8 = new Planet(8, 6)
		Planet planet9 = new Planet(8 ,7)

		Grid testGrid = new Grid(50, 50, 5)
		testGrid.add(planet1)
		testGrid.add(planet2)
		testGrid.add(planet3)
		testGrid.add(planet4)
		testGrid.add(planet5)
		testGrid.add(planet6)
		testGrid.add(planet7)
		testGrid.add(planet8)
		testGrid.add(planet9)


		when:
		List<GameObject> list = testGrid.getInLine(startX, startY, endX, endY, range)

		then:
		list.containsAll(List.of(planet1,planet2,planet3,planet4,planet5,planet6))
		!list.contains(planet7)
		!list.contains(planet8)
		!list.contains(planet9)
	}

	def "should return path list with range one"() {
		given:
		int startX = 1
		int startY = 0
		int endX = 2
		int endY = 9
		int range = 1
		Planet planet1 = new Planet(0, 0)
		Planet planet2 = new Planet(2, 0)
		Planet planet3 = new Planet(2, 1)
		Planet planet4 = new Planet(1, 8)
		Planet planet5 = new Planet(3, 9)
		Planet planet6 = new Planet(1, 10)

		Planet planet7 = new Planet(0, 10)
		Planet planet8 = new Planet(3, 7)
		Planet planet9 = new Planet(4 ,9)

		Grid testGrid = new Grid(50, 50, 5)
		testGrid.add(planet1)
		testGrid.add(planet2)
		testGrid.add(planet3)
		testGrid.add(planet4)
		testGrid.add(planet5)
		testGrid.add(planet6)
		testGrid.add(planet7)
		testGrid.add(planet8)
		testGrid.add(planet9)


		when:
		List<GameObject> list = testGrid.getInLine(startX, startY, endX, endY, range)

		then:
		list.containsAll(List.of(planet1,planet2,planet3,planet4,planet5,planet6))
		!list.contains(planet7)
		!list.contains(planet8)
		!list.contains(planet9)
	}

	def "should get most accurate path"() {
		when:
		List<Position> path = Grid.getMostAccuratePath(8, 4, 14, 9)

		then:
		path.size() == 7
		path.containsAll(List.of(
				new Position(8, 4),
				new Position(9, 4),
				new Position(10, 5),
				new Position(11, 6),
				new Position(12, 7),
				new Position(13, 8),
				new Position(14, 9)
		))
	}

	def "should get the most accurate path for diagonal direction"() {
		when:
		List<Position> path = Grid.getMostAccuratePath(0, 0, 10, 10)

		then:
		path.size() == 11
		path.containsAll(List.of(
				new Position(0, 0),
				new Position(1, 1),
				new Position(2, 2),
				new Position(3, 3),
				new Position(4, 4),
				new Position(5, 5),
				new Position(6, 6),
				new Position(7, 7),
				new Position(8, 8),
				new Position(9, 9),
				new Position(10, 10)
		))
	}

	def "should get most accurate path with common divisor"() {
		when:
		List<Position> path = Grid.getMostAccuratePath(2, 2, 8, 6)

		then:
		path.size() == 7
		path.containsAll(List.of(
				new Position(2, 2),
				new Position(3, 2),
				new Position(4, 3),
				new Position(5, 4),
				new Position(6, 4),
				new Position(7, 5),
				new Position(8, 6)
		))
	}

	def "should get most accurate path with x negative direction"() {
		when:
		List<Position> path = Grid.getMostAccuratePath(8, 2, 2, 6)

		then:
		path.size() == 7
		path.containsAll(List.of(
				new Position(8, 2),
				new Position(7, 2),
				new Position(6, 3),
				new Position(5, 4),
				new Position(4, 4),
				new Position(3, 5),
				new Position(2, 6)
		))
	}

	def "should get most accurate path with y negative direction"() {
		when:
		List<Position> path = Grid.getMostAccuratePath(2, 6, 8, 2)

		then:
		path.size() == 7
		path.containsAll(List.of(
				new Position(2, 6),
				new Position(3, 5),
				new Position(4, 4),
				new Position(5, 4),
				new Position(6, 3),
				new Position(7, 2),
				new Position(8, 2)
		))
	}

	def "should get most accurate path with both negative directions"() {
		when:
		List<Position> path = Grid.getMostAccuratePath(8, 6, 2, 2)

		then:
		path.size() == 7
		path.containsAll(List.of(
				new Position(8, 6),
				new Position(7, 5),
				new Position(6, 4),
				new Position(5, 4),
				new Position(4, 3),
				new Position(3, 2),
				new Position(2, 2)
		))
	}


	def "should return true if point is in range"() {
		given:
		int startX = 11
		int startY = 11
		int endX = 15
		int endY = 15
		int x0 = 18
		int y0 = 12
		int range = 5
		Grid testGrid = new Grid(20, 20, 5)

		when:
		boolean resultBool = testGrid.checkIfPointInRange(startX, startY, endX, endY, x0, y0, range)

		then:
		resultBool
	}

	def "should get most accurate path for horizontal lane"() {
		when:
		List<Position> path = Grid.getMostAccuratePath(8, 6, 12, 6)

		then:
		path.size() == 5
		path.containsAll(List.of(
				new Position(8, 6),
				new Position(9, 6),
				new Position(10, 6),
				new Position(11, 6),
				new Position(12, 6)
		))
	}

	def "should get most accurate path for vertical lane"() {
		when:
		List<Position> path = Grid.getMostAccuratePath(8, 6, 8, 10)

		then:
		path.size() == 5
		path.containsAll(List.of(
				new Position(8, 6),
				new Position(8, 7),
				new Position(8, 8),
				new Position(8, 9),
				new Position(8, 10)
		))
	}

	def "should return shortest path when surrounded by units safe to move through"(){
		given:
		Grid grid = new Grid(20, 20, 10)
		AbstractUnitFactory unitFactory = new UnitFactory(GameDifficulty.GOOD)
		Unit unit1 = unitFactory.createEnterprise(UUID.randomUUID()).create(0, 0)
		Unit unit2 = unitFactory.createEnterprise(UUID.randomUUID()).create(1, 0)
		Unit unit3 = unitFactory.createEnterprise(UUID.randomUUID()).create(2, 0)
		Unit unit4 = unitFactory.createEnterprise(UUID.randomUUID()).create(0, 1)
		Unit unit5 = unitFactory.createEnterprise(UUID.randomUUID()).create(2, 1)
		Unit unit6 = unitFactory.createEnterprise(UUID.randomUUID()).create(0, 2)
		Unit unit7 = unitFactory.createEnterprise(UUID.randomUUID()).create(1, 2)
		Unit unit8 = unitFactory.createEnterprise(UUID.randomUUID()).create(2, 2)
		grid.add(unit1)
		grid.add(unit2)
		grid.add(unit3)
		grid.add(unit4)
		grid.add(unit5)
		grid.add(unit6)
		grid.add(unit7)
		grid.add(unit8)
		List<Unit> unitsSafeToMoveThrough = List.of(
				unit1,
				unit2,
				unit3,
				unit4,
				unit5,
				unit6,
				unit7,
				unit8,
		)

		when:
		List<Position> shortestPath = grid.findShortestPath(new Position(1, 1), new Position(3, 1), unitsSafeToMoveThrough)

		then:
		!shortestPath.isEmpty()
	}

	def "should not return shortest path when surrounded by units not safe to move through"(){
		given:
		Grid grid = new Grid(20, 20, 10)
		AbstractUnitFactory unitFactory = new UnitFactory(GameDifficulty.GOOD)
		Unit unit1 = unitFactory.createEnterprise(UUID.randomUUID()).create(0, 0)
		Unit unit2 = unitFactory.createEnterprise(UUID.randomUUID()).create(1, 0)
		Unit unit3 = unitFactory.createEnterprise(UUID.randomUUID()).create(2, 0)
		Unit unit4 = unitFactory.createEnterprise(UUID.randomUUID()).create(0, 1)
		Unit unit5 = unitFactory.createEnterprise(UUID.randomUUID()).create(2, 1)
		Unit unit6 = unitFactory.createEnterprise(UUID.randomUUID()).create(0, 2)
		Unit unit7 = unitFactory.createEnterprise(UUID.randomUUID()).create(1, 2)
		Unit unit8 = unitFactory.createEnterprise(UUID.randomUUID()).create(2, 2)
		grid.add(unit1)
		grid.add(unit2)
		grid.add(unit3)
		grid.add(unit4)
		grid.add(unit5)
		grid.add(unit6)
		grid.add(unit7)
		grid.add(unit8)
		List<Unit> unitsSafeToMoveThrough = List.of()

		when:
		List<Position> shortestPath = grid.findShortestPath(new Position(1, 1), new Position(3, 1), unitsSafeToMoveThrough)

		then:
		shortestPath.isEmpty()
	}

	def "should return shortest path when units not safe to move through on way"(){
		given:
		Grid grid = new Grid(20, 20, 10)
		AbstractUnitFactory unitFactory = new UnitFactory(GameDifficulty.GOOD)
		Unit unit1 = unitFactory.createEnterprise(UUID.randomUUID()).create(0, 0)
		Unit unit2 = unitFactory.createEnterprise(UUID.randomUUID()).create(1, 0)
		Unit unit3 = unitFactory.createEnterprise(UUID.randomUUID()).create(2, 0)
		Unit unit4 = unitFactory.createEnterprise(UUID.randomUUID()).create(0, 1)
		Unit unit5 = unitFactory.createEnterprise(UUID.randomUUID()).create(2, 1)
		Unit unit6 = unitFactory.createEnterprise(UUID.randomUUID()).create(0, 2)
		Unit unit7 = unitFactory.createEnterprise(UUID.randomUUID()).create(2, 2)
		grid.add(unit1)
		grid.add(unit2)
		grid.add(unit3)
		grid.add(unit4)
		grid.add(unit5)
		grid.add(unit6)
		grid.add(unit7)
		List<Unit> unitsSafeToMoveThrough = List.of()

		when:
		List<Position> shortestPath = grid.findShortestPath(new Position(1, 1), new Position(3, 1), unitsSafeToMoveThrough)

		then:
		!shortestPath.isEmpty()
	}

	def "should return empty path when target is same as source"(){
		given:
		Grid grid = new Grid(20, 20, 10)

		when:
		List<Position> shortestPath = grid.findShortestPath(new Position(1, 1), new Position(1, 1), List.of())

		then:
		shortestPath.isEmpty()
	}

	def "should return empty path when target is same as source in empty world"(){
		given:
		Grid grid = new Grid(20, 20, 10)

		when:
		List<Position> shortestPath = grid.findPathInEmptyWorld(new Position(1, 1), new Position(1, 1))

		then:
		shortestPath.isEmpty()
	}

	def "should return path when target is correct"(){
		given:
		Grid grid = new Grid(20, 20, 10)

		when:
		List<Position> shortestPath = grid.findPathInEmptyWorld(new Position(1, 1), new Position(10, 10))

		then:
		!shortestPath.isEmpty()
	}

	def "should return empty list when no game objects were added" (){
		given:
		Grid grid = new Grid(20, 20, 10)

		when:
		List<GameObject> gameObjects = grid.getGameObjectsInWorld()

		then:
		gameObjects.isEmpty()
	}

	def "should return list containing all game objects added to the grid" (){
		given:
		Grid grid = new Grid(20, 20, 10)
		grid.add(new Planet(0, 4))
		grid.add(new Planet(1, 8))
		grid.add(new Planet(0, 15))
		grid.add(new Planet(6, 19))

		when:
		List<GameObject> gameObjects = grid.getGameObjectsInWorld()

		then:
		gameObjects.size() == 4
	}

	def "should throw IndexOutOfBounds exception when any of coordinates not in range"(){
		given:
		Grid grid = new Grid(20, 20, 10)

		when:
		grid.findShortestPath(new Position(srcX, srcY), new Position(tarX, tarY), List.of())

		then:
		def ex = thrown(IndexOutOfBoundsException)
		ex.message == "One or more coordinates out of world range"

		where:
		srcX	|	srcY	|	tarX	|	tarY
		-1		|	10		|	10		|	10
		20		|	10		|	10		|	10
		10		|	-1		|	10		|	10
		10		|	20		|	10		|	10
		10		|	10		|	-1		|	10
		10		|	10		|	20		|	10
		10		|	10		|	10		|	-1
		10		|	10		|	10		|	20
		-1		|	-1		|	10		|	10
		-1		|	10		|	-1		|	10
		-1		|	10		|	10		|	-1
		-1		|	20		|	10		|	10
		-1		|	10		|	20		|	10
		-1		|	10		|	10		|	20
		20		|	-1		|	10		|	10
		20		|	10		|	-1		|	10
		20		|	10		|	10		|	-1
		20		|	20		|	10		|	10
		20		|	10		|	20		|	10
		20		|	10		|	10		|	20
		-1		|	-1		|	-1		|	10
		-1		|	-1		|	20		|	10
		-1		|	-1		|	10		|	-1
		-1		|	-1		|	10		|	20
		-1		|	20		|	-1		|	10
		-1		|	20		|	20		|	10
		-1		|	20		|	10		|	-1
		-1		|	20		|	10		|	20
		20		|	20		|	-1		|	10
		20		|	20		|	20		|	10
		20		|	20		|	10		|	-1
		20		|	20		|	10		|	20
		20		|	-1		|	-1		|	10
		20		|	-1		|	20		|	10
		20		|	-1		|	10		|	-1
		20		|	-1		|	10		|	20
		-1		|	-1		|	-1		|	-1
		-1		|	-1		|	-1		|	20
		-1		|	-1		|	20		|	-1
		-1		|	-1		|	20		|	20
		20		|	-1		|	-1		|	-1
		20		|	-1		|	-1		|	20
		20		|	-1		|	20		|	-1
		20		|	-1		|	20		|	20
		-1		|	20		|	-1		|	-1
		-1		|	20		|	-1		|	20
		-1		|	20		|	20		|	-1
		-1		|	20		|	20		|	20
		20		|	20		|	-1		|	-1
		20		|	20		|	-1		|	20
		20		|	20		|	20		|	-1
		20		|	20		|	20		|	20
	}

	def "should throw IndexOutOfBounds exception when any of coordinates not in range in empty world"(){
		given:
		Grid grid = new Grid(20, 20, 10)

		when:
		grid.findPathInEmptyWorld(new Position(srcX, srcY), new Position(tarX, tarY))

		then:
		def ex = thrown(IndexOutOfBoundsException)
		ex.message == "One or more coordinates out of world range"

		where:
		srcX	|	srcY	|	tarX	|	tarY
		-1		|	10		|	10		|	10
		20		|	10		|	10		|	10
		10		|	-1		|	10		|	10
		10		|	20		|	10		|	10
		10		|	10		|	-1		|	10
		10		|	10		|	20		|	10
		10		|	10		|	10		|	-1
		10		|	10		|	10		|	20
		-1		|	-1		|	10		|	10
		-1		|	10		|	-1		|	10
		-1		|	10		|	10		|	-1
		-1		|	20		|	10		|	10
		-1		|	10		|	20		|	10
		-1		|	10		|	10		|	20
		20		|	-1		|	10		|	10
		20		|	10		|	-1		|	10
		20		|	10		|	10		|	-1
		20		|	20		|	10		|	10
		20		|	10		|	20		|	10
		20		|	10		|	10		|	20
		-1		|	-1		|	-1		|	10
		-1		|	-1		|	20		|	10
		-1		|	-1		|	10		|	-1
		-1		|	-1		|	10		|	20
		-1		|	20		|	-1		|	10
		-1		|	20		|	20		|	10
		-1		|	20		|	10		|	-1
		-1		|	20		|	10		|	20
		20		|	20		|	-1		|	10
		20		|	20		|	20		|	10
		20		|	20		|	10		|	-1
		20		|	20		|	10		|	20
		20		|	-1		|	-1		|	10
		20		|	-1		|	20		|	10
		20		|	-1		|	10		|	-1
		20		|	-1		|	10		|	20
		-1		|	-1		|	-1		|	-1
		-1		|	-1		|	-1		|	20
		-1		|	-1		|	20		|	-1
		-1		|	-1		|	20		|	20
		20		|	-1		|	-1		|	-1
		20		|	-1		|	-1		|	20
		20		|	-1		|	20		|	-1
		20		|	-1		|	20		|	20
		-1		|	20		|	-1		|	-1
		-1		|	20		|	-1		|	20
		-1		|	20		|	20		|	-1
		-1		|	20		|	20		|	20
		20		|	20		|	-1		|	-1
		20		|	20		|	-1		|	20
		20		|	20		|	20		|	-1
		20		|	20		|	20		|	20
	}
}
