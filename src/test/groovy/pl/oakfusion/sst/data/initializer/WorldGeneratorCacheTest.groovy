package pl.oakfusion.sst.data.initializer

import pl.oakfusion.sst.data.world.gameobject.GameObject
import pl.oakfusion.sst.data.util.Position
import pl.oakfusion.sst.data.world.gameobject.Star
import pl.oakfusion.sst.data.world.StarDate
import pl.oakfusion.sst.data.world.World
import spock.lang.Specification

class WorldGeneratorCacheTest extends Specification{

	def world
	def worldGeneratorCache

	def setup() {
		world = Mock(World)
		worldGeneratorCache = new WorldGeneratorCache(world)
	}

	def "should return absolute height"() {
		given:
		world.getAbsoluteHeight() >> 30

		when:
		def absoluteWorldHeight = worldGeneratorCache.getAbsoluteWorldHeight()

		then:
		absoluteWorldHeight == 30
	}

	def "should return absolute width"() {
		given:
		world.getAbsoluteWidth() >> 25

		when:
		int absoluteWorldWidth = worldGeneratorCache.getAbsoluteWorldWidth()

		then:
		absoluteWorldWidth == 25
	}

	def "if x is less than 0 isSpaceAvailable method should return false"() {
		given:
		world.getAbsoluteWidth() >> 20
		world.getAbsoluteHeight() >> 20

		when:
		def isSpaceAvailable = worldGeneratorCache.isSpaceAvailable(x,y,range)

		then:
		!isSpaceAvailable

		where:
		x		|y		|range
		-2		|4		|1
		-2		|8		|1
		-3		|12		|2
		-1		|2		|1
	}

	def "if x is less than 0 or y is less than 0 isSpaceAvailable method should return false"() {
		given:
		world.getAbsoluteWidth() >> 20
		world.getAbsoluteHeight() >> 20

		when:
		def isSpaceAvailable = worldGeneratorCache.isSpaceAvailable(x,y,range)

		then:
		!isSpaceAvailable

		where: "(y<0 || x<0)"
		x		|y		|range
		15		|-2		|1
		4		|-3		|1
		-2		|-1		|2
		-4		|-10	|1
		-10		|2		|1
		-6		|6		|1
	}

	def "if x is greater than or equal to absolute world width or y is greater than or equal to absolute world height isSpaceAvailable method should return false"() {
		given:
		def x = 5
		def y = 10
		worldGeneratorCache.getAbsoluteWorldHeight() >> absoluteWH
		worldGeneratorCache.getAbsoluteWorldWidth() >> absoluteWW

		when:
		def isSpaceAvailable = worldGeneratorCache.isSpaceAvailable(x, y, 0)

		then:
		!isSpaceAvailable

		where:
		absoluteWH 	|absoluteWW
		1 			|11
		4			|4
		6 			|9
	}

	def "if objectsCache contains object in sector isSpaceAvailable should return false"() {
		given:
		world.getAbsoluteWidth() >> 20
		world.getAbsoluteHeight() >> 20
		world.sectorIndex(5,5) >> 105
		worldGeneratorCache.getObjectsCache().put(105,new Star(5,5))

		when:
		def isSpaceAvailable = worldGeneratorCache.isSpaceAvailable(5,5,0)

		then:
		!isSpaceAvailable
	}

	def "if x is not greater than or equal to absolute world width and y is not greater than or equal to absolute world height and x is not less than 0 and y is not less than 0 and objectsCache doesn't contain object in sector, isSpaceAvailable should return true" () {
		given:
		world.getAbsoluteWidth() >> 20
		world.getAbsoluteHeight() >> 20
		world.sectorIndex(0,0) >> 105

		when:
		def isSpaceAvailable = worldGeneratorCache.isSpaceAvailable(5,5,0)

		then:
		isSpaceAvailable
	}

	def "fillRange should fill range fields by ForceField"() {
		given:
		world.sectorIndex(_,_) >>> [-6,-5,-4,-1,0,1,4,5,6]
		world.getAbsoluteWidth() >> 5
		world.getAbsoluteHeight() >> 5
		def objectCache = worldGeneratorCache.getObjectsCache()

		when:
		worldGeneratorCache.fillRange(0,0,1)

		then:
		assert objectCache.values()
				.stream()
				.allMatch(fillRange -> fillRange instanceof WorldGeneratorCache.ForceField)
		objectCache.size() == 4
		objectCache.get(0).getX() == 0
		objectCache.get(0).getY() == 0
		objectCache.get(1).getX() == 1
		objectCache.get(1).getY() == 0
		objectCache.get(5).getX() == 0
		objectCache.get(5).getY() == 1
		objectCache.get(6).getX() == 1
		objectCache.get(6).getY() == 1
	}

	def "getEmptySurroundingInRange should return positions where object can be placed"() {
		given:
		def star = Mock(StarDate)
		World world = new World(1,1,5,star)
		WorldGeneratorCache worldGeneratorCache = new WorldGeneratorCache(world)
		worldGeneratorCache.getObjectsCache().put(4,new GameObject(4,0))

		when:
		List<Position> positions = worldGeneratorCache.getEmptySurroundingInRange(0,0,1 ,1)

		then:
		positions.size() == 4
		positions.stream().allMatch(position -> position instanceof Position)
		positions.get(0).getX() == 0
		positions.get(0).getY() == 0
		positions.get(1).getX() == 0
		positions.get(1).getY() == 1
		positions.get(2).getX() == 1
		positions.get(2).getY() == 0
		positions.get(3).getX() == 1
		positions.get(3).getY() == 1
	}

	def "if forceFieldRange of object = 0 should fill 1 position by object"() {
		given:
		def gameObject = Mock(GameObject)
		world.sectorIndex(2, 3) >> 17
		def objectsCaches = worldGeneratorCache.getObjectsCache()

		when:
		worldGeneratorCache.put(2, 3, 0, gameObject)

		then:
		objectsCaches.size() == 1
		objectsCaches.get(17) instanceof GameObject

	}
	def "if forceFieldRange of object = 1 should fill 1 position by object and 1 range by forceFields"() {
		given:
		worldGeneratorCache = new WorldGeneratorCache(world)
		def gameObject = Mock(GameObject)
		world.sectorIndex(_, _) >>> [17, 11, 12, 13, 16, 17, 18, 21, 22, 23]
		def objectsCaches = worldGeneratorCache.getObjectsCache()
		world.getAbsoluteWidth() >> 5
		world.getAbsoluteHeight() >> 5

		when:
		worldGeneratorCache.put(2, 3, 1, gameObject)

		then:
		objectsCaches.size() == 9
		objectsCaches.get(17) instanceof GameObject
		objectsCaches.values()
				.stream()
				.filter(x -> !x.equals(objectsCaches.get(17)))
				.allMatch(forceField -> forceField instanceof WorldGeneratorCache.ForceField)
	}
}
