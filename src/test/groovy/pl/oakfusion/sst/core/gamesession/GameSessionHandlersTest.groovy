package pl.oakfusion.sst.core.gamesession

import pl.oakfusion.bus.MessageBus
import pl.oakfusion.bus.guava.GuavaMessageBus
import pl.oakfusion.sst.core.aggregates.HelpResolver
import pl.oakfusion.sst.core.gamedata.GameSessionDataRepository
import pl.oakfusion.sst.core.gamedata.InMemoryGameSessionDataRepository
import pl.oakfusion.sst.data.util.RandomGenerator
import spock.lang.Specification

class GameSessionHandlersTest extends Specification {
	RandomGenerator randomGenerator = new RandomGenerator()
	GameSessionDataRepository gameSessionDataRepository = new InMemoryGameSessionDataRepository()
	MessageBus messageBus = new GuavaMessageBus()
	HelpResolver helpResolver = new HelpResolver()
	GameSessionHandlers gameSessionHandlers = new GameSessionHandlers(randomGenerator, messageBus, gameSessionDataRepository, helpResolver)


}
