package pl.oakfusion.sst.core.gamesession.configuration

import pl.oakfusion.sst.core.gamesession.configuration.SavedGameConfigurationBuilder
import pl.oakfusion.sst.data.world.civilizations.klingoncivilization.factories.CommanderFromConfigFactory
import pl.oakfusion.sst.data.world.civilizations.federationcivilization.factories.EnterpriseFromConfigFactory
import pl.oakfusion.sst.data.world.civilizations.klingoncivilization.factories.KlingonFromConfigFactory
import pl.oakfusion.sst.data.world.civilizations.klingoncivilization.factories.SuperCommanderFromConfigFactory
import pl.oakfusion.sst.data.world.gameobject.GameObject
import pl.oakfusion.sst.data.configuration.GameSessionDataConfiguration
import pl.oakfusion.sst.data.configuration.UnitConfiguration
import pl.oakfusion.sst.data.util.RandomGenerator
import pl.oakfusion.sst.data.world.civilizations.Unit
import pl.oakfusion.testutils.SstTestUtils
import spock.lang.Specification

import java.lang.reflect.Field

class ConfigurationFactoryTest extends Specification {
	SavedGameConfigurationBuilder configBuilder = new SavedGameConfigurationBuilder()
	SstTestUtils sstTestUtils = new SstTestUtils()
	GameSessionDataConfiguration gameDataConfiguration = configBuilder.createConfiguration(sstTestUtils.getGameData())


	def "enterprise from config should be the same as original enterprise"(){
		when:
		Unit enterprise = sstTestUtils.getGameData().getActivePlayer().getCivilization().getUnits().get(0)
		EnterpriseFromConfigFactory enterpriseFromConfigFactory = new EnterpriseFromConfigFactory()
		Unit enterpriseFromConfig = enterpriseFromConfigFactory.createUnit(gameDataConfiguration.unitConfigurations.get(0), new RandomGenerator())
		then:
		getUncommonTraits(enterpriseFromConfig, enterprise).isEmpty()
	}

	def "opponents from config should be the same as original one"(){
		when:
		List<Unit> units = (sstTestUtils.getAIPlayer().getCivilization().getUnits())
		Unit klingon = units.get(0)
		Unit commander = units.get(1)
		Unit superCommander = units.get(2)
		List<UnitConfiguration> unitsConfig = gameDataConfiguration.unitConfigurations
		RandomGenerator randomGenerator = new RandomGenerator()
		Unit configKlingon = new KlingonFromConfigFactory().createUnit(unitsConfig.get(1), randomGenerator)
		Unit configCommander = new CommanderFromConfigFactory().createUnit(unitsConfig.get(2), randomGenerator)
		Unit configSuperCommander = new SuperCommanderFromConfigFactory().createUnit(unitsConfig.get(3), randomGenerator)

		then:
		getUncommonTraits(klingon, configKlingon).isEmpty()
		getUncommonTraits(commander, configCommander).isEmpty()
		getUncommonTraits(superCommander, configSuperCommander).isEmpty()

	}

	private static <T extends GameObject> Map<String, List<Object>> getUncommonTraits(T p1, T p2) {
		Map<String, List<Object>> result = new HashMap<>()
		for(Field field : p1.getClass().getDeclaredFields()){
			try {
				if(!(field.get(p1).equals(field.get(p2)))){
					result.put(field.getName(), new ArrayList<Object>(Arrays.asList(field.get(p1), field.get(p2))))
				}
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace()
			}
		}

		return result
	}
}
