package pl.oakfusion.sst.core.testutils;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.parameter.ObjectType;
import pl.oakfusion.sst.data.game.commands.GameCommand;
import pl.oakfusion.sst.data.world.component.Condition;
import pl.oakfusion.sst.data.world.civilizations.Unit;
import pl.oakfusion.sst.data.world.civilizations.UnitType;
import pl.oakfusion.sst.data.world.civilizations.UnitSpecification;
import pl.oakfusion.sst.data.world.component.ComponentSystem;

import java.util.UUID;
@Getter
@Setter
public class MockUnit extends Unit {
	public MockUnit(final int x, final int y, final UUID playerUUID, final ComponentSystem componentSystem,
					final UnitSpecification unitSpecification) {
		super(x, y, playerUUID, componentSystem, unitSpecification, UnitRank.GENERAL);
	}
	private double getHealth;
	private double getHealthWithShields;
	private double getHealthAsFraction;
	private boolean isObjectAlive;
	private UnitType getType;
	private Condition getCondition;
	private boolean canPerformCommand;
	private int getX;
	private int getY;
	private ObjectType getObjectType;
	private UUID getPlayerUUID;
	private UUID getUuid;
	private String getUnitName;
	private double getActionPoints;
	private ComponentSystem getComponentSystem;

	@Override
	public double getHealth() {
		return getHealth;
	}

	@Override
	public double getHealthWithShields() {
		return getHealthWithShields;
	}

	@Override
	public double getHealthAsFraction() {
		return getHealthAsFraction;
	}

	@Override
	public boolean isObjectAlive() {
		return isObjectAlive;
	}

	@Override
	public UnitType getType() {
		return getType;
	}

	@Override
	public Condition getCondition() {
		return getCondition;
	}

	@Override
	public <C extends GameCommand> boolean canPerformCommand(final Class<C> command) {
		return canPerformCommand;
	}

	@Override
	public int getX() {
		return getX;
	}

	@Override
	public int getY() {
		return getY;
	}

	@Override
	public ObjectType getObjectType() {
		return getObjectType;
	}

	@Override
	public UUID getPlayerUUID() {
		return getPlayerUUID;
	}

	@Override
	public UUID getUuid() {
		return getUuid;
	}

	@Override
	public String getUnitName() {
		return getUnitName;
	}

	@Override
	public double getActionPoints() {
		return getActionPoints;
	}

	@Override
	public ComponentSystem getComponentSystem() {
		return getComponentSystem;
	}
}
