package pl.oakfusion.sst.core.game.handlers

import com.google.common.eventbus.EventBus
import pl.oakfusion.bus.MessageBus
import pl.oakfusion.bus.guava.GuavaMessageBus
import pl.oakfusion.sst.core.gamedata.InMemoryGameSessionDataRepository
import pl.oakfusion.sst.data.SstEvent
import pl.oakfusion.sst.data.game.commands.mayday.Mayday
import pl.oakfusion.sst.data.game.events.mayday.MaydayFailed
import pl.oakfusion.sst.data.game.events.mayday.MaydayPerformed
import pl.oakfusion.sst.data.game.events.mayday.RadioBroken
import pl.oakfusion.sst.data.util.RandomGenerator
import pl.oakfusion.sst.data.world.*
import pl.oakfusion.sst.data.world.component.ComponentId
import pl.oakfusion.sst.data.world.component.Computer
import pl.oakfusion.sst.data.world.component.Condition
import pl.oakfusion.sst.data.world.gameobject.Starbase
import pl.oakfusion.sst.test.TestContext
import pl.oakfusion.testutils.SstTestUtils
import spock.lang.Specification
import spock.lang.Unroll

class MaydayHandlersTest extends Specification {
	SstTestUtils sstTestUtils = new SstTestUtils()
	RandomGenerator randomGenerator = Stub()
	RandomGenerator randomGeneratorFailingHandler = Stub()
	MessageBus messageBus = sstTestUtils.getMessageBus()
	Computer computer = Stub()
	InMemoryGameSessionDataRepository inMemoryGameSessionDataRepository = new InMemoryGameSessionDataRepository()
	RandomFailureCommandHandler randomFailureCommandHandler = new RandomFailureCommandHandler(randomGeneratorFailingHandler)
	TestContext testContext = new TestContext(messageBus)
	StarDate starDate = new StarDate(10, 2)
	World world = new World(10, 10, 10, starDate)
	UUID randomUUID
	UUID gameSessionUUID
	UUID playerUUID
	def message

	//TODO: fix set condition in unit to make test pass
	/*def 'should return AlreadyDocked event when unit is docked'() {
		given:
		sstTestUtils.getGameData().getActivePlayer().getCivilization().getActiveUnits().get(0).setCondition(Condition.DOCKED)
		randomUUID = UUID.randomUUID()
		message = new Mayday(randomUUID, randomUUID)
		inMemoryGameSessionDataRepository.save(randomUUID, sstTestUtils.getGameData())
		new MaydayHandlers(inMemoryGameSessionDataRepository, messageBus, new RandomGenerator())

		when:
		testContext.postingMessage(message)

		then:
		testContext.received(AlreadyDocked.class, m -> { })
	}*/

	def 'should return RadioBroken event when radio is broken'() {
		given:

		sstTestUtils.getGameData().getActivePlayer()
				.getCivilization().getActiveUnits().get(0).setCondition(Condition.GREEN)
		sstTestUtils.getGameData().getActivePlayer()
				.getCivilization().getActiveUnits().get(0).getComponentSystem()
				.getComponent(ComponentId.SUBSPACE_RADIO).setCurrentHealth(0)
		randomUUID = UUID.randomUUID()
		message = new Mayday(randomUUID, randomUUID)
		inMemoryGameSessionDataRepository.save(randomUUID, sstTestUtils.getGameData())
		randomGeneratorFailingHandler.nextDoubleInRange(0, 1) >> 1
		new MaydayHandlers(inMemoryGameSessionDataRepository, messageBus, randomFailureCommandHandler, new RandomGenerator())

		when:
		testContext.postingMessage(message)

		then:
		testContext.received(RadioBroken.class, m -> { })
	}

	def 'should return getMaydayFailed event when StarBase is null'() {
		given:
		sstTestUtils.getGameData().setWorld(world)
		sstTestUtils.getGameData().getActivePlayer()
				.getCivilization().getActiveUnits().get(0).getComponentSystem()
				.getComponent(ComponentId.SUBSPACE_RADIO).setCurrentHealth(100)
		randomUUID = UUID.randomUUID()
		inMemoryGameSessionDataRepository.save(randomUUID, sstTestUtils.getGameData())
		message = new Mayday(randomUUID, randomUUID)
		randomGeneratorFailingHandler.nextDoubleInRange(0, 1) >> 1
		new MaydayHandlers(inMemoryGameSessionDataRepository, messageBus, randomFailureCommandHandler, new RandomGenerator())

		when:
		testContext.postingMessage(message)

		then:
		testContext.received(MaydayFailed.class, m -> { })
	}

	def 'should return maydayPerformed event'() {
		given:
		computer.getClosestStarbase(world, 10, 10) >> new Starbase(10, 10)
		randomGenerator.nextDoubleInRange(0, 1) >> 0
		sstTestUtils.getGameData().getActivePlayer()
				.getCivilization().getActiveUnits().get(0).getComponentSystem().components.put(ComponentId.COMPUTER, computer)
		sstTestUtils.getGameData().getActivePlayer()
				.getCivilization().getActiveUnits().get(0).getComponentSystem()
				.getComponent(ComponentId.SUBSPACE_RADIO).setCurrentHealth(100)
		sstTestUtils.getGameData().setWorld(world)
		randomUUID = UUID.randomUUID()
		message = new Mayday(randomUUID, randomUUID)
		inMemoryGameSessionDataRepository.save(randomUUID, sstTestUtils.getGameData())
		randomGeneratorFailingHandler.nextDoubleInRange(0, 1) >> 1
		new MaydayHandlers(inMemoryGameSessionDataRepository, messageBus, randomFailureCommandHandler, randomGenerator)

		when:
		testContext.postingMessage(message)

		then:
		testContext.received(MaydayPerformed.class, m -> { })
	}

	def 'should return maydayFailed event when distance is too long'() {
		given:
		inMemoryGameSessionDataRepository = new InMemoryGameSessionDataRepository()
		computer.getClosestStarbase(world, 10, 10) >> new Starbase(60, 60)
		randomGenerator.nextDoubleInRange(0, 1) >> 91
		sstTestUtils.getGameData().getActivePlayer()
				.getCivilization().getActiveUnits().get(0).getComponentSystem().components.put(ComponentId.COMPUTER, computer)
		sstTestUtils.getGameData().getActivePlayer()
				.getCivilization().getActiveUnits().get(0).getComponentSystem()
				.getComponent(ComponentId.SUBSPACE_RADIO).setCurrentHealth(100)
		sstTestUtils.getGameData().setWorld(world)
		randomUUID = UUID.randomUUID()
		message = new Mayday(randomUUID, randomUUID)
		inMemoryGameSessionDataRepository.save(randomUUID, sstTestUtils.getGameData())
		randomGeneratorFailingHandler.nextDoubleInRange(0, 1) >> 1
		new MaydayHandlers(inMemoryGameSessionDataRepository, messageBus, randomFailureCommandHandler, randomGenerator)

		when:
		testContext.postingMessage(message)

		then:
		testContext.received(MaydayFailed.class, m -> { })
	}

	def 'should return maydayFailed event when World have a one quadrants'() {
		given:
		world = new World(1, 1, 1, starDate)
		computer.getClosestStarbase(world, 10, 10) >> new Starbase(10, 10)
		randomGenerator.nextDoubleInRange(0, 1) >> 1
		sstTestUtils.getGameData().getActivePlayer()
				.getCivilization().getActiveUnits().get(0).getComponentSystem().components.put(ComponentId.COMPUTER, computer)
		sstTestUtils.getGameData().getActivePlayer()
				.getCivilization().getActiveUnits().get(0).getComponentSystem()
				.getComponent(ComponentId.SUBSPACE_RADIO).setCurrentHealth(100)
		sstTestUtils.getGameData().setWorld(world)
		randomUUID = UUID.randomUUID()
		message = new Mayday(randomUUID, randomUUID)
		inMemoryGameSessionDataRepository.save(randomUUID, sstTestUtils.getGameData())
		randomGeneratorFailingHandler.nextDoubleInRange(0, 1) >> 1
		new MaydayHandlers(inMemoryGameSessionDataRepository, messageBus, randomFailureCommandHandler, new RandomGenerator())

		when:
		testContext.postingMessage(message)

		then:
		testContext.received(MaydayFailed.class, m -> { })
	}


	def 'should return maydayFailed event with correctly data'() {
		given:
		def playerName = "PlayerName"
		computer.getClosestStarbase(world, 10, 10) >> null
		randomGenerator.nextDoubleInRange(0, 1) >> 0
		gameSessionUUID = UUID.randomUUID()
		playerUUID = UUID.randomUUID()
		sstTestUtils.getGameData().getActivePlayer().setPlayerName(playerName)
		sstTestUtils.getGameData().getActivePlayer()
				.getCivilization().getActiveUnits().get(0).getComponentSystem().components.put(ComponentId.COMPUTER, computer)
		sstTestUtils.getGameData().getActivePlayer()
				.getCivilization().getActiveUnits().get(0).getComponentSystem()
				.getComponent(ComponentId.SUBSPACE_RADIO).setCurrentHealth(100)
		sstTestUtils.getGameData().setWorld(world)
		message = new Mayday(gameSessionUUID, playerUUID)
		inMemoryGameSessionDataRepository.save(gameSessionUUID, sstTestUtils.getGameData())
		randomGeneratorFailingHandler.nextDoubleInRange(0, 1) >> 1
		new MaydayHandlers(inMemoryGameSessionDataRepository, messageBus, randomFailureCommandHandler, new RandomGenerator())

		when:
		testContext.postingMessage(message)

		then:
		testContext.received(MaydayFailed.class, m -> {
			assert m.getDeadPlayersDTO().get(0).getDeadPlayer() == playerName
			assert m.getPlayerUuid() == playerUUID
			assert m.getGameSessionUuid() == gameSessionUUID
			assert m instanceof MaydayFailed
		})
	}

	@Unroll
	def 'should return correctly probability to different positions'() {
		given:
		world = new World(5, 5, 10, starDate)
		inMemoryGameSessionDataRepository = new InMemoryGameSessionDataRepository()
		sstTestUtils.getGameData().getActivePlayer().getCivilization().getActiveUnits().get(0).setCondition(Condition.GREEN)
		computer.getClosestStarbase(world, unitX, unitY) >> new Starbase(starBaseX, starBaseY)
		gameSessionUUID = UUID.randomUUID()
		playerUUID = UUID.randomUUID()
		sstTestUtils.getGameData().setWorld(world)
		randomGenerator.nextDoubleInRange(0, 1) >> random
		sstTestUtils.getGameData().getActivePlayer()
				.getCivilization().getActiveUnits().get(0).getComponentSystem().components.put(ComponentId.COMPUTER, computer)
		sstTestUtils.getGameData().getActivePlayer()
				.getCivilization().getActiveUnits().get(0).setX(unitX)
		sstTestUtils.getGameData().getActivePlayer()
				.getCivilization().getActiveUnits().get(0).setY(unitY)
		sstTestUtils.getGameData().setWorld(world)
		sstTestUtils.getGameData().getActivePlayer()
				.getCivilization().getActiveUnits().get(0).getComponentSystem()
				.getComponent(ComponentId.SUBSPACE_RADIO).setCurrentHealth(100)
		message = new Mayday(gameSessionUUID, playerUUID)
		inMemoryGameSessionDataRepository.save(gameSessionUUID, sstTestUtils.getGameData())
		randomGeneratorFailingHandler.nextDoubleInRange(0, 1) >> 1
		new MaydayHandlers(inMemoryGameSessionDataRepository, messageBus, randomFailureCommandHandler, randomGenerator)

		when:
		testContext.postingMessage(message)

		then:
		testContext.received(eventType as Class<SstEvent>, m -> {
			assert eventType.is(m.class)
		})

		where:
		random | unitX | unitY | starBaseX | starBaseY | eventType
		0.51   | 32    | 32    | 21        | 48        | MaydayPerformed
		0.51   | 0     | 0     | 30        | 0         | MaydayPerformed
		0.51   | 0     | 0     | 33        | 0         | MaydayFailed
	}

	@Unroll
	def 'should throw IllegalArgumentException when constructor param will be null'() {
		when:
		new MaydayHandlers(repoParam, messageBusParam, randomFailureHandlerParam, randomGeneratorParam)

		then:
		thrown(IllegalArgumentException)

		where:
		repoParam                               | messageBusParam                     | randomGeneratorParam  | randomFailureHandlerParam
		null                                    | null                                | null				  | null
		new InMemoryGameSessionDataRepository() | null                                | null				  | null
		null                                    | new GuavaMessageBus(new EventBus()) | null				  | null
		null                                    | null                                | new RandomGenerator() | null
		null                                    | null                                | null				  | new RandomFailureCommandHandler(new RandomGenerator())
		new InMemoryGameSessionDataRepository() | new GuavaMessageBus(new EventBus()) | null				  | null
		new InMemoryGameSessionDataRepository() | null                                | new RandomGenerator() | null
		new InMemoryGameSessionDataRepository() | null                                | null				  | new RandomFailureCommandHandler(new RandomGenerator())
		null                                    | new GuavaMessageBus(new EventBus()) | new RandomGenerator() | null
		null                                    | new GuavaMessageBus(new EventBus()) | null				  | new RandomFailureCommandHandler(new RandomGenerator())
		null                                    | null								  | new RandomGenerator() | new RandomFailureCommandHandler(new RandomGenerator())
		new InMemoryGameSessionDataRepository() | new GuavaMessageBus(new EventBus()) | new RandomGenerator() | null
		new InMemoryGameSessionDataRepository() | new GuavaMessageBus(new EventBus()) | null				  | new RandomFailureCommandHandler(new RandomGenerator())
		new InMemoryGameSessionDataRepository() | null								  | new RandomGenerator() | new RandomFailureCommandHandler(new RandomGenerator())
		null								    | new GuavaMessageBus(new EventBus()) | new RandomGenerator() | new RandomFailureCommandHandler(new RandomGenerator())
	}
}
