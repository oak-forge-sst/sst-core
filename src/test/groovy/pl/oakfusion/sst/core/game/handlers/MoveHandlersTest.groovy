package pl.oakfusion.sst.core.game.handlers

import pl.oakfusion.bus.MessageBus
import pl.oakfusion.sst.core.gamedata.GameSessionData
import pl.oakfusion.sst.core.gamedata.GameSessionDataRepository
import pl.oakfusion.sst.core.gamedata.InMemoryGameSessionDataRepository
import pl.oakfusion.sst.data.game.commands.move.Move
import pl.oakfusion.sst.data.game.events.move.MovePerformed
import pl.oakfusion.sst.data.world.component.ComponentId
import pl.oakfusion.sst.data.world.World
import pl.oakfusion.sst.data.world.civilizations.Unit
import pl.oakfusion.sst.data.world.component.EnergyContainer
import pl.oakfusion.sst.data.world.component.WarpEngines
import pl.oakfusion.sst.test.TestContext
import pl.oakfusion.testutils.SstTestUtils
import spock.lang.Specification

class MoveHandlersTest extends Specification {

	SstTestUtils testUtils 				= new SstTestUtils()
	UUID playerId 						= testUtils.getHumanPlayer().getUuid()
	GameSessionDataRepository gameSessionDataRepository = new InMemoryGameSessionDataRepository()
	MessageBus messageBus 				= testUtils.getMessageBus()
	TestContext testContext 			= new TestContext(messageBus)
    MoveHandlers moveHandlers 			= new MoveHandlers(messageBus, gameSessionDataRepository)
	GameSessionData gameData 			= testUtils.getGameData()
	Unit unit
	World world 						= gameData.getWorld()
	UUID gameSessionUuid				= UUID.randomUUID()

	def setup() {
		gameSessionDataRepository.save(gameSessionUuid, gameData)
		Unit enterprise = gameData.getActivePlayer().getCivilization().getUnits().get(0)
		gameData.getActivePlayer().getCivilization().setActiveUnits(List.of(enterprise))
		unit = gameData.getActivePlayer().getCivilization().getActiveUnits().get(0)
		((WarpEngines)unit.getComponentSystem().getComponent(ComponentId.WARP_ENGINES)).setWarpFactor(2)
		((EnergyContainer)unit.getComponentSystem().getComponent(ComponentId.ENERGY_CONTAINER)).setEnergyToMax()
	}

	static boolean makeSureMoveHasConsequences(Unit unit, World world, ArrayList<Double> beforeMoveData){
		def currentEnergy = ((EnergyContainer)unit.getComponentSystem()
				.getComponent(ComponentId.ENERGY_CONTAINER)).getEnergy()
		def currentTime = world.getStarDate().getDate()
		def oldEnergyAmount = beforeMoveData[1]
		def oldTime = beforeMoveData[0]

		//TODO: after action points issue is fixed
		//boolean actionPointsOK =

		boolean energyOk = currentEnergy < oldEnergyAmount
		boolean timeOK = currentTime > oldTime

		return energyOk && timeOK //&& actionPointsOK
	}

	static double getUnitEnergy(Unit unit) {
		return ((EnergyContainer)unit.getComponentSystem().getComponent(ComponentId.ENERGY_CONTAINER)).getEnergy()
	}

	static def getUnitEnergyAndTime(Unit unit, World world){
		return [world.getStarDate().getDate(), getUnitEnergy(unit)]
	}

	def "should handle valid auto move"() {
		given:
		def beforeMoveResources = getUnitEnergyAndTime(unit, world)

		when:
		testContext.postingMessage(new Move(gameSessionUuid, playerId, true, 5,6, false))

		then:
		testContext.received(MovePerformed.class, m -> {})
		assert unit.getX() == 5 && unit.getY() == 6
		assert makeSureMoveHasConsequences(unit, world, beforeMoveResources)
	}

	def "should handle valid auto move for multiple active units"() {
		given:
		gameData.setActivePlayer(gameData.getPlayersRepository().getPlayerList().get(1))
		gameData.getActivePlayer().getCivilization().setActiveUnits(gameData.getActivePlayer().getCivilization().getUnits())
		def superCommander = gameData.getActivePlayer().getCivilization().getActiveUnits().stream().filter(unit -> unit.getUnitRank() == Unit.UnitRank.values()[Unit.UnitRank.values().size()-1]).findAny().get()
		def commander = gameData.getActivePlayer().getCivilization().getActiveUnits().stream().filter(unit -> unit.getUnitRank() == Unit.UnitRank.values()[Unit.UnitRank.values().size()-2]).findAny().get()
		def klingon = gameData.getActivePlayer().getCivilization().getActiveUnits().stream().filter(unit -> unit.getUnitRank() == Unit.UnitRank.values()[Unit.UnitRank.values().size()-3]).findAny().get()
		def beforeMoveResourcesSuperCommander = getUnitEnergyAndTime(superCommander, world)
		def beforeMoveResourcesCommander = getUnitEnergyAndTime(commander, world)
		def beforeMoveResourcesKlingon = getUnitEnergyAndTime(klingon, world)
		superCommander.setX(2)
		superCommander.setY(2)
		commander.setX(3)
		commander.setY(2)
		klingon.setX(6)
		klingon.setY(7)

		when:
		testContext.postingMessage(new Move(gameSessionUuid, playerId, true, 5,6, false))

		then:
		testContext.received(MovePerformed.class, m -> {})
		superCommander.getX() == 5 && superCommander.getY() == 6
		commander.getX() <= 6 && commander.getX() >= 4 && commander.getY() <= 7 && commander.getY() >= 5
		klingon.getX() <= 6 && klingon.getX() >= 4 && klingon.getY() <= 7 && klingon.getY() >= 5
		makeSureMoveHasConsequences(superCommander, world, beforeMoveResourcesSuperCommander)
		makeSureMoveHasConsequences(commander, world, beforeMoveResourcesCommander)
		makeSureMoveHasConsequences(klingon, world, beforeMoveResourcesKlingon)
	}

	def "should handle valid manual move"() {
		given:
		def beforeMoveResources = getUnitEnergyAndTime(unit, world)
		unit.setX(5)
		unit.setY(3)

		when:
		testContext.postingMessage(new Move(gameSessionUuid, playerId, false, 5,4, false))

		then:
		testContext.received(MovePerformed.class, m -> {})
		assert unit.getX() == 10 && unit.getY() == 7
		assert makeSureMoveHasConsequences(unit, world, beforeMoveResources)
	}

	def "should handle valid auto move with force"() {
		given:
		def beforeMoveResources = getUnitEnergyAndTime(unit, world)
		unit.setX(0)
		unit.setY(0)
		((WarpEngines)unit.getComponentSystem().getComponent(ComponentId.WARP_ENGINES)).setWarpFactor(3)
		((EnergyContainer)unit.getComponentSystem().getComponent(ComponentId.ENERGY_CONTAINER)).setEnergy(50)


		when:
		testContext.postingMessage(new Move(gameSessionUuid, playerId, true, 10,10, true))

		then:
		testContext.received(MovePerformed.class, m -> {})
		unit.getX() != 0 && unit.getY() != 0 && unit.getX() != 10 && unit.getY() != 10
		assert makeSureMoveHasConsequences(unit, world, beforeMoveResources)
	}

	def "should handle valid manual move with force"() {
		given:
		def beforeMoveResources = getUnitEnergyAndTime(unit, world)
		unit.setX(0)
		unit.setY(0)
		((WarpEngines)unit.getComponentSystem().getComponent(ComponentId.WARP_ENGINES)).setWarpFactor(3)
		((EnergyContainer)unit.getComponentSystem().getComponent(ComponentId.ENERGY_CONTAINER)).setEnergy(12)

		when:
		testContext.postingMessage(new Move(gameSessionUuid, playerId, false, 5,5, true))
		println(unit.getX() + " " + unit.getY())

		then:
		testContext.received(MovePerformed.class, m -> {})
		//p -> q === -p || q
		assert (unit.getX() != 0 || unit.getY() == 0)
		assert (unit.getY() != 5 || unit.getY() == 5)
		assert makeSureMoveHasConsequences(unit, world, beforeMoveResources)

	}

	def "should not perform move when validation failure occurs"() {
		given:
		def beforeMoveResources = getUnitEnergyAndTime(unit, world)
		unit.setX(0)
		unit.setY(0)
		((WarpEngines)unit.getComponentSystem().getComponent(ComponentId.WARP_ENGINES)).setWarpFactor(9)
		((EnergyContainer)unit.getComponentSystem().getComponent(ComponentId.ENERGY_CONTAINER)).setEnergyToMax()

		when:
		messageBus.post(new Move(gameSessionUuid, playerId, true, -5,-5, true))

		then:
		assert (unit.getY() == 0 || unit.getX() == 0)
		assert !makeSureMoveHasConsequences(unit, world, beforeMoveResources)

	}

}
