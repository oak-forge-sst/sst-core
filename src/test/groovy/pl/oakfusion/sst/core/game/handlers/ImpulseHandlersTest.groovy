package pl.oakfusion.sst.core.game.handlers

import pl.oakfusion.bus.MessageBus
import pl.oakfusion.sst.core.gamedata.GameSessionData
import pl.oakfusion.sst.core.gamedata.GameSessionDataRepository
import pl.oakfusion.sst.data.game.commands.impulse.Impulse
import pl.oakfusion.sst.data.world.component.ComponentId
import pl.oakfusion.sst.data.world.World
import pl.oakfusion.sst.data.world.civilizations.Unit
import pl.oakfusion.sst.data.world.component.EnergyContainer
import pl.oakfusion.sst.data.world.component.WarpEngines
import pl.oakfusion.sst.test.TestContext
import pl.oakfusion.testutils.SstTestUtils
import spock.lang.Specification

class ImpulseHandlersTest extends Specification {
	SstTestUtils testUtils 				= new SstTestUtils()
	UUID thisSessionID 					= testUtils.getGameSessionUUID()
	UUID playerId 						= testUtils.getHumanPlayer().getUuid()
	GameSessionDataRepository gameSessionDataRepository = testUtils.getGameSessionDataRepo()
	MessageBus messageBus 				= testUtils.getMessageBus()
	TestContext testContext 			= new TestContext(messageBus)
    ImpulseHandlers moveHandlers 			= new ImpulseHandlers(messageBus, gameSessionDataRepository)
	GameSessionData gameData 			= gameSessionDataRepository.get(thisSessionID).get()
	Unit unit 							= gameData.getActivePlayer().getCivilization().getActiveUnits().get(0)
	World world 						= gameData.getWorld()

	def setup() {
		((EnergyContainer)unit.getComponentSystem().getComponent(ComponentId.ENERGY_CONTAINER)).setEnergyToMax()
	}

	static boolean makeSureMoveHasConsequences(Unit unit, World world, ArrayList<Double> beforeMoveData){
		def currentEnergy = ((EnergyContainer)unit.getComponentSystem()
				.getComponent(ComponentId.ENERGY_CONTAINER)).getEnergy()
		def currentTime = world.getStarDate().getDate()
		def oldEnergyAmount = beforeMoveData[1]
		def oldTime = beforeMoveData[0]

		//TODO: after action points issue is fixed
		//boolean actionPointsOK =

		boolean energyOk = currentEnergy < oldEnergyAmount
		boolean timeOK = currentTime > oldTime

		return energyOk && timeOK //&& actionPointsOK
	}

	static double getUnitEnergy(Unit unit) {
		return ((EnergyContainer)unit.getComponentSystem().getComponent(ComponentId.ENERGY_CONTAINER)).getEnergy()
	}

	static def getUnitEnergyAndTime(Unit unit, World world){
		return [world.getStarDate().getDate(), getUnitEnergy(unit)]
	}

	def "should handle valid auto move"() {
		given:
		def beforeMoveResources = getUnitEnergyAndTime(unit, world)

		when:
		messageBus.post(new Impulse(thisSessionID, playerId,  5, 6, true, false))

		then:
		 unit.getX() == 5 && unit.getY() == 6
		 makeSureMoveHasConsequences(unit, world, beforeMoveResources)
	}

	def "should handle valid manual move"() {
		given:
		def beforeMoveResources = getUnitEnergyAndTime(unit, world)
		unit.setX(5)
		unit.setY(3)

		when:
		messageBus.post(new Impulse(thisSessionID, playerId,  5,4,false, false))

		then:
		 unit.getX() == 10 && unit.getY() == 7
		 makeSureMoveHasConsequences(unit, world, beforeMoveResources)
	}


	def "should handle valid auto move with force"() {
		given:
		def beforeMoveResources = getUnitEnergyAndTime(unit, world)
		unit.setX(0)
		unit.setY(0)
		((EnergyContainer)unit.getComponentSystem().getComponent(ComponentId.ENERGY_CONTAINER)).setEnergy(6)

		when:
		messageBus.post(new Impulse(thisSessionID, playerId, 5,5, true, true))
		println(unit.getX() + " " + unit.getY())

		then:
		unit.getX() != 0 && unit.getY() != 0 && unit.getX() != 5 && unit.getY() != 5
		makeSureMoveHasConsequences(unit, world, beforeMoveResources)

	}

	def "should handle valid manual move with force"() {
		given:
		def beforeMoveResources = getUnitEnergyAndTime(unit, world)
		unit.setX(0)
		unit.setY(0)
		((WarpEngines)unit.getComponentSystem().getComponent(ComponentId.WARP_ENGINES)).setWarpFactor(3)
		((EnergyContainer)unit.getComponentSystem().getComponent(ComponentId.ENERGY_CONTAINER)).setEnergy(10)

		when:
		messageBus.post(new Impulse(thisSessionID, playerId, 5,5, false, true))
		println(unit.getX() + " " + unit.getY())

		then:
		 (unit.getX() != 0 || unit.getY() == 0)
		 (unit.getY() != 5 || unit.getY() == 5)
		 makeSureMoveHasConsequences(unit, world, beforeMoveResources)

	}

	def "should not perform move when validation failure occurs"() {
		given:
		def beforeMoveResources = getUnitEnergyAndTime(unit, world)
		unit.setX(0)
		unit.setY(0)
		((WarpEngines)unit.getComponentSystem().getComponent(ComponentId.WARP_ENGINES)).setWarpFactor(9)
		((EnergyContainer)unit.getComponentSystem().getComponent(ComponentId.ENERGY_CONTAINER)).setEnergyToMax()

		when:
		messageBus.post(new Impulse(thisSessionID, playerId,  -5,-5, true, true))

		then:
		 (unit.getY() == 0 || unit.getX() == 0)
		 !makeSureMoveHasConsequences(unit, world, beforeMoveResources)

	}


}
