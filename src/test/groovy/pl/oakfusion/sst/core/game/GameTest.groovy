package pl.oakfusion.sst.core.game

import com.google.common.eventbus.EventBus
import pl.oakfusion.bus.guava.GuavaMessageBus
import pl.oakfusion.sst.core.aggregates.HelpResolver
import pl.oakfusion.sst.core.gamedata.InMemoryGameSessionDataRepository
import pl.oakfusion.sst.data.util.RandomGenerator
import spock.lang.Specification
import spock.lang.Unroll

class GameTest extends Specification {

	@Unroll
	def 'should throw IllegalArgumentException when constructor param will be null'() {
		given:
		def messageBus = new GuavaMessageBus(new EventBus())

		when:
		new Game(repoParam, messageBus, randomGeneratorParam, helpResolver)

		then:
		thrown(IllegalArgumentException)

		where:
		repoParam                               | helpResolver       | randomGeneratorParam
		null                                    | null               | null
		null                                    | new HelpResolver() | null
		null                                    | null               | new RandomGenerator()
		null                                    | new HelpResolver() | new RandomGenerator()
		new InMemoryGameSessionDataRepository() | null | null
		new InMemoryGameSessionDataRepository() | null               | new RandomGenerator()
		new InMemoryGameSessionDataRepository() | new HelpResolver() | null
		new InMemoryGameSessionDataRepository() | new HelpResolver() | null
	}
}
