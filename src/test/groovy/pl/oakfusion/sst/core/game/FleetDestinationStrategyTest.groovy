package pl.oakfusion.sst.core.game

import pl.oakfusion.sst.core.gamedata.GameSessionData
import pl.oakfusion.sst.data.game.UnitsMoveDestinations
import pl.oakfusion.sst.data.game.commands.move.Move
import pl.oakfusion.sst.data.parameter.GameDifficulty
import pl.oakfusion.sst.data.world.StarDate
import pl.oakfusion.sst.data.world.World
import pl.oakfusion.sst.data.world.civilizations.AbstractUnitFactory
import pl.oakfusion.sst.data.world.civilizations.Unit
import pl.oakfusion.sst.data.world.civilizations.UnitFactory
import pl.oakfusion.testutils.SstTestUtils
import spock.lang.Specification

class FleetDestinationStrategyTest extends Specification {
	SstTestUtils testUtils = new SstTestUtils()

	def "returns correct flag depending on amount of units energy and force mode"(){
		given:
		GameSessionData gData = testUtils.getGameData()
		FleetDestinationStrategy fleetDestinationStrategy = new FleetDestinationStrategy(FleetDestinationStrategy.FleetFormation.RANDOM, gData)
		Move moveCommand = new Move(UUID.randomUUID(), gData.activePlayer.uuid, true, 3, 3, forced)
		Unit enterprise = gData.getActivePlayer().getCivilization().getUnits().get(0)
		enterprise.getComponentSystem().getEnergyContainer().setEnergy(energy)
		enterprise.setX(0)
		enterprise.setY(0)
		gData.getActivePlayer().getCivilization().setActiveUnits(List.of(enterprise))

		when:
        UnitsMoveDestinations unitsMoveDestinations = fleetDestinationStrategy.getUnitsDestinations(moveCommand, gData.getActivePlayer().getCivilization().getActiveUnits())

		then:
		unitsMoveDestinations.getDestinationByUnit(enterprise).orElseThrow().isAbleToReachDestination() == result

		where:
		energy		|	forced		|	result
		5000.0		|	true		|	true
		5000.0		|	false		|	true
		100.0		|	true		|	true
		100.0		|	false		|	false
	}

	def "returns empty UnitsMoveDestinations when highest rank unit cannot reach any destination in forcedMode"(){
		given:
		GameSessionData gData = testUtils.getGameData()
		FleetDestinationStrategy fleetDestinationStrategy = new FleetDestinationStrategy(FleetDestinationStrategy.FleetFormation.RANDOM, gData)
		Move moveCommand = new Move(UUID.randomUUID(), gData.activePlayer.uuid, true, 3, 3, true)
		Unit enterprise = gData.getActivePlayer().getCivilization().getUnits().get(0)
		enterprise.getComponentSystem().getEnergyContainer().setEnergy(0.0)
		enterprise.setX(0)
		enterprise.setY(0)
		gData.getActivePlayer().getCivilization().setActiveUnits(List.of(enterprise))

		when:
		UnitsMoveDestinations unitsMoveDestinations = fleetDestinationStrategy.getUnitsDestinations(moveCommand, gData.getActivePlayer().getCivilization().getActiveUnits())

		then:
		unitsMoveDestinations.isEmpty()
	}

	def "returns UnitsMoveDestinations with false flag for unit which cannot reach destination"(){
		given:
		GameSessionData gData = testUtils.getGameData()
		FleetDestinationStrategy fleetDestinationStrategy = new FleetDestinationStrategy(FleetDestinationStrategy.FleetFormation.RANDOM, gData)
		Move moveCommand = new Move(UUID.randomUUID(), gData.activePlayer.uuid, true, 2, 2, false)
		gData.setActivePlayer(gData.getPlayersRepository().getPlayerList().get(1))
		List<Unit> units = gData.getActivePlayer().getCivilization().getUnits()
		Unit klingon = units.stream().filter(unit -> unit.getUnitRank() == Unit.UnitRank.CADET).findAny().get()
		Unit commander = units.stream().filter(unit -> unit.getUnitRank() == Unit.UnitRank.CAPTAIN).findAny().get()
		Unit superCommander = units.stream().filter(unit -> unit.getUnitRank() == Unit.UnitRank.GENERAL).findAny().get()
		klingon.setX(19)
		klingon.setY(19)
		superCommander.setX(1)
		superCommander.setY(1)
		klingon.getComponentSystem().getEnergyContainer().setEnergy(50.0)
		commander.getComponentSystem().getEnergyContainer().setEnergyToMax()
		superCommander.getComponentSystem().getEnergyContainer().setEnergyToMax()
		gData.getActivePlayer().getCivilization().setActiveUnits(units)

		when:
		UnitsMoveDestinations unitsMoveDestinations = fleetDestinationStrategy.getUnitsDestinations(moveCommand, gData.getActivePlayer().getCivilization().getActiveUnits())

		then:
		!unitsMoveDestinations.getDestinationByUnit(klingon).orElseThrow().isAbleToReachDestination()
	}

	def "returns UnitsMoveDestinations containing zero records when any of active units cannot reach any destination in forcedMode"(){
		given:
		GameSessionData gData = testUtils.getGameData()
		FleetDestinationStrategy fleetDestinationStrategy = new FleetDestinationStrategy(FleetDestinationStrategy.FleetFormation.RANDOM, gData)
		Move moveCommand = new Move(UUID.randomUUID(), gData.activePlayer.uuid, true, 3, 3, true)
		gData.setActivePlayer(gData.getPlayersRepository().getPlayerList().get(1))
		List<Unit> units = gData.getActivePlayer().getCivilization().getUnits()
		Unit klingon = units.stream().filter(unit -> unit.getUnitRank() == Unit.UnitRank.CADET).findAny().get()
		Unit commander = units.stream().filter(unit -> unit.getUnitRank() == Unit.UnitRank.CAPTAIN).findAny().get()
		Unit superCommander = units.stream().filter(unit -> unit.getUnitRank() == Unit.UnitRank.GENERAL).findAny().get()
		klingon.getComponentSystem().getEnergyContainer().setEnergy(0.0)
		commander.getComponentSystem().getEnergyContainer().setEnergyToMax()
		superCommander.getComponentSystem().getEnergyContainer().setEnergyToMax()
		gData.getActivePlayer().getCivilization().setActiveUnits(units)

		when:
		UnitsMoveDestinations unitsMoveDestinations = fleetDestinationStrategy.getUnitsDestinations(moveCommand, gData.getActivePlayer().getCivilization().getActiveUnits())

		then:
		unitsMoveDestinations.getDestinationsList().isEmpty()
	}

	def "returns UnitsMoveDestinations with true flag for all units when all can reach destination"(){
		given:
		GameSessionData gData = testUtils.getGameData()
		FleetDestinationStrategy fleetDestinationStrategy = new FleetDestinationStrategy(FleetDestinationStrategy.FleetFormation.RANDOM, gData)
		Move moveCommand = new Move(UUID.randomUUID(), gData.activePlayer.uuid, true, 0, 0, false)
		gData.setActivePlayer(gData.getPlayersRepository().getPlayerList().get(1))
		List<Unit> units = gData.getActivePlayer().getCivilization().getUnits()
		Unit klingon = units.stream().filter(unit -> unit.getUnitRank() == Unit.UnitRank.CADET).findAny().get()
		Unit commander = units.stream().filter(unit -> unit.getUnitRank() == Unit.UnitRank.CAPTAIN).findAny().get()
		Unit superCommander = units.stream().filter(unit -> unit.getUnitRank() == Unit.UnitRank.GENERAL).findAny().get()
		klingon.setX(11)
		klingon.setY(2)
		commander.setX(11)
		commander.setY(3)
		superCommander.setX(11)
		superCommander.setY(1)
		klingon.getComponentSystem().getEnergyContainer().setEnergyToMax()
		commander.getComponentSystem().getEnergyContainer().setEnergyToMax()
		superCommander.getComponentSystem().getEnergyContainer().setEnergyToMax()
		gData.getActivePlayer().getCivilization().setActiveUnits(units)

		when:
		UnitsMoveDestinations unitsMoveDestinations = fleetDestinationStrategy.getUnitsDestinations(moveCommand, gData.getActivePlayer().getCivilization().getActiveUnits())

		then:
		unitsMoveDestinations.getDestinationByUnit(klingon).orElseThrow().isAbleToReachDestination()
		unitsMoveDestinations.getDestinationByUnit(commander).orElseThrow().isAbleToReachDestination()
		unitsMoveDestinations.getDestinationByUnit(superCommander).orElseThrow().isAbleToReachDestination()
	}

	def "returns UnitsMoveDestinations containing zero records, when there is no empty surrounding around destination"(){
		given:
		GameSessionData gData = testUtils.getGameData()
		World emptyWorld = new World(2, 2, 10, new StarDate(StarDate.MIN_STARDATE, 1))
		gData.setWorld(emptyWorld)
		FleetDestinationStrategy fleetDestinationStrategy = new FleetDestinationStrategy(FleetDestinationStrategy.FleetFormation.RANDOM, gData)
		Move moveCommand = new Move(UUID.randomUUID(), gData.activePlayer.uuid, true, 0, 0, false)
		gData.setActivePlayer(gData.getPlayersRepository().getPlayerList().get(1))
		List<Unit> units = gData.getActivePlayer().getCivilization().getUnits()
		Unit klingon = units.stream().filter(unit -> unit.getUnitRank() == Unit.UnitRank.CADET).findAny().get()
		Unit commander = units.stream().filter(unit -> unit.getUnitRank() == Unit.UnitRank.CAPTAIN).findAny().get()
		Unit superCommander = units.stream().filter(unit -> unit.getUnitRank() == Unit.UnitRank.GENERAL).findAny().get()
		klingon.setX(11)
		klingon.setY(2)
		commander.setX(11)
		commander.setY(3)
		superCommander.setX(11)
		superCommander.setY(1)
		gData.getWorld().add(klingon)
		gData.getWorld().add(commander)
		gData.getWorld().add(superCommander)
		klingon.getComponentSystem().getEnergyContainer().setEnergyToMax()
		commander.getComponentSystem().getEnergyContainer().setEnergyToMax()
		superCommander.getComponentSystem().getEnergyContainer().setEnergyToMax()
		gData.getActivePlayer().getCivilization().setActiveUnits(units)
		AbstractUnitFactory unitFactory = new UnitFactory(GameDifficulty.FAIR)
		for(int i = 1; i <= 10; i++){
			gData.world.add(unitFactory.createKlingon(UUID.randomUUID()).create(i, 0))
		}
		for(int i = 1; i <= 10; i++){
			for(int j = 0; j <= 10; j++){
				gData.world.add(unitFactory.createKlingon(UUID.randomUUID()).create(j, i))
			}
		}

		when:
		UnitsMoveDestinations unitsMoveDestinations = fleetDestinationStrategy.getUnitsDestinations(moveCommand, gData.getActivePlayer().getCivilization().getActiveUnits())

		then:
		unitsMoveDestinations.getDestinationsList().isEmpty()
	}
}
