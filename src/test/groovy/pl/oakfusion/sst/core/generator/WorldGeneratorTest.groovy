
package pl.oakfusion.sst.core.generator

import pl.oakfusion.sst.core.gamedata.PlayersRepository
import pl.oakfusion.sst.data.world.gameobject.GameObject
import pl.oakfusion.sst.data.initializer.WorldSpecification
import pl.oakfusion.sst.data.world.World
import pl.oakfusion.testutils.SstTestUtils
import spock.lang.Shared
import spock.lang.Specification

class WorldGeneratorTest extends Specification {
	@Shared
	SstTestUtils testUtils
	@Shared
	WorldGenerator worldGenerator
	@Shared
	WorldSpecification specification
	@Shared
	PlayersRepository playersRepository
	@Shared
	World world
	@Shared
	List<GameObject> gameObjects

	//TODO: Uncomment after fixing issue with world generation looping
	/*def setupSpec() {
		testUtils = new SstTestUtils()
		worldGenerator = new WorldGenerator(new RandomGenerator())
		specification = testUtils.smallWorldSpecification
		playersRepository = testUtils.getPlayersRepository()
		world = worldGenerator.generateWorld(
				specification,
				playersRepository,
				GameType.SINGLEPLAYER
		)
		gameObjects = world.getAllGameObjects()

	}

	def "GenerateWorld should generate world consistent with WorldSpecification"() {


		when:

		List<Unit> opponentsUnits = playersRepository.getPlayerList().get(1).getCivilization().getUnits()

		then:
		assert world.getHeightInQuadrants() == specification.getHeightInQuadrants()
		world.getWidthInQuadrants() == specification.getWidthInQuadrants()
		gameObjects.stream().filter(x -> x instanceof Planet).count() == specification.getPlanetQuantity()
		gameObjects.stream().filter(x -> x instanceof Starbase).count() == specification.getStarbaseQuantity()
		gameObjects.stream().filter(x -> x instanceof Star).count() == specification.getStarQuantity()
		gameObjects.stream().filter(x -> x instanceof BlackHole).count() == specification.getBlackHoleQuantity()
		playersRepository.getPlayerList().get(0).getCivilization().getUnits().get(0) instanceof Enterprise
		opponentsUnits.stream().filter(x -> x instanceof Klingon).count() != 0

	}

	static def generateForceFieldForDifferentTypesOfObject(GameObject object){
		switch (object.getClass()){
			case Planet.class:
				return 1
			case Starbase.class:
				return 1
			case Star.class:
				return 1
			case BlackHole.class:
				return 2
			case Unit.class:
				return 0
		}

	}

	def "world generator should be able to tell space is available in range of objects' force fields and not available in the point they are"() {

		expect:
		!worldGenerator.worldGeneratorCache.isSpaceAvailable(x, y, range)
		for (rangex in 1..range){
			for (i in [-1,0,1]){
				for (j in [-1,0,1]){
					worldGenerator.worldGeneratorCache.isSpaceAvailable(x+i*rangex, y+j*rangex, range)
				}
			}
		}

		where:
		range << gameObjects.collect { generateForceFieldForDifferentTypesOfObject(it) }
		x << gameObjects.collect { it.getX() }
		y << gameObjects.collect { it.getY() }


	}



	def "should_respect_quadrant_bounds_during_filling_range"() {
		expect:
		!worldGenerator.worldGeneratorCache.isSpaceAvailable(x, y,0)

		where:
		x << [-1, specification.getQuadrantSize()]
		y << [-1, specification.getQuadrantSize()]
	}*/


}
