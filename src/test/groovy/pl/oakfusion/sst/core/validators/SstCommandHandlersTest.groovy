package pl.oakfusion.sst.core.validators

import com.google.common.eventbus.EventBus
import pl.oakfusion.bus.guava.GuavaMessageBus
import pl.oakfusion.data.message.Event
import pl.oakfusion.sst.core.gamedata.GameSessionData
import pl.oakfusion.sst.core.gamedata.GameSessionDataRepository
import pl.oakfusion.sst.core.gamedata.InMemoryGameSessionDataRepository
import pl.oakfusion.sst.data.errors.NotFoundError
import pl.oakfusion.sst.core.game.validators.SstCommandHandlers
import pl.oakfusion.sst.core.game.validators.SstCommandValidator
import pl.oakfusion.sst.data.errors.ValidationError
import pl.oakfusion.sst.data.validationfailures.ValidationFailure
import pl.oakfusion.sst.data.SstCommand
import pl.oakfusion.sst.data.SstEvent
import pl.oakfusion.sst.data.gamesession.GameSessionState
import pl.oakfusion.sst.test.TestContext
import pl.oakfusion.testutils.SstTestUtils
import spock.lang.Specification

import static pl.oakfusion.sst.core.game.validators.ValidatorsCollector.noValidators
import static pl.oakfusion.sst.core.game.validators.ValidatorsCollector.validators

class SstCommandHandlersTest extends Specification {

	def messageBus = new GuavaMessageBus(new EventBus())
	GameSessionDataRepository repository = new InMemoryGameSessionDataRepository()
	def sstCommandHandlers = new SstCommandHandlers(messageBus, repository) {}
	def testContext = new TestContext(messageBus)
	def sstTestUtils = new SstTestUtils()


	def "if repository returns null then should post not found error"() {
		given:
		testContext.handlerForMessage(SstCommand.class, sstCommandHandlers.handleWith(this::mockHandle, noValidators()))

		when:
		testContext.postingMessage(new SstCommand(UUID.randomUUID(), UUID.randomUUID()){})

		then:
		testContext.received(NotFoundError.class, e -> { })
	}

	def "if validation finishes with failures should post validation error"() {
		given:
		def uuid = new UUID(1, 1)
		repository.save(uuid, sstTestUtils.getGameData())
		testContext.handlerForMessage(SstCommand.class, sstCommandHandlers.handleWith(this::mockHandle, validators(new SstCommandValidator<SstCommand>(){
			@Override
			List<ValidationFailure> validate(SstCommand command, GameSessionData gameSessionData) {
				return List.of(new ValidationFailure("Test error", List.of()))
			}
		})))

		when:
		testContext.postingMessage(new SstCommand(uuid, UUID.randomUUID()){})

		then:
		testContext.received(ValidationError.class, e -> { })
	}

	def "if validation passes should post all events passed by handler"() {
		given:
		def uuid = new UUID(1, 1)
		repository.save(uuid, sstTestUtils.getGameData())
		testContext.handlerForMessage(SstCommand.class, sstCommandHandlers.handleWith(this::mockHandle, validators(new SstCommandValidator<SstCommand>(){
			@Override
			List<ValidationFailure> validate(SstCommand command, GameSessionData gameSessionData) {
				return List.of()
			}
		})))

		when:
		testContext.postingMessage(new SstCommand(uuid, UUID.randomUUID()){})

		then:
		repository.get(uuid).get().getGameSessionState() == GameSessionState.NOT_RUNNING
		testContext.received(mockEvent1.class, e -> { })
		repository.get(uuid).get().getGameSessionState() == GameSessionState.RUNNING
		testContext.received(mockEvent2.class, e -> { })
	}

	private List<Event> mockHandle(SstCommand command, GameSessionData gameSessionData) {
		gameSessionData.setGameSessionState(GameSessionState.RUNNING)
		return List.of(new mockEvent1(), new mockEvent2())
	}

	private class mockEvent1 extends SstEvent{
	}
	private class mockEvent2 extends SstEvent{
	}
}
