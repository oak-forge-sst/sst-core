package pl.oakfusion.sst.core.validators

import pl.oakfusion.sst.data.validationfailures.ExitGameFailed
import pl.oakfusion.sst.core.gamesession.validators.GameSessionValidators
import pl.oakfusion.sst.core.game.validators.SstCommandValidator
import pl.oakfusion.sst.data.validationfailures.ValidationFailure
import pl.oakfusion.testutils.SstTestUtils
import pl.oakfusion.sst.data.gamesession.commands.ExitGame
import spock.lang.Specification
import spock.lang.Unroll

class GameSessionValidatorsTest extends Specification {
	SstTestUtils sstTestUtils = new SstTestUtils()

	@Unroll
	def "if player is not game master should return list with exit game failed and message not game master"() {
		given:
        SstCommandValidator validator = GameSessionValidators.getExitGameCommandValidator()
		sstTestUtils.getGameData().getPlayersRepository().getPlayerList().get(0).setGameMaster(false)
		UUID playerUuid = sstTestUtils.getGameData().getPlayersRepository().getPlayerList().get(0).getUuid()

		when:
		List<ValidationFailure> failureList = validator.validate(new ExitGame(UUID.randomUUID(), playerUuid), sstTestUtils.getGameData())

		then:
		assert failureList.get(0) instanceof ExitGameFailed
		assert failureList.get(0).toString().contains("You're not game master.")
	}


	//TODO Check if method hasContinuation in GameSessionData is correct with current game concept
	/*@Unroll
	def "if game has continuation should return list with continuation warn and message game has continuation"() {
		given:
		SstCommandValidator validator = GameSessionValidators.getEndTurnCommandValidator()
		sstTestUtils.getGameData().getPlayersRepository().getPlayerList().get(0).setGameMaster(false)
		UUID playerUuid = sstTestUtils.getGameData().getPlayersRepository().getPlayerList().get(0).getUuid()

		when:
		List<ValidationFailure> failureList = validator.validate(new ExitGame(UUID.randomUUID(), playerUuid), sstTestUtils.getGameData())

		then:
		assert failureList.get(0) instanceof ExitGameFailed
		failureList.get(0).toString().contains("You're not game master.");
	}*/
}
