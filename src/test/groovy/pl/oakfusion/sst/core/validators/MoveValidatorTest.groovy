package pl.oakfusion.sst.core.validators

import pl.oakfusion.sst.core.gamedata.GameSessionData
import pl.oakfusion.sst.core.game.validators.MoveCache
import pl.oakfusion.sst.core.game.validators.MoveValidator
import pl.oakfusion.sst.data.validationfailures.ValidationFailure
import pl.oakfusion.sst.data.game.commands.move.Move
import pl.oakfusion.sst.data.game.events.move.ForcedMoveFailed
import pl.oakfusion.sst.data.game.events.move.MoveCollisionDetected
import pl.oakfusion.sst.data.game.events.move.MoveFailed
import pl.oakfusion.sst.data.game.events.move.MoveOutOfBorderDetected
import pl.oakfusion.sst.data.validationfailures.NotEnoughResources
import pl.oakfusion.sst.data.world.civilizations.AbstractUnitFactory
import pl.oakfusion.sst.data.world.civilizations.Unit
import pl.oakfusion.testutils.SstTestUtils
import spock.lang.Specification

class MoveValidatorTest extends Specification {

	MoveValidator moveValidator = new MoveValidator(new MoveCache())
	SstTestUtils testUtils = new SstTestUtils()

	def "should return MoveFailed if more than one unit is active and player wants to perform manual move" (){
		given:
		GameSessionData gData = testUtils.getGameData()
		gData.setActivePlayer(testUtils.getPlayersRepository().getPlayerList().get(1))
		gData.getActivePlayer().getCivilization().setActiveUnits(gData.getActivePlayer().getCivilization().getUnits())
		Move moveCommand = new Move(UUID.randomUUID(), gData.activePlayer.uuid, false, 5, 5, false)

		when:
		ArrayList<ValidationFailure> listOfFailures = moveValidator.validate(moveCommand, gData)

		then:
		assert listOfFailures.stream().anyMatch(c -> c instanceof MoveFailed && c.toString().contains("Manual move possible only for single unit"))
	}

	def "if computer is down and move is in auto mode should return MoveFailed validation failure"() {
		given:
		GameSessionData gData = testUtils.getGameData()
		Move moveCommand = new Move(UUID.randomUUID(), gData.activePlayer.uuid, true, 1, 1, false)
		Unit enterprise = gData.getActivePlayer().getCivilization().getUnits().get(0)
		enterprise.setActionPoints(100)
		enterprise.getComponentSystem().getComputer().setCurrentHealth(0)
		enterprise.getComponentSystem().getWarpEngines().setWarpFactor(1)
		gData.getActivePlayer().getCivilization().setActiveUnits(List.of(enterprise))

		when:
		ArrayList<ValidationFailure> listOfFailures = moveValidator.validate(moveCommand, gData)

		then:
		assert listOfFailures.stream().anyMatch(c -> c instanceof MoveFailed && c.toString().contains("You cannot move automatically while computer is down"))

	}

	def "if player wants to move outside the world boundaries should return MoveOutOfBorderDetected validation failure (manual)"() {
		given:
		GameSessionData gData = testUtils.getGameData()
		gData.getActivePlayer().getCivilization().setActiveUnits(List.of(gData.getActivePlayer().getCivilization().getUnits().get(0)))
		Move moveCommand = new Move(UUID.randomUUID(), gData.getActivePlayer().getUuid(), false, x, y, false)

		when:
		ArrayList<ValidationFailure> listOfFailures = moveValidator.validate(moveCommand, gData)

		then:
		assert listOfFailures.stream().anyMatch(c -> c instanceof MoveOutOfBorderDetected)

		where:
		x 	|	y
		50	|	50
		3	|	100
		100	|	3
	}

	def "if player wants to move outside the world boundaries should return MoveOutOfBorderDetected validation failure (auto)"() {
		given:
		GameSessionData gData = testUtils.getGameData()
		Unit enterprise = gData.getActivePlayer().getCivilization().getUnits().get(0)
		enterprise.getComponentSystem().getComputer().fullRepair()
		gData.getActivePlayer().getCivilization().setActiveUnits(List.of(enterprise))
		Move moveCommand = new Move(UUID.randomUUID(), gData.activePlayer.uuid, true, x, y, false)

		when:
		ArrayList<ValidationFailure> listOfFailures = moveValidator.validate(moveCommand, gData)

		then:
		assert listOfFailures.stream().anyMatch(c -> c instanceof MoveOutOfBorderDetected)

		where:
		x 	|	y
		-1 	|	-1
		-1	|	0
		0 	|	-1
		50	|	50
		3	|	100
		100	|	3
	}

	def "if player wants to move within the world boundaries should not return MoveOutOfBorderDetected validation failure (auto)"() {
		given:
		GameSessionData gData = testUtils.getGameData()
		Unit enterprise = gData.getActivePlayer().getCivilization().getUnits().get(0)
		enterprise.getComponentSystem().getComputer().fullRepair()
		gData.getActivePlayer().getCivilization().setActiveUnits(List.of(enterprise))
		Move moveCommand = new Move(UUID.randomUUID(), gData.activePlayer.uuid, true, x, y, false)

		when:
		ArrayList<ValidationFailure> listOfFailures = moveValidator.validate(moveCommand, gData)

		then:
		assert !listOfFailures.stream().anyMatch(c -> c instanceof MoveOutOfBorderDetected)

		where:
		x << [1, 10, 3, 14, 0]
		y << [2, 5, 9, 13, 1]
	}

	def "if player wants to move within the world boundaries should not return MoveOutOfBorderDetected validation failure (manual)"() {
		given:
		GameSessionData gData = testUtils.getGameData()
		Unit enterprise = gData.getActivePlayer().getCivilization().getUnits().get(0)
		enterprise.getComponentSystem().getComputer().fullRepair()
		gData.getActivePlayer().getCivilization().setActiveUnits(List.of(enterprise))
		Move moveCommand = new Move(UUID.randomUUID(), gData.activePlayer.uuid, false, x, y, false)

		when:
		ArrayList<ValidationFailure> listOfFailures = moveValidator.validate(moveCommand, gData)

		then:
		assert !listOfFailures.stream().anyMatch(c -> c instanceof MoveOutOfBorderDetected)

		where:
		x << [1, -2, 3, -4, 0]
		y << [2, -1, 9, 5, 1]
	}

	def "if unit collision in target point has been detected should return MoveCollisionDetected validation failure (auto)"() {
		GameSessionData gData = testUtils.getGameData()
		Unit enterprise = gData.getActivePlayer().getCivilization().getUnits().get(0)
		enterprise.getComponentSystem().getComputer().fullRepair()
		gData.getActivePlayer().getCivilization().setActiveUnits(List.of(enterprise))
		Move moveCommand = new Move(UUID.randomUUID(), gData.activePlayer.uuid, true, 3, 3, false)

		gData.getWorld().add(testUtils.unitFactory.createKlingon(UUID.randomUUID()).create(3,3))
		Unit unit = gData.getActivePlayer().getCivilization().getActiveUnits().get(0)
		unit.setX(1)
		unit.setY(11)

		when:
		ArrayList<ValidationFailure> listOfFailures = moveValidator.validate(moveCommand, gData)

		then:
		assert listOfFailures.stream().anyMatch(c -> c instanceof MoveCollisionDetected)
	}

	def "if unit collision in target point has been detected should return MoveCollisionDetected validation failure (manual)"() {
		GameSessionData gData = testUtils.getGameData()
		Unit enterprise = gData.getActivePlayer().getCivilization().getUnits().get(0)
		enterprise.getComponentSystem().getComputer().fullRepair()
		gData.getActivePlayer().getCivilization().setActiveUnits(List.of(enterprise))
		Move moveCommand = new Move(UUID.randomUUID(), gData.activePlayer.uuid, false, 2, 2, false)

		gData.getWorld().add(testUtils.unitFactory.createKlingon(UUID.randomUUID()).create(3,3))
		Unit unit = gData.getActivePlayer().getCivilization().getActiveUnits().get(0)
		unit.setX(1)
		unit.setY(1)

		when:
		ArrayList<ValidationFailure> listOfFailures = moveValidator.validate(moveCommand, gData)

		then:
		assert listOfFailures.stream().anyMatch(c -> c instanceof MoveCollisionDetected)
	}

	def "if unit is surrounded by opponents, validator should not return MoveCollisionDetected (manual)"(){
		GameSessionData gData = testUtils.getGameData()
		Move moveCommand = new Move(UUID.randomUUID(), gData.activePlayer.uuid, false, 3, 3, false)
		Unit enterprise = gData.getActivePlayer().getCivilization().getUnits().get(0)
		enterprise.getComponentSystem().getComputer().fullRepair()
		enterprise.getComponentSystem().getWarpEngines().fullRepair()
		enterprise.setX(0)
		enterprise.setY(0)
		gData.getActivePlayer().getCivilization().setActiveUnits(List.of(enterprise))
		AbstractUnitFactory unitFactory = testUtils.getUnitFactory()

		for (coords in [[1,0], [1,1], [0,1]]) {
			gData.getWorld().add(unitFactory.createKlingon(UUID.randomUUID()).create(coords[0], coords[1]))
		}

		when:
		ArrayList<ValidationFailure> listOfFailures = moveValidator.validate(moveCommand, gData)

		then:
		assert !listOfFailures.stream().anyMatch(c -> c instanceof MoveCollisionDetected)
	}

	def "if unit is surrounded by opponents, validator should not return MoveCollisionDetected (auto)"(){
		GameSessionData gData = testUtils.getGameData()
		Move moveCommand = new Move(UUID.randomUUID(), gData.activePlayer.uuid, true, 3, 3, false)
		Unit enterprise = gData.getActivePlayer().getCivilization().getUnits().get(0)
		enterprise.getComponentSystem().getComputer().fullRepair()
		enterprise.getComponentSystem().getWarpEngines().setWarpFactor(10)
		enterprise.setX(0)
		enterprise.setY(0)
		gData.getActivePlayer().getCivilization().setActiveUnits(List.of(enterprise))
		AbstractUnitFactory unitFactory = testUtils.getUnitFactory()

		for (coords in [[1,0], [1,1], [0,1]]) {
			gData.getWorld().add(unitFactory.createKlingon(UUID.randomUUID()).create(coords[0], coords[1]))
		}

		when:
		ArrayList<ValidationFailure> listOfFailures = moveValidator.validate(moveCommand, gData)

		then:
		assert !listOfFailures.stream().anyMatch(c -> c instanceof MoveCollisionDetected)
	}

	def "if computer is not down and move is on auto mode should not return AutoMoveFailed validation failure"() {
		given:
		GameSessionData gData = testUtils.getGameData()
		Move moveCommand = new Move(UUID.randomUUID(), gData.activePlayer.uuid, true, 1, 1, false)
		Unit enterprise = gData.getActivePlayer().getCivilization().getUnits().get(0)
		enterprise.getComponentSystem().getComputer().fullRepair()
		enterprise.getComponentSystem().getWarpEngines().setWarpFactor(10)
		enterprise.setX(0)
		enterprise.setY(0)
		gData.getActivePlayer().getCivilization().setActiveUnits(List.of(enterprise))

		when:
		ArrayList<ValidationFailure> listOfFailures = moveValidator.validate(moveCommand, gData)

		then:
		assert !listOfFailures.stream().anyMatch(c -> c instanceof MoveFailed)

	}

	def "if player wants to make a valid move should return no failures (manual)"() {
		given:
		GameSessionData gData = testUtils.getGameData()
		Move moveCommand = new Move(UUID.randomUUID(), gData.activePlayer.uuid, false, 1, 1, false)
		Unit enterprise = gData.getActivePlayer().getCivilization().getUnits().get(0)
		enterprise.getComponentSystem().getEnergyContainer().setEnergyToMax()
		gData.getActivePlayer().getCivilization().setActiveUnits(List.of(enterprise))

		when:
		ArrayList<ValidationFailure> listOfFailures = moveValidator.validate(moveCommand, gData)

		then:
		assert listOfFailures.isEmpty()

	}

	def "if player wants to make a valid move should return no failures (auto)"() {
		given:
		GameSessionData gData = testUtils.getGameData()
		Move moveCommand = new Move(UUID.randomUUID(), gData.activePlayer.uuid, true, 5, 5, false)
		Unit enterprise = gData.getActivePlayer().getCivilization().getUnits().get(0)
		enterprise.getComponentSystem().getEnergyContainer().setEnergyToMax()
		gData.getActivePlayer().getCivilization().setActiveUnits(List.of(enterprise))

		when:
		ArrayList<ValidationFailure> listOfFailures = moveValidator.validate(moveCommand, gData)

		then:
		assert listOfFailures.isEmpty()

	}

	//TODO: uncomment once action points issue is resolved
//	def "if unit is not in force mode does not have enough action points should return NotEnoughActionPoints validation failure"() {
//		given:
//		GameSessionData gData = testUtils.getGameData()
//		Move moveCommand = new Move(UUID.randomUUID(), gData.activePlayer.uuid, true, 1, 1, false)
//		Unit unit = gData.getActivePlayer().getCivilization().getActiveUnit()
//		unit.setActionPoints(0)
//
//		when:
//		ArrayList<ValidationFailure> listOfFailures = moveValidator.validate(moveCommand, gData)
//
//		then:
//		assert listOfFailures.stream().anyMatch(c -> c instanceof NotEnoughActionPoints)
//
//	}

	def "if unit has enough resources should not return NotEnoughResources validation failure"() {
		given:
		GameSessionData gData = testUtils.getGameData()
		Move moveCommand = new Move(UUID.randomUUID(), gData.activePlayer.uuid, autoMode, 10, 0, forceMode)

		Unit enterprise = gData.getActivePlayer().getCivilization().getUnits().get(0)
		enterprise.getComponentSystem().getComputer().fullRepair()
		enterprise.getComponentSystem().getEnergyContainer().setEnergy(1000)
		enterprise.getComponentSystem().getWarpEngines().setWarpFactor(6)
		gData.getActivePlayer().getCivilization().setActiveUnits(List.of(enterprise))

		when:
		def listOfFailures = moveValidator.validate(moveCommand, gData)

		then:
		assert !listOfFailures.stream().anyMatch(x -> x instanceof NotEnoughResources)

		where:
		forceMode | autoMode
		false  	  | false
		true      | true
		false     | true
		true      | false

	}

	def "if unit does not have enough resources, should return NotEnoughResources validation failure"() {
		given:
		GameSessionData gData = testUtils.getGameData()
		Move moveCommand = new Move(UUID.randomUUID(), gData.activePlayer.uuid, autoMode, x, y, false)
		Unit enterprise = gData.getActivePlayer().getCivilization().getUnits().get(0)
		enterprise.getComponentSystem().getComputer().fullRepair()
		enterprise.getComponentSystem().getEnergyContainer().setEnergy(5)
		gData.getActivePlayer().getCivilization().setActiveUnits(List.of(enterprise))

		when:
		def listOfFailures = moveValidator.validate(moveCommand, gData)

		then:
		assert listOfFailures.stream().anyMatch(f -> f instanceof NotEnoughResources)

		where:
		autoMode  | x	| y
		true  	  | 19  | 19
		false  	  | 9  	| 9
	}

	def "if any unit does not have enough resources, should return NotEnoughResources validation failure"() {
		given:
		GameSessionData gData = testUtils.getGameData()
		gData.setActivePlayer(gData.getPlayersRepository().getPlayerList().get(1))
		Move moveCommand = new Move(UUID.randomUUID(), gData.activePlayer.uuid, true, 19, 19, false)
		Unit klingon = gData.getActivePlayer().getCivilization().getUnits().stream().filter(unit -> unit.getUnitRank() == Unit.UnitRank.values()[Unit.UnitRank.values().size()-3]).findAny().get()
		klingon.getComponentSystem().getComputer().fullRepair()
		klingon.getComponentSystem().getEnergyContainer().setEnergy(5)
		gData.getActivePlayer().getCivilization().setActiveUnits(gData.getActivePlayer().getCivilization().getUnits())

		when:
		def listOfFailures = moveValidator.validate(moveCommand, gData)

		then:
		assert listOfFailures.stream().anyMatch(f -> f instanceof NotEnoughResources)
	}

	//TODO: uncomment once action points issue is resolved
//	def "if unit is in force mode, and has too few action points, can perform shorter move and should not create ForceMoveFailed"() {
//		given:
//		GameSessionData gData = testUtils.getGameData()
//		Move moveCommand = new Move(UUID.randomUUID(), gData.activePlayer.uuid,
//				true, 19, 19, true)
//		Unit unit = gData.getActivePlayer().getCivilization().getActiveUnit()
//		unit.setActionPoints(3)
//		unit.getComponentSystem().getWarpEngines()
//				.setWarpFactor(WARP_FACTOR_SONIC_THRESHOLD-1)
//
//		when:
//		def listOfFailures = moveValidator.validate(moveCommand, gData)
//
//		then:
//		assert !listOfFailures.stream().anyMatch(x -> x instanceof ForcedMoveFailed)
//	}

//	//TODO: unignore after action points issue is fixed
//	@Ignore
//	def "if unit is in force mode, and has 0 action points, can not perform shorter move and should create ForceMoveFailed"() {
//		given:
//		GameSessionData gData = testUtils.getGameData()
//		Move moveCommand = new Move(UUID.randomUUID(), gData.activePlayer.uuid,
//				true, 19, 19, true)
//		Unit unit = gData.getActivePlayer().getCivilization().getActiveUnit()
//		unit.setActionPoints(0)
//
//		when:
//		def listOfFailures = moveValidator.validate(moveCommand, gData)
//
//		then:
//		assert listOfFailures.stream().anyMatch(x -> x instanceof ForcedMoveFailed)
//	}

	def "if unit is in force mode and has energy amount threatening life support, cannot perform shorter move and should create ForceMoveFailed"() {
		given:
		GameSessionData gData = testUtils.getGameData()
		Move moveCommand = new Move(UUID.randomUUID(), gData.activePlayer.uuid,
				autoMode, x, y, true)
		Unit enterprise = gData.getActivePlayer().getCivilization().getUnits().get(0)
		enterprise.getComponentSystem().getComputer().fullRepair()
		enterprise.setX(10)
		enterprise.setY(10)
		enterprise.getComponentSystem().getEnergyContainer().setEnergy(1)
		gData.getActivePlayer().getCivilization().setActiveUnits(List.of(enterprise))

		when:
		def listOfFailures = moveValidator.validate(moveCommand, gData)

		then:
		assert listOfFailures.stream().anyMatch(f -> f instanceof ForcedMoveFailed)

		where:
		autoMode  | x	| y
		true  	  | 19  | 19
		false  	  | 9  	| 9
	}

	def "if unit is in force mode and has energy but not sufficient amount to perform move, can perform shorter move and should not create ForceMoveFailed"() {
		given:
		GameSessionData gData = testUtils.getGameData()
		Move moveCommand = new Move(UUID.randomUUID(), gData.activePlayer.uuid, autoMode, x, y, true)
		Unit enterprise = gData.getActivePlayer().getCivilization().getUnits().get(0)
		enterprise.getComponentSystem().getComputer().fullRepair()
		enterprise.setX(10)
		enterprise.setY(10)
		enterprise.getComponentSystem().getEnergyContainer().setEnergy(50)
		gData.getActivePlayer().getCivilization().setActiveUnits(List.of(enterprise))

		when:
		def listOfFailures = moveValidator.validate(moveCommand, gData)

		then:
		assert !listOfFailures.stream().anyMatch(f -> f instanceof ForcedMoveFailed)

		where:
		autoMode  | x	| y
		true  	  | 19  | 19
		false  	  | 9  	| 9
	}

	//TODO: Adjust after resolving issue with action points
	/*def "if unit is in force mode and has not sufficient energy and not enough action points, can perform shorter move and should not create ForceMoveFailed"() {
		given:
		GameSessionData gData = testUtils.getGameData()
		Move moveCommand = new Move(UUID.randomUUID(), gData.activePlayer.uuid,
				true, 19, 19, true)
		Unit unit = gData.getActivePlayer().getCivilization().getActiveUnit()
		unit.setActionPoints(5)
		((EnergyContainer)unit.getComponentSystem()
				.getComponent(ComponentId.ENERGY_CONTAINER)).setEnergy(70)

		when:
		def listOfFailures = moveValidator.validate(moveCommand, gData)

		then:
		assert !listOfFailures.stream().anyMatch(x -> x instanceof ForcedMoveFailed)
	}*/

}
