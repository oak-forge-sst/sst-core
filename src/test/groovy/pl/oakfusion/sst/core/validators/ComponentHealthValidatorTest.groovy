package pl.oakfusion.sst.core.validators

import pl.oakfusion.sst.core.game.validators.ComponentHealthValidator
import pl.oakfusion.sst.data.validationfailures.ComponentLowHealthWarning
import pl.oakfusion.sst.data.validationfailures.ValidationFailure
import pl.oakfusion.sst.core.testutils.MockUnit
import pl.oakfusion.sst.data.game.commands.shields.ShieldsCommand
import pl.oakfusion.sst.data.world.civilizations.UnitSpecification
import pl.oakfusion.sst.data.world.component.ComponentId
import pl.oakfusion.sst.data.world.component.Condition
import pl.oakfusion.sst.data.world.component.Hull
import pl.oakfusion.sst.data.world.component.ComponentSystem
import pl.oakfusion.sst.data.world.component.DamagedFunctionalitySpecification
import pl.oakfusion.sst.data.world.component.Shields
import pl.oakfusion.testutils.SstTestUtils
import spock.lang.Specification
import spock.lang.Unroll

class ComponentHealthValidatorTest extends Specification {

	ShieldsCommand command = new ShieldsCommand(UUID.randomUUID(), UUID.randomUUID())
	SstTestUtils sstTestUtils = new SstTestUtils()
	ComponentSystem componentSystem = Mock()
	Shields shieldMock = Mock()
	def componentID = ComponentId.SHIELDS
    ComponentHealthValidator componentHealthValidator = new ComponentHealthValidator()
	def errorMessage = "Low health of component - high probability of failure. If you want to execute anyway send command with ignore component low health parameter."

	@Unroll
	def 'should return correctly validationFailures list size'() {

		given:
		shieldMock.calculateHealthPercentage() >> DamagedFunctionalitySpecification.DEFAULT_LEVEL_OF_FUNCTIONALITY + additionalValue
		componentSystem.getComponent(componentID) >> shieldMock
		componentSystem.getHull() >> new Hull()
		def unit = new MockUnit(0, 0, UUID.randomUUID(), componentSystem, new UnitSpecification(1, Condition.RED))
		unit.setObjectAlive(true)
		unit.setGetComponentSystem(componentSystem)
		sstTestUtils.getGameData().getActivePlayer().getCivilization().setActiveUnits(List.of(unit))

		when:
		List<ValidationFailure> result = componentHealthValidator.validate(command, sstTestUtils.getGameData())

		then:
		result.empty == booleanResult

		where:
		additionalValue | booleanResult
		-1              | false
		0               | false
		1               | true
	}

	def 'should return warning with correctly data'() {

		given:
		shieldMock.calculateHealthPercentage() >> DamagedFunctionalitySpecification.DEFAULT_LEVEL_OF_FUNCTIONALITY - 1
		componentSystem.getComponent(componentID) >> shieldMock
		componentSystem.getHull() >> new Hull()
		def unitUUID = UUID.randomUUID()
		def unit = new MockUnit(0, 0, UUID.randomUUID(), componentSystem, new UnitSpecification(1, Condition.RED))
		unit.setObjectAlive(true)
		unit.setUuid(unitUUID)
		unit.setGetUuid(unitUUID)
		unit.setGetComponentSystem(componentSystem)
		sstTestUtils.getGameData().getActivePlayer().getCivilization().setActiveUnits(List.of(unit))

		when:
        ComponentLowHealthWarning warning = componentHealthValidator
				.validate(command, sstTestUtils.getGameData()).get(0) as ComponentLowHealthWarning

		then:
		warning.errorMessage == errorMessage
		warning.getUnitUuid() == unitUUID
		warning.getComponentId() == componentID
	}

	@Unroll
	def 'should return correctly validationFailures list size for couple units'() {

		given:
		shieldMock.calculateHealthPercentage() >> DamagedFunctionalitySpecification.DEFAULT_LEVEL_OF_FUNCTIONALITY + additionalValue
		componentSystem.getComponent(componentID) >> shieldMock
		componentSystem.getHull() >> new Hull()
		def unit = new MockUnit(0, 0, UUID.randomUUID(), componentSystem, new UnitSpecification(1, Condition.RED))
		def unit1 = new MockUnit(0, 0, UUID.randomUUID(), componentSystem, new UnitSpecification(1, Condition.RED))
		unit.setObjectAlive(true)
		unit1.setObjectAlive(true)
		unit.setGetComponentSystem(componentSystem)
		unit1.setGetComponentSystem(componentSystem)
		sstTestUtils.getGameData().getActivePlayer().getCivilization().setActiveUnits(List.of(unit, unit1))

		when:
		List<ValidationFailure> result = componentHealthValidator.validate(command, sstTestUtils.getGameData())

		then:
		result.empty == booleanResult

		where:
		additionalValue | booleanResult
		-1              | false
		0               | false
		1               | true
	}

	def 'should not return any exception when unit list is empty'() {

		given:
		shieldMock.calculateHealthPercentage() >> DamagedFunctionalitySpecification.DEFAULT_LEVEL_OF_FUNCTIONALITY - 1
		componentSystem.getComponent(componentID) >> shieldMock
		componentSystem.getHull() >> new Hull()
		sstTestUtils.getGameData().getActivePlayer().getCivilization().setActiveUnits(List.of())

		when:
		componentHealthValidator
				.validate(command, sstTestUtils.getGameData())

		then:
		noExceptionThrown()
	}
}
