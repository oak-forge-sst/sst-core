package pl.oakfusion.sst.core.validators

import pl.oakfusion.sst.data.validationfailures.NoTechnologyError
import pl.oakfusion.sst.core.game.validators.TechnologyValidator
import pl.oakfusion.sst.data.validationfailures.ValidationFailure
import pl.oakfusion.sst.data.game.commands.shields.ShieldsCommand
import pl.oakfusion.sst.data.world.civilizations.UnitSpecification
import pl.oakfusion.sst.data.world.component.Condition
import pl.oakfusion.sst.data.world.component.ComponentSystem
import pl.oakfusion.testutils.SstTestUtils
import spock.lang.Specification
import pl.oakfusion.sst.core.testutils.MockUnit
class TechnologyValidatorTest extends Specification {

	ShieldsCommand command = new ShieldsCommand(UUID.randomUUID(), UUID.randomUUID())
	SstTestUtils sstTestUtils = new SstTestUtils()
	ComponentSystem componentSystem = Mock()
    TechnologyValidator technologyValidator = new TechnologyValidator()
	def unit = new MockUnit(0, 0, UUID.randomUUID(), componentSystem, new UnitSpecification(1, Condition.RED))

	def 'should return validationFailures list with NoTechnologyError'() {

		given:
		sstTestUtils.getGameData().getActivePlayer().getCivilization().setActiveUnits(List.of(unit))
		unit.setCanPerformCommand(false)
		unit.setObjectAlive(true)
		when:
		List<ValidationFailure> result = technologyValidator.validate(command, sstTestUtils.getGameData())

		then:
		!result.empty
		result.get(0).getClass().isCase(NoTechnologyError.class)
	}

	def 'should return empty validationFailures list'() {

		given:
		sstTestUtils.getGameData().getActivePlayer().getCivilization().setActiveUnits(List.of(unit))
		unit.setCanPerformCommand(true)
		unit.setObjectAlive(true)

		when:
		List<ValidationFailure> result = technologyValidator.validate(command, sstTestUtils.getGameData())

		then:
		result.empty
	}

	def 'should return NoTechnologyError with correctly data'() {

		given:
		def unitUUID = UUID.randomUUID()
		unit.setUuid(unitUUID)
		sstTestUtils.getGameData().getActivePlayer().getCivilization().setActiveUnits(List.of(unit))
		unit.setCanPerformCommand(false)
		unit.setObjectAlive(true)
		unit.setGetUuid(unitUUID)

		when:
		NoTechnologyError result = technologyValidator.validate(command, sstTestUtils.getGameData()).get(0) as NoTechnologyError

		then:
		result.getErrorMessage() == ""
		result.getUnitUuid() == unitUUID
	}

	def 'should not return any exception when unit list is empty'() {

		given:
		sstTestUtils.getGameData().getActivePlayer().getCivilization().setActiveUnits(List.of())

		when:
		technologyValidator.validate(command, sstTestUtils.getGameData())

		then:
		noExceptionThrown()
	}
}
