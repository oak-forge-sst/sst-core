package pl.oakfusion.sst.core.validators

import pl.oakfusion.sst.core.gamesession.validators.StartGameCommandValidator
import pl.oakfusion.sst.data.validationfailures.StartGameFailed
import pl.oakfusion.sst.data.validationfailures.ValidationFailure
import pl.oakfusion.testutils.SstTestUtils
import pl.oakfusion.sst.data.gamesession.GameSessionState
import pl.oakfusion.sst.data.gamesession.commands.StartGame
import spock.lang.Specification

class StartGameCommandValidatorTest extends Specification {
	SstTestUtils sstTestUtils = new SstTestUtils()
    StartGameCommandValidator validator = new StartGameCommandValidator()

	def "if player is not game master should return list with start game failed and message not game master"() {
		given:
		sstTestUtils.getGameData().getPlayersRepository().getPlayerList().get(0).setGameMaster(false)
		UUID playerUuid = sstTestUtils.getGameData().getPlayersRepository().getPlayerList().get(0).getUuid()

		when:
		List<ValidationFailure> failureList = validator.validate(new StartGame(UUID.randomUUID(), playerUuid),
				sstTestUtils.getGameData())

		then:
		assert failureList.get(0) instanceof StartGameFailed
		failureList.get(0).toString().contains("You're not game master.")

	}

	def "if player is game master and game is running should return list with start game failed and message game is running"() {
		given:
		sstTestUtils.getGameData().getPlayersRepository().getPlayerList().get(0).setGameMaster(true)
		UUID playerUuid = sstTestUtils.getGameData().getPlayersRepository().getPlayerList().get(0).getUuid()
		sstTestUtils.getGameData().setGameSessionState(GameSessionState.RUNNING)

		when:
		List<ValidationFailure> failureList = validator.validate(new StartGame(UUID.randomUUID(), playerUuid),
				sstTestUtils.getGameData())

		then:
		assert failureList.get(0) instanceof StartGameFailed
		failureList.get(0).toString().contains("Game already started.")

	}

	def "if player is game master and game is not running should return empty list"() {
		given:
		sstTestUtils.getGameData().getPlayersRepository().getPlayerList().get(0).setGameMaster(true)
		UUID playerUuid = sstTestUtils.getGameData().getPlayersRepository().getPlayerList().get(0).getUuid()
		sstTestUtils.getGameData().setGameSessionState(GameSessionState.NOT_RUNNING)

		when:
		List<ValidationFailure> failureList = validator.validate(new StartGame(UUID.randomUUID(), playerUuid),
				sstTestUtils.getGameData())

		then:
		failureList.isEmpty()

	}
}
