package pl.oakfusion.sst.core.validators

import pl.oakfusion.sst.core.gamedata.GameSessionData
import pl.oakfusion.sst.core.game.validators.ImpulseEstimationCache
import pl.oakfusion.sst.core.game.validators.ImpulseValidator
import pl.oakfusion.sst.data.validationfailures.ValidationFailure
import pl.oakfusion.sst.data.validationfailures.ForcedMoveFailed
import pl.oakfusion.sst.data.validationfailures.NotEnoughResources
import pl.oakfusion.sst.data.game.commands.impulse.Impulse
import pl.oakfusion.sst.data.game.events.move.MoveCollisionDetected
import pl.oakfusion.sst.data.game.events.move.MoveFailed
import pl.oakfusion.sst.data.game.events.move.MoveOutOfBorderDetected
import pl.oakfusion.sst.data.game.events.move.NotEnoughActionPoints
import pl.oakfusion.sst.data.world.component.ComponentId
import pl.oakfusion.sst.data.world.civilizations.AbstractUnitFactory
import pl.oakfusion.sst.data.world.civilizations.Unit
import pl.oakfusion.sst.data.world.component.EnergyContainer
import pl.oakfusion.testutils.SstTestUtils
import spock.lang.Specification

class ImpulseValidatorTest extends Specification {


	ImpulseValidator validator = new ImpulseValidator(new ImpulseEstimationCache())
	SstTestUtils testUtils = new SstTestUtils()
	//UNIT STARTING POINT = (10, 10)

	def "if player wants to move outside the world boundaries should return MoveOutOfBorderDetected validation failure (manual)"() {
		given:
		GameSessionData gData = testUtils.getGameData()
		Impulse command = new Impulse(UUID.randomUUID(), gData.activePlayer.uuid, x, y, false, false)

		when:
		ArrayList<ValidationFailure> listOfFailures = validator.validate(command, gData)

		then:
		assert listOfFailures.stream().anyMatch(c -> c instanceof MoveOutOfBorderDetected)

		where:
		x 	|	y
		50	|	50
		3	|	100
		100	|	3
	}

	def "if player wants to move outside the world boundaries should return MoveOutOfBorderDetected validation failure (auto)"() {
		given:
		GameSessionData gData = testUtils.getGameData()
		Impulse moveCommand2 = new Impulse(UUID.randomUUID(), gData.activePlayer.uuid,  x, y, true, false)

		when:
		ArrayList<ValidationFailure> listOfFailures2 = validator.validate(moveCommand2, gData)

		then:
		assert listOfFailures2.stream().anyMatch(c -> c instanceof MoveOutOfBorderDetected)

		where:
		x 	|	y
		-1 	|	-1
		-1	|	0
		0 	|	-1
		50	|	50
		3	|	100
		100	|	3
	}

	def "if player wants to move within the world boundaries should not return MoveOutOfBorderDetected validation failure (auto)"() {
		given:
		GameSessionData gData = testUtils.getGameData()
		Impulse moveCommand2 = new Impulse(UUID.randomUUID(), gData.activePlayer.uuid, x, y,true, false)

		when:
		ArrayList<ValidationFailure> listOfFailures2 = validator.validate(moveCommand2, gData)

		then:
		assert !listOfFailures2.stream().anyMatch(c -> c instanceof MoveOutOfBorderDetected)

		where:
		x << [1, 10, 3, 14, 0]
		y << [2, 5, 9, 13, 1]
	}

	def "if player wants to move within the world boundaries should not return MoveOutOfBorderDetected validation failure (manual)"() {
		given:
		GameSessionData gData = testUtils.getGameData()
		Impulse moveCommand = new Impulse(UUID.randomUUID(), gData.activePlayer.uuid,  x, y, false, false)

		when:
		ArrayList<ValidationFailure> listOfFailures = validator.validate(moveCommand, gData)

		then:
		assert !listOfFailures.stream().anyMatch(c -> c instanceof MoveOutOfBorderDetected)

		where:
		x << [1, -2, 3, -4, 0]
		y << [2, -1, 9, 5, 1]
	}

	def "if unit collision in target point has been detected should return MoveCollisionDetected validation failure"() {
		GameSessionData gData = testUtils.getGameData()
		Impulse moveCommand = new Impulse(UUID.randomUUID(), gData.activePlayer.uuid, 3, 3, true, false)

		gData.getWorld().add(testUtils.unitFactory.createKlingon(UUID.randomUUID()).create(3,3))
		Unit unit = gData.getActivePlayer().getCivilization().getActiveUnits().get(0)
		unit.setX(1)
		unit.setY(11)

		when:
		ArrayList<ValidationFailure> listOfFailures = validator.validate(moveCommand, gData)

		then:
		assert listOfFailures.stream().anyMatch(c -> c instanceof MoveCollisionDetected)
	}

	def "if unit collision in target point has been detected should return MoveCollisionDetected validation failure with super sonic speed"() {
		GameSessionData gData = testUtils.getGameData()
		Impulse moveCommand = new Impulse(UUID.randomUUID(), gData.activePlayer.uuid,  3, 3,true, false)

		gData.getWorld().add(testUtils.unitFactory.createKlingon(UUID.randomUUID()).create(3,3))
		Unit unit = gData.getActivePlayer().getCivilization().getActiveUnits().get(0)
		unit.getComponentSystem().getWarpEngines().setWarpFactor(10)
		unit.setX(1)
		unit.setY(11)

		when:
		ArrayList<ValidationFailure> listOfFailures = validator.validate(moveCommand, gData)

		then:
		assert listOfFailures.stream().anyMatch(c -> c instanceof MoveCollisionDetected)
	}

	def "if computer is down and move is in auto mode should return AutoMoveFailed validation failure"() {
		given:
		GameSessionData gData = testUtils.getGameData()
		Impulse moveCommand = new Impulse(UUID.randomUUID(), gData.activePlayer.uuid,  1, 1,true, false)
		Unit unit = gData.getActivePlayer().getCivilization().getActiveUnits().get(0)
		unit.setActionPoints(100)
		unit.getComponentSystem().getComputer().setCurrentHealth(0)
		unit.getComponentSystem().getWarpEngines().setWarpFactor(1)

		when:
		ArrayList<ValidationFailure> listOfFailures = validator.validate(moveCommand, gData)

		then:
		assert listOfFailures.stream().anyMatch(c -> c instanceof MoveFailed)

	}

	def "if computer is not down and move is on auto mode should not return AutoMoveFailed validation failure"() {
		given:
		GameSessionData gData = testUtils.getGameData()
		Impulse moveCommand = new Impulse(UUID.randomUUID(), gData.activePlayer.uuid,  1, 1,true, false)
		Unit unit = gData.getActivePlayer().getCivilization().getActiveUnits().get(0)
		unit.setActionPoints(100)
		unit.getComponentSystem().getComputer().setCurrentHealth(1000)

		when:
		ArrayList<ValidationFailure> listOfFailures = validator.validate(moveCommand, gData)

		then:
		assert !listOfFailures.stream().anyMatch(c -> c instanceof MoveFailed)

	}

	def "if player wants to make a valid move should return no failures"() {
		given:
		GameSessionData gData = testUtils.getGameData()
		Impulse moveCommand = new Impulse(UUID.randomUUID(), gData.activePlayer.uuid,  5, 5,true, false)

		when:
		ArrayList<ValidationFailure> listOfFailures = validator.validate(moveCommand, gData)

		then:
		assert listOfFailures.isEmpty()

	}

	def "should return MoveCollisionDetected when there is no path without obstacles to target point and no sonic speed (manual move)"() {
		GameSessionData gData = testUtils.getGameData()
		Impulse moveCommand = new Impulse(UUID.randomUUID(), gData.activePlayer.uuid,  3, 3, false, false)
		AbstractUnitFactory unitFactory = testUtils.getUnitFactory()

		for (coords in [[1,0],[1,1],[0,1]]) {
			gData.getWorld().add(unitFactory.createKlingon(UUID.randomUUID()).create(coords[0], coords[1]))
		}
		Unit unit = gData.getActivePlayer().getCivilization().getActiveUnits().get(0)
		unit.setX(0)
		unit.setY(0)

		when:
		ArrayList<ValidationFailure> listOfFailures = validator.validate(moveCommand, gData)

		then:
		assert listOfFailures.stream().anyMatch(c -> c instanceof MoveCollisionDetected)
	}




	def "should return MoveCollisionDetected when there is no path without obstacles to target point and no sonic speed (auto)"() {
		GameSessionData gData = testUtils.getGameData()
		Impulse moveCommand = new Impulse(UUID.randomUUID(), gData.activePlayer.uuid,  3, 3, true, false)
		AbstractUnitFactory unitFactory = testUtils.getUnitFactory()

		for (coords in [[1,0], [1,1], [0,1]]) {
			gData.getWorld().add(unitFactory.createKlingon(UUID.randomUUID()).create(coords[0], coords[1]))
		}

		Unit unit = gData.getActivePlayer().getCivilization().getActiveUnits().get(0)
		unit.getComponentSystem().getWarpEngines().setWarpFactor(0)
		unit.setX(0)
		unit.setY(0)

		when:
		ArrayList<ValidationFailure> listOfFailures = validator.validate(moveCommand, gData)

		then:
		assert listOfFailures.stream().anyMatch(c -> c instanceof MoveCollisionDetected)
	}


	def "should not return MoveCollisionDetected when there is a path without obstacles to target point (auto)"() {
		GameSessionData gData = testUtils.getGameData()
		Impulse moveCommand = new Impulse(UUID.randomUUID(), gData.activePlayer.uuid, 3, 3,true, false)
		AbstractUnitFactory unitFactory = testUtils.getUnitFactory()


		for (coords in [[1,0], [1,1], [2,1], [3,1], [4,1]]) {
			gData.getWorld().add(unitFactory.createKlingon(UUID.randomUUID()).create(coords[0], coords[1]))
		}

		Unit unit = gData.getActivePlayer().getCivilization().getActiveUnits().get(0)
		unit.setX(0)
		unit.setY(0)

		when:
		ArrayList<ValidationFailure> listOfFailures = validator.validate(moveCommand, gData)

		then:
		assert !listOfFailures.stream().anyMatch(c -> c instanceof MoveCollisionDetected)
	}

	//TODO: uncomment once action points issue is resolved
//	def "if unit is not in force mode does not have enough action points should return NotEnoughActionPoints validation failure"() {
//		given:
//		GameSessionData gData = testUtils.getGameData()
//		Impulse moveCommand = new Impulse(UUID.randomUUID(), gData.activePlayer.uuid, true, 1, 1, false)
//		Unit unit = gData.getActivePlayer().getCivilization().getActiveUnits().get(0
//		unit.setActionPoints(0)
//
//		when:
//		ArrayList<ValidationFailure> listOfFailures = moveValidator.validate(moveCommand, gData)
//
//		then:
//		assert listOfFailures.stream().anyMatch(c -> c instanceof NotEnoughActionPoints)
//
//	}

	def "if unit has enough action points should not return NotEnoughActionPoints validation failure"() {
		given:
		GameSessionData gData = testUtils.getGameData()
		Impulse moveCommand = new Impulse(UUID.randomUUID(), gData.activePlayer.uuid, 1, 1,true, false)
		Unit unit = gData.getActivePlayer().getCivilization().getActiveUnits().get(0)
		unit.setActionPoints(100)

		when:
		ArrayList<ValidationFailure> listOfFailures = validator.validate(moveCommand, gData)

		then:
		assert !listOfFailures.stream().anyMatch(c -> c instanceof NotEnoughActionPoints)

	}

	def "if unit has enough resources should not return NotEnoughResources validation failure"() {
		given:
		GameSessionData gData = testUtils.getGameData()
		Impulse moveCommand = new Impulse(UUID.randomUUID(), gData.activePlayer.uuid,  10, 0,true, forceMode)
		Unit unit = gData.getActivePlayer().getCivilization().getActiveUnits().get(0)
		unit.getComponentSystem().getEnergyContainer().setEnergy(1000)
		unit.getComponentSystem().getWarpEngines().setWarpFactor(6)

		when:
		def listOfFailures = validator.validate(moveCommand, gData)

		then:
		assert !listOfFailures.stream().anyMatch(x -> x instanceof NotEnoughResources)

		where:
		forceMode << [true, false]
	}

	def "if unit does not have enough resources, should return NotEnoughResources validation failure"() {
		given:
		GameSessionData gData = testUtils.getGameData()
		Impulse moveCommand = new Impulse(UUID.randomUUID(), gData.activePlayer.uuid,  19, 19,true, false)
		Unit unit = gData.getActivePlayer().getCivilization().getActiveUnits().get(0)
		unit.getComponentSystem().getEnergyContainer().setEnergy(0)

		when:
		def listOfFailures = validator.validate(moveCommand, gData)

		then:
		assert listOfFailures.stream().anyMatch(x -> x instanceof NotEnoughResources)
	}

	//TODO: uncomment once action points issue is resolved
//	def "if unit is in force mode, and has too few action points, can perform shorter move and should not create ForceMoveFailed"() {
//		given:
//		GameSessionData gData = testUtils.getGameData()
//		Impulse moveCommand = new Impulse(UUID.randomUUID(), gData.activePlayer.uuid,
//				true, 19, 19, true)
//		Unit unit = gData.getActivePlayer().getCivilization().getActiveUnits().get(0
//		unit.setActionPoints(3)
//		unit.getComponentSystem().getWarpEngines()
//				.setWarpFactor(WARP_FACTOR_SONIC_THRESHOLD-1)
//
//		when:
//		def listOfFailures = moveValidator.validate(moveCommand, gData)
//
//		then:
//		assert !listOfFailures.stream().anyMatch(x -> x instanceof ForcedMoveFailed)
//	}

//	//TODO: unignore after action points issue is fixed
//	@Ignore
//	def "if unit is in force mode, and has 0 action points, can not perform shorter move and should create ForceMoveFailed"() {
//		given:
//		GameSessionData gData = testUtils.getGameData()
//		Impulse moveCommand = new Impulse(UUID.randomUUID(), gData.activePlayer.uuid,
//				true, 19, 19, true)
//		Unit unit = gData.getActivePlayer().getCivilization().getActiveUnits().get(0
//		unit.setActionPoints(0)
//
//		when:
//		def listOfFailures = moveValidator.validate(moveCommand, gData)
//
//		then:
//		assert listOfFailures.stream().anyMatch(x -> x instanceof ForcedMoveFailed)
//	}


	def "if unit is in force mode and has energy amount threatening life support, can not perform shorter move and should create ForceMoveFailed"() {
		given:
		GameSessionData gData = testUtils.getGameData()
		Impulse moveCommand = new Impulse(UUID.randomUUID(), gData.activePlayer.uuid,
				 18, 18,true, true)
		Unit unit = gData.getActivePlayer().getCivilization().getActiveUnits().get(0)
		unit.setActionPoints(100)
		((EnergyContainer)unit.getComponentSystem()
				.getComponent(ComponentId.ENERGY_CONTAINER)).setEnergy(0)

		when:
		def listOfFailures = validator.validate(moveCommand, gData)

		then:
		assert listOfFailures.stream().anyMatch(x -> x instanceof ForcedMoveFailed)
	}

	def "if unit is in force mode and has energy but not sufficient amount to perform move, can perform shorter move and should not create ForceMoveFailed"() {
		given:
		GameSessionData gData = testUtils.getGameData()
		Impulse moveCommand = new Impulse(UUID.randomUUID(), gData.activePlayer.uuid,
				 19, 19, true, true)
		Unit unit = gData.getActivePlayer().getCivilization().getActiveUnits().get(0)
		unit.setActionPoints(100)
		((EnergyContainer)unit.getComponentSystem()
				.getComponent(ComponentId.ENERGY_CONTAINER)).setEnergy(70)

		when:
		def listOfFailures = validator.validate(moveCommand, gData)

		then:
		assert !listOfFailures.stream().anyMatch(x -> x instanceof ForcedMoveFailed)
	}

	def "if unit is in force mode and has not sufficient energy and not enough action points, can perform shorter move and should not create ForceMoveFailed"() {
		given:
		GameSessionData gData = testUtils.getGameData()
		Impulse moveCommand = new Impulse(UUID.randomUUID(), gData.activePlayer.uuid,
				 19, 19,true, true)
		Unit unit = gData.getActivePlayer().getCivilization().getActiveUnits().get(0)
		unit.setActionPoints(5)
		((EnergyContainer)unit.getComponentSystem()
				.getComponent(ComponentId.ENERGY_CONTAINER)).setEnergy(70)

		when:
		def listOfFailures = validator.validate(moveCommand, gData)

		then:
		assert !listOfFailures.stream().anyMatch(x -> x instanceof ForcedMoveFailed)
	}

}
