package pl.oakfusion.sst.core.validators

import pl.oakfusion.sst.data.validationfailures.JoinGameFailed
import pl.oakfusion.sst.core.gamesession.validators.JoinGameSessionCommandValidator
import pl.oakfusion.sst.data.validationfailures.ValidationFailure
import pl.oakfusion.testutils.SstTestUtils
import pl.oakfusion.sst.data.gamesession.GameSessionState
import pl.oakfusion.sst.data.gamesession.commands.JoinGameSession
import pl.oakfusion.sst.data.world.player.CivilizationType
import spock.lang.Specification
import spock.lang.Unroll

class JoinGameSessionCommandValidatorTest extends Specification {
	JoinGameSessionCommandValidator validator = new JoinGameSessionCommandValidator()
	SstTestUtils sstTestUtils = new SstTestUtils()

	@Unroll
	def "if game session state is running should return list with join game failed and message cannot join running game"() {
		given:
		sstTestUtils.getGameData().setGameSessionState(GameSessionState.RUNNING)

		when:
		List<ValidationFailure> failureList = validator.validate(new JoinGameSession(UUID.randomUUID(), UUID.randomUUID(), CivilizationType.FEDERATION, ""),
				sstTestUtils.getGameData())

		then:
		assert failureList.get(0) instanceof JoinGameFailed
		failureList.get(0).toString().contains("Cannot join running game.")
	}

	@Unroll
	def "if maximum number of players reached should return list with join game failed and message that max number of players reached"() {
		given:
		def numberOfRegisteredPlayers = sstTestUtils.getPlayersRepository().getPlayerList().size()
		sstTestUtils.getGameData().setMaximumNumberOfPlayers(numberOfRegisteredPlayers)

		when:
		List<ValidationFailure> failureList = validator.validate(new JoinGameSession(UUID.randomUUID(), UUID.randomUUID(), CivilizationType.FEDERATION, ""),
				sstTestUtils.getGameData())

		then:
		assert failureList.get(0) instanceof JoinGameFailed
		failureList.get(0).toString().contains("Maximum number of players in game session reached.")
	}

	@Unroll
	def "if player with given name is in game should return list with join game failed and message that name is used"() {
		given:
		def numberOfRegisteredPlayers = sstTestUtils.getPlayersRepository().getPlayerList().size()
		sstTestUtils.getGameData().setMaximumNumberOfPlayers(numberOfRegisteredPlayers + 1)
		def name = sstTestUtils.getGameData().getPlayersRepository().getPlayerList().getFirst().getPlayerName()

		when:
		List<ValidationFailure> failureList = validator.validate(new JoinGameSession(UUID.randomUUID(), UUID.randomUUID(), CivilizationType.FEDERATION, name),
				sstTestUtils.getGameData())

		then:
		assert failureList.get(0) instanceof JoinGameFailed
		failureList.get(0).toString().contains("Player with given name is in game.")
	}
}
