package pl.oakfusion.sst.core.validators

import pl.oakfusion.sst.core.gamedata.GameSessionData
import pl.oakfusion.sst.data.game.UnitMoveDestination
import pl.oakfusion.sst.data.game.UnitsMoveDestinations
import pl.oakfusion.sst.core.game.validators.MoveCache
import pl.oakfusion.sst.data.game.MoveEstimation
import pl.oakfusion.sst.data.util.Position
import pl.oakfusion.sst.data.world.World
import pl.oakfusion.sst.data.world.civilizations.Unit
import pl.oakfusion.sst.data.world.component.Computer
import pl.oakfusion.testutils.SstTestUtils
import spock.lang.Specification

class MoveCacheTest extends Specification {
	MoveCache moveCache = new MoveCache()
	SstTestUtils testUtils = new SstTestUtils()
	GameSessionData gameData = testUtils.getGameData()
	Unit unit1 = gameData.getPlayersRepository().getPlayerList().get(0).getCivilization().getUnits().get(0)
	Unit unit2 = gameData.getPlayersRepository().getPlayerList().get(1).getCivilization().getUnits().get(0)
	Computer unit1Computer = unit1.getComponentSystem().getComputer()
	Computer unit2Computer = unit2.getComponentSystem().getComputer()
	World world = gameData.getWorld()
	UnitMoveDestination unitMoveDestination1
	UnitMoveDestination unitMoveDestination2
	List<UUID> playerUuidList = new ArrayList<>()

	def setup(){
		Position destination1 = new Position(3, 3)
		Position destination2 = new Position(5, 5)
		List<Position> path1 = world.findShortestPathWarp(unit1.getPosition(), destination1)
		List<Position> path2 = world.findShortestPathWarp(unit2.getPosition(), destination2)
		MoveEstimation estimation1 = unit1Computer.estimateMove(world, unit1, path1, unit1.getPosition(), destination1)
		MoveEstimation estimation2 = unit2Computer.estimateMove(world, unit2, path2, unit2.getPosition(), destination2)
		unitMoveDestination1 = new UnitMoveDestination(unit1, destination1, estimation1, true)
		unitMoveDestination2 = new UnitMoveDestination(unit2, destination2, estimation2, true)
		for(int i = 0; i <= 12; i++){
			playerUuidList.add(UUID.randomUUID())
		}
	}

	def "should replace old destination for a new on when setting under same player UUID"(){
		given:
		UnitsMoveDestinations unitsMoveDestinations1 = new UnitsMoveDestinations()
		UnitsMoveDestinations unitsMoveDestinations2 = new UnitsMoveDestinations()
		unitsMoveDestinations1.addDestination(unitMoveDestination1)
		unitsMoveDestinations2.addDestination(unitMoveDestination2)
		moveCache.setDestinations(playerUuidList.get(0), unitsMoveDestinations1)

		when:
		moveCache.setDestinations(playerUuidList.get(0), unitsMoveDestinations2)

		then:
		moveCache.getDestinations(playerUuidList.get(0)).orElseThrow() == unitsMoveDestinations2
	}

	//TODO: sleep has to be added here, because there is a problem probably with LocalDateTime in MoveCache when records are added with too short intervals, remove sleep after changing implementation of MoveCache
	//https://gitlab.com/oak-forge-sst/sst-core/-/issues/47
	def "should remove oldest record after reaching more than 10 records"(){
		given:
		UnitsMoveDestinations unitsMoveDestinations = new UnitsMoveDestinations()
		unitsMoveDestinations.addDestination(unitMoveDestination1)

		when:
		for(int i = 0; i <= 12; i++){
			moveCache.setDestinations(playerUuidList.get(i), unitsMoveDestinations)
			sleep(100)
		}

		then:
		moveCache.getDestinations(playerUuidList.get(0)).isEmpty()
	}
}
