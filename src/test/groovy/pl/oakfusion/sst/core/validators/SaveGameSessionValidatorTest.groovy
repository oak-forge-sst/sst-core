package pl.oakfusion.sst.core.validators

import pl.oakfusion.sst.data.validationfailures.InvalidRecordName
import pl.oakfusion.sst.data.validationfailures.RecordAlreadyExist
import pl.oakfusion.sst.data.validationfailures.SaveFailed
import pl.oakfusion.sst.core.gamesession.validators.SaveGameSessionValidator
import pl.oakfusion.sst.data.validationfailures.ValidationFailure
import pl.oakfusion.testutils.SstTestUtils
import pl.oakfusion.sst.core.gamesession.FileManager
import pl.oakfusion.sst.data.parameter.GameType
import pl.oakfusion.sst.data.gamesession.SavedGameData
import pl.oakfusion.sst.data.gamesession.commands.SaveGameSession
import spock.lang.Specification
import spock.lang.Unroll

class SaveGameSessionValidatorTest extends Specification {
	SaveGameSessionValidator validator = new SaveGameSessionValidator()
	SstTestUtils sstTestUtils = new SstTestUtils()

	@Unroll
	def "if game is multiplayer should return list with save failed and message cannot save multiplayer game"() {
		given:
		sstTestUtils.getGameData().setGameType(GameType.MULTIPLAYER)

		when:
		List<ValidationFailure> failureList = validator.validate(new SaveGameSession(UUID.randomUUID(), UUID.randomUUID(), "", ""),
				sstTestUtils.getGameData())

		then:
		assert failureList.get(0) instanceof SaveFailed
		failureList.get(0).toString().contains("Cannot save multiplayer game")
	}

	@Unroll
	def "if name is not valid should return list with invalid record name and message change name"() {
		when:
		List<ValidationFailure> failureList = validator.validate(new SaveGameSession(UUID.randomUUID(), UUID.randomUUID(), name, ""),
				sstTestUtils.getGameData())

		then:
		assert failureList.get(0) instanceof InvalidRecordName
		failureList.get(0).toString().contains("Change name")

		where:
		name << ["3", "0.5", "abc?"]
	}

	@Unroll
	def "if name exists should return list with record already exists and message change name"() {
		given:
		FileManager fileManager = Stub()
		fileManager.readSavedGames() >> [new SavedGameData(name, "", new Date())]
		println (fileManager.readSavedGames())
		validator.fileManager = fileManager

		when:
		List<ValidationFailure> failureList = validator.validate(new SaveGameSession(UUID.randomUUID(), UUID.randomUUID(), name, ""),
				sstTestUtils.getGameData())

		then:
		assert failureList.get(0) instanceof RecordAlreadyExist
		failureList.get(0).toString().contains("Change name")

		where:
		name << ["abc"]
	}

	@Unroll
	def "if everything ok should return empty list"() {
		given:
		FileManager fileManager = Stub()
		fileManager.readSavedGames() >> [new SavedGameData(name, "", new Date())]
		println (fileManager.readSavedGames())
		validator.fileManager = fileManager

		when:
		List<ValidationFailure> failureList = validator.validate(new SaveGameSession(UUID.randomUUID(), UUID.randomUUID(), nameToSave, ""),
				sstTestUtils.getGameData())

		then:
		failureList.isEmpty()

		where:
		name << ["abc"]
		nameToSave << ["def"]
	}
}
