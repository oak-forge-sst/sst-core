package pl.oakfusion.sst.core.validators

import pl.oakfusion.sst.core.gamedata.GameSessionData
import pl.oakfusion.sst.core.game.validators.TorpedoValidator
import pl.oakfusion.sst.data.validationfailures.ValidationFailure
import pl.oakfusion.sst.data.game.commands.torpedo.Torpedo
import pl.oakfusion.sst.data.game.events.torpedoes.TorpedoLauncherOverloaded
import pl.oakfusion.sst.data.game.events.torpedoes.TorpedoesLessThanIntendedToShot
import pl.oakfusion.sst.data.util.Position
import pl.oakfusion.sst.data.world.component.ComponentId
import pl.oakfusion.sst.data.world.civilizations.Unit
import pl.oakfusion.sst.data.world.component.TorpedoesDevice
import pl.oakfusion.testutils.SstTestUtils
import spock.lang.Specification

class TorpedoValidatorTest extends Specification {

	TorpedoValidator torpedoValidator = new TorpedoValidator()
	SstTestUtils testUtils = new SstTestUtils()

	def "if player wants to shoot excessive number of torpedoes should return TorpedoesLessThanIntendedToShoot validation failure"() {
		given:
		GameSessionData gData = testUtils.getGameData()
		gData.getActivePlayer().getCivilization().setActiveUnits(gData.getActivePlayer().getCivilization().getUnits())
		((TorpedoesDevice)gData.getActivePlayer().getCivilization()
				.getActiveUnits().get(0).getComponentSystem().getComponent(ComponentId.TORPEDOES_DEVICE))
				.setTorpedoes(0)
		UUID currentUnitUUID = gData.getActivePlayer().getUuid()

		Torpedo torpedoCommand = new Torpedo(UUID.randomUUID(), currentUnitUUID, 1,
				List.of(new Position(3,3)))

		when:
		var failures = torpedoValidator.validate(torpedoCommand, gData)

		then:
		assert failures.stream().anyMatch(c -> c instanceof TorpedoesLessThanIntendedToShot)
	}

	def "if player has more torpedoes than enterprise spec allows return TorpedoLauncherOverloaded validation failure"() {

		given:

		GameSessionData gData = testUtils.getGameData()
		gData.getActivePlayer().getCivilization().setActiveUnits(gData.getActivePlayer().getCivilization().getUnits())
		Unit unit = gData.getActivePlayer().getCivilization().getActiveUnits().get(0)
		int moreThanMaxTorps =  unit.getComponentSystem().getTorpedoesDevice().getMaxTorpedoes() + 10

		((TorpedoesDevice)unit.getComponentSystem().getComponent(ComponentId.TORPEDOES_DEVICE))
				.setTorpedoes(moreThanMaxTorps+20)
		UUID currentUnitUUID = gData.getActivePlayer().getUuid()

		Torpedo torpedoCommand = new Torpedo(UUID.randomUUID(), currentUnitUUID,
				moreThanMaxTorps, List.of(new Position(3,3)))

		when:
		List<ValidationFailure> failures = torpedoValidator.validate(torpedoCommand, gData)

		then:
		assert failures.stream().anyMatch(c -> c instanceof TorpedoLauncherOverloaded)


	}

	def "should pass torpedo command"() {
		given:

		GameSessionData gData = testUtils.getGameData()
		gData.getActivePlayer().getCivilization().setActiveUnits(gData.getActivePlayer().getCivilization().getUnits())
		((TorpedoesDevice)gData.getActivePlayer().getCivilization()
				.getActiveUnits().get(0).getComponentSystem().getComponent(ComponentId.TORPEDOES_DEVICE))
				.setTorpedoes(10)
		UUID currentUnitUUID = gData.getActivePlayer().getUuid()

		Torpedo torpedoCommand = new Torpedo(UUID.randomUUID(), currentUnitUUID,
				3, List.of(new Position(3,3)))

		when:
		List<ValidationFailure> failures = torpedoValidator.validate(torpedoCommand, gData)

		then:
		assert failures.isEmpty()

	}

}
