package pl.oakfusion.sst.core.aggregates

import spock.lang.Specification

import static pl.oakfusion.sst.core.aggregates.HelpResolver.NO_COMMAND

class HelpResolverSpecification extends Specification {

	def 'return command on correct mnemonic'() {
		given:
		def helpResolver = new HelpResolver()
		def mnemonic = "SRSCAN"

		when:
		def temp = helpResolver.resolve(mnemonic)

		then:
		temp.contains("SRSCAN")
		!temp.contains("#")
	}

	def 'check many commands'(String mnemonic) {
		given:
		def helpResolver = new HelpResolver()

		expect:
		helpResolver.resolve(mnemonic).contains(mnemonic)

		where:
		mnemonic << ["SRSCAN", "CLOAK", "ESTIMATE", "SELECT", "WARP"]
	}

	def 'return default on wrong mnemonic'() {
		given:
		def helpResolver = new HelpResolver()
		def mnemonic = "DESTROY"

		when:
		def temp = helpResolver.resolve(mnemonic)

		then:
		temp == NO_COMMAND
	}

	def 'return default on empty mnemonic'() {
		given:
		def helpResolver = new HelpResolver()
		def mnemonic = ""

		when:
		def temp = helpResolver.resolve(mnemonic)

		then:
		temp == NO_COMMAND
	}
}
