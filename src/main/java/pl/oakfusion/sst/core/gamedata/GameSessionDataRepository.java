package pl.oakfusion.sst.core.gamedata;

import java.util.Optional;
import java.util.UUID;

public interface GameSessionDataRepository {
	Optional<GameSessionData> get(UUID uid);
	void save(UUID uid, GameSessionData gameSessionData);
	boolean containsKey(UUID uuid);
}
