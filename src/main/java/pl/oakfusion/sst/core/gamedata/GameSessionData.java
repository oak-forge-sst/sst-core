package pl.oakfusion.sst.core.gamedata;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.oakfusion.sst.data.world.civilizations.Civilization;
import pl.oakfusion.sst.data.DTO.DamagedObjects;
import pl.oakfusion.sst.data.DTO.DeadPlayersDTO;
import pl.oakfusion.sst.data.DTO.score.ScoreDTO;
import pl.oakfusion.sst.data.parameter.GameType;
import pl.oakfusion.sst.data.parameter.ObjectType;
import pl.oakfusion.sst.data.gamesession.GameSessionState;
import pl.oakfusion.sst.data.initializer.WorldSpecification;
import pl.oakfusion.sst.data.world.World;
import pl.oakfusion.sst.data.world.civilizations.UnitType;
import pl.oakfusion.sst.data.world.player.Player;

import java.util.*;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
public class GameSessionData {
	private World world;
	private WorldSpecification worldSpecification;
	private Player activePlayer;
	private PlayersRepository playersRepository;
	private int maximumNumberOfPlayers;
	private boolean isMultiSelect;
	private GameSessionState gameSessionState = GameSessionState.NOT_RUNNING;
	private GameType gameType;
	private int turn = 0;
	private List<DeadPlayersDTO> deadPlayersDTOS = new ArrayList<>();
	private ArrayList<DamagedObjects> damagedObjects = new ArrayList<>();
	private Map<UUID, ScoreDTO> scorePlayersDTOS = new HashMap<>();


	public GameSessionData(World world, PlayersRepository playersRepository, GameType gameType) {
		this.world = world;
		this.playersRepository = playersRepository;
		this.gameType =gameType;
		this.bindScoreDtoWithPlayers();
	}

	public GameSessionData(WorldSpecification worldSpecification, PlayersRepository playersRepository, GameType gameType) {
		this.worldSpecification = worldSpecification;
		this.playersRepository = playersRepository;
		this.gameType = gameType;
	}

	public void addElementToDamagedObjects(Player attacker, Player defender, boolean isAlive, UnitType defenderUnitType, int x, int y, String name, UnitType attackerUnitType) {
		damagedObjects.add(new DamagedObjects(attacker, defender, isAlive, defenderUnitType, x, y, attackerUnitType, name, defender.getTurn()));
	}

	public void addElementToDamagedObjects(Player defender, boolean isAlive, UnitType defenderUnitType, int x, int y, String name) {
		damagedObjects.add(new DamagedObjects(defender, isAlive, defenderUnitType, x, y, name, defender.getTurn()));
	}

	public void addElementToDamagedObjects(Player attacker, UnitType attackerUnitType, ObjectType destroyedObject) {
		damagedObjects.add(new DamagedObjects(attacker, attackerUnitType, destroyedObject));
	}

	public List<Civilization> getOpponentsCivilizations() {
		return playersRepository.getPlayerList().stream().filter(player -> player != activePlayer)
				.map(Player::getCivilization).collect(Collectors.toList());
	}

	public List<Player> getOpponents() {
		return playersRepository.getPlayerList().stream().filter(player -> player != activePlayer)
				.collect(Collectors.toList());
	}

	public List<String> getOpponentsNames() {
		return getOpponents().stream().map(Player::getPlayerName).collect(Collectors.toList());
	}

	public void addDeadPlayer(List<DeadPlayersDTO> deadPlayersDTO) {
		deadPlayersDTOS.addAll(deadPlayersDTO);
	}

	//TODO: think how to check if game has continuation
	public boolean hasContinuation() {
		//return lastCommands.containsKey(getActivePlayer().getCivilization().getActiveUnit());
		return false;
	}

	public void setActivePlayer(Player activePlayer) {
		this.activePlayer = activePlayer;
	}

	public void setMultiSelect(boolean multiSelect) {
		isMultiSelect = multiSelect;
	}

	public void bindScoreDtoWithPlayers() {
		Map<UUID, ScoreDTO> scoreDTOMap = new HashMap<>();
		getPlayersRepository().getPlayerList().forEach(player -> scoreDTOMap.put(player.getUuid(), new ScoreDTO(new LinkedList<>())));
		setScorePlayersDTOS(scoreDTOMap);
	}

}
