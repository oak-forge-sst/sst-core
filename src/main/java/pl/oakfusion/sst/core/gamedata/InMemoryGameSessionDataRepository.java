package pl.oakfusion.sst.core.gamedata;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public class InMemoryGameSessionDataRepository implements GameSessionDataRepository {
	Map<UUID, GameSessionData> repository = new HashMap<>();

	@Override
	public Optional<GameSessionData> get(UUID uuid) {
		return Optional.ofNullable(repository.get(uuid));
	}

	@Override
	public void save(UUID uuid, GameSessionData gameSessionData) {
		repository.put(uuid, gameSessionData);
	}

	@Override
	public boolean containsKey(UUID uuid) {
		return repository.containsKey(uuid);
	}
}
