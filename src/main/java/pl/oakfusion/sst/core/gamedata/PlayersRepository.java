package pl.oakfusion.sst.core.gamedata;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.oakfusion.sst.data.exceptions.NotFoundException;
import pl.oakfusion.sst.data.world.civilizations.Civilization;
import pl.oakfusion.sst.data.world.player.Player;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.UUID;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class PlayersRepository {
private LinkedList<Player> playerList = new LinkedList<>();

	public void clearList(){
		playerList.clear();
	}

	public void addToPlayerRepository(Player player){
		playerList.add(player);
	}

	public Civilization getCivilization(UUID uuid) throws NotFoundException {
		return playerList.stream().filter(player -> player.getUuid().equals(uuid))
				.findFirst().orElseThrow(NotFoundException::new).getCivilization();
	}

	public void addPlayersList(Player... players){
		playerList.addAll(Arrays.asList(players));
	}

		public Player getPlayerByUuid(UUID uuid) throws NotFoundException {
			return playerList.stream().filter(p -> p.getUuid().equals(uuid)).findFirst().orElseThrow(NotFoundException::new);
	}
}
