package pl.oakfusion.sst.core.generator;


import pl.oakfusion.sst.core.gamedata.GameSessionData;
import pl.oakfusion.sst.core.gamedata.PlayersRepository;
import pl.oakfusion.sst.core.gamedata.GameSessionDataRepository;
import pl.oakfusion.sst.data.parameter.GameType;
import pl.oakfusion.sst.data.initializer.WorldSpecification;
import pl.oakfusion.sst.data.util.RandomGenerator;
import pl.oakfusion.sst.data.world.World;

import java.util.UUID;

public class GameSessionDataGenerator{

	private final GameSessionDataRepository gameSessionDataRepository;
	private final WorldGenerator worldGenerator;

	public GameSessionDataGenerator(RandomGenerator random, GameSessionDataRepository gameSessionDataRepository) {
		this.gameSessionDataRepository = gameSessionDataRepository;
		this.worldGenerator = new WorldGenerator(random);
	}

	public UUID createGameSession(WorldSpecification specification, PlayersRepository playersRepository, GameType gameType, int maxNumberOfPlayers) {
		GameSessionData gameSessionData = new GameSessionData(specification, playersRepository, gameType);
		gameSessionData.setMaximumNumberOfPlayers(maxNumberOfPlayers);
		UUID gameSessionUuid = UUID.randomUUID();
		gameSessionDataRepository.save(gameSessionUuid, gameSessionData);
		return gameSessionUuid;
	}

	public void generateWorldForGameSession(GameSessionData gameSessionData){
		WorldSpecification specification = gameSessionData.getWorldSpecification();
		PlayersRepository playersRepository = gameSessionData.getPlayersRepository();
		World world = worldGenerator.generateWorld(specification, playersRepository, gameSessionData.getGameType());
		gameSessionData.setWorld(world);
		gameSessionData.setActivePlayer(playersRepository.getPlayerList().get(0));
		//FIXME
		//gameSessionData.getActivePlayer().setUpPlayerForNewTurn();
	}
}
