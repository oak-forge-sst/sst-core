package pl.oakfusion.sst.core.generator;


import com.google.common.annotations.VisibleForTesting;
import pl.oakfusion.sst.core.gamedata.PlayersRepository;
import pl.oakfusion.sst.data.world.civilizations.klingoncivilization.factories.KlingonSpecificationFactory;
import pl.oakfusion.sst.data.world.gameobject.*;
import pl.oakfusion.sst.data.parameter.GameType;
import pl.oakfusion.sst.data.initializer.WorldGeneratorCache;
import pl.oakfusion.sst.data.initializer.WorldSpecification;
import pl.oakfusion.sst.data.parameter.GameDifficulty;
import pl.oakfusion.sst.data.parameter.GameLength;
import pl.oakfusion.sst.data.util.RandomGenerator;
import pl.oakfusion.sst.data.world.*;
import pl.oakfusion.sst.data.world.civilizations.AbstractUnitFactory;
import pl.oakfusion.sst.data.world.civilizations.CivilizationFactory;
import pl.oakfusion.sst.data.world.civilizations.UnitFactory;
import pl.oakfusion.sst.data.world.civilizations.federationcivilization.factories.FederationFactory;
import pl.oakfusion.sst.data.world.civilizations.klingoncivilization.factories.KlingonCivilizationFactory;
import pl.oakfusion.sst.data.world.player.CivilizationType;
import pl.oakfusion.sst.data.world.player.Player;
import pl.oakfusion.sst.data.world.player.PlayerType;

import java.util.Objects;
import java.util.stream.IntStream;

import static pl.oakfusion.sst.data.world.StarDate.MAX_STARDATE;
import static pl.oakfusion.sst.data.world.StarDate.MIN_STARDATE;

public class WorldGenerator {

	private static final int STAR_FORCE_FIELD_RANGE = 1;
	private static final int STARBASE_FORCE_FIELD_RANGE = 1;
	private static final int PLANET_FORCE_FIELD_RANGE = 1;
	private static final int KLINGON_FORCE_FIELD_RANGE = 0;
	private static final int BLACK_HOLE_FORCE_FIELD_RANGE = 2;
	private final RandomGenerator random;
	@VisibleForTesting
	private WorldGeneratorCache worldGeneratorCache;
	private PlayersRepository playersRepository;
	World world;

	public WorldGenerator(RandomGenerator random) {
		this.random = random;

	}

	public World generateWorld(WorldSpecification specification, PlayersRepository playersRepository, GameType gameType) {
		int widthInQuadrants = specification.getWidthInQuadrants();
		int heightInQuadrants = specification.getHeightInQuadrants();
		int quadrantSize = specification.getQuadrantSize();
		int starQuantity = specification.getStarQuantity();
		int starbaseQuantity = specification.getStarbaseQuantity();
		int planetQuantity = specification.getPlanetQuantity();
		int blackHoleQuantity = specification.getBlackHoleQuantity();
		int globalForceFieldRange = globalForceFieldRange(starQuantity, starbaseQuantity, planetQuantity, blackHoleQuantity);
		StarDate date = new StarDate(random.nextDoubleInRange(MIN_STARDATE, MAX_STARDATE), 3);
		world = new World(widthInQuadrants, heightInQuadrants, quadrantSize, date);
		worldGeneratorCache = new WorldGeneratorCache(world);
		ensureWorldCapacity(world, globalForceFieldRange);
		spawnObjectsInWorld(world, Star::new, STAR_FORCE_FIELD_RANGE, starQuantity);
		spawnObjectsInWorld(world, Starbase::new, STARBASE_FORCE_FIELD_RANGE, starbaseQuantity);
		spawnObjectsInWorld(world, Planet::new, PLANET_FORCE_FIELD_RANGE, planetQuantity);
		spawnObjectsInWorld(world, BlackHole::new, BLACK_HOLE_FORCE_FIELD_RANGE, blackHoleQuantity);
		generateCivilizationsForPlayers(playersRepository, specification, gameType);
		return world;
	}

	private <G extends GameObject> G spawnObjectInWorld(GameObjectFactory<G> factory, int forceFieldRange) {
		G spawnedObject = null;
		while (spawnedObject == null){
			int x = random.nextInt(world.getAbsoluteWidth());
			int y = random.nextInt(world.getAbsoluteHeight());

			if (worldGeneratorCache.isSpaceAvailable(x, y, forceFieldRange)) {
				spawnedObject = factory.create(x, y);
				worldGeneratorCache.put(x, y, forceFieldRange, spawnedObject);
			}
		}
		return spawnedObject;
	}

	private void ensureWorldCapacity(World world, int globalForceFieldRange) {
		if ((globalForceFieldRange) >= world.getWidthInQuadrants() * world.getHeightInQuadrants() * world.getQuadrantSize() * world.getQuadrantSize()) {
			throw new RuntimeException("Not enough space to fit this amount of objects.");
		}
	}

	private int globalForceFieldRange(int starQuantity, int starbaseQuantity, int planetQuantity, int blackHoleQuantity) {
		int starForceFieldRange = getField(STAR_FORCE_FIELD_RANGE) * starQuantity;
		int starbaseForceFieldRange = getField(STARBASE_FORCE_FIELD_RANGE) * starbaseQuantity;
		int planetForceFieldRange = getField(PLANET_FORCE_FIELD_RANGE) * planetQuantity;
		int blackHoleForceFieldRange = getField(BLACK_HOLE_FORCE_FIELD_RANGE) * blackHoleQuantity;
		return starForceFieldRange + starbaseForceFieldRange + planetForceFieldRange + blackHoleForceFieldRange;
	}

	private int getField(int range) {
		return (int) Math.pow(2 * range + 1, 2);
	}

	private <G extends GameObject> void spawnObjectsInWorld(World world, GameObjectFactory<G> factory, int forceFieldRange, int objectQuantity) {
		IntStream.range(0, objectQuantity).forEach(i -> spawnObjectInWorld(factory, forceFieldRange));
	}



	private void generateCivilizationsForPlayers(PlayersRepository playersRepository, WorldSpecification specification, GameType gameType) {
		playersRepository.getPlayerList().forEach(player ->
				player.setCivilization(
						Objects.requireNonNull(getCivilizationFactory(player, specification, gameType)).createCivilization()
				)
		);
	}

	private CivilizationFactory getCivilizationFactory(Player player, WorldSpecification specification, GameType gameType) {
		AbstractUnitFactory unitFactory = new UnitFactory(specification.getGameDifficulty());

		if(player.getPlayerType() == PlayerType.AI_PLAYER) {

			if (player.getCivilizationType().equals(CivilizationType.FEDERATION)) {
				return new FederationFactory(player.getUuid(), this::spawnObjectInWorld, unitFactory);

			} else if (player.getCivilizationType().equals(CivilizationType.KLINGON_EMPIRE)) {
				GameLength gameLength = specification.getGameLength();
				return new KlingonCivilizationFactory(KlingonSpecificationFactory.createAIPlayerSpecification(specification.getGameDifficulty(), gameLength),
						unitFactory,
						player.getUuid(),
						this::spawnObjectInWorld);

			} else {
				return null;
			}

		}else if(player.getPlayerType() == PlayerType.HUMAN_PLAYER){

			if (player.getCivilizationType().equals(CivilizationType.FEDERATION)) {
				return new FederationFactory(player.getUuid(), this::spawnObjectInWorld, unitFactory);

			} else if (player.getCivilizationType().equals(CivilizationType.KLINGON_EMPIRE)) {

				if(gameType == GameType.SINGLEPLAYER) {
					return new KlingonCivilizationFactory(KlingonSpecificationFactory.createHumanPlayerSpecification(specification.getGameDifficulty(), specification.getGameLength()),
							unitFactory,
							player.getUuid(),
							this::spawnObjectInWorld);

				}else if(gameType == GameType.MULTIPLAYER){
					return new KlingonCivilizationFactory(KlingonSpecificationFactory.createHumanPlayerSpecification(GameDifficulty.GOOD, specification.getGameLength()),
							unitFactory,
							player.getUuid(),
							this::spawnObjectInWorld
					);

				}else{
					return null;
				}

			} else {
				return null;
			}

		}else{
			return null;
		}
	}

}
