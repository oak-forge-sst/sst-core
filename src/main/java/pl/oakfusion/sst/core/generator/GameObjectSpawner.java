package pl.oakfusion.sst.core.generator;

import pl.oakfusion.sst.data.world.gameobject.GameObject;
import pl.oakfusion.sst.data.world.gameobject.GameObjectFactory;

@FunctionalInterface
public interface GameObjectSpawner {
	<G extends GameObject> G spawnObjectInWorld(GameObjectFactory<G> factory, int forceFieldRange);
}
