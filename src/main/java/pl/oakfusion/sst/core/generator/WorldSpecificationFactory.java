package pl.oakfusion.sst.core.generator;

import lombok.Getter;
import pl.oakfusion.sst.data.initializer.WorldSpecification;
import pl.oakfusion.sst.data.parameter.GameDifficulty;
import pl.oakfusion.sst.data.parameter.GameLength;

public class WorldSpecificationFactory {
	@Getter
	private GameDifficulty gameDifficulty;
	@Getter
	private GameLength gameLength;
	private int multiplier;

	public WorldSpecificationFactory(GameDifficulty gameDifficulty, GameLength gameLength) {
		this.gameDifficulty = gameDifficulty;
		this.gameLength = gameLength;
		multiplier = chooseMultiplierForLength();
	}

	public WorldSpecification createSpecification() {
		WorldSpecification.WorldSpecificationBuilder builder = WorldSpecification.builder()
				.widthInQuadrants(10)
				.heightInQuadrants(10)
				.quadrantSize(10)
				.gameDifficulty(gameDifficulty)
				.gameLength(gameLength);

		switch (gameDifficulty) {
			case NOVICE: return getWorldForNoviceGame(builder);
			case FAIR: return getWorldForFairGame(builder);
			case GOOD: return getWorldForGoodGame(builder);
			case EXPERT: return getWorldForExpertGame(builder);
			case EMERITUS: return getWorldForEmeritusGame(builder);
			}
			return getWorldForNoviceGame(builder);
		}

		private WorldSpecification getWorldForNoviceGame (WorldSpecification.WorldSpecificationBuilder builder){
			return builder
					.starQuantity(100)
					.starbaseQuantity(10)
					.blackHoleQuantity(10)
					.planetQuantity(30 * multiplier)
					.build();
		}

		private WorldSpecification getWorldForFairGame (WorldSpecification.WorldSpecificationBuilder builder){
			return builder
					.starQuantity(100)
					.starbaseQuantity(9)
					.blackHoleQuantity(15)
					.planetQuantity(27 * multiplier)
					.build();
		}

		private WorldSpecification getWorldForGoodGame (WorldSpecification.WorldSpecificationBuilder builder){
			return builder
					.starQuantity(150)
					.starbaseQuantity(7)
					.blackHoleQuantity(18)
					.planetQuantity(13 * multiplier)
					.build();
		}

		private WorldSpecification getWorldForExpertGame (WorldSpecification.WorldSpecificationBuilder builder){
			return builder
					.starQuantity(200)
					.starbaseQuantity(5)
					.blackHoleQuantity(20)
					.planetQuantity(10 * multiplier)
					.build();
		}

		private WorldSpecification getWorldForEmeritusGame (WorldSpecification.WorldSpecificationBuilder builder){
			return builder
					.starQuantity(200)
					.starbaseQuantity(3)
					.blackHoleQuantity(27)
					.planetQuantity(10 * multiplier)
					.build();
		}

		private int chooseMultiplierForLength () {
			switch (gameLength) {
			case LONG: return 3;
			case MEDIUM: return 2;
			case SHORT: return 1;
			}
			return 1;
		}
	}
