package pl.oakfusion.sst.core.gamesession;

import pl.oakfusion.bus.MessageBus;
import pl.oakfusion.data.message.Event;
import pl.oakfusion.sst.core.gamedata.GameSessionData;
import pl.oakfusion.sst.core.gamedata.PlayersRepository;
import pl.oakfusion.sst.core.aggregates.HelpResolver;
import pl.oakfusion.sst.core.game.validators.*;
import pl.oakfusion.sst.core.gamedata.GameSessionDataRepository;
import pl.oakfusion.sst.core.gamesession.validators.JoinGameSessionCommandValidator;
import pl.oakfusion.sst.core.gamesession.validators.SaveGameSessionValidator;
import pl.oakfusion.sst.core.gamesession.validators.StartGameCommandValidator;
import pl.oakfusion.sst.core.generator.GameSessionDataGenerator;
import pl.oakfusion.sst.core.generator.WorldSpecificationFactory;
import pl.oakfusion.sst.data.parameter.GameType;
import pl.oakfusion.sst.data.gamesession.SavedGameData;
import pl.oakfusion.sst.data.game.commands.Cancel;
import pl.oakfusion.sst.data.errors.ApplicationError;
import pl.oakfusion.sst.data.game.events.CancelPerformed;
import pl.oakfusion.sst.data.gamesession.commands.EndTurn;
import pl.oakfusion.sst.data.game.events.turn.ReportBeforeTurn;
import pl.oakfusion.sst.data.gamesession.GameSessionState;
import pl.oakfusion.sst.data.gamesession.commands.*;
import pl.oakfusion.sst.data.gamesession.events.*;
import pl.oakfusion.sst.data.util.RandomGenerator;
import pl.oakfusion.sst.data.world.player.AIPlayer;
import pl.oakfusion.sst.data.world.player.HumanPlayer;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static pl.oakfusion.sst.core.gamesession.validators.GameSessionValidators.*;
import static pl.oakfusion.sst.core.game.validators.ValidatorsCollector.*;

public class GameSessionHandlers extends SstCommandHandlers {

	private final GameSessionDataGenerator gameSessionDataGenerator;
	private final GameSessionDataRepository gameSessionDataRepository;
	private HelpResolver helpResolver;
	private final FileManager fileManager = new FileManager();

	public GameSessionHandlers(RandomGenerator randomGenerator, MessageBus messageBus, GameSessionDataRepository gameSessionDataRepository, HelpResolver helpResolver){
		super(messageBus, gameSessionDataRepository);
		this.gameSessionDataGenerator = new GameSessionDataGenerator(randomGenerator, gameSessionDataRepository);
		this.gameSessionDataRepository = gameSessionDataRepository;
		this.helpResolver = helpResolver;
		registerCommandHandler(InitializeConfiguredGameSession.class, this::handle);
		registerCommandHandler(InitializeRegularGameSession.class, this::handle);
		registerCommandHandler(InitializeMultiplayerRegularGameSession.class, this::handle);
		registerCommandHandler(InitializeMultiplayerConfiguredGameSession.class, this::handle);
		registerCommandHandler(InitializeLoadedGameSession.class, handleWith(this::handle, noValidators()));
		registerCommandHandler(LoadGameSession.class, this::handle);
		registerCommandHandler(JoinGameSession.class, handleWith(this::handle, validators(new JoinGameSessionCommandValidator())));
		registerCommandHandler(StartGame.class, handleWith(this::handle, validators(new StartGameCommandValidator())));
		registerCommandHandler(EndTurn.class, handleWith(this::handle, validators(getEndTurnCommandValidator())));
		registerCommandHandler(ExitGame.class, handleWith(this::handle, validators(getExitGameCommandValidator())));
		registerCommandHandler(SaveGameSession.class, handleWith(this::handle, validators(new SaveGameSessionValidator())));
		registerCommandHandler(RestartGame.class, this::handle);
		registerCommandHandler(ExitGameSession.class, this::handle);
		registerCommandHandler(EmergencyExitGameSession.class, this::handle);
		registerCommandHandler(Cancel.class, this::handle);
	}

	private List<Event> handle(InitializeLoadedGameSession command, GameSessionData gameSessionData) {
		ReportBeforeTurn reportBeforeTurn = new ReportBeforeTurn(command.getGameSessionUuid(), command.getPlayerUuid());
		reportBeforeTurn.setName(gameSessionData.getActivePlayer().getPlayerName());
		List<Event> events = new ArrayList<>();
		events.add(reportBeforeTurn);
		events.add(new LoadedGameInitialized(command.getGameSessionUuid(), command.getPlayerUuid()));
		return events;
	}

	private void handle(InitializeConfiguredGameSession command) {
		try {
			HumanPlayer humanPlayer = new HumanPlayer(command.getUserCivilization(), command.getName(), command.getPlayerUuid(), true);
			AIPlayer AIPlayer = new AIPlayer(command.getOpponentCivilization(), "AI", command.getAIuuid());
			PlayersRepository playersRepository = new PlayersRepository();
			playersRepository.addToPlayerRepository(humanPlayer);
			playersRepository.addToPlayerRepository(AIPlayer);
			UUID gameSessionUuid = gameSessionDataGenerator.createGameSession(command.getSpecification(), playersRepository, GameType.SINGLEPLAYER, command.getMaxNumberOfPlayers());
			post(new GameSessionInitialized(gameSessionUuid, command.getPlayerUuid()));
		}
		catch(Exception exception){
			post(new ApplicationError(command.getGameSessionUuid(), command.getPlayerUuid(), "Error during initialization"));
		}
	}

	private void handle(InitializeRegularGameSession command) {
		try {
			HumanPlayer humanPlayer = new HumanPlayer(command.getUserCivilization(), command.getName(), command.getPlayerUuid(), true);
			AIPlayer AIPlayer = new AIPlayer(command.getOpponentCivilization(), "AI", command.getAIuuid());
			WorldSpecificationFactory worldSpecificationFactory = new WorldSpecificationFactory(command.getGameDifficulty(), command.getGameLength());
			PlayersRepository playersRepository = new PlayersRepository();
			playersRepository.addToPlayerRepository(humanPlayer);
			playersRepository.addToPlayerRepository(AIPlayer);
			UUID gameSessionUuid = gameSessionDataGenerator.createGameSession(worldSpecificationFactory.createSpecification(), playersRepository, GameType.SINGLEPLAYER, command.getMaxNumberOfPlayers());
			post(new GameSessionInitialized(gameSessionUuid, command.getPlayerUuid()));
		}
		catch(Exception exception){
			post(new ApplicationError(command.getGameSessionUuid(), command.getPlayerUuid(), "Error during initialization"));
		}
	}

	private void handle(InitializeMultiplayerRegularGameSession command) {
		try {
			PlayersRepository playersRepository = new PlayersRepository();
			playersRepository.addToPlayerRepository(new HumanPlayer(command.getUserCivilization(), command.getName(), command.getPlayerUuid(), true));
			WorldSpecificationFactory worldSpecificationFactory = new WorldSpecificationFactory(command.getGameDifficulty(), command.getGameLength());
			UUID gameSessionUuid = gameSessionDataGenerator.createGameSession(worldSpecificationFactory.createSpecification(), playersRepository, GameType.MULTIPLAYER, command.getMaxNumberOfPlayers());
			post(new GameSessionInitialized(gameSessionUuid, command.getPlayerUuid()));
		}
		catch(Exception exception){
			post(new ApplicationError(command.getGameSessionUuid(), command.getPlayerUuid(), "Error during initialization"));
		}
	}

	private void handle(InitializeMultiplayerConfiguredGameSession command) {
		try {
			PlayersRepository playersRepository = new PlayersRepository();
			playersRepository.addToPlayerRepository(new HumanPlayer(command.getUserCivilization(), command.getName(), command.getPlayerUuid(), true));
			UUID gameSessionUuid = gameSessionDataGenerator.createGameSession(command.getWorldSpecification(), playersRepository, GameType.MULTIPLAYER, command.getMaxNumberOfPlayers());
			post(new GameSessionInitialized(gameSessionUuid, command.getPlayerUuid()));
		}
		catch(Exception exception){
			post(new ApplicationError(command.getGameSessionUuid(), command.getPlayerUuid(), "Error during initialization"));
		}
	}

	private List<Event> handle(JoinGameSession command , GameSessionData gameSessionData){
		HumanPlayer player = new HumanPlayer(command.getUserCivilization(), command.getName(), command.getPlayerUuid(), false);
		gameSessionData.getPlayersRepository().addToPlayerRepository(player);
		return List.of(new JoinGamePerformed(command.getGameSessionUuid(), command.getPlayerUuid()));
	}

	private List<Event> handle(StartGame command, GameSessionData gameSessionData){

		List<Event> events = new ArrayList<>();
		gameSessionDataGenerator.generateWorldForGameSession(gameSessionData);
		gameSessionData.setGameSessionState(GameSessionState.RUNNING);
		if(gameSessionData.getGameType() == GameType.MULTIPLAYER){
			gameSessionData.getPlayersRepository().getPlayerList().forEach(
					player -> {
						ReportBeforeTurn reportBeforeTurn = new ReportBeforeTurn(command.getGameSessionUuid(), player.getUuid());
						reportBeforeTurn.setName(gameSessionData.getActivePlayer().getPlayerName());
						events.add(reportBeforeTurn);
						events.add(new StartGamePerformed(command.getGameSessionUuid(), player.getUuid()));
					}
			);
		}
		else {
			ReportBeforeTurn reportBeforeTurn = new ReportBeforeTurn(command.getGameSessionUuid(), command.getPlayerUuid());
			reportBeforeTurn.setName(gameSessionData.getActivePlayer().getPlayerName());
			events.add(reportBeforeTurn);
			events.add(new StartGamePerformed(command.getGameSessionUuid(), command.getPlayerUuid()));
		}
		return events;

	}

	private List<Event> handle(ExitGame command, GameSessionData gameSessionData) {
		List<Event> events = new ArrayList<>();
		gameSessionData.setGameSessionState(GameSessionState.NOT_RUNNING);
		if(gameSessionData.getGameType() == GameType.MULTIPLAYER){
			gameSessionData.getPlayersRepository().getPlayerList().forEach(
					player -> events.add(new GameExited(command.getGameSessionUuid(), player.getUuid()))
			);
		}
		else {
			events.add(new GameExited(command.getGameSessionUuid(), command.getPlayerUuid()));
		}
		return events;
	}

	private List<Event> handle(SaveGameSession command, GameSessionData gameSessionData) {
			List<SavedGameData> savedGameDataList = fileManager.readSavedGames();
			saveGames(savedGameDataList, command, gameSessionData);
			return List.of(new GameSaved(command.getGameSessionUuid(), command.getPlayerUuid()));
	}

	private void saveGames(List<SavedGameData> savedGameDataList, SaveGameSession command, GameSessionData gameSessionData) {
		try {
			fileManager.saveGame(savedGameDataList, command, gameSessionData);
			post(new GameSaved(command.getGameSessionUuid(), command.getPlayerUuid()));
		} catch (IOException e) {
			e.printStackTrace();
			post(new SaveGameFailed(command.getGameSessionUuid(), command.getPlayerUuid()));
		}
	}
	//TODO check if needed
	private void handle(RestartGame command) {
		post(new GameRestarted(command.getGameSessionUuid(), command.getPlayerUuid()));
	}
	//TODO check if needed
	private void handle(ExitGameSession command) {
		post(new GameSessionExited(command.getGameSessionUuid(), command.getPlayerUuid()));
	}
	//TODO check if needed
	private void handle(EmergencyExitGameSession command) {
		post(new GameSessionEmergencyExited(command.getGameSessionUuid(), command.getPlayerUuid()));
	}

	private List<Event> handle(EndTurn command, GameSessionData gameSessionData) {
		gameSessionData.setTurn(gameSessionData.getTurn() + 1);
		return List.of(new EndTurnPerformed(command.getGameSessionUuid(), command.getPlayerUuid()));
	}
	//TODO check if needed
	private void handle(Cancel command) {
		post(new CancelPerformed(command.getGameSessionUuid(), command.getPlayerUuid()));
	}

	private void handle(LoadGameSession command) {
		if (command.getFileName().isPresent()) {
			try {
				GameSessionData gameSessionData = fileManager.readConfig(command.getFileName().get());
				UUID gameSessionUuid = UUID.randomUUID();
				gameSessionDataRepository.save(gameSessionUuid, gameSessionData);
				post(new GameLoadedSuccessfully(gameSessionUuid, command.getPlayerUuid()));
			} catch (FileNotFoundException ioException) {
				post(new FileNotFound(command.getPlayerUuid()));
			} catch (IOException ioException) {
				post(new CannotLoadGame(command.getPlayerUuid()));
				ioException.printStackTrace();
			}
		} else {
			List<SavedGameData> savedGameDataList = fileManager.readSavedGames();
			Collections.reverse(fileManager.getSavedGameData());
			post(new LoadSavedGames(command.getPlayerUuid(), savedGameDataList));
		}
	}

}
