package pl.oakfusion.sst.core.gamesession;

import pl.oakfusion.bus.MessageBus;
import pl.oakfusion.bus.QueryDriven;
import pl.oakfusion.sst.core.gamedata.GameSessionData;
import pl.oakfusion.sst.core.gamedata.GameSessionDataRepository;
import pl.oakfusion.sst.data.DTO.PlayerDTO;
import pl.oakfusion.sst.data.gamesession.queries.GetPlayersList;
import pl.oakfusion.sst.data.gamesession.queryresponses.GetPlayersListPerformed;

import java.util.ArrayList;
import java.util.List;

public class GameSessionQueryProcessors extends QueryDriven {
	private GameSessionDataRepository gameSessionDataRepository;

	public GameSessionQueryProcessors(MessageBus messageBus, GameSessionDataRepository gameSessionDataRepository) {
		super(messageBus);
		this.gameSessionDataRepository = gameSessionDataRepository;
		//registerQueryHandler(GetPlayersList.class, new GameSessionDataQueryValidator<>(gameSessionDataRepository, this::process));
	}

	private void process(GetPlayersList command, GameSessionData gameSessionData){
		List<PlayerDTO> playerDTOS = new ArrayList<>();
		gameSessionData.getPlayersRepository().getPlayerList().forEach(player -> playerDTOS.add(new PlayerDTO(player)));
		post(new GetPlayersListPerformed(command.getGameSessionUuid(), command.getPlayerUuid(), playerDTOS));
	}
}
