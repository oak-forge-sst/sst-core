package pl.oakfusion.sst.core.gamesession.validators;

import pl.oakfusion.sst.core.gamedata.GameSessionData;
import pl.oakfusion.sst.core.game.validators.SstCommandValidator;
import pl.oakfusion.sst.data.validationfailures.ValidationFailure;
import pl.oakfusion.sst.core.gamesession.FileManager;
import pl.oakfusion.sst.data.parameter.GameType;
import pl.oakfusion.sst.data.gamesession.SavedGameData;
import pl.oakfusion.sst.data.gamesession.commands.SaveGameSession;
import pl.oakfusion.sst.data.validationfailures.InvalidRecordName;
import pl.oakfusion.sst.data.validationfailures.RecordAlreadyExist;
import pl.oakfusion.sst.data.validationfailures.SaveFailed;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import static pl.oakfusion.sst.data.util.Types.isDouble;
import static pl.oakfusion.sst.data.util.Types.isInteger;

public class SaveGameSessionValidator implements SstCommandValidator<SaveGameSession> {
	FileManager fileManager = new FileManager();
	@Override
	public List<ValidationFailure> validate(SaveGameSession command, GameSessionData gameSessionData) {
		if(gameSessionData.getGameType() == GameType.MULTIPLAYER){
			return List.of(new SaveFailed("Cannot save multiplayer game", new ArrayList<>()));
		}
		if (isValidName(command.getName())) {
			List<SavedGameData> savedGameDataList = fileManager.readSavedGames();
			if (savedGameDataList.stream().anyMatch(record -> record.getName().equals(command.getName()))) {
				return List.of(new RecordAlreadyExist("Change name", new ArrayList<>()));
			}
		} else {
			return List.of(new InvalidRecordName("Change name", new ArrayList<>()));
		}
		return new ArrayList<>();
	}

	private boolean isValidName(String name) {
		return !isInteger(name) && !isDouble(name) && Pattern.compile("\\w+").matcher(name).matches();
	}
}
