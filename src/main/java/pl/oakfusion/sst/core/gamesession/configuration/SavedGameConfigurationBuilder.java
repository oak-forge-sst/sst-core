package pl.oakfusion.sst.core.gamesession.configuration;

import pl.oakfusion.sst.core.gamedata.GameSessionData;
import pl.oakfusion.sst.data.world.gameobject.GameObject;
import pl.oakfusion.sst.data.parameter.ObjectType;
import pl.oakfusion.sst.data.configuration.*;
import pl.oakfusion.sst.data.world.World;
import pl.oakfusion.sst.data.world.civilizations.Unit;
import pl.oakfusion.sst.data.world.player.Player;

import java.util.stream.Collectors;

public class SavedGameConfigurationBuilder {

	public GameSessionDataConfiguration createConfiguration(GameSessionData gameSessionData) {
		return GameSessionDataConfiguration.builder().objectsConfiguration((gameSessionData.getWorld().getAllGameObjects().stream().filter(e -> !e.getObjectType().equals(ObjectType.UNIT)).collect(Collectors.toList())))
				.unitConfigurations(gameSessionData.getPlayersRepository().getPlayerList().stream().flatMap(p -> p.getCivilization().getUnits().stream()
						.filter(Unit::isObjectAlive)
						.map(this::createUnitConfiguration)).collect(Collectors.toList()))
				.playerConfigurationList(gameSessionData.getPlayersRepository().getPlayerList().stream().map(this::createPlayerConfiguration).collect(Collectors.toList()))
				.worldSpecificationConfiguration(createWorldConfiguration(gameSessionData.getWorld()))
				.stardate(gameSessionData.getWorld().getStarDate())
				.activePlayer(gameSessionData.getActivePlayer().getUuid())
				.build();
	}

	private UnitConfiguration createUnitConfiguration(Unit unit) {
		if (unit.isObjectAlive()) {
			return UnitConfiguration.builder()
					.actionPoints(unit.getActionPoints())
					.components(unit.getComponentSystem().getComponents())
					.condition(unit.getCondition())
					.positionX(unit.getX())
					.positionY(unit.getY())
					.unitType(unit.getType())
					.uuid(unit.getUuid())
					.playerUUID(unit.getPlayerUUID())
					.name(unit.getUnitName())
					.build();
		}
		return null;
	}

	private PlayerConfiguration createPlayerConfiguration(Player player) {
		return PlayerConfiguration.builder()
				.civilizationType(player.getCivilizationType())
				.uuid(player.getUuid())
				.name(player.getPlayerName())
				.playerType(player.getPlayerType())
				.build();
	}

	private WorldSpecificationConfiguration createWorldConfiguration(World world) {
		return WorldSpecificationConfiguration.builder()
				.widthInQuadrants(world.getWidthInQuadrants())
				.heightInQuadrants(world.getHeightInQuadrants())
				.quadrantSize(world.getQuadrantSize())
				.build();
	}

	private ObjectsConfiguration createObjectsConfiguration(GameObject gameObject) {
		return ObjectsConfiguration.builder()
				.objectType(gameObject.getObjectType())
				.positionX(gameObject.getX())
				.positionY(gameObject.getY())
				.build();
	}

}
