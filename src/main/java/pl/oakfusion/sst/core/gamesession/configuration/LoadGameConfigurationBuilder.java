package pl.oakfusion.sst.core.gamesession.configuration;

import pl.oakfusion.sst.core.gamedata.GameSessionData;
import pl.oakfusion.sst.core.gamedata.PlayersRepository;
import pl.oakfusion.sst.data.exceptions.NotFoundException;
import pl.oakfusion.sst.data.world.civilizations.UnitFromConfigFactory;
import pl.oakfusion.sst.data.world.civilizations.federationcivilization.factories.EnterpriseFromConfigFactory;
import pl.oakfusion.sst.data.world.civilizations.klingoncivilization.factories.CommanderFromConfigFactory;
import pl.oakfusion.sst.data.world.civilizations.klingoncivilization.factories.KlingonFromConfigFactory;
import pl.oakfusion.sst.data.world.civilizations.klingoncivilization.factories.SuperCommanderFromConfigFactory;
import pl.oakfusion.sst.data.world.gameobject.ObjectsFromConfigFactory;
import pl.oakfusion.sst.data.world.civilizations.CivilizationFromConfigFactory;
import pl.oakfusion.sst.data.world.civilizations.federationcivilization.factories.FederationFromConfigFactory;
import pl.oakfusion.sst.data.world.civilizations.klingoncivilization.factories.KlingonCivilizationFromConfigFactory;
import pl.oakfusion.sst.data.world.gameobject.*;
import pl.oakfusion.sst.data.parameter.ObjectType;
import pl.oakfusion.sst.data.configuration.*;
import pl.oakfusion.sst.data.util.RandomGenerator;
import pl.oakfusion.sst.data.world.*;
import pl.oakfusion.sst.data.world.civilizations.Unit;
import pl.oakfusion.sst.data.world.civilizations.UnitType;
import pl.oakfusion.sst.data.world.player.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

public class LoadGameConfigurationBuilder {
	private RandomGenerator randomGenerator;
	private Map<UnitType, UnitFromConfigFactory> unitFactories;
	private Map<ObjectType, ObjectsFromConfigFactory> objectsFactories;
	private Map<CivilizationType, CivilizationFromConfigFactory> civilizationFactories;

	public LoadGameConfigurationBuilder(RandomGenerator randomGenerator) {
		unitFactories = Map.of(UnitType.KLINGON, new KlingonFromConfigFactory(), UnitType.COMMANDER, new CommanderFromConfigFactory(), UnitType.SUPER_COMMANDER, new SuperCommanderFromConfigFactory(), UnitType.ENTERPRISE, new EnterpriseFromConfigFactory());
		objectsFactories = Map.of(ObjectType.BLACK_HOLE, BlackHole::new, ObjectType.PLANET, Planet::new, ObjectType.STAR, Star::new, ObjectType.STARBASE, Starbase::new);
		civilizationFactories = Map.of(CivilizationType.FEDERATION, new FederationFromConfigFactory(), CivilizationType.KLINGON_EMPIRE, new KlingonCivilizationFromConfigFactory());
		this.randomGenerator = randomGenerator;
	}

	public GameSessionData createGameDatafromConfig(GameSessionDataConfiguration gameSessionDataConfiguration) throws NotFoundException {
		List<Unit> unitList = gameSessionDataConfiguration.getUnitConfigurations().stream().map(this::createUnitFromConfig).collect(Collectors.toList());
		List<Player> playerList = gameSessionDataConfiguration.getPlayerConfigurationList().stream().map(p -> createPlayerFromConfig(p, unitList)).collect(Collectors.toList());
		UUID activePlayer = gameSessionDataConfiguration.getActivePlayer();
		GameSessionData gameSessionData = new GameSessionData(createWorldFromConfig(gameSessionDataConfiguration.getWorldSpecificationConfiguration(), gameSessionDataConfiguration.getStardate(), unitList, gameSessionDataConfiguration.getObjectsConfiguration()), new PlayersRepository(new LinkedList<>(playerList)), gameSessionDataConfiguration.getGameType());
		gameSessionData.setActivePlayer(playerList.stream().filter(player -> player.getUuid().equals(activePlayer)).findFirst().orElseThrow(NotFoundException::new));
		return gameSessionData;
	}

	private Unit createUnitFromConfig(UnitConfiguration unitConfiguration) {
		Unit unit = unitFactories.get(unitConfiguration.getUnitType()).createUnit(unitConfiguration, randomGenerator);
		unit.setUnitName(unitConfiguration.getName());
		return unit;
	}

	private Player createPlayerFromConfig(PlayerConfiguration playerConfiguration, List<Unit> unitList) {
		Player player = (playerConfiguration.getPlayerType().equals(PlayerType.HUMAN_PLAYER)) ? new HumanPlayer(playerConfiguration.getCivilizationType(), playerConfiguration.getName(), playerConfiguration.getUuid(), playerConfiguration.isGameMaster()) : new AIPlayer(playerConfiguration.getCivilizationType(), playerConfiguration.getName(), playerConfiguration.getUuid());
		player.setCivilization(civilizationFactories.get(playerConfiguration.getCivilizationType()).createCivilization(unitList.stream().filter(u -> u.getUuid().equals(player.getUuid())).collect(Collectors.toList())));
		return player;
	}

	private World createWorldFromConfig(WorldSpecificationConfiguration worldSpecificationConfiguration, StarDate starDate, List<Unit> unitList, List<GameObject> gameObjectList) {
		World world = new World(worldSpecificationConfiguration.getWidthInQuadrants(), worldSpecificationConfiguration.getHeightInQuadrants(), worldSpecificationConfiguration.getQuadrantSize(), starDate);
		unitList.forEach(world::add);
		gameObjectList.forEach(world::add);
		return world;
	}

	private GameObject createGameObjectsFromConfig(ObjectsConfiguration objectsConfiguration) {
		return objectsFactories.get(objectsConfiguration.getObjectType()).createObject(objectsConfiguration.getPositionX(), objectsConfiguration.getPositionY());
	}
}
