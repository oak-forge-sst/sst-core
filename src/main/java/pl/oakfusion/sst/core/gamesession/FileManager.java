package pl.oakfusion.sst.core.gamesession;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import lombok.Getter;
import pl.oakfusion.sst.core.gamedata.GameSessionData;
import pl.oakfusion.sst.core.gamesession.configuration.LoadGameConfigurationBuilder;
import pl.oakfusion.sst.core.gamesession.configuration.SavedGameConfigurationBuilder;
import pl.oakfusion.sst.data.gamesession.SavedGameData;
import pl.oakfusion.sst.data.gamesession.commands.SaveGameSession;
import pl.oakfusion.sst.data.configuration.GameSessionDataConfiguration;
import pl.oakfusion.sst.data.parameter.ObjectType;
import pl.oakfusion.sst.data.util.RandomGenerator;
import pl.oakfusion.sst.data.world.component.Component;
import pl.oakfusion.sst.data.world.component.DefaultMixIn;
import pl.oakfusion.sst.data.world.gameobject.GameObject;
import pl.oakfusion.sst.data.world.gameobject.GameObjectDeserializer;
import pl.oakfusion.sst.data.world.gameobject.GameObjectMixIn;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class FileManager {
	private ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
	private final static String HOME_DIRECTORY = System.getProperty("user.home");
	private final static String BASE_DIRECTORY = HOME_DIRECTORY + File.separator + "SST" + File.separator;
	private final static String DESCRIPTION_DIR = BASE_DIRECTORY + "descriptions";
	private final static String DESCRIPTION_FILE = DESCRIPTION_DIR + File.separator + "descriptions.yaml";
	private RandomGenerator randomGenerator = new RandomGenerator();
	private List<File> listFilesInFolder = new ArrayList<>();
	@Getter
	List<SavedGameData> savedGameData;


	public FileManager() {
		mapper.findAndRegisterModules();
		mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
		mapper.addMixIn(Component.class, DefaultMixIn.class);
		Arrays.stream(ObjectType.values()).forEach(v -> mapper.addMixIn(v.getClazz(), GameObjectMixIn.class));
		SimpleModule module = new SimpleModule();
		module.addDeserializer(GameObject.class, new GameObjectDeserializer());
		mapper.registerModule(module);
	}

	private void createOrRetrieveDirectory() throws IOException {

		final Path path = Paths.get(FileManager.DESCRIPTION_DIR);

		if (Files.notExists(path)) {
			Files.createDirectories(path).toFile();
		}
		path.toFile();
	}

	public void deleteRecord(String name) {
		try {
			List<SavedGameData> games = readSavedGames().stream().filter(g -> !g.getName().equals(name)).collect(Collectors.toList());
			mapper.writeValue(new File(DESCRIPTION_FILE), games);
			Files.deleteIfExists(Paths.get(getFilePath(name)));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public List<SavedGameData> readSavedGames() {
		try {
			savedGameData = mapper.readValue(new File(DESCRIPTION_FILE), mapper.getTypeFactory().constructCollectionType(List.class, SavedGameData.class));
			manageDescriptionFile(savedGameData);
			return savedGameData;
		} catch (IOException ioException) {
			return new ArrayList<>();
		}
	}

	private void manageDescriptionFile(List<SavedGameData> savedGameData) throws IOException {
		listFilesForFolder();
		deleteDescriptionRecord(listFilesInFolder, savedGameData);
	}

	private void deleteDescriptionRecord(List<File> listFilesInFolder, List<SavedGameData> savedGameData) throws IOException {
		Map<String, List<File>> fileMap = listFilesInFolder.stream()
				.collect(Collectors.groupingBy(File::getName, Collectors.toList()));

		this.savedGameData = savedGameData.stream()
				.filter(savedGameData1 -> fileMap.containsKey(savedGameData1.getName() + ".yaml"))
				.collect(Collectors.toList());

		mapper.writeValue(new File(DESCRIPTION_FILE), savedGameData);
	}

	private void listFilesForFolder() {
		File folder = new File(BASE_DIRECTORY);
		for (final File fileEntry : Objects.requireNonNull(folder.listFiles())) {
			if (fileEntry.isFile()) {
				listFilesInFolder.add(fileEntry);
			}
		}
	}


	private static String getFilePath(String name) {
		return BASE_DIRECTORY + name + ".yaml";
	}

	void saveGame(List<SavedGameData> savedGameDataList, SaveGameSession command, GameSessionData gameSessionData) throws IOException {
		savedGameDataList.add(new SavedGameData(command.getName(), command.getDescription(), new Date()));
		createOrRetrieveDirectory();
		mapper.writeValue(new File(DESCRIPTION_FILE), savedGameDataList);
		mapper.writeValue(new File(getFilePath(command.getName())), new SavedGameConfigurationBuilder().createConfiguration(gameSessionData));
	}

	GameSessionData readConfig(String fileName) throws IOException {
		return new LoadGameConfigurationBuilder(randomGenerator).createGameDatafromConfig(mapper.readValue(new File(getFilePath(fileName)), GameSessionDataConfiguration.class));
	}


}
