package pl.oakfusion.sst.core.gamesession.validators;

import pl.oakfusion.sst.core.game.validators.SstCommandValidator;
import pl.oakfusion.sst.data.gamesession.commands.EndTurn;
import pl.oakfusion.sst.data.gamesession.commands.ExitGame;
import pl.oakfusion.sst.data.validationfailures.ContinuationWarn;
import pl.oakfusion.sst.data.validationfailures.ExitGameFailed;

import java.util.ArrayList;
import java.util.List;

public class GameSessionValidators {
	public static SstCommandValidator<ExitGame> getExitGameCommandValidator() {
		return (command, gameSessionData) -> {
			if (!gameSessionData.getPlayersRepository().getPlayerByUuid(command.getPlayerUuid()).isGameMaster()) {
				return List.of(new ExitGameFailed("You're not game master.", new ArrayList<>()));
			}
			return new ArrayList<>();
		};
	}

	public static SstCommandValidator<EndTurn> getEndTurnCommandValidator() {
		return (command, gameSessionData) -> {
			if (gameSessionData.hasContinuation()) {
				return List.of(new ContinuationWarn("Game has continuation.", new ArrayList<>()));
			}
			return new ArrayList<>();
		};
	}
}
