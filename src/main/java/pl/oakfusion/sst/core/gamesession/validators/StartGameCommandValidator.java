package pl.oakfusion.sst.core.gamesession.validators;

import pl.oakfusion.sst.core.gamedata.GameSessionData;
import pl.oakfusion.sst.core.game.validators.SstCommandValidator;
import pl.oakfusion.sst.data.exceptions.NotFoundException;
import pl.oakfusion.sst.data.validationfailures.ValidationFailure;
import pl.oakfusion.sst.data.gamesession.GameSessionState;
import pl.oakfusion.sst.data.gamesession.commands.StartGame;
import pl.oakfusion.sst.data.validationfailures.StartGameFailed;

import java.util.ArrayList;
import java.util.List;

public class StartGameCommandValidator implements SstCommandValidator<StartGame> {

	@Override
	public List<ValidationFailure> validate(StartGame command, GameSessionData gameSessionData) throws NotFoundException {

		List<ValidationFailure> validationFailures = new ArrayList<>();
		if(gameSessionData.getPlayersRepository().getPlayerByUuid(command.getPlayerUuid()).isGameMaster()) {
			if (gameSessionData.getGameSessionState() == GameSessionState.RUNNING) {
				validationFailures.add(new StartGameFailed("Game already started.", new ArrayList<>()));
			}
		}
		else{
			validationFailures.add(new StartGameFailed("You're not game master.", new ArrayList<>()));
		}
		return validationFailures;
	}
}
