package pl.oakfusion.sst.core.gamesession.validators;

import pl.oakfusion.sst.core.gamedata.GameSessionData;
import pl.oakfusion.sst.core.game.validators.SstCommandValidator;
import pl.oakfusion.sst.data.validationfailures.ValidationFailure;
import pl.oakfusion.sst.data.gamesession.GameSessionState;
import pl.oakfusion.sst.data.gamesession.commands.JoinGameSession;
import pl.oakfusion.sst.data.validationfailures.JoinGameFailed;
import pl.oakfusion.sst.data.world.player.Player;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class JoinGameSessionCommandValidator implements SstCommandValidator<JoinGameSession> {

	@Override
	public List<ValidationFailure> validate(JoinGameSession command, GameSessionData gameSessionData) {
		if(gameSessionData.getGameSessionState() == GameSessionState.RUNNING){
			return List.of(new JoinGameFailed("Cannot join running game.", new ArrayList<>()));
		}
		LinkedList<Player> playersList = gameSessionData.getPlayersRepository().getPlayerList();
		if(playersList.size() >= gameSessionData.getMaximumNumberOfPlayers()){
			return List.of(new JoinGameFailed("Maximum number of players in game session reached.", new ArrayList<>()));
		}
		if(playersList.stream().anyMatch(
				player -> player.getPlayerName().equals(command.getName()))){
			return List.of(new JoinGameFailed("Player with given name is in game.", new ArrayList<>()));
		}
		return new ArrayList<>();
	}
}
