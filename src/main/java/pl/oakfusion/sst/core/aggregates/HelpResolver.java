package pl.oakfusion.sst.core.aggregates;

import java.io.*;
import java.util.HashMap;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public class HelpResolver {

	static final String NO_COMMAND = "No command";
	private final HashMap<String, String> commands;
	private static final Logger logger = Logger.getLogger(HelpResolver.class.getName());

	public HelpResolver() {
		commands = loadProperties();
	}

	private HashMap<String, String> loadProperties() {
		HashMap<String, String> temp = new HashMap<String, String>();
		Properties properties = new Properties();

		try (InputStream fis = getClass().getClassLoader().getResourceAsStream("message_mapping.properties")) {
			properties.load(fis);
			for (String key : properties.stringPropertyNames()) {
				String value = properties.getProperty(key);
				temp.put(key, value);
			}
		} catch (FileNotFoundException e) {
			System.out.println("Resources not found");
		} catch (IOException e) {
			System.out.println("IO Exception");
		}
		return temp;
	}

	public String resolve(String mnemonic) {
		return mnemonic.isEmpty() ?
				NO_COMMAND :
				getFileContentByMnemonic(mnemonic);
	}

	private String getFileNameByMnemonic(String mnemonic) {
		return commands.getOrDefault(mnemonic, NO_COMMAND);
	}

	private String getFileContentByMnemonic(String mnemonic) {
		String name = getFileNameByMnemonic(mnemonic);
		if ( name.equals(NO_COMMAND) ) { return name; }

		try (InputStream inputStream = getClass().getClassLoader().getResourceAsStream("help/" + name + ".md")) {
			InputStreamReader streamReader = new InputStreamReader(inputStream);
			BufferedReader reader = new BufferedReader(streamReader);

			StringBuilder sb = new StringBuilder("");
			for (String line; (line = reader.readLine()) != null;) {
				sb.append(line + "\n");
			}

			Pattern p1 = Pattern.compile("\\*(.*?)\\*|^([#~].*)", Pattern.MULTILINE);
			String temp = p1.matcher(sb.toString()).replaceAll("");

			return temp;
		} catch (NullPointerException e) {
			logger.log(Level.WARNING, "Resources not found: " + e.toString());
			return NO_COMMAND;
		} catch (IOException e) {
			logger.log(Level.SEVERE, "IO Exception: " + e.toString());
			return "Problem with file";
		}
	}
}
