package pl.oakfusion.sst.core.aggregates;

import pl.oakfusion.bus.CommandDriven;
import pl.oakfusion.bus.MessageBus;

public abstract class Aggregate extends CommandDriven {
	private MessageBus messageBus;

	public Aggregate(MessageBus messageBus) {
		super(messageBus);
	}

}
