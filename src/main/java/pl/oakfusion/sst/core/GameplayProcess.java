package pl.oakfusion.sst.core;

import lombok.Setter;
import pl.oakfusion.bus.EventDriven;
import pl.oakfusion.bus.MessageBus;
import pl.oakfusion.sst.core.game.CreateActionReport;
import pl.oakfusion.sst.core.gamedata.GameSessionData;
import pl.oakfusion.sst.core.gamedata.GameSessionDataRepository;
import pl.oakfusion.sst.data.game.commands.GameOverForPlayerCommand;
import pl.oakfusion.sst.data.gamesession.commands.EndTurn;
import pl.oakfusion.sst.data.game.events.GameOverForPlayerEvent;
import pl.oakfusion.sst.data.game.events.deathRay.DeathRayDead;
import pl.oakfusion.sst.data.game.events.mayday.MaydayFailed;
import pl.oakfusion.sst.data.game.events.turn.ReportBeforeTurn;
import pl.oakfusion.sst.data.game.events.turn.TurnPerformed;
import pl.oakfusion.sst.data.gamesession.commands.ExitGame;
import pl.oakfusion.sst.data.world.StarDateSpecification;
import pl.oakfusion.sst.data.world.player.Player;
import pl.oakfusion.sst.data.world.gameobject.Rubbish;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Setter
public class GameplayProcess extends EventDriven {
	//private final MessageBus messageBus;
	private GameSessionDataRepository gameSessionDataRepository;
	CreateActionReport createActionReport = new CreateActionReport();

	public GameplayProcess(MessageBus messageBus, GameSessionDataRepository gameSessionDataRepository) {
		super(messageBus);
		this.gameSessionDataRepository = gameSessionDataRepository;
		//this.messageBus = messageBus;
		//COMMANDS ENDING TURN
		//registerEventHandler(TurnPerformed.class, new GameSessionDataEventValidator<>(gameSessionDataRepository, this::handle));
		//registerEventHandler(GameOverForPlayerEvent.class, new GameSessionDataEventValidator<>(gameSessionDataRepository, this::handle));
		//COMMANDS ENDING PLAYERS GAME
		//registerEventHandler(MaydayFailed.class, new GameSessionDataEventValidator<>(gameSessionDataRepository, this::handle));
		//registerEventHandler(DeathRayDead.class, new GameSessionDataEventValidator<DeathRayDead>(gameSessionDataRepository, this::handle));
	}

	private void handle(TurnPerformed event, GameSessionData gameSessionData) {
		endTurn(event.getGameSessionUuid(), event.getPlayerUuid(), gameSessionData);
	}

	private void handle(GameOverForPlayerEvent event, GameSessionData gameSessionData) {
		if (event.isLastPlayer()) {
			post(new ExitGame(event.getGameSessionUuid(), event.getPlayerUuid()));
		} else {
			endTurn(event.getGameSessionUuid(), event.getPlayerUuid(), gameSessionData);
		}
	}

	private void endTurn(UUID gameSessionUuid, UUID playerUuid, GameSessionData gameSessionData) {
		ReportBeforeTurn reportBeforeTurn = new ReportBeforeTurn(gameSessionUuid, playerUuid);
		List<Player> playersLeft = gameSessionData.getPlayersRepository().getPlayerList().stream().filter(p -> p.isAlive() || p.equals(gameSessionData.getActivePlayer())).collect(Collectors.toList());
		int activeIndex = playersLeft.indexOf(gameSessionData.getActivePlayer());
		int nextIndex = (activeIndex + 1) % playersLeft.size();
		Player player = playersLeft.get(nextIndex);
		gameSessionData.setDeadPlayersDTOS(
				gameSessionData
						.getDeadPlayersDTOS()
						.stream()
						.filter(dto -> !dto.getKiller().orElse(dto.getDeadPlayer()).equals(player.getPlayerName()))
						.collect(Collectors.toList())
		);

		gameSessionData.setActivePlayer(player);
		reportBeforeTurn.setName(player.getPlayerName());

		if (!gameSessionData.getDamagedObjects().isEmpty()) {
			createActionReport.createReport(player.getTurn(), gameSessionData.getDamagedObjects(), player, reportBeforeTurn, gameSessionData.getDeadPlayersDTOS());
		}
		player.incrementPlayerTurn();
		if (nextIndex == 0) {
			updateTime(gameSessionData);
		}
		if (gameSessionData.getPlayersRepository().getPlayerList().stream().filter(Player::isAlive).count() < 2) {
			post(new GameOverForPlayerCommand(gameSessionUuid, playerUuid,true));
		} else {
			try {
				if (gameSessionData.getDeadPlayersDTOS().stream().anyMatch(dto -> dto.getDeadPlayer().equals(player.getPlayerName()))) {
					player.setAlive(false);
					post(new GameOverForPlayerCommand(gameSessionUuid, playerUuid, false));
				} else {
					//FIXME
					//player.setUpPlayerForNewTurn();
					post(new EndTurn(gameSessionUuid, playerUuid));
					//post(reportBeforeTurn);
				}
			} catch (Exception e) {
				System.out.println("Find me here: " + e);
			}

		}
	}

	private void handle(MaydayFailed event, GameSessionData gameSessionData) {
		gameSessionData.getActivePlayer().setAlive(false);
		post(new GameOverForPlayerCommand(event.getGameSessionUuid(), event.getPlayerUuid(), false));
	}

	private void handle(DeathRayDead event, GameSessionData gameSessionData) {
		gameSessionData.getActivePlayer().setAlive(false);
		post(new GameOverForPlayerCommand(event.getGameSessionUuid(), event.getPlayerUuid(), false));
	}

	private void updateTime(GameSessionData gameSessionData) {
		gameSessionData.getWorld().getStarDate().increment(StarDateSpecification.UPDATE_STARDATES_AFTER_TURN);
		List<Rubbish> rubbishes = gameSessionData.getWorld().getRubbishes();
		if (rubbishes != null) {
			for (Rubbish rubbish : rubbishes) {
				if (rubbish.getTimeForDelete() > StarDateSpecification.STARDATES_IN_ONE_TURN) {
					rubbish.deletTime(StarDateSpecification.STARDATES_IN_ONE_TURN);
				} else {
					gameSessionData.getWorld().remove(rubbish);
					gameSessionData.getWorld().getRubbishes().remove(rubbish);
				}
			}
		}
	}
}


