package pl.oakfusion.sst.core;


import pl.oakfusion.bus.MessageBus;
import pl.oakfusion.sst.core.aggregates.HelpResolver;
import pl.oakfusion.sst.core.game.Game;
import pl.oakfusion.sst.core.gamedata.GameSessionDataRepository;
import pl.oakfusion.sst.core.gamesession.GameSessionHandlers;
import pl.oakfusion.sst.core.gamesession.GameSessionQueryProcessors;
import pl.oakfusion.sst.data.util.RandomGenerator;


public final class Application {

	public Application(RandomGenerator randomGenerator, MessageBus messageBus, HelpResolver helpResolver, GameSessionDataRepository gameSessionDataRepository) {
		new Game(gameSessionDataRepository, messageBus, randomGenerator, helpResolver);
		new GameplayProcess(messageBus, gameSessionDataRepository);
		new GameSessionHandlers(randomGenerator, messageBus, gameSessionDataRepository, helpResolver);
		new GameSessionQueryProcessors(messageBus, gameSessionDataRepository);
	}
}

