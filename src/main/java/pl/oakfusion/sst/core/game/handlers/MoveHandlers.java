package pl.oakfusion.sst.core.game.handlers;

import pl.oakfusion.bus.MessageBus;
import pl.oakfusion.data.message.Event;
import pl.oakfusion.sst.core.gamedata.GameSessionData;
import pl.oakfusion.sst.data.game.UnitsMoveDestinations;
import pl.oakfusion.sst.core.gamedata.GameSessionDataRepository;
import pl.oakfusion.sst.core.game.validators.MoveCache;
import pl.oakfusion.sst.core.game.validators.MoveValidator;
import pl.oakfusion.sst.core.game.validators.SstCommandHandlers;
import pl.oakfusion.sst.data.game.MoveEstimation;
import pl.oakfusion.sst.data.game.commands.move.Move;
import pl.oakfusion.sst.data.game.events.move.MovePerformed;
import pl.oakfusion.sst.data.util.Position;
import pl.oakfusion.sst.data.world.component.ComponentId;
import pl.oakfusion.sst.data.world.World;
import pl.oakfusion.sst.data.world.civilizations.Unit;
import pl.oakfusion.sst.data.world.component.EnergyContainer;

import java.util.List;

import static pl.oakfusion.sst.core.game.validators.ValidatorsCollector.validators;

public class MoveHandlers extends SstCommandHandlers {

	MoveCache moveCache;

	public MoveHandlers(MessageBus messageBus, GameSessionDataRepository gameSessionDataRepository) {

		super(messageBus, gameSessionDataRepository);
		moveCache = new MoveCache();
		registerCommandHandler(Move.class, handleWith(this::handle, validators(new MoveValidator(moveCache))));
		//TODO: Move Estimate when SstEventsHandlersValidators are created
		//registerCommandHandler(Estimate.class, this::handle);
	}

	private List<Event> handle(Move command, GameSessionData gameSessionData) {

		World world = gameSessionData.getWorld();
		UnitsMoveDestinations destinations = moveCache.getDestinations(gameSessionData.getActivePlayer().getUuid()).orElseThrow();

		destinations.getDestinationsList().forEach(unitMoveDestination -> {
			Unit unit = unitMoveDestination.getUnit();
			MoveEstimation estimation = unitMoveDestination.getEstimation();
			List<Position> path = estimation.getPath();
			Position source = new Position(unit.getX(), unit.getY());
			Position target = path.get(path.size()-1);

			if(!unitMoveDestination.isAbleToReachDestination() && command.isForceMode() && !command.isAutoMode()){
				estimation = unit.getComponentSystem().getComputer().estimateForcedMove(world, unit, path, source, target, List.of());
				path = estimation.getPath();
				if(path.size() > 0){
					target = path.get(path.size()-1);
				} else{
					target = unit.getPosition();
				}

			}

			int xDestination = target.getX();
			int yDestination = target.getY();
			unit.setX(xDestination);
			unit.setY(yDestination);

			((EnergyContainer) unit.getComponentSystem().getComponent(ComponentId.ENERGY_CONTAINER)).setEnergy(estimation.getEnergyLeft());
			//TODO: decrease action points when fixed
			world.rest(estimation.getTimeCostToLocation());
			world.updatePositionOf(unit);
		});

		return List.of(new MovePerformed(command.getGameSessionUuid(), command.getPlayerUuid(),
				command.isAutoMode(), command.isForceMode(), destinations));
	}



	//TODO: Move Estimate when SstEventsHandlersValidators are created
	/*public void handle(Estimate command) {
		Unit unit = gameData.getActivePlayer().getCivilization().getActiveUnit();
		Computer computer = unit.getSystem().getComponent(ComponentId.COMPUTER);
		if (computer.isComputerDown()) {
			post(new EstimateFailed());
		}
		System.out.println("handle estimate2");
		MoveEstimation estimation = computer.estimate(
				gameData.getWorld(),
				new UnitDTO(unit),
				command.isAutoMode(),
				command.getX(),
				command.getY(),
				command.getWarpFactor()
		);
		System.out.println("New Estimation");
		post(new MovementEstimated(command.isAutoMode(), command.getWarpFactor(), estimation));
	}*/



}
