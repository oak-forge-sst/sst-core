package pl.oakfusion.sst.core.game.validators;

import pl.oakfusion.sst.core.gamedata.GameSessionData;
import pl.oakfusion.sst.data.validationfailures.NoTechnologyError;
import pl.oakfusion.sst.data.validationfailures.ValidationFailure;
import pl.oakfusion.sst.data.game.commands.mayday.Mayday;

import java.util.ArrayList;
import java.util.List;

public class MaydayCommandValidator implements SstCommandValidator<Mayday> {
	Boolean hasTechnology = true;

	// TODO change for proper technology check
	@Override
	public List<ValidationFailure> validate(Mayday command, GameSessionData gameSessionData) {
		if ( hasTechnology ) {
			return new ArrayList<>();
		} else {
			return List.of(new NoTechnologyError( "You do not have sufficient technology", new ArrayList<>()));
		}
	}
}
