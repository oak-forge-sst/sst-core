package pl.oakfusion.sst.core.game.validators;

import pl.oakfusion.data.message.Message;
import pl.oakfusion.sst.core.gamedata.GameSessionData;
import pl.oakfusion.sst.data.exceptions.NotFoundException;
import pl.oakfusion.sst.data.validationfailures.ValidationFailure;

import java.util.List;

@FunctionalInterface
public interface Validator<M extends Message> {

	List<ValidationFailure> validate(M message, GameSessionData gameSessionData) throws NotFoundException;
}
