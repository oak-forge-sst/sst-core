package pl.oakfusion.sst.core.game.handlers;

import pl.oakfusion.bus.MessageBus;
import pl.oakfusion.data.message.Event;
import pl.oakfusion.sst.core.game.EmptySurrounding;
import pl.oakfusion.sst.core.gamedata.GameSessionData;
import pl.oakfusion.sst.core.game.validators.MaydayCommandValidator;
import pl.oakfusion.sst.core.gamedata.GameSessionDataRepository;
import pl.oakfusion.sst.core.game.validators.SstCommandHandlers;
import pl.oakfusion.sst.data.DTO.DeadPlayersDTO;
import pl.oakfusion.sst.data.game.commands.mayday.Mayday;
import pl.oakfusion.sst.data.game.events.dock.AlreadyDocked;
import pl.oakfusion.sst.data.game.events.mayday.MaydayFailed;
import pl.oakfusion.sst.data.game.events.mayday.MaydayPerformed;
import pl.oakfusion.sst.data.game.events.mayday.RadioBroken;
import pl.oakfusion.sst.data.util.Position;
import pl.oakfusion.sst.data.util.RandomGenerator;
import pl.oakfusion.sst.data.world.component.ComponentId;
import pl.oakfusion.sst.data.world.component.Condition;
import pl.oakfusion.sst.data.world.gameobject.Starbase;
import pl.oakfusion.sst.data.world.component.SubspaceRadio;
import pl.oakfusion.sst.data.world.civilizations.Unit;
import pl.oakfusion.sst.data.world.component.Computer;
import pl.oakfusion.utils.PreconditionUtils;
import java.util.List;
import java.util.UUID;

public class MaydayHandlers extends SstCommandHandlers {
	private final RandomGenerator randomGenerator;
	private final EmptySurrounding emptySurrounding = new EmptySurrounding();
	private static final int MAX_RANGE = 1;
	private static final int NUMBER_SECTORS = 30;
	private static final int MINIMUM_CHANCE = 0;
	private static final int MAXIMUM_CHANCE = 1;
	private static final double PROBABILITY_BELOW_NUMBER_SECTORS = 0.9;
	private static final double PROBABILITY_ABOVE_NUMBER_SECTORS = 0.5;

	public MaydayHandlers(GameSessionDataRepository gameSessionDataRepository, MessageBus messageBus,
                          RandomFailureCommandHandler randomFailureCommandHandler, RandomGenerator randomGenerator) {
		super(messageBus, gameSessionDataRepository);
		this.randomGenerator = PreconditionUtils.checkNotNull(randomGenerator);
		final RandomFailureCommandHandler randomly = PreconditionUtils.checkNotNull(randomFailureCommandHandler);
		registerCommandHandler(Mayday.class, handleWith(randomly.failingHandler(this::handleMayday), List.of(new MaydayCommandValidator())));
	}

	private List<Event> handleMayday(Mayday command, GameSessionData gameSessionData, UUID unitUuid) {
		Unit unit = gameSessionData.getActivePlayer().getCivilization().getActiveUnits()
				.stream().filter(u -> u.getUuid().equals(unitUuid)).findFirst().orElseThrow();
		if (unit.getCondition() == Condition.DOCKED) {
			return List.of(new AlreadyDocked(command.getGameSessionUuid(), command.getPlayerUuid()));
		}
		if (((SubspaceRadio) unit.getComponentSystem().getComponent(ComponentId.SUBSPACE_RADIO)).isBroken()) {
			return List.of(new RadioBroken(command.getGameSessionUuid(), command.getPlayerUuid()));
		}

		Computer computer = unit.getComponentSystem().getComponent(ComponentId.COMPUTER);
		Starbase starbase = computer
				.getClosestStarbase(gameSessionData.getWorld(), unit.getX(), unit.getY());
		if (starbase != null) {
			double distance = Computer
					.computeDistance(starbase.getX(), starbase.getY(), unit.getX(), unit.getY());
			double probability = getMaterializationProbability(distance);

			if (randomGenerator.nextDoubleInRange(MINIMUM_CHANCE, MAXIMUM_CHANCE) < probability) {
				List<Position> emptySurroundingPositions = emptySurrounding.getEmptySurrounding(
						new Position(starbase.getX(), starbase.getY()), gameSessionData, MAX_RANGE);
				if (!emptySurroundingPositions.isEmpty()) {
					Position randomPosition = emptySurroundingPositions
							.get(randomGenerator.nextInt(emptySurroundingPositions.size()));
					unit.moveTo(randomPosition.getX(), randomPosition.getY());
					gameSessionData.getWorld().updatePositionOf(unit);
					unit.dock();
					return List.of(new MaydayPerformed(command.getGameSessionUuid(), command.getPlayerUuid()));
				}
			}
		}
		return List.of(getMaydayFailed(command, gameSessionData));
	}

	private MaydayFailed getMaydayFailed(Mayday command, GameSessionData gameSessionData) {
		return new MaydayFailed(command.getGameSessionUuid(), command.getPlayerUuid(),
				new DeadPlayersDTO(gameSessionData.getActivePlayer().getPlayerName(), MaydayFailed.class));
	}

	private double getMaterializationProbability(double distance) {
		return distance <= NUMBER_SECTORS ?
				PROBABILITY_BELOW_NUMBER_SECTORS : PROBABILITY_ABOVE_NUMBER_SECTORS;
	}
}
