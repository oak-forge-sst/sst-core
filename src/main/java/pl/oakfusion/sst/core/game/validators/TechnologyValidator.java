package pl.oakfusion.sst.core.game.validators;

import pl.oakfusion.sst.core.gamedata.GameSessionData;
import pl.oakfusion.sst.data.validationfailures.NoTechnologyError;
import pl.oakfusion.sst.data.game.commands.GameCommand;
import pl.oakfusion.sst.data.validationfailures.ValidationFailure;
import pl.oakfusion.sst.data.world.civilizations.Unit;

import java.util.ArrayList;
import java.util.List;

public class TechnologyValidator <C extends GameCommand> implements SstCommandValidator<C> {
	@Override
	public List<ValidationFailure> validate(C command, GameSessionData gameSessionData) {
		List<ValidationFailure> validationFailures = new ArrayList<>();
		List<Unit> units = gameSessionData.getActivePlayer().getCivilization().getActiveUnits();
		units.forEach(unit -> {
			if(!unit.canPerformCommand(command.getClass())){
				validationFailures.add(new NoTechnologyError("", List.of(), unit.getUuid()));
			}
		});
		return validationFailures;
	}
}
