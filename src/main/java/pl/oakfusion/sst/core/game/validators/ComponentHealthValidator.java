package pl.oakfusion.sst.core.game.validators;

import pl.oakfusion.sst.core.gamedata.GameSessionData;
import pl.oakfusion.sst.data.game.commands.GameCommand;
import pl.oakfusion.sst.data.validationfailures.ComponentLowHealthWarning;
import pl.oakfusion.sst.data.validationfailures.ValidationFailure;
import pl.oakfusion.sst.data.world.component.ComponentId;
import pl.oakfusion.sst.data.world.civilizations.Unit;
import pl.oakfusion.sst.data.world.component.CommandToComponentIdChanger;
import pl.oakfusion.sst.data.world.component.DamagedFunctionalitySpecification;

import java.util.ArrayList;
import java.util.List;

public class ComponentHealthValidator <C extends GameCommand> implements SstCommandValidator<C> {
	@Override
	public List<ValidationFailure> validate(C command, GameSessionData gameSessionData) {
		ComponentId componentId = CommandToComponentIdChanger.getComponentIdByCommand(command.getClass()).orElseThrow();
		List<ValidationFailure> validationFailures = new ArrayList<>();
		List<Unit> units = gameSessionData.getActivePlayer().getCivilization().getActiveUnits();
		units.forEach(
				unit -> {
					if(unit.getComponentSystem().getComponent(componentId).calculateHealthPercentage() <= DamagedFunctionalitySpecification.DEFAULT_LEVEL_OF_FUNCTIONALITY && !command.isIgnoreComponentLowHealth()){
						validationFailures.add(new ComponentLowHealthWarning("Low health of component - high probability of failure. If you want to execute anyway send command with ignore component low health parameter.",
								List.of(), componentId, unit.getUuid()));
					}
				});
		return validationFailures;
	}
}
