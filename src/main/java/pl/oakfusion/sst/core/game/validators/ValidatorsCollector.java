package pl.oakfusion.sst.core.game.validators;

import pl.oakfusion.data.message.Message;
import pl.oakfusion.sst.data.validationfailures.ValidationFailure;

import java.util.ArrayList;
import java.util.List;

public abstract class ValidatorsCollector {

	@SafeVarargs
	public static <M extends Message> List<Validator<M>> validators(Validator<M>... validators) {
		return List.of(validators);
	}

	public static <M extends Message> List<Validator<M>> noValidators() {
		return List.of();
	}

	@SafeVarargs
	public static <M extends Message> List<Validator<M>> failFirstValidators(Validator<M>... validators){
		return List.of((message, gameSessionData) -> {
			List<ValidationFailure> failures = new ArrayList<>();
			for(Validator<M> validator: validators){
				List<ValidationFailure> validatorFailures = validator.validate(message, gameSessionData);
				failures.addAll(validatorFailures);
				if(!failures.isEmpty()){
					return failures;
				}
			}
			return failures;
		});
	}
}
