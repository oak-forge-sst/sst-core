package pl.oakfusion.sst.core.game;

import pl.oakfusion.sst.data.DTO.DamagedObjects;
import pl.oakfusion.sst.data.DTO.DeadPlayersDTO;
import pl.oakfusion.sst.data.DTO.ReportDTO;
import pl.oakfusion.sst.data.game.events.turn.ReportBeforeTurn;
import pl.oakfusion.sst.data.messagegenerator.MessageKey;
import pl.oakfusion.sst.data.util.Position;
import pl.oakfusion.sst.data.world.player.Player;

import java.util.List;
import java.util.Objects;

public class CreateActionReport {

	public void createReport(int turn, List<DamagedObjects> damagedObjects, Player player, ReportBeforeTurn reportBeforeTurn, List<DeadPlayersDTO> deadPlayersDTO) {
		for (DamagedObjects object : damagedObjects) {
			if (object.getTurn() == turn && object.getDefender() != null && object.getDefender().getUuid().equals(player.getUuid())) {
				reportBeforeTurn.addElementToReport(new ReportDTO(
						object.getName(),
						object.getDefenderUnitType(),
						new Position(object.getX(), object.getY()),
						object.isAlive() ? MessageKey.ATTACKED_BY : MessageKey.DESTROYED_BY,
						object.getAttackerUnitType(),
						object.getAttacker().getPlayerName()));
			}
		}
		deadPlayersDTO
				.stream()
				.filter(Objects::nonNull)
				.forEach(reportBeforeTurn::addElementToDeadPlayers);
	}
}
