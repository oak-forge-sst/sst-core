package pl.oakfusion.sst.core.game.validators;

import pl.oakfusion.sst.data.SstCommand;


public interface SstCommandValidator<C extends SstCommand> extends Validator<C> {
}
