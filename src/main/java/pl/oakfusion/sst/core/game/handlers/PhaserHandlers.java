
package pl.oakfusion.sst.core.game.handlers;

import pl.oakfusion.bus.CommandDriven;
import pl.oakfusion.bus.MessageBus;
import pl.oakfusion.sst.core.gamedata.GameSessionData;
import pl.oakfusion.sst.core.game.DeceasedPlayersBuilder;
import pl.oakfusion.sst.core.gamedata.GameSessionDataRepository;
import pl.oakfusion.sst.data.DTO.phasersDTO.PhasersDTO;
import pl.oakfusion.sst.data.DTO.phasersDTO.PhasersUnitHitDTO;
import pl.oakfusion.sst.data.exceptions.NotFoundException;
import pl.oakfusion.sst.data.world.gameobject.GameObject;
import pl.oakfusion.sst.data.game.commands.phasers.Phasers;
import pl.oakfusion.sst.data.game.events.phasers.EnergyNotSpecifiedForPhasers;
import pl.oakfusion.sst.data.game.events.phasers.PhasersFailed;
import pl.oakfusion.sst.data.game.events.phasers.PhasersPerformed;
import pl.oakfusion.sst.data.game.events.phasers.UseWhenDock;
import pl.oakfusion.sst.data.util.Position;
import pl.oakfusion.sst.data.util.RandomGenerator;
import pl.oakfusion.sst.data.world.component.ComponentId;
import pl.oakfusion.sst.data.world.component.Condition;
import pl.oakfusion.sst.data.world.civilizations.Unit;
import pl.oakfusion.sst.data.world.component.Computer;
import pl.oakfusion.sst.data.world.component.DamagedFunctionalitySpecification;
import pl.oakfusion.sst.data.world.component.EnergyContainer;

import java.util.*;
import java.util.stream.Collectors;

import static pl.oakfusion.sst.data.initializer.CommandSpecification.*;
import static pl.oakfusion.sst.data.util.MathUtils.max;

public class PhaserHandlers extends CommandDriven {
	public static final int RANGE = 7;
	private final MessageBus messageBus;
	private GameSessionDataRepository gameSessionDataRepository;
	private RandomGenerator random;
	private double energyWasted = 0;
	private List<PhasersUnitHitDTO> unitHit = new ArrayList<>();

	public PhaserHandlers(GameSessionDataRepository gameSessionDataRepository, MessageBus messageBus, RandomGenerator random) {
		super(messageBus);
		this.gameSessionDataRepository = gameSessionDataRepository;
		this.messageBus = messageBus;
		this.random = random;
	}

	public void handle(Phasers command, GameSessionData gameSessionData, UUID unitUuid) throws NotFoundException {
		DeceasedPlayersBuilder deceasedPlayersBuilder = new DeceasedPlayersBuilder(gameSessionData.getPlayersRepository().getPlayerList());
		Unit unit = gameSessionData.getActivePlayer().getCivilization().getActiveUnitByUuid(unitUuid)
				.orElseThrow();
		Position unitPosition = new Position(unit.getX(), unit.getY());
		//TODO: when moving Phasers, take care of attack power:
		// attack power was a field of Unit, but was only used by this piece of code,
		// so it should be moved to PhasersDevice
		int usedEnergy = 0;
//		double usedEnergy = unit.getAttackPower().orElse(Double.valueOf(command.getPhaserFactor().stream().reduce(Integer::sum).orElse(-1)));
		if (usedEnergy == -1) {
			post(new EnergyNotSpecifiedForPhasers(command.getGameSessionUuid(), command.getPlayerUuid()));
			return;
		}
		if (unit.getCondition().equals(Condition.DOCKED)){
			post(new UseWhenDock(command.getGameSessionUuid(), command.getPlayerUuid()));
			return;
		}
		if (unit.getComponentSystem().getComponents().containsKey(ComponentId.ENERGY_CONTAINER) ) {
			double sumEnergy = ((EnergyContainer) unit.getComponentSystem().getComponent(ComponentId.ENERGY_CONTAINER)).getEnergy();
			if (sumEnergy < usedEnergy ) {
				post(new PhasersFailed(command.getGameSessionUuid(), command.getPlayerUuid()));
				return;
			} else {
				removeEnergy(usedEnergy, gameSessionData, unitUuid);
			}
		}

		unitHit.clear();
		if (command.getUnits() != null) {
			Map<Unit, Double> unitList = new LinkedHashMap<>();
			command.getUnits().forEach(u -> {
				if(!u.getCondition().equals(Condition.DOCKED)) {
					double distance = Computer.computeDistance(unitPosition.getX(), unitPosition.getY(), u.getX(), u.getY());
					unitList.put((Unit) gameSessionData.getWorld().getGameObject(u.getX(), u.getY()).orElseThrow(), distance);
				}
			});
			attackOpponentsManual(getTrueEnergyFired(command.getPhaserFactor(), gameSessionData, unitUuid), unitList, gameSessionData, unitUuid);
		} else {
			if (command.isAuto()) {
				attackOpponentsAuto(unitPosition, getTrueEnergyFired(usedEnergy, gameSessionData, unitUuid), gameSessionData, unitUuid);
			} else {
				attackOpponentsManual(getTrueEnergyFired(command.getPhaserFactor(), gameSessionData, unitUuid), sortByValue(getOpponentsWithDistance(unitPosition, gameSessionData)), gameSessionData, unitUuid);
			}
		}
		//TODO: when moving Phasers, take care of canPerformAction:
		// it was a field of Unit, but was only used by this piece of code (it is never read, only set in here)
		// so it should probably be moved to PhasersDevice. Drop in the logic, should
		// Phasers be deactivated after usage? If so, add additional functionality to Phasers device
//		unit.setCanPerformAction(false);
		post(new PhasersPerformed(command.getGameSessionUuid(), command.getPlayerUuid(),
				getPhasersDTO(), deceasedPlayersBuilder.getRecentlyDeceasedPlayers(gameSessionData.getActivePlayer().getPlayerName(), PhasersPerformed.class)));
	}

	private List<Double> getTrueEnergyFired(List<Integer> energyUsedList, GameSessionData gameSessionData, UUID unitUuid) {
		//todo: as before, move attack power
		Optional<Double> attackPower = Optional.ofNullable(1.0);
//		Optional<Double> attackPower = gameSessionData.getActivePlayer().getCivilization().getActiveUnit().getAttackPower();
		if (attackPower.isPresent()) {
			double attackPowerLeft = attackPower.get();
			List<Double> trueEnergyFiredList = new ArrayList<>();
			for (double a : energyUsedList) {
				if (a > attackPowerLeft) {
					trueEnergyFiredList.add(attackPowerLeft);
					attackPowerLeft = 0;
				} else {
					trueEnergyFiredList.add(a);
					attackPowerLeft -= a;
				}
			}
			return trueEnergyFiredList;
		} else {
			return energyUsedList.stream().map(e -> getTrueEnergyFired(e, gameSessionData, unitUuid)).collect(Collectors.toList());
		}
	}

	private double getTrueEnergyFired(Integer energyUsed, GameSessionData gameSessionData, UUID unitUuid) {
		Unit unit = gameSessionData.getActivePlayer().getCivilization().getActiveUnitByUuid(unitUuid)
				.orElseThrow();
		return energyUsed * max(unit.getComponentSystem().getComponent(ComponentId.PHASERS_DEVICE).calculateHealthPercentage(), DamagedFunctionalitySpecification.DEFAULT_LEVEL_OF_FUNCTIONALITY).orElseThrow();
	}

	private void removeEnergy(double usedEnergy, GameSessionData gameSessionData, UUID unitUuid) {
		EnergyContainer energyContainer = gameSessionData.getActivePlayer().getCivilization().getActiveUnits().stream().
				filter(u -> u.getUuid().equals(unitUuid)).findAny().orElseThrow().getComponentSystem().getComponent(ComponentId.ENERGY_CONTAINER);
		double currentEnergy = energyContainer.getEnergy() - usedEnergy;
		energyContainer.setEnergy(currentEnergy);
	}

	public List<Unit> getOpponentsInRange(Position unitPosition, GameSessionData gameSessionData) {
		List<GameObject> objectsInRange = gameSessionData.getWorld().getGameObjectsInRange(
				unitPosition.getX(),
				unitPosition.getY(),
				RANGE
		);
		return objectsInRange.stream().filter(gameObject -> gameObject instanceof Unit).map(gameObject -> (Unit) gameObject).filter(g -> !g.getUuid().equals(gameSessionData.getActivePlayer().getUuid())).filter(unit -> !unit.getCondition().equals(Condition.DOCKED)).collect(Collectors.toList());
	}

	private Map<Unit, Double> getOpponentsWithDistance(Position unitPosition, GameSessionData gameSessionData) {
		Map<Unit, Double> opponentsMap = new HashMap<>();
		List<Unit> opponents = getOpponentsInRange(unitPosition, gameSessionData);

		for (Unit o : opponents) {
			double dist = Math.sqrt((unitPosition.getX() - o.getX()) * (unitPosition.getX() - o.getX()) + (unitPosition.getY() - o.getY()) * (unitPosition.getY() - o.getY()));
			opponentsMap.put(o, dist);
		}

		return opponentsMap;
	}

	private double estimateAttack(double dmg, double dist, double rand) {
		double div = Math.log(dist + 1) / Math.log(2);
		return (rand * dmg) * div;
	}

	private double attack(double dmg, double dist, double rand) {
		double div = Math.log(dist + 1) / Math.log(2);
		return (rand * dmg) / div;
	}


	private void attackOpponentsAuto(Position unitPosition, double usedEnergy, GameSessionData gameSessionData, UUID unitUuid) throws NotFoundException {
		HashMap<Unit, Double> sortOpponentsMap = (HashMap<Unit, Double>) sortByValue(getOpponentsWithDistance(unitPosition, gameSessionData));
		for (Map.Entry<Unit, Double> entry : sortOpponentsMap.entrySet()) {
			if (usedEnergy > 0) {
				double rand = random.nextDoubleInRange(MIN_DMG, MAX_DMG);
				double estimateAttack = estimateAttack(entry.getKey().getHealthWithShields(), entry.getValue(), rand);
				double attack;
				if (estimateAttack > usedEnergy) {
					attack = attack(usedEnergy, entry.getValue(), rand);
					usedEnergy = 0;
				} else {
					attack = entry.getKey().getHealthWithShields();
					usedEnergy -= estimateAttack;
				}
				entry.getKey().receiveHit(attack);
				unitHit.add(new PhasersUnitHitDTO(attack, entry.getKey().getType(), entry.getKey().isObjectAlive(), new Position(entry.getKey().getX(), entry.getKey().getY())));
				gameSessionData.addElementToDamagedObjects(gameSessionData.getActivePlayer(),
						gameSessionData.getPlayersRepository().getPlayerByUuid(entry.getKey().getUuid()),
						entry.getKey().isObjectAlive(),
						entry.getKey().getType(),
						entry.getKey().getX(),
						entry.getKey().getY(),
						entry.getKey().getUnitName(),
						gameSessionData.getActivePlayer().getCivilization().getActiveUnits().stream()
								.filter(u -> u.getUuid().equals(unitUuid)).findAny().orElseThrow().getType());
				if (!entry.getKey().isObjectAlive()) {
					gameSessionData.getWorld().remove(entry.getKey());
				}
			}
		}
		energyWasted = usedEnergy;
	}

	private void attackOpponentsManual(List<Double> phaserFactor, Map<Unit, Double> sortOpponentsMap, GameSessionData gameSessionData, UUID unitUuid) throws NotFoundException {
		int maxSize = Math.min(phaserFactor.size(), sortOpponentsMap.size());
		ArrayList<Unit> keyList = new ArrayList<>(sortOpponentsMap.keySet());
		for (int i = 0; i < maxSize; i++) {
			if (phaserFactor.get(i) > 0) {
				double rand = random.nextDoubleInRange(MIN_DMG, MAX_DMG);
				double attack = attack(phaserFactor.get(i), sortOpponentsMap.get(keyList.get(i)), rand);
				keyList.get(i).receiveHit(attack);
				unitHit.add(new PhasersUnitHitDTO(attack, keyList.get(i).getType(), keyList.get(i).isObjectAlive(), new Position(keyList.get(i).getX(), keyList.get(i).getY())));
				gameSessionData.addElementToDamagedObjects(gameSessionData.getActivePlayer(),
						gameSessionData.getPlayersRepository().getPlayerByUuid(keyList.get(i).getUuid()),
						keyList.get(i).isObjectAlive(),
						keyList.get(i).getType(),
						keyList.get(i).getX(),
						keyList.get(i).getY(),
						keyList.get(i).getUnitName(),
						gameSessionData.getActivePlayer().getCivilization().getActiveUnits().stream()
								.filter(u -> u.getUuid().equals(unitUuid)).findAny().orElseThrow().getType());
				if (!keyList.get(i).isObjectAlive()) {
					gameSessionData.getWorld().remove(keyList.get(i));
				}
			}
		}
	}

	private static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
		List<Map.Entry<K, V>> list = new ArrayList<>(map.entrySet());
		list.sort(Map.Entry.comparingByValue());

		Map<K, V> result = new LinkedHashMap<>();
		for (Map.Entry<K, V> entry : list) {
			result.put(entry.getKey(), entry.getValue());
		}

		return result;
	}

	private PhasersDTO getPhasersDTO() {
		return new PhasersDTO(unitHit, energyWasted);
	}

}

