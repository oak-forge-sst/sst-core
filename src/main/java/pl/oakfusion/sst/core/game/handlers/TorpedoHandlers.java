
package pl.oakfusion.sst.core.game.handlers;

import pl.oakfusion.bus.MessageBus;
import pl.oakfusion.data.message.Event;
import pl.oakfusion.sst.core.game.Game;
import pl.oakfusion.sst.core.gamedata.GameSessionData;
import pl.oakfusion.sst.core.game.DeceasedPlayersBuilder;
import pl.oakfusion.sst.core.gamedata.GameSessionDataRepository;
import pl.oakfusion.sst.core.game.validators.SstCommandHandlers;
import pl.oakfusion.sst.core.game.validators.TorpedoValidator;
import pl.oakfusion.sst.data.exceptions.NotFoundException;
import pl.oakfusion.sst.data.initializer.CommandSpecification;
import pl.oakfusion.sst.data.DTO.torpedo.ExplosionEventDTO;
import pl.oakfusion.sst.data.DTO.torpedo.ObjectExplosionDTO;
import pl.oakfusion.sst.data.DTO.torpedo.TorpedoTrackDTO;
import pl.oakfusion.sst.data.DTO.torpedo.UnitHitDTO;
import pl.oakfusion.sst.data.world.gameobject.GameObject;
import pl.oakfusion.sst.data.world.gameobject.MovableGameObject;
import pl.oakfusion.sst.data.world.gameobject.StaticGameObject;
import pl.oakfusion.sst.data.game.commands.torpedo.Torpedo;
import pl.oakfusion.sst.data.game.events.torpedoes.TorpedoPerformed;
import pl.oakfusion.sst.data.util.Position;
import pl.oakfusion.sst.data.util.RandomGenerator;
import pl.oakfusion.sst.data.world.component.ComponentId;
import pl.oakfusion.sst.data.world.gameobject.Star;
import pl.oakfusion.sst.data.world.civilizations.Unit;
import pl.oakfusion.sst.data.world.component.TorpedoesDevice;

import java.util.*;
import java.util.stream.Collectors;

import static pl.oakfusion.sst.core.game.validators.ValidatorsCollector.validators;


public class TorpedoHandlers extends SstCommandHandlers {

	private final RandomGenerator randomGenerator;
	private List<ExplosionEventDTO> explosionEventDTOList;

	public TorpedoHandlers(MessageBus messageBus, RandomGenerator randomGenerator,
						   GameSessionDataRepository gameSessionDataRepository, RandomFailureCommandHandler randomFailureCommandHandler) {
		super(messageBus, gameSessionDataRepository);
		this.randomGenerator = randomGenerator;
		final RandomFailureCommandHandler randomly = randomFailureCommandHandler;
		registerCommandHandler(Torpedo.class, handleWith(randomly.failingHandler(this::handle), validators(new TorpedoValidator())));
	}

	List<Event> handle(Torpedo command, GameSessionData gameSessionData, UUID unitUuid) throws NotFoundException {
		Unit unit = gameSessionData.getActivePlayer().getCivilization().getActiveUnitByUuid(unitUuid)
				.orElseThrow();
		Position unitPosition = new Position(unit.getX(), unit.getY());
		int torpedoes = ((TorpedoesDevice) unit.getComponentSystem().getComponent(ComponentId.TORPEDOES_DEVICE)).getTorpedoes();

		List<TorpedoTrackDTO> hitObjectsPositions = new ArrayList<>();
		DeceasedPlayersBuilder deceasedPlayersBuilder = new DeceasedPlayersBuilder(gameSessionData.getPlayersRepository().getPlayerList());

		for (Position p : command.getTarget()) {
			Position positionWithDeviation = getActualHitPosition(p, unitPosition);
			Optional<GameObject> targetObject = getHitObject(positionWithDeviation, unitPosition, gameSessionData);
			if(targetObject.isPresent()){
				hitObject(targetObject.get(), unitPosition, gameSessionData, unit);
				hitObjectsPositions.add(new TorpedoTrackDTO(positionWithDeviation, explosionEventDTOList));
			}
			if(targetObject.isPresent()) {
				hitObject(targetObject.get(), unitPosition, gameSessionData, unit);
				hitObjectsPositions.add(new TorpedoTrackDTO(positionWithDeviation, explosionEventDTOList));
			}
		}

		((TorpedoesDevice) unit.getComponentSystem()
				.getComponent(ComponentId.TORPEDOES_DEVICE)).setTorpedoes(torpedoes - command.getTorpedoAmountIntendedToShoot());

		return List.of(new TorpedoPerformed(command.getGameSessionUuid(), command.getPlayerUuid(),
				hitObjectsPositions, command.getTorpedoAmountIntendedToShoot(),
				deceasedPlayersBuilder.getRecentlyDeceasedPlayers(gameSessionData.getActivePlayer().getPlayerName(), TorpedoPerformed.class)));

	}

	private Position getActualHitPosition(Position p, Position enterprisePosition) {
		return (randomGenerator.nextDoubleInRange(0, 1) < CommandSpecification.MISS_RANGE) ?
				getHitPositionWithDeviation(p, enterprisePosition) :
				p;
	}

	private Position getHitPositionWithDeviation(Position p, Position enterprisePosition) {
		int dx = p.getX() - enterprisePosition.getX();
		int dy = p.getY() - enterprisePosition.getY();
		double distance = Math.sqrt(dx * dx + dy * dy);
		double directionX = Integer.compare(dx, 0);
		double directionY = Integer.compare(dy, 0);
		if (isVertical(dx, dy)) {
			directionX = 0;
		} else if (isHorizontal(dx, dy)) {
			directionY = 0;
		}
		return new Position(
				(int) (p.getX() + distance * randomGenerator.nextDoubleInRange(-CommandSpecification.MISS_PROBABILITY, CommandSpecification.MISS_PROBABILITY) + distance * directionX * CommandSpecification.MISS_PROBABILITY),
				(int) (p.getY() + distance * randomGenerator.nextDoubleInRange(-CommandSpecification.MISS_PROBABILITY, CommandSpecification.MISS_PROBABILITY) + distance * directionY * CommandSpecification.MISS_PROBABILITY)
		);
	}

	private Optional<GameObject> getHitObject(Position p, Position unitPosition, GameSessionData gameSessionData) {
		List<GameObject> objectsOnWay = gameSessionData.getWorld().getGameObjectsInLineRange(
				unitPosition.getX(),
				unitPosition.getY(),
				p.getX(),
				p.getY(),
				CommandSpecification.RANGE_FOR_GET_IN_LINE
		);

		if (!objectsOnWay.isEmpty()) {
			List<GameObject> objectsOnWayInRange;
			objectsOnWayInRange = objectsOnWay.stream()
					.filter(gameObject -> gameObject.getX() > unitPosition.getX() - CommandSpecification.RANGE_FOR_TORPEDOES)
					.filter(gameObject -> gameObject.getX() < unitPosition.getX() + CommandSpecification.RANGE_FOR_TORPEDOES)
					.filter(gameObject -> gameObject.getY() > unitPosition.getY() - CommandSpecification.RANGE_FOR_TORPEDOES)
					.filter(gameObject -> gameObject.getY() < unitPosition.getY() + CommandSpecification.RANGE_FOR_TORPEDOES)
					.collect(Collectors.toList());
			if (!objectsOnWayInRange.isEmpty()) {
				if (objectsOnWayInRange.get(0).getX() == unitPosition.getX() && objectsOnWayInRange.get(0).getY() == unitPosition.getY()) {
					if (objectsOnWayInRange.size() > 1) {
						return Optional.ofNullable((objectsOnWayInRange.get(1)));
					}
				} else {
					return Optional.ofNullable((objectsOnWayInRange.get(0)));
				}
			}
		}
		return Optional.empty();
	}

	private void hitObject(GameObject hitObject, Position unitPosition, GameSessionData gameSessionData, Unit attacker) throws NotFoundException {
		explosionEventDTOList = new ArrayList<>();
		receiveHit(hitObject, unitPosition, gameSessionData, attacker);
		if (hitObject instanceof StaticGameObject) {
			explosionEventDTOList.add(new ObjectExplosionDTO(new Position(hitObject.getX(), hitObject.getY()), hitObject.getObjectType(),false));
			gameSessionData.addElementToDamagedObjects(gameSessionData.getActivePlayer(),
					attacker.getType(),
					hitObject.getObjectType());
			explode(new Position(hitObject.getX(), hitObject.getY()), gameSessionData);
		} else if (hitObject instanceof Unit) {
			explosionEventDTOList.add(new UnitHitDTO(new Position(hitObject.getX(), hitObject.getY()), ((Unit) hitObject).getType(), false));
		}
	}

	private void explode(Position starPosition, GameSessionData gameSessionData) throws NotFoundException {
		List<GameObject> objectsInExplosionRange = gameSessionData.getWorld().getGameObjectsInRange(starPosition.getX(), starPosition.getY(), CommandSpecification.EXPLOSION_RANGE);
		List<Position> starsToExplode = new ArrayList<>();
		for(GameObject object : objectsInExplosionRange){
			if (object instanceof Star) {
				starsToExplode.add(new Position(object.getX(), object.getY()));
				explosionEventDTOList.add(new ObjectExplosionDTO(new Position(object.getX(),object.getY()),object.getObjectType(),false));
			}
			else if (object instanceof Unit) {
				explosionEventDTOList.add(new UnitHitDTO(new Position(object.getX(),object.getY()),((Unit)object).getType(),false));
			}
			receiveHitFromExplosion(object, new Position(starPosition.getX(), starPosition.getY()), gameSessionData);
		}
		for(Position star : starsToExplode){
			explode(star, gameSessionData);
		}
	}

	private void receiveHit(GameObject receiver, Position attackerPosition, GameSessionData gameSessionData, Unit attacker) throws NotFoundException {
		receiver.receiveHit(CommandSpecification.EXPLOSION_HIT);
		if (receiver instanceof Unit) {
			gameSessionData.addElementToDamagedObjects(gameSessionData.getActivePlayer(),
					gameSessionData.getPlayersRepository().getPlayerByUuid(((Unit) receiver).getUuid()),
					receiver.isObjectAlive(),
					((Unit) receiver).getType(),
					receiver.getX(),
					receiver.getY(),
					((Unit) receiver).getUnitName(),
					attacker.getType());
		}
		if (!receiver.isObjectAlive()) {
			gameSessionData.getWorld().remove(receiver);
		} else {
			if (receiver instanceof MovableGameObject) {
				blowAway(attackerPosition, (MovableGameObject) receiver, gameSessionData);
			}
		}
	}

	private void receiveHitFromExplosion(GameObject receiver, Position attackerPosition, GameSessionData gameSessionData) throws NotFoundException {
		receiver.receiveHit(CommandSpecification.EXPLOSION_HIT);
		if (receiver instanceof Unit) {
			gameSessionData.addElementToDamagedObjects(
					gameSessionData.getPlayersRepository().getPlayerByUuid(((Unit) receiver).getUuid()),
					receiver.isObjectAlive(),
					((Unit) receiver).getType(),
					receiver.getX(),
					receiver.getY(),
					((Unit) receiver).getUnitName());
		}
		if (!receiver.isObjectAlive()) {
			gameSessionData.getWorld().remove(receiver);
		} else {
			if (receiver instanceof MovableGameObject) {
				blowAway(attackerPosition, (MovableGameObject) receiver, gameSessionData);
			}
		}
	}

	private void blowAway(Position explosionPosition, MovableGameObject object, GameSessionData gameSessionData) {
		int x = object.getX();
		int y = object.getY();
		int dx = x - explosionPosition.getX();
		int dy = y - explosionPosition.getY();
		int directionX = Integer.compare(dx, 0);
		int directionY = Integer.compare(dy, 0);
		List<Position> positionList;

		if (isHorizontal(dx, dy)) {
			positionList = getPositionsForHorizontal(x, y, directionX);
		} else if (isVertical(dx, dy)) {
			positionList = getPositionsForVertical(x, y, directionY);
		} else {
			positionList = getPositionsForDiagonal(x, y, directionX, directionY);
		}

		positionList = positionList.stream().filter(p ->
				p.getY() >= 0 && p.getY() < gameSessionData.getWorld().getAbsoluteWidth() &&
						p.getX() >= 0 && p.getX() < gameSessionData.getWorld().getAbsoluteHeight()).collect(Collectors.toList());

		if (!positionList.isEmpty()) {
			Position position = positionList.get(randomGenerator.nextInt(positionList.size()));
			if(gameSessionData.getWorld().getGameObject(position.getX(),position.getY()).isEmpty()){
				object.moveTo(position.getX(), position.getY());
				gameSessionData.getWorld().updatePositionOf(object);
			}

		}


	}

	private List<Position> getPositionsForVertical(int x, int y, int directionY) {
		int newY = y + directionY;
		return Arrays.asList(
				new Position(x - 1, newY),
				new Position(x, newY),
				new Position(x + 1, newY)
		);
	}

	private List<Position> getPositionsForHorizontal(int x, int y, int directionX) {
		int newX = x + directionX;
		return Arrays.asList(
				new Position(newX, y - 1),
				new Position(newX, y),
				new Position(newX, y + 1)
		);
	}

	private List<Position> getPositionsForDiagonal(int x, int y, int directionX, int directionY) {
		return Arrays.asList(
				new Position(x + directionX, y),
				new Position(x + directionX, y + directionY),
				new Position(x, y + directionY)
		);
	}

	private boolean isHorizontal(double dx, double dy) {
		return dx != 0 && Math.abs(dy / dx) < Math.tan(Math.toRadians(22.5));
	}

	private boolean isVertical(double dx, double dy) {
		return dy != 0 && Math.abs(dx / dy) < Math.tan(Math.toRadians(22.5));
	}

}

