package pl.oakfusion.sst.core.game;

import pl.oakfusion.sst.data.DTO.DeadPlayersDTO;
import pl.oakfusion.data.message.Event;
import pl.oakfusion.sst.data.game.events.GameEvent;
import pl.oakfusion.sst.data.world.player.Player;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public class DeceasedPlayersBuilder {
	private final List<UUID> deadPlayers;
	private final List<Player> allPlayersList;

	public DeceasedPlayersBuilder(List<Player> players) {
		deadPlayers = players.stream().filter(this::checkIfPlayerDead).map(Player::getUuid).collect(Collectors.toList());
		allPlayersList = players;
	}

	boolean checkIfPlayerDead(Player player) {
		return player.getCivilization().getLivingUnits().isEmpty();
	}

	public List<DeadPlayersDTO> getRecentlyDeceasedPlayers(Class<? extends GameEvent> causeOfDeath) {
		return getRecentlyDeceasedPlayers(Optional.empty(), causeOfDeath);
	}

	public List<DeadPlayersDTO> getRecentlyDeceasedPlayers(String killer, Class<? extends GameEvent> causeOfDeath) {
		return getRecentlyDeceasedPlayers(Optional.of(killer), causeOfDeath);
	}

	private List<DeadPlayersDTO> getRecentlyDeceasedPlayers(Optional<String> killer, Class<? extends Event> causeOfDeath) {
		return allPlayersList
				.stream()
				.filter(p -> !deadPlayers.contains(p.getUuid()))
				.filter(this::checkIfPlayerDead)
				.map(p -> new DeadPlayersDTO(p.getPlayerName(), causeOfDeath, killer,p.getTurn()))
				.collect(Collectors.toList());
	}

}
