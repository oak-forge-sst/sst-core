package pl.oakfusion.sst.core.game.validators;

import pl.oakfusion.sst.data.game.UnitsMoveDestinations;

import java.time.LocalDateTime;
import java.util.*;

public class MoveCache {
	//TODO: Try to think of another implementation of Cache, this one has probably a problem with LocalDateTime when records are added with too short intervals
	//https://gitlab.com/oak-forge-sst/sst-core/-/issues/47
	LinkedHashMap<UUID, UnitsMoveDestinations> moveDestinationsCollection = new LinkedHashMap<>();
	private final SortedMap<LocalDateTime, UUID> timeMap = new TreeMap<>();

	public void setDestinations(UUID playerUUID, UnitsMoveDestinations moveDestinations){
		//if id is not present creates an entry otherwise changes value
		moveDestinationsCollection.put(playerUUID, moveDestinations);
		timeMap.put(LocalDateTime.now(), playerUUID);
		if(moveDestinationsCollection.size() > 10){
			UUID oldestEntryUuid = timeMap.get(timeMap.firstKey());
			moveDestinationsCollection.remove(oldestEntryUuid);
			timeMap.remove(timeMap.firstKey());
		}
	}

	public Optional<UnitsMoveDestinations> getDestinations(UUID playerUUID){
		return Optional.ofNullable(moveDestinationsCollection.get(playerUUID));
	}

}
