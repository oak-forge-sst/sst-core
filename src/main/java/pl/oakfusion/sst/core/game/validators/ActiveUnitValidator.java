package pl.oakfusion.sst.core.game.validators;

import pl.oakfusion.sst.core.gamedata.GameSessionData;
import pl.oakfusion.sst.data.game.commands.GameCommand;
import pl.oakfusion.sst.data.validationfailures.NoActiveUnit;
import pl.oakfusion.sst.data.validationfailures.ValidationFailure;

import java.util.ArrayList;
import java.util.List;

public class ActiveUnitValidator <C extends GameCommand> implements SstCommandValidator<C> {
	@Override
	public List<ValidationFailure> validate(C command, GameSessionData gameSessionData) {
		List<ValidationFailure> validationFailures = new ArrayList<>();
		if (gameSessionData.getActivePlayer().getCivilization().getActiveUnits().isEmpty()) {
			validationFailures.add(new NoActiveUnit("You haven't chosen unit!", List.of()));
		}
		return validationFailures;
	}
}
