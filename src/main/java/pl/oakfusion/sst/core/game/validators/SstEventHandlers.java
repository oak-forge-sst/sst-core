package pl.oakfusion.sst.core.game.validators;

import pl.oakfusion.bus.EventDriven;
import pl.oakfusion.bus.EventHandler;
import pl.oakfusion.bus.MessageBus;
import pl.oakfusion.data.message.Command;
import pl.oakfusion.sst.core.gamedata.GameSessionData;
import pl.oakfusion.sst.core.gamedata.GameSessionDataRepository;
import pl.oakfusion.sst.data.SstEvent;
import pl.oakfusion.sst.data.SstEventHandler;
import pl.oakfusion.sst.data.errors.ApplicationError;
import pl.oakfusion.sst.data.errors.NotFoundError;
import pl.oakfusion.sst.data.errors.ValidationError;
import pl.oakfusion.sst.data.exceptions.NotFoundException;
import pl.oakfusion.sst.data.exceptions.ValidationException;
import pl.oakfusion.sst.data.validationfailures.ValidationFailure;

import java.util.ArrayList;
import java.util.List;

public abstract class SstEventHandlers extends EventDriven {
	private final GameSessionDataRepository gameSessionDataRepository;

	public SstEventHandlers(MessageBus messageBus, GameSessionDataRepository gameSessionDataRepository) {
		super(messageBus);
		this.gameSessionDataRepository = gameSessionDataRepository;
	}

	public <E extends SstEvent> EventHandler<E> handleWith(SstEventHandler<E> handler, List<Validator<E>> validators) {
		return event -> {
			try {
				GameSessionData gameSessionData = gameSessionDataRepository.get(event.getGameSessionUuid()).orElseThrow(NotFoundException::new);

				List<ValidationFailure> validationFailures = validateWith(validators, event, gameSessionData);
				if (!validationFailures.isEmpty()) {
					throw new ValidationException(validationFailures);
				}
				List<Command> events = handler.handle(event, gameSessionData);
				events.forEach(this::post);
			} catch (NotFoundException exception) {
				post(new NotFoundError(event.getGameSessionUuid(), event.getPlayerUuid()));
			} catch (ValidationException exception) {
				post(new ValidationError(event.getGameSessionUuid(), event.getPlayerUuid(), exception.getValidationFailures()));
			} catch (Exception exception) {
				post(new ApplicationError(event.getGameSessionUuid(), event.getPlayerUuid(), "Error during Query validation"));
			}
		};
	}
	private <E extends SstEvent> List<ValidationFailure> validateWith(List<Validator<E>> validators, E event, GameSessionData gameSessionData) throws NotFoundException {
		List<ValidationFailure> validationFailures = new ArrayList<>();
		for(Validator<E> validator : validators){
			List<ValidationFailure> validatorFailures = validator.validate(event, gameSessionData);
			validationFailures.addAll(validatorFailures);
		}
		return validationFailures;
	}
}
