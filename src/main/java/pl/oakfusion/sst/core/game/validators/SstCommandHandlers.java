package pl.oakfusion.sst.core.game.validators;

import pl.oakfusion.bus.CommandDriven;
import pl.oakfusion.bus.CommandHandler;
import pl.oakfusion.bus.MessageBus;
import pl.oakfusion.data.message.Event;
import pl.oakfusion.sst.core.gamedata.GameSessionData;
import pl.oakfusion.sst.core.gamedata.GameSessionDataRepository;
import pl.oakfusion.sst.data.SstCommand;
import pl.oakfusion.sst.data.SstCommandHandler;
import pl.oakfusion.sst.data.errors.ApplicationError;
import pl.oakfusion.sst.data.errors.NotFoundError;
import pl.oakfusion.sst.data.errors.ValidationError;
import pl.oakfusion.sst.data.exceptions.NotFoundException;
import pl.oakfusion.sst.data.exceptions.ValidationException;
import pl.oakfusion.sst.data.validationfailures.ValidationFailure;
import pl.oakfusion.utils.PreconditionUtils;

import java.util.*;


public abstract class SstCommandHandlers extends CommandDriven {

	private final GameSessionDataRepository gameSessionDataRepository;

	public SstCommandHandlers(MessageBus messageBus, GameSessionDataRepository gameSessionDataRepository) {
		super(messageBus);
		this.gameSessionDataRepository = PreconditionUtils.checkNotNull(gameSessionDataRepository);
	}

	public <C extends SstCommand> CommandHandler<C> handleWith(SstCommandHandler<C> handler, List<Validator<C>> validators) {
		return command -> {
			try {
				GameSessionData gameSessionData = gameSessionDataRepository.get(command.getGameSessionUuid()).orElseThrow(NotFoundException::new);

				List<ValidationFailure> validationFailures = validateWith(validators, command, gameSessionData);
				if (!validationFailures.isEmpty()) {
					throw new ValidationException(validationFailures);
				}
				List<Event> events = handler.handle(command, gameSessionData);
				gameSessionDataRepository.save(command.getGameSessionUuid(), gameSessionData);
				events.forEach(this::post);
			} catch (NotFoundException exception) {
				post(new NotFoundError(command.getGameSessionUuid(), command.getPlayerUuid()));
			} catch (ValidationException exception) {
				post(new ValidationError(command.getGameSessionUuid(), command.getPlayerUuid(), exception.getValidationFailures()));
			} catch (Exception exception) {
				post(new ApplicationError(command.getGameSessionUuid(), command.getPlayerUuid(), "Error during Command validation"));
			}
		};
	}

	private <C extends SstCommand> List<ValidationFailure> validateWith(List<Validator<C>> validators, C command, GameSessionData gameSessionData) throws NotFoundException {
		List<ValidationFailure> validationFailures = new ArrayList<>();
		for(Validator<C> validator : validators){
			List<ValidationFailure> validatorFailures = validator.validate(command, gameSessionData);
			validationFailures.addAll(validatorFailures);
		}
		return validationFailures;
	}
}
