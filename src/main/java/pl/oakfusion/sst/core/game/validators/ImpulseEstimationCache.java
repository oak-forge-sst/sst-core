package pl.oakfusion.sst.core.game.validators;

import pl.oakfusion.sst.data.game.MoveEstimation;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ImpulseEstimationCache {

	Map<UUID, MoveEstimation> moveEstimationCollection = new HashMap<>();

	public void setEstimation(UUID playerUUID, MoveEstimation moveEstimation){
		//if id is not present creates an entry otherwise changes value
		moveEstimationCollection.put(playerUUID, moveEstimation);
	}

	public MoveEstimation getEstimation(UUID playerUUID){
		return moveEstimationCollection.get(playerUUID);
	}

}
