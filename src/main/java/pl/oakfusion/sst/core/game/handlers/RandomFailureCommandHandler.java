package pl.oakfusion.sst.core.game.handlers;

import pl.oakfusion.data.message.Event;
import pl.oakfusion.sst.data.SstCommandHandler;
import pl.oakfusion.sst.data.game.commands.GameCommand;
import pl.oakfusion.sst.data.game.events.ComponentCommandFailed;
import pl.oakfusion.sst.data.util.RandomGenerator;
import pl.oakfusion.sst.data.world.civilizations.Unit;
import pl.oakfusion.sst.data.world.component.ComponentId;
import pl.oakfusion.sst.data.world.component.CommandToComponentIdChanger;
import pl.oakfusion.sst.data.world.component.Component;
import pl.oakfusion.sst.data.world.component.DamagedFunctionalitySpecification;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RandomFailureCommandHandler {
	final static Map<Double, Double> FAILURE_PROBABILITY_MAP = Map.ofEntries(
			Map.entry(100.0,		0.1),
			Map.entry(90.0,		0.2),
			Map.entry(80.0,		0.3),
			Map.entry(70.0,		0.4),
			Map.entry(60.0,		0.5),
			Map.entry(50.0,		0.6),
			Map.entry(40.0,		0.7),
			Map.entry(30.0,		0.8),
			Map.entry(20.0,		0.9),
			Map.entry(10.0,		0.95),
			Map.entry(0.0,		1.0)
	);
	private final RandomGenerator randomGenerator;

	public RandomFailureCommandHandler(RandomGenerator randomGenerator) {
		this.randomGenerator = randomGenerator;
	}

	public <C extends GameCommand> SstCommandHandler<C> failingHandler(GameCommandHandler<C> handler){
		return (command, gameSessionData) -> {
			ComponentId componentId = CommandToComponentIdChanger.getComponentIdByCommand(command.getClass()).orElseThrow();
			List<Event> events = new ArrayList<>();
			List<Unit> activeUnits = gameSessionData.getActivePlayer().getCivilization().getActiveUnits();
			for (Unit unit : activeUnits){
				List<Event> unitEvents;
				Component component = unit.getComponentSystem().getComponent(componentId);
				if (!actionFailedRandomly(component.calculateHealthPercentage())){
					unitEvents = handler.handle(command, gameSessionData, unit.getUuid());
				} else {
					unitEvents = List.of(new ComponentCommandFailed(command.getGameSessionUuid(), command.getPlayerUuid(), componentId, unit.getUuid()));
					component.receiveDamageAndGetOverflow(component.getMaxHealth() * DamagedFunctionalitySpecification.DEFAULT_PENALTY_FOR_COMPONENT);
				}
				events.addAll(unitEvents);
			}
			return events;
		};
	}

	public boolean actionFailedRandomly(double componentHealthPercentage){
		double failureProbability = calculateFailureProbability(componentHealthPercentage);
		return randomGenerator.nextDoubleInRange(0, 1) < failureProbability;
	}

	private double calculateFailureProbability(double componentHealthPercentage){
		double roundedHealthPercentage = Math.floor(componentHealthPercentage/10) * 10;
		return FAILURE_PROBABILITY_MAP.get(roundedHealthPercentage);
	}
}
