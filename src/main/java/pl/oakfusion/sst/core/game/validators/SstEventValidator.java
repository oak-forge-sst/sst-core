package pl.oakfusion.sst.core.game.validators;

import pl.oakfusion.sst.data.SstEvent;

public interface SstEventValidator <E extends SstEvent> extends Validator<E>{
}
