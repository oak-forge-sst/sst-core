package pl.oakfusion.sst.core.game.validators;

import pl.oakfusion.sst.core.gamedata.GameSessionData;
import pl.oakfusion.sst.data.game.commands.GameCommand;
import pl.oakfusion.sst.data.validationfailures.NotActivePlayer;
import pl.oakfusion.sst.data.validationfailures.ValidationFailure;

import java.util.ArrayList;
import java.util.List;

public class ActivePlayerValidator <C extends GameCommand> implements SstCommandValidator<C> {
	@Override
	public List<ValidationFailure> validate(C command, GameSessionData gameSessionData) {
		List<ValidationFailure> validationFailures = new ArrayList<>();
		if(gameSessionData.getActivePlayer().getUuid() != command.getPlayerUuid()){
			validationFailures.add(new NotActivePlayer("It's not your turn!", List.of()));
		}
		return validationFailures;
	}
}
