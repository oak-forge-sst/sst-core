package pl.oakfusion.sst.core.game.handlers;

import pl.oakfusion.bus.MessageBus;
import pl.oakfusion.data.message.Event;
import pl.oakfusion.sst.core.gamedata.GameSessionData;
import pl.oakfusion.sst.core.gamedata.GameSessionDataRepository;
import pl.oakfusion.sst.core.game.validators.ActiveUnitValidator;
import pl.oakfusion.sst.core.game.validators.ImpulseEstimationCache;
import pl.oakfusion.sst.core.game.validators.ImpulseValidator;
import pl.oakfusion.sst.core.game.validators.SstCommandHandlers;
import pl.oakfusion.sst.data.game.commands.impulse.Impulse;
import pl.oakfusion.sst.data.game.events.impulse.ImpulsePerformed;
import pl.oakfusion.sst.data.world.component.ComponentId;
import pl.oakfusion.sst.data.world.component.EnergyContainer;

import java.util.List;

import static pl.oakfusion.sst.core.game.validators.ValidatorsCollector.validators;

public class ImpulseHandlers extends SstCommandHandlers {

	ImpulseEstimationCache cache;

	public ImpulseHandlers(MessageBus messageBus, GameSessionDataRepository gameSessionDataRepository) {
		super(messageBus, gameSessionDataRepository);
		cache = new ImpulseEstimationCache();
		registerCommandHandler(Impulse.class, handleWith(this::handle, validators(new ActiveUnitValidator<>(), new ImpulseValidator(cache))));
	}

	private List<Event> handle(Impulse command, GameSessionData gameSessionData){

		var unit = gameSessionData.getActivePlayer().getCivilization().getActiveUnits().get(0);
		var world = gameSessionData.getWorld();

		var estimation = cache.getEstimation(unit.getPlayerUUID());
		var path = estimation.getPath();
		var target = path.get(path.size()-1);

		int xDestination = target.getX();
		int yDestination = target.getY();

		unit.setX(xDestination);
		unit.setY(yDestination);

		((EnergyContainer) unit.getComponentSystem().getComponent(ComponentId.ENERGY_CONTAINER)).setEnergy(estimation.getEnergyLeft());
		//TODO: decrease action points when fixed
		world.rest(estimation.getTimeCostToLocation());
		world.updatePositionOf(unit);

		return List.of(new ImpulsePerformed(command.getGameSessionUuid(), command.getPlayerUuid(),
				xDestination, yDestination, command.isInAutoMode(), command.isForced()));

	}
}
