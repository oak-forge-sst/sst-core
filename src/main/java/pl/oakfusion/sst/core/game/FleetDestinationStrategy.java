package pl.oakfusion.sst.core.game;

import pl.oakfusion.sst.core.gamedata.GameSessionData;
import pl.oakfusion.sst.data.game.MoveEstimation;
import pl.oakfusion.sst.data.game.UnitMoveDestination;
import pl.oakfusion.sst.data.game.UnitsMoveDestinations;
import pl.oakfusion.sst.data.game.commands.move.Move;
import pl.oakfusion.sst.data.util.Position;
import pl.oakfusion.sst.data.world.World;
import pl.oakfusion.sst.data.world.civilizations.Unit;
import pl.oakfusion.sst.data.world.component.Computer;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * Class designed to find destinations for move with multiple units depending on chosen formation.
 */

public class FleetDestinationStrategy {

	public static final int MAX_DISTANCE_FROM_CENTRAL_UNIT = 10;

	public enum FleetFormation {
		RANDOM
	}

	public FleetDestinationStrategy(FleetFormation fleetFormation, GameSessionData gameSessionData) {
		this.fleetFormation = fleetFormation;
		this.gameSessionData = gameSessionData;
		this.world = gameSessionData.getWorld();
	}

	private final FleetFormation fleetFormation;
	private final GameSessionData gameSessionData;
	private final World world;
	private final Map<Unit, MoveEstimation> highestRankUnitsEstimations = new HashMap<>();

	/**
	 * Main method of class which returns object of UnitsMoveDestinations class. If returned object is empty it means that for at least one unit
	 * from list passed as parameter path have not been found. If any of units in returned object has boolean parameter set as false it means it
	 * has not enough energy to reach destination.
	 *
	 * @param	command	move command with validated coordinates (coordinates are inside world of game and aren't occupied by any object)
	 * @param 	units	list of units for which destinations should be appointed
	 */
	public UnitsMoveDestinations getUnitsDestinations(Move command, List<Unit> units) {
		UnitsMoveDestinations unitsMoveDestinations = new UnitsMoveDestinations();
		Position centralPosition = new Position(command.getX(), command.getY());
		estimateHighestRankUnitsMoveToCentralPosition(centralPosition, units);
		//When map is empty after running method above it means path for any of highest rank units was not found
		if (highestRankUnitsEstimations.isEmpty()) {
			return unitsMoveDestinations;
		}
		Unit centralUnit = getFirstUnitThatCanMoveToEstimatedPosition(highestRankUnitsEstimations);
		boolean canCentralUnitReachDestination;
		if (centralUnit != null) {
			canCentralUnitReachDestination = true;
		} else if (!command.isForceMode()) {
			centralUnit = findUnitWithLowestEnergyNeededToEstimatedPosition(highestRankUnitsEstimations);
			canCentralUnitReachDestination = false;
		} else {
			centralUnit = findUnitWithLowestEnergyNeededToEstimatedPosition(highestRankUnitsEstimations);
			Computer computer = centralUnit.getComponentSystem().getComputer();
			MoveEstimation normalEstimation = highestRankUnitsEstimations.get(centralUnit);
			MoveEstimation forcedEstimation = computer.estimateForcedMove(world, centralUnit, normalEstimation.getPath(), new Position(centralUnit.getX(), centralUnit.getY()), centralPosition, List.of());
			if (!forcedEstimation.getPath().isEmpty()) {
				centralPosition = forcedEstimation.getPath().get(forcedEstimation.getPath().size() - 1);
			} else {
				return unitsMoveDestinations;
			}
			highestRankUnitsEstimations.put(centralUnit, forcedEstimation);
			canCentralUnitReachDestination = true;
		}
		unitsMoveDestinations.addDestination(new UnitMoveDestination(centralUnit, centralPosition, highestRankUnitsEstimations.get(centralUnit), canCentralUnitReachDestination));
		List<Unit> unitsToBeMoved = new ArrayList<>(units);
		Unit finalCentralUnit = centralUnit;
		unitsToBeMoved.stream().filter(unit -> unit.getUuid().equals(finalCentralUnit.getUuid())).findAny().ifPresent(unitsToBeMoved::remove);
		unitsMoveDestinations.addAll(getPositionsForFormation(unitsToBeMoved, centralPosition, command.isForceMode()));
		if (unitsMoveDestinations.getDestinationsList().size() < units.size()) {
			unitsMoveDestinations.getDestinationsList().clear();
		}
		return unitsMoveDestinations;
	}

	private UnitsMoveDestinations getPositionsForFormation(List<Unit> units, Position centralPosition, boolean isForced) {
		UnitsMoveDestinations unitsMoveDestinations = new UnitsMoveDestinations();
		units.sort(Comparator.comparing(unit -> unit.getComponentSystem().getEnergyContainer().getEnergy()));
		if (fleetFormation == FleetFormation.RANDOM) {
			units.forEach(unit -> findPositionForUnit(unitsMoveDestinations, unit, centralPosition, isForced));
		}
		return unitsMoveDestinations;
	}

	private void findPositionForUnit (UnitsMoveDestinations unitsMoveDestinations, Unit unit, Position centralPosition, boolean isForced){
		EmptySurrounding emptySurrounding = new EmptySurrounding();
		Position unitPosition = new Position(unit.getX(), unit.getY());
		Computer computer = unit.getComponentSystem().getComputer();
		int range = 1;
		List<Position> previousPositions = new ArrayList<>();
		AtomicReference<MoveEstimation> estimationLowestEnergyNeeded = new AtomicReference<>();
		while (!unitsMoveDestinations.containsUnit(unit) && range <= MAX_DISTANCE_FROM_CENTRAL_UNIT) {
			List<Position> availablePositions = emptySurrounding.getEmptySurrounding(centralPosition, gameSessionData, range);
			previousPositions.forEach(previousPosition -> {
				availablePositions.stream().filter(position -> position.getY() == previousPosition.getY() && position.getX() == previousPosition.getX()).findAny().ifPresent(availablePositions::remove);
			});
			availablePositions.forEach(position -> checkPositionReachability(unitsMoveDestinations, unit, unitPosition, computer, estimationLowestEnergyNeeded, position));
			if (range == MAX_DISTANCE_FROM_CENTRAL_UNIT && !unitsMoveDestinations.containsUnit(unit)) {
				if (estimationLowestEnergyNeeded.get() != null) {
					List<Position> occupiedPositions = availablePositions.stream().filter(unitsMoveDestinations::containsDestination).collect(Collectors.toList());
					List<Position> lowestEnergyNeededPath = estimationLowestEnergyNeeded.get().getPath();
					if (isForced) {
						MoveEstimation forcedEstimation = computer.estimateForcedMove(world, unit, lowestEnergyNeededPath, unitPosition, lowestEnergyNeededPath.get(lowestEnergyNeededPath.size() - 1), occupiedPositions);
						if (!forcedEstimation.getPath().isEmpty()) {
							unitsMoveDestinations.addDestination(new UnitMoveDestination(unit, forcedEstimation.getPath().get(forcedEstimation.getPath().size() - 1), forcedEstimation, true));
						}
					} else {
						unitsMoveDestinations.addDestination(new UnitMoveDestination(unit, lowestEnergyNeededPath.get(lowestEnergyNeededPath.size() - 1), estimationLowestEnergyNeeded.get(), false));
					}
				}
			}
			previousPositions.addAll(availablePositions);
			range++;
		}
	}

	private void checkPositionReachability(UnitsMoveDestinations unitsMoveDestinations, Unit unit, Position unitPosition, Computer computer, AtomicReference<MoveEstimation> estimationLowestEnergyNeeded, Position position) {
		if(!unitsMoveDestinations.containsUnit(unit)) {
			List<Position> shortestPath = world.findShortestPathWarp(unitPosition, position);
			if (!shortestPath.isEmpty()) {
				MoveEstimation estimation = computer.estimateMove(world, unit, shortestPath, unitPosition, position);
				if (estimation.isEnoughResources() && !unitsMoveDestinations.containsDestination(position)) {
					unitsMoveDestinations.addDestination(new UnitMoveDestination(unit, position, estimation, true));
				}
				if (estimationLowestEnergyNeeded.get() == null) {
					estimationLowestEnergyNeeded.set(estimation);
				} else if (estimation.getEnergyCostToLocation() < estimationLowestEnergyNeeded.get().getEnergyCostToLocation()) {
					estimationLowestEnergyNeeded.set(estimation);
				}
			}
		}
	}

	private void estimateHighestRankUnitsMoveToCentralPosition(Position centralPosition, List<Unit> units) {
		List<Unit> highestRankUnits = getHighestRankUnits(units);
		highestRankUnits.forEach(unit -> {
			Computer computer = unit.getComponentSystem().getComputer();
			Position unitPosition = new Position(unit.getX(), unit.getY());
			List<Position> shortestPath = world.findShortestPathWarp(unitPosition, centralPosition);
			if (!shortestPath.isEmpty()) {
				MoveEstimation estimation = computer.estimateMove(world, unit, shortestPath, unitPosition, centralPosition);
				highestRankUnitsEstimations.put(unit, estimation);
			}
		});
	}

	private Unit findUnitWithLowestEnergyNeededToEstimatedPosition(Map<Unit, MoveEstimation> unitsMoveEstimations) {
		final AtomicReference<MoveEstimation> estimationLowestEnergy = new AtomicReference<>();
		final AtomicReference<Unit> unitLowestEnergy = new AtomicReference<>();
		unitsMoveEstimations.forEach((unit, estimation) -> {
			if (estimationLowestEnergy.get() == null && unitLowestEnergy.get() == null) {
				estimationLowestEnergy.set(estimation);
				unitLowestEnergy.set(unit);
			} else if (estimation.getEnergyCostToLocation() < estimationLowestEnergy.get().getEnergyCostToLocation()) {
				estimationLowestEnergy.set(estimation);
				unitLowestEnergy.set(unit);
			}
		});
		return unitLowestEnergy.get();
	}

	private Unit getFirstUnitThatCanMoveToEstimatedPosition(Map<Unit, MoveEstimation> unitsMoveEstimations) {
		AtomicReference<Unit> resultUnit = new AtomicReference<>();
		unitsMoveEstimations.forEach((unit, estimation) -> {
			if (estimation.isEnoughResources() && resultUnit.get() == null) {
				resultUnit.set(unit);
			}
		});
		return resultUnit.get();
	}

	private List<Unit> getHighestRankUnits(List<Unit> units) {
		Map<Unit.UnitRank, List<UUID>> unitsSortedByRank = getUnitsSortedByRank(units);
		int length = Unit.UnitRank.values().length - 1;
		List<Unit> highestRankUnits = new ArrayList<>();
		for (int i = length; i >= 0; i--) {
			Unit.UnitRank currentRank = Unit.UnitRank.values()[i];
			if (unitsSortedByRank.containsKey(currentRank)) {
				unitsSortedByRank.get(currentRank).forEach(uuid -> {
					units.stream().filter(unit -> unit.getUuid().equals(uuid)).findAny().ifPresent(highestRankUnits::add);
				});
				return highestRankUnits;
			}
		}
		return highestRankUnits;
	}

	private Map<Unit.UnitRank, List<UUID>> getUnitsSortedByRank(List<Unit> units) {
		Map<Unit.UnitRank, List<UUID>> unitsSortedByRank = new HashMap<>();
		units.forEach(unit -> {
			Unit.UnitRank unitRank = unit.getUnitRank();
			unitsSortedByRank.put(unitRank, unitsSortedByRank.getOrDefault(unitRank, new ArrayList<>()));
			unitsSortedByRank.get(unitRank).add(unit.getUuid());
		});
		return unitsSortedByRank;
	}

	public boolean isEnoughSpaceForUnits(Move command, GameSessionData gameSessionData) {
		EmptySurrounding emptySurrounding = new EmptySurrounding();
		List<Position> freeSectors = emptySurrounding.getEmptySurrounding(new Position(command.getX(), command.getY()), gameSessionData, MAX_DISTANCE_FROM_CENTRAL_UNIT);
		return gameSessionData.getActivePlayer().getCivilization().getActiveUnits().size() <= freeSectors.size();
	}
}
