package pl.oakfusion.sst.core.game.validators;

import pl.oakfusion.sst.data.SstQuery;

public interface SstQueryValidator <Q extends SstQuery> extends Validator<Q>{
}
