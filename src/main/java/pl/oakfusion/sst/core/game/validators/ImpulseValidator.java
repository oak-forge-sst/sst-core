package pl.oakfusion.sst.core.game.validators;

import pl.oakfusion.sst.core.gamedata.GameSessionData;
import pl.oakfusion.sst.data.validationfailures.ForcedMoveFailed;
import pl.oakfusion.sst.data.validationfailures.NotEnoughResources;
import pl.oakfusion.sst.data.game.MoveEstimation;
import pl.oakfusion.sst.data.game.commands.impulse.Impulse;
import pl.oakfusion.sst.data.game.events.move.MoveCollisionDetected;
import pl.oakfusion.sst.data.game.events.move.MoveFailed;
import pl.oakfusion.sst.data.game.events.move.MoveOutOfBorderDetected;
import pl.oakfusion.sst.data.util.Position;
import pl.oakfusion.sst.data.validationfailures.ValidationFailure;
import pl.oakfusion.sst.data.world.World;
import pl.oakfusion.sst.data.world.civilizations.Unit;
import pl.oakfusion.sst.data.world.component.Computer;

import java.util.ArrayList;
import java.util.List;

public class ImpulseValidator implements SstCommandValidator<Impulse> {

	ArrayList<ValidationFailure> listOfFails = new ArrayList<>();
	Unit unit;
	Computer computer;
	World world;
	Impulse command;
	Position source;
	Position target;
	ImpulseEstimationCache cache;
	List<Position> path;
	MoveEstimation estimation;

	public ImpulseValidator(ImpulseEstimationCache cache){
		this.cache = cache;
	}

	@Override
	public List<ValidationFailure> validate(Impulse message, GameSessionData gameSessionData) {

		initialiseFields(message, gameSessionData);

		if(gameSessionData.getActivePlayer().getCivilization().getActiveUnits().size() > 1){
			listOfFails.add(new MoveFailed("You cannot move more than one unit with impulse at a time", new ArrayList<>()));
			return listOfFails;
		}

		if (computer.isComputerDown() && command.isInAutoMode()) {
			listOfFails.add(new MoveFailed("You cannot move automatically while computer is down", new ArrayList<>()));
			return listOfFails;
		}

		computeTargetPoint();

		if (isEndOfTheWorld()) {
			listOfFails.add(new MoveOutOfBorderDetected(
					"Impulse move not possible, end of the world reached!", new ArrayList<>()));
			return listOfFails;
		}

		if (isCollisionInTargetPoint()) {
			listOfFails.add(new MoveCollisionDetected(
					"A collision has been detected, you can't move", new ArrayList<>()));
			return listOfFails;
		}

		path = world.findShortestPath(source, target, gameSessionData.getActivePlayer().getCivilization().getLivingUnits());
		if (path.isEmpty()){
			listOfFails.add(new MoveCollisionDetected("There is no path without obstacles from source to target", new ArrayList<>()));
		}

		if (!listOfFails.isEmpty()) return listOfFails;

		estimation = computer.estimateImpulseMove(world, unit, path, source, target);

		double actionPointsNeeded = estimation.getActionPointsCostToLocation();
		boolean moveRequiresTooManyActionPoints = actionPointsNeeded > unit.getActionPoints();
		boolean moveRequiresTooMuchEnergy = !estimation.isEnoughResources();

		validateResources(moveRequiresTooManyActionPoints, moveRequiresTooMuchEnergy);

			//validate force mode
		if ((moveRequiresTooManyActionPoints || moveRequiresTooMuchEnergy) && command.isForced()){
			MoveEstimation forcedEstimation = computer.estimateForcedImpulse(world, unit, path, source, target, List.of());
			int forcedDistance = forcedEstimation.getPath().size();
			if (forcedDistance <= 0){
				listOfFails.add(new ForcedMoveFailed(
							"Force move cannot be executed due to lack of either action points or energy", new ArrayList<>()));
				return listOfFails;
			}

			estimation = forcedEstimation;
			path = forcedEstimation.getPath();
		}

		cache.setEstimation(unit.getPlayerUUID(), estimation);

		return listOfFails;
}

	private void initialiseFields(Impulse message, GameSessionData gameSessionData){
		command = message;
		unit = gameSessionData.getActivePlayer().getCivilization().getActiveUnits().get(0);
		computer = unit.getComponentSystem().getComputer();
		world = gameSessionData.getWorld();
		source = new Position(unit.getX(), unit.getY());
	}

	private boolean isEndOfTheWorld() {
		int xDestination = target.getX();
		int yDestination = target.getY();
		return !(xDestination >= 0 && xDestination < world.getAbsoluteWidth()
				&& yDestination >= 0 && yDestination < world.getAbsoluteHeight());
	}

	private void validateResources(boolean moveRequiresTooManyActionPoints, boolean moveRequiresTooMuchEnergy) {
		//TODO: uncomment when action points mechanism is repaired
//		if (moveRequiresTooManyActionPoints && !command.isForceMode()) {
//			listOfFails.add(new NotEnoughActionPoints(
//					"You do not have enough Action Points", new ArrayList<>()
//			));
//		}

		if (moveRequiresTooMuchEnergy && !command.isForced()){
			listOfFails.add(new NotEnoughResources("Not enough energy to reach chosen destination", new ArrayList<>()));
		}
	}

	private void computeTargetPoint(){
		if (command.isInAutoMode()){
			target = new Position(command.getX(), command.getY());
		}
		else {
			target = new Position(source.getX() + command.getX(), source.getY() + command.getY());
		}
	}

	private boolean isCollisionInTargetPoint(){
		return world.getGameObject(target.getX(), target.getY()).isPresent();
	}

}
