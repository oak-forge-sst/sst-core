package pl.oakfusion.sst.core.game.validators;

import pl.oakfusion.sst.core.gamedata.GameSessionData;
import pl.oakfusion.sst.data.game.commands.torpedo.Torpedo;
import pl.oakfusion.sst.data.game.events.torpedoes.TorpedoLauncherOverloaded;
import pl.oakfusion.sst.data.game.events.torpedoes.TorpedoesLessThanIntendedToShot;
import pl.oakfusion.sst.data.validationfailures.ValidationFailure;
import pl.oakfusion.sst.data.world.component.ComponentId;
import pl.oakfusion.sst.data.world.civilizations.Unit;
import pl.oakfusion.sst.data.world.component.TorpedoesDevice;

import java.util.ArrayList;
import java.util.List;

public class TorpedoValidator implements SstCommandValidator<Torpedo>{

	@Override
	public List<ValidationFailure> validate(Torpedo command, GameSessionData gameSessionData) {

		ArrayList<ValidationFailure> failures = new ArrayList<>();

		List<Unit> units = gameSessionData.getActivePlayer().getCivilization().getActiveUnits();
		units.forEach(unit -> {
			int torpedoes = ((TorpedoesDevice) unit.getComponentSystem().getComponent(ComponentId.TORPEDOES_DEVICE)).getTorpedoes();
			int torpedoesIntendedToShoot = command.getTorpedoAmountIntendedToShoot();

			if (torpedoes - torpedoesIntendedToShoot < 0) {
				failures.add(new TorpedoesLessThanIntendedToShot("You do not have enough torpedoes", new ArrayList<>(), torpedoes));
			}
			if (torpedoesIntendedToShoot > unit.getComponentSystem().getTorpedoesDevice().getMaxTorpedoes()) {
				failures.add(new TorpedoLauncherOverloaded("You have more torpedoes than your unit allows", new ArrayList<>()));
			}
		});

		return failures;
	}
}
