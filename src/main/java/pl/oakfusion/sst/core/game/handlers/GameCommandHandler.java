package pl.oakfusion.sst.core.game.handlers;

import pl.oakfusion.data.message.Event;
import pl.oakfusion.sst.core.gamedata.GameSessionData;
import pl.oakfusion.sst.data.exceptions.NotFoundException;
import pl.oakfusion.sst.data.game.commands.GameCommand;

import java.util.List;
import java.util.UUID;

@FunctionalInterface
public interface GameCommandHandler<C extends GameCommand>  {
	List<Event> handle(C command, GameSessionData gameSessionData, UUID unitUuid) throws NotFoundException;
}
