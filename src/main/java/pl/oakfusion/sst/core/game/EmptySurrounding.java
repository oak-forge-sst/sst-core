package pl.oakfusion.sst.core.game;

import pl.oakfusion.sst.core.gamedata.GameSessionData;
import pl.oakfusion.sst.data.util.Position;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class EmptySurrounding {
	public List<Position> getEmptySurrounding(Position object, GameSessionData gameSessionData, int range) {
		List<Position> positions = new ArrayList<>();
		int xBeg = max(object.getX() - range, 0);
		int xEnd = min(object.getX() + range, gameSessionData.getWorld().getAbsoluteWidth() - range);
		int yBeg = max(object.getY() - range, 0);
		int yEnd = min(object.getY() + range, gameSessionData.getWorld().getAbsoluteHeight() - range);

		for (int x = xBeg; x <= xEnd; x++) {
			for (int y = yBeg; y <= yEnd; y++) {
				if (gameSessionData.getWorld().getGameObject(x, y).isEmpty() && !(x == object.getX() && y == object.getY())) {
					positions.add(new Position(x, y));
				}
			}
		}
		return positions;
	}
}
