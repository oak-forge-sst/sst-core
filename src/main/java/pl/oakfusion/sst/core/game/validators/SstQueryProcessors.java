package pl.oakfusion.sst.core.game.validators;

import pl.oakfusion.bus.MessageBus;
import pl.oakfusion.bus.QueryDriven;
import pl.oakfusion.bus.QueryHandler;
import pl.oakfusion.data.message.QueryResponse;
import pl.oakfusion.sst.core.gamedata.GameSessionData;
import pl.oakfusion.sst.core.gamedata.GameSessionDataRepository;
import pl.oakfusion.sst.data.SstQuery;
import pl.oakfusion.sst.data.SstQueryProcessor;
import pl.oakfusion.sst.data.errors.ApplicationError;
import pl.oakfusion.sst.data.errors.NotFoundError;
import pl.oakfusion.sst.data.errors.ValidationError;
import pl.oakfusion.sst.data.exceptions.NotFoundException;
import pl.oakfusion.sst.data.exceptions.ValidationException;
import pl.oakfusion.sst.data.validationfailures.ValidationFailure;

import java.util.ArrayList;
import java.util.List;

public abstract class SstQueryProcessors extends QueryDriven {
	private final GameSessionDataRepository gameSessionDataRepository;

	public SstQueryProcessors(MessageBus messageBus, GameSessionDataRepository gameSessionDataRepository) {
		super(messageBus);
		this.gameSessionDataRepository = gameSessionDataRepository;
	}

	public <Q extends SstQuery> QueryHandler<Q> handleWith(SstQueryProcessor<Q> processor, List<Validator<Q>> validators) {
		return query -> {
			try {
				GameSessionData gameSessionData = gameSessionDataRepository.get(query.getGameSessionUuid()).orElseThrow(NotFoundException::new);

				List<ValidationFailure> validationFailures = validateWith(validators, query, gameSessionData);
				if (!validationFailures.isEmpty()) {
					throw new ValidationException(validationFailures);
				}
				List<QueryResponse> events = processor.process(query, gameSessionData);
				events.forEach(this::post);
			} catch (NotFoundException exception) {
				post(new NotFoundError(query.getGameSessionUuid(), query.getPlayerUuid()));
			} catch (ValidationException exception) {
				post(new ValidationError(query.getGameSessionUuid(), query.getPlayerUuid(), exception.getValidationFailures()));
			} catch (Exception exception) {
				post(new ApplicationError(query.getGameSessionUuid(), query.getPlayerUuid(), "Error during Query validation"));
			}
		};
	}

	private <Q extends SstQuery> List<ValidationFailure> validateWith(List<Validator<Q>> validators, Q query, GameSessionData gameSessionData) throws NotFoundException {
		List<ValidationFailure> validationFailures = new ArrayList<>();
		for(Validator<Q> validator : validators){
			List<ValidationFailure> validatorFailures = validator.validate(query, gameSessionData);
			validationFailures.addAll(validatorFailures);
		}
		return validationFailures;
	}
}
