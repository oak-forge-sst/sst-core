package pl.oakfusion.sst.core.game;

import pl.oakfusion.bus.MessageBus;
import pl.oakfusion.sst.core.aggregates.HelpResolver;
import pl.oakfusion.sst.core.game.handlers.MaydayHandlers;
import pl.oakfusion.sst.core.game.handlers.MoveHandlers;
import pl.oakfusion.sst.core.game.handlers.RandomFailureCommandHandler;
import pl.oakfusion.sst.core.game.handlers.TorpedoHandlers;
import pl.oakfusion.sst.core.game.validators.SstCommandHandlers;
import pl.oakfusion.sst.core.gamedata.GameSessionDataRepository;
import pl.oakfusion.sst.data.util.RandomGenerator;
import pl.oakfusion.utils.PreconditionUtils;


public class Game extends SstCommandHandlers {
	private final GameSessionDataRepository gameSessionDataRepository;
	private RandomGenerator randomGenerator;
	private HelpResolver helpResolver;

	public Game(GameSessionDataRepository gameSessionDataRepository, MessageBus messageBus, RandomGenerator randomGenerator, HelpResolver helpResolver) {
		this(gameSessionDataRepository, messageBus, randomGenerator);
		this.helpResolver = PreconditionUtils.checkNotNull(helpResolver);
	}

	public Game(GameSessionDataRepository gameSessionDataRepository, MessageBus messageBus, RandomGenerator randomGenerator) {
		super(messageBus, gameSessionDataRepository);
		this.gameSessionDataRepository = PreconditionUtils.checkNotNull(gameSessionDataRepository);
		this.randomGenerator = PreconditionUtils.checkNotNull(randomGenerator);
		RandomFailureCommandHandler randomFailureCommandHandler = new RandomFailureCommandHandler(randomGenerator);
		new MaydayHandlers(gameSessionDataRepository, messageBus, randomFailureCommandHandler, randomGenerator);
		new TorpedoHandlers(messageBus, randomGenerator, gameSessionDataRepository, randomFailureCommandHandler);
		new MoveHandlers(messageBus, gameSessionDataRepository);
	}
}
