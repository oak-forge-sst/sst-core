package pl.oakfusion.sst.core.game.validators;

import pl.oakfusion.sst.core.gamedata.GameSessionData;
import pl.oakfusion.sst.core.game.FleetDestinationStrategy;
import pl.oakfusion.sst.data.game.UnitMoveDestination;
import pl.oakfusion.sst.data.game.UnitsMoveDestinations;
import pl.oakfusion.sst.data.game.MoveEstimation;
import pl.oakfusion.sst.data.game.commands.move.Move;
import pl.oakfusion.sst.data.game.events.move.ForcedMoveFailed;
import pl.oakfusion.sst.data.game.events.move.MoveCollisionDetected;
import pl.oakfusion.sst.data.game.events.move.MoveFailed;
import pl.oakfusion.sst.data.game.events.move.MoveOutOfBorderDetected;
import pl.oakfusion.sst.data.validationfailures.NotEnoughResources;
import pl.oakfusion.sst.data.util.Position;
import pl.oakfusion.sst.data.validationfailures.ValidationFailure;
import pl.oakfusion.sst.data.world.World;
import pl.oakfusion.sst.data.world.civilizations.Unit;
import pl.oakfusion.sst.data.world.component.Computer;
import pl.oakfusion.sst.data.world.component.DamagedFunctionalitySpecification;

import java.util.ArrayList;
import java.util.List;

public class MoveValidator implements SstCommandValidator<Move> {

	ArrayList<ValidationFailure> listOfFails = new ArrayList<>();
	List<Unit> units;
	World world;
	GameSessionData gameData;
	Move command;
	Position source;
	Position target;
	MoveCache moveCache;
	List<Position> manualPath;
	FleetDestinationStrategy fleetDestinationStrategy;
	MoveOutOfBorderDetected moveOutOfBorderFailure = new MoveOutOfBorderDetected(
						"You may not move, you have reached the end of the world!", new ArrayList<>());
	MoveCollisionDetected moveCollisionDetectedFailure = new MoveCollisionDetected(
						"A collision in target  has been detected, you can't move", new ArrayList<>());

	public MoveValidator(MoveCache pathCache) {
		this.moveCache = pathCache;
	}

	@Override
	public List<ValidationFailure> validate(Move moveCommand, GameSessionData gameSessionData) {

		command = moveCommand;
		units = gameSessionData.getActivePlayer().getCivilization().getActiveUnits();
		world = gameSessionData.getWorld();
		gameData = gameSessionData;

		List<String> unitsWithDamagedWarpEngines = new ArrayList<>();
		units.forEach(unit -> {
			if(unit.getComponentSystem().getWarpEngines().calculateHealthPercentage() < DamagedFunctionalitySpecification.DEFAULT_LEVEL_OF_FUNCTIONALITY){
				unitsWithDamagedWarpEngines.add(unit.getUuid().toString());
			}
		});
		if(!unitsWithDamagedWarpEngines.isEmpty()){
			listOfFails.add(new MoveFailed("Warp engines damaged, cannot move", unitsWithDamagedWarpEngines));
			return listOfFails;
		}
		if (units.size() > 1 && !command.isAutoMode()) {
			listOfFails.add(new MoveFailed("Manual move possible only for single unit.", List.of()));
		} else if (units.size() == 1 && !command.isAutoMode()) {
			Unit unit = units.get(0);
			source = new Position(unit.getX(), unit.getY());

			if(!isTargetPointValid()){
				return listOfFails;
			}

			validateManualMode(unit);
		} else if (command.isAutoMode()) {
			fleetDestinationStrategy = new FleetDestinationStrategy(command.getFleetFormation(), gameSessionData);
			List<String> unitsWithComputerDownUuid = new ArrayList<>();

			units.forEach(unit -> {
				if(unit.getComponentSystem().getComputer().isComputerDown()){
					unitsWithComputerDownUuid.add(unit.getUuid().toString());
				}
			});

			if(!unitsWithComputerDownUuid.isEmpty()) {
				listOfFails.add(new MoveFailed("You cannot move automatically while computer is down", unitsWithComputerDownUuid));
				return listOfFails;
			}
			if(!isTargetPointValid()){
				return listOfFails;
			}

			UnitsMoveDestinations unitsMoveDestinations = fleetDestinationStrategy.getUnitsDestinations(command, units);
			validateAutoMode(unitsMoveDestinations);
		}
		return listOfFails;
	}

	private boolean isTargetPointValid()
	{
		computeTargetPoint();
		if (isEndOfTheWorld()) {
			listOfFails.add(moveOutOfBorderFailure);
			return false;
		}
		if (unitDetectedCollisionInTargetPoint()) {
			listOfFails.add(moveCollisionDetectedFailure);
			return false;
		}
		return true;
	}

	private boolean isEndOfTheWorld () {
		int xDestination = target.getX();
		int yDestination = target.getY();
		return !(xDestination >= 0 && xDestination < world.getAbsoluteWidth()
				&& yDestination >= 0 && yDestination < world.getAbsoluteHeight());
	}

	private void validateAutoMode(UnitsMoveDestinations unitsMoveDestinations){
		if(unitsMoveDestinations.getDestinationsList().size() < units.size()){
			List<String> unitsWithPathNotFound = new ArrayList<>();
			units.forEach(unit -> {
				if(!unitsMoveDestinations.containsUnit(unit)){
					unitsWithPathNotFound.add(unit.getUuid().toString());
				}
			});
			if(command.isForceMode()){
				listOfFails.add(new ForcedMoveFailed("Not enough energy to perform move and keep life support working", unitsWithPathNotFound));
			}else {
				listOfFails.add(new MoveFailed("Path to target not found", unitsWithPathNotFound));
			}
			return;
		}
		List<String> unitsNotAbleToReachDestination = new ArrayList<>();
		unitsMoveDestinations.getDestinationsList().forEach(unitMoveDestination -> {
			if(!unitMoveDestination.isAbleToReachDestination()){
				unitsNotAbleToReachDestination.add(unitMoveDestination.getUnit().getUuid().toString());
			}
		});
		validateResources(false, !unitsMoveDestinations.canAllUnitsReachDestinations(), unitsNotAbleToReachDestination);
		moveCache.setDestinations(gameData.getActivePlayer().getUuid(), unitsMoveDestinations);
	}

	private void validateManualMode (Unit unit) {
		manualPath = world.findShortestPathWarp(source, target);
		if (manualPath.isEmpty()) {
			listOfFails.add(new MoveFailed("Path to target not found", List.of(unit.getUuid().toString())));
			return;
		}
		Computer computer = unit.getComponentSystem().getComputer();
		MoveEstimation estimation;
		if(command.isForceMode()){
			estimation = computer.estimateForcedMove(world, unit, manualPath, source, target, List.of());
			if(!estimation.getPath().isEmpty()){
				manualPath = estimation.getPath();
				target = manualPath.get(manualPath.size()-1);
			}else{
				listOfFails.add(new ForcedMoveFailed("Not enough energy to perform move and keep life support working", List.of()));
				return;
			}
		} else {
			estimation = computer.estimateMove(world, unit, manualPath, source, target);
		}
		boolean enoughResources = estimation.isEnoughResources();
		validateResources(false, !enoughResources, List.of(unit.getUuid().toString()));
		moveCache.setDestinations(gameData.getActivePlayer().getUuid(), new UnitsMoveDestinations(List.of(new UnitMoveDestination(unit, target, estimation, enoughResources))));
	}


	private void validateResources ( boolean moveRequiresTooManyActionPoints, boolean moveRequiresTooMuchEnergy, List<String> energyTags){
		//TODO: uncomment when action points mechanism is repaired
//		if (moveRequiresTooManyActionPoints && !command.isForceMode()) {
//			listOfFails.add(new NotEnoughActionPoints(
//					"You do not have enough Action Points", new ArrayList<>()
//			));
//		}
		if (moveRequiresTooMuchEnergy && !command.isForceMode()) {
			listOfFails.add(new NotEnoughResources("Not enough energy to reach chosen destination", energyTags));
		}
	}

	private void computeTargetPoint () {
		if (command.isAutoMode()) {
			target = new Position(command.getX(), command.getY());
		} else {
			target = new Position(source.getX() + command.getX(), source.getY() + command.getY());
		}
	}

	private boolean unitDetectedCollisionInTargetPoint () {
		return world.getGameObject(target.getX(), target.getY()).isPresent();
	}

}
