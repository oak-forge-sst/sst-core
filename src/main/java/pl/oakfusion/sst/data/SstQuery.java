package pl.oakfusion.sst.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.oakfusion.data.message.Query;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public abstract class SstQuery extends Query {
	private UUID gameSessionUuid;
	private UUID playerUuid;
	public SstQuery (UUID playerUuid){
		this.playerUuid = playerUuid;
	}
}
