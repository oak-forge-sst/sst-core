package pl.oakfusion.sst.data.initializer;

public class CommandSpecification {
	public final static int EXPLOSION_RANGE = 1;
	public final static double EXPLOSION_HIT = 350;
	public final static double MISS_RANGE = 0.6;
	public final static double MISS_PROBABILITY = 0.05;
	public final static double TRACTOR_BEAM_FOR_COMMANDER = 0.4;
	public final static int TRACTOR_BEAM_RANGE = 4;
	public final static double TRACTOR_BEAM_FOR_SUPER_COMMANDER = 0.5;
	public final static double PROBABILITY_GENERATE_RUBBISH = 0.7;
	public final static int RANGE_FOR_DEATHRAY = 10;
	public final static double DEATHRAY_DEAD_PROBABILITY = 0.5;
	public final static double DEATHRAY_SUCCESSFUL_PROBABILITY = 0.7;
	public final static double TRACTOR_BEAM_MAX_PROBABILITY = 1;
	public final static double TRACTOR_BEAM_MIN_PROBABILITY = 0;
	public final static int RANGE_FOR_TORPEDOES = 10;
	public static final int RANGE = 7;
	public static final int RANGE_FOR_GET_IN_LINE = 0;

	public static final double MAX_DMG = 1.1;
	public static final double MIN_DMG = 0.9;

}
