package pl.oakfusion.sst.data.initializer;

import pl.oakfusion.sst.data.world.component.Component;
import pl.oakfusion.sst.data.world.component.ComponentId;

public class ComponentSpecification<C extends Component> {

    private final ComponentId id;
    private final double maxHealth;
    private final double repairRatio;
    private final ComponentFactory<C> componentFactory;

    public ComponentSpecification(ComponentId id, double maxHealth, double repairRatio, ComponentFactory<C> componentFactory) {
        this.id = id;
        this.maxHealth = maxHealth;
        this.repairRatio = repairRatio;
        this.componentFactory = componentFactory;
    }

    public ComponentId getId() {
        return id;
    }

    public C createComponent() {
        return componentFactory.createComponent(maxHealth, repairRatio);
    }
}
