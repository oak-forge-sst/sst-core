package pl.oakfusion.sst.data.initializer;

import pl.oakfusion.sst.data.world.component.Condition;
import pl.oakfusion.sst.data.world.civilizations.UnitSpecification;

import static com.google.common.base.Preconditions.checkArgument;


//TODO more
public class UnitSpecificationBuilder {
	int actionPoints;
	Condition condition;
	int health;


	public UnitSpecificationBuilder setActionPoints(int actionPoints){
		this.actionPoints = actionPoints;
		return this;
	}

	public UnitSpecificationBuilder setCondition(Condition condition){
		this.condition = condition;
		return this;
	}

	private void validate(){
		checkArgument(condition != null, "Condition specification is required");
		checkArgument(actionPoints >= 0, "Action points need to be positive");
	}

	public UnitSpecification build() {
		return new UnitSpecification(actionPoints, condition);
	}
}
