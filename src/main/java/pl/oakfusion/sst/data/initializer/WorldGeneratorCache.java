package pl.oakfusion.sst.data.initializer;

import com.google.common.annotations.VisibleForTesting;
import lombok.Getter;
import pl.oakfusion.sst.data.world.gameobject.GameObject;
import pl.oakfusion.sst.data.util.Position;
import pl.oakfusion.sst.data.world.World;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
public class WorldGeneratorCache {
	private final Map<Integer, GameObject> objectsCache;
	private final World world;


	public WorldGeneratorCache(World world) {
		objectsCache = new HashMap<>();
		this.world = world;
	}

	public int getAbsoluteWorldHeight() {
		return world.getAbsoluteHeight();
	}

	public int getAbsoluteWorldWidth() {
		return world.getAbsoluteWidth();
	}

	public <G extends GameObject> void put(int x, int y, int forceFieldRange, G spawnedObject) {
		int index = world.sectorIndex(x, y);
		world.add(spawnedObject);
		objectsCache.put(index, spawnedObject);
		if (forceFieldRange > 0) {
			fillRange(x, y, forceFieldRange);
		}
	}

	@VisibleForTesting
	public boolean isSpaceAvailable(int x, int y, int range) {
		if (x >= getAbsoluteWorldWidth() || y >= getAbsoluteWorldHeight() || x < 0 || y < 0) {
			return false;
		}
		int size = range * 2 + 1;
		for (int rX = 0; rX < size; rX++) {
			for (int rY = 0; rY < size; rY++) {
				int checkX = x - range + rX;
				int checkY = y - range + rY;
				if (checkX >= getAbsoluteWorldWidth() || checkY >= getAbsoluteWorldHeight() || checkX < 0 || checkY < 0) {
					continue;
				}
				int sectorIndex = world.sectorIndex(checkX, checkY);
				if (objectsCache.get(sectorIndex) != null) {
					return false;
				}
			}
		}
		return true;
	}

	@VisibleForTesting
	private void fillRange(int x, int y, int range) {
		int size = range * 2 + 1;
		for (int rX = 0; rX < size; rX++) {
			for (int rY = 0; rY < size; rY++) {
				int fillX = x - range + rY;
				int fillY = y - range + rX;
				int forceIdx = world.sectorIndex(fillX, fillY);
				if (objectsCache.get(forceIdx) != null || (fillX < 0 || fillY < 0) ||
						fillX >= world.getAbsoluteWidth() || fillY >= world.getAbsoluteHeight()) {
					continue;
				}
				GameObject forceField = new ForceField(fillX, fillY);
				objectsCache.put(forceIdx, forceField);
			}
		}
	}

	private static class ForceField extends GameObject {
		private ForceField(int x, int y) {
			super(x, y);
		}
	}

	public List<Position> getEmptySurroundingInRange(int objectXPosition, int objectYPosition, int objectForceRange, int range) {
		List<Position> positions = new ArrayList<>();

		for (int x = objectXPosition - range; x <= objectXPosition + range; x++) {
			for (int y = objectYPosition - range; y <= objectYPosition + range; y++) {
				if (isSpaceAvailable(x, y, objectForceRange)) {
					positions.add(new Position(x, y));
				}
			}
		}

		return positions;
	}
}
