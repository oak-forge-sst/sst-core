package pl.oakfusion.sst.data.initializer;

import pl.oakfusion.sst.data.world.component.Component;
import pl.oakfusion.sst.data.world.component.ComponentId;

import java.util.HashMap;
import java.util.Map;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;


public class ComponentSystemSpecificationBuilder {

	private final Map<ComponentId, ComponentSpecification<? extends Component>> specifications = new HashMap<>();

     public  <C extends Component> ComponentSystemSpecificationBuilder specifyComponent(ComponentId componentId, double maxHealth, double repairRatio, ComponentFactory<C> componentFactory) {
    	validate(componentId, maxHealth, repairRatio);
        ComponentSpecification<? extends Component> spec = new ComponentSpecification<>(componentId, maxHealth, repairRatio, componentFactory);
        specifications.put(componentId, spec);
        return this;
    }

    private void validate(ComponentId id, double maxHealth, double repairRatio) {
        checkArgument(maxHealth >= 0, "maxHealth should be higher than zero");
        checkArgument(repairRatio >= 0, "repairRatio should be higher than zero");
        checkNotNull(id, "componentId shouldn't be null");
    }

    public ComponentSystemSpecification build() {
        return new ComponentSystemSpecification(specifications);
    }
}
