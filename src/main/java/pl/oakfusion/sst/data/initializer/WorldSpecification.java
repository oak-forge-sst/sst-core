package pl.oakfusion.sst.data.initializer;


import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.oakfusion.sst.data.parameter.GameDifficulty;
import pl.oakfusion.sst.data.parameter.GameLength;

import static com.google.common.base.Preconditions.checkArgument;
import static java.lang.Math.ceil;

@NoArgsConstructor
@Builder(builderClassName = "WorldSpecificationBuilder", buildMethodName = "build")
@Getter
public class WorldSpecification {
	public static final Integer PLAYERUNIT = 1;
	public static final Integer STARSBASE = 10;
	public static final Integer OPPONENTUNIT = 100;
	public static final Integer SUPERNOVA = 1000;
	public static final Integer EMPTY = 0;

	private int widthInQuadrants;
	private int heightInQuadrants;
	private int quadrantSize;
	private int starQuantity;
	private int starbaseQuantity;
	private int blackHoleQuantity;
	private int planetQuantity;
	private GameDifficulty gameDifficulty;
	private GameLength gameLength;

    public WorldSpecification(int widthInQuadrants,
							  int heightInQuadrants,
							  int quadrantSize, int starQuantity,
							  int starbaseQuantity,
							  int blackHoleQuantity, int planetQuantity,
							  GameDifficulty gameDifficulty,
							  GameLength gameLength) {

        this.widthInQuadrants = widthInQuadrants;
        this.heightInQuadrants = heightInQuadrants;
        this.quadrantSize = quadrantSize;
        this.starQuantity = starQuantity;
        this.starbaseQuantity = starbaseQuantity;
        this.blackHoleQuantity = blackHoleQuantity;
        this.planetQuantity = planetQuantity;
        this.gameDifficulty = gameDifficulty;
        this.gameLength = gameLength;
    }

	@Override
	public String toString() {
		return "WorldSpecification{" +
				"widthInQuadrants=" + widthInQuadrants +
				", heightInQuadrants=" + heightInQuadrants +
				", starQuantity=" + starQuantity +
				", starbaseQuantity=" + starbaseQuantity +
				", blackHoleQuantity=" + blackHoleQuantity +
				", planetQuantity=" + planetQuantity +
				'}';
	}

	public static class WorldSpecificationBuilder {

		final static int MIN_QUADRANT_SIZE = 5;

		final static double MAX_STARBASE_RATIO = 0.1;
		final static double MAX_KLINGON_RATIO = 2.5;

		final static double MAX_STAR_PER_QUADRANT_RATIO = 0.3;
		final static double MAX_BLACK_HOLE_PER_QUADRANT_RATIO = 0.01;
		final static double MAX_PLANET_PER_QUADRANT_RATIO = 0.10;


		void validate() {
			checkArgument(widthInQuadrants > 0, "World width must be positive.");
			checkArgument(heightInQuadrants > 0, "World height must be positive.");
			checkArgument(
					quadrantSize >= MIN_QUADRANT_SIZE,
					"Quadrant size must be bigger or equal to %s",
					MIN_QUADRANT_SIZE
			);
			checkArgument(
					starbaseQuantity <= maxObjectQuantityByAbsoluteRatio(MAX_STARBASE_RATIO),
					"Starbase ratio must be less or equal to %s",
					MAX_STARBASE_RATIO
			);

			checkArgument(
					starQuantity <= maxObjectQuantityByQuadrantRatio(MAX_STAR_PER_QUADRANT_RATIO),
					"Star ratio per quadrant must be less or equal to %s",
					MAX_STAR_PER_QUADRANT_RATIO
			);
			checkArgument(
					blackHoleQuantity <= maxObjectQuantityByQuadrantRatio(MAX_BLACK_HOLE_PER_QUADRANT_RATIO),
					"Black hole ratio per quadrant must be less or equal to %s",
					MAX_BLACK_HOLE_PER_QUADRANT_RATIO
			);
			checkArgument(
					planetQuantity <= maxObjectQuantityByQuadrantRatio(MAX_PLANET_PER_QUADRANT_RATIO),
					"Planet ratio per quadrant must be less or equal to %s",
					MAX_PLANET_PER_QUADRANT_RATIO
			);
		}

		public int maxObjectQuantityByQuadrantRatio(double ratioPerQuadrant) {
			return (int) ceil(ratioPerQuadrant * quadrantSize * quadrantSize * widthInQuadrants * heightInQuadrants);
		}

		public int maxObjectQuantityByAbsoluteRatio(double absoluteRatio) {
			return (int) ceil(absoluteRatio * widthInQuadrants * heightInQuadrants);
		}

		public WorldSpecification build() {
			validate();
			return new WorldSpecification(
					widthInQuadrants,
					heightInQuadrants,
					quadrantSize,
					starQuantity,
					starbaseQuantity,
					blackHoleQuantity,
					planetQuantity,
					gameDifficulty,
					gameLength
			);
		}
	}
}
