package pl.oakfusion.sst.data.initializer;

import pl.oakfusion.sst.data.world.component.Condition;
import pl.oakfusion.sst.data.world.component.Hull;
import pl.oakfusion.sst.data.world.component.SubspaceRadio;
import pl.oakfusion.sst.data.world.civilizations.UnitSpecification;
import pl.oakfusion.sst.data.world.component.*;

import static pl.oakfusion.sst.data.world.component.ComponentId.*;

public class GoodLevelUnitsSpecification implements UnitsSpecification {

	private final static double DEFAULT_MAX_HEALTH = 100;
	private final static double DEFAULT_REPAIR_RATIO = 20;
	private final static double FAST_REPAIR_RATIO = 40;
	private final static double SLOW_REPAIR_RATIO = 10;
	private final static double NULL_SPECIFICATION = 0;

	public static final UnitSpecificationBuilder DEFAULT_REGULAR_SPECIFICATION_BUILDER = new UnitSpecificationBuilder()
			.setActionPoints(3)
			.setCondition(Condition.GREEN);

	public static final ComponentSystemSpecificationBuilder DEFAULT_REGULAR_COMPONENT_BUILDER = new ComponentSystemSpecificationBuilder()
			.specifyComponent(SHIELDS, DEFAULT_MAX_HEALTH, SLOW_REPAIR_RATIO, Shields::new)
			.specifyComponent(HULL, DEFAULT_MAX_HEALTH, DEFAULT_REPAIR_RATIO, Hull::new)
			.specifyComponent(ENERGY_CONTROLLER, DEFAULT_MAX_HEALTH, FAST_REPAIR_RATIO, EnergyController::new)
			.specifyComponent(LIFE_SUPPORT, DEFAULT_MAX_HEALTH, FAST_REPAIR_RATIO, LifeSupport::new)
			.specifyComponent(SUBSPACE_RADIO, DEFAULT_MAX_HEALTH, FAST_REPAIR_RATIO, SubspaceRadio::new)
			.specifyComponent(COMPUTER, DEFAULT_MAX_HEALTH, SLOW_REPAIR_RATIO, Computer::new)
			.specifyComponent(SHORT_RANGE_SENSOR, DEFAULT_MAX_HEALTH, DEFAULT_REPAIR_RATIO, ShortRangeSensor::new)
			.specifyComponent(LONG_RANGE_SENSOR, DEFAULT_MAX_HEALTH, DEFAULT_REPAIR_RATIO, LongRangeSensor::new)
			.specifyComponent(WARP_ENGINES, DEFAULT_MAX_HEALTH, DEFAULT_REPAIR_RATIO, (double maxHealth, double repairRatio) -> new WarpEngines(maxHealth, repairRatio, 6, true))
			.specifyComponent(TORPEDOES_DEVICE, DEFAULT_MAX_HEALTH, DEFAULT_REPAIR_RATIO, (double maxHealth, double repairRatio) -> new TorpedoesDevice(maxHealth, repairRatio, 10))
			.specifyComponent(PHASERS_DEVICE, DEFAULT_MAX_HEALTH, DEFAULT_REPAIR_RATIO, PhasersDevice::new)
			.specifyComponent(ENERGY_CONTAINER, DEFAULT_MAX_HEALTH, DEFAULT_REPAIR_RATIO, (double maxHealth, double repairRatio) -> new EnergyContainer(5000, 5000, maxHealth, repairRatio))
			.specifyComponent(DEATH_RAY_COMPONENT, DEFAULT_MAX_HEALTH, FAST_REPAIR_RATIO, DeathRayComponent::new);

	public final UnitSpecification ENTERPRISE_REGULAR_SPECIFICATION
			= DEFAULT_REGULAR_SPECIFICATION_BUILDER.build();

	public final ComponentSystemSpecification ENTERPRISE_COMPONENT_REGULAR_SPECIFICATION
			= DEFAULT_REGULAR_COMPONENT_BUILDER.build();

	public final UnitSpecification KLINGON_REGULAR_SPECIFICATION
			= DEFAULT_REGULAR_SPECIFICATION_BUILDER.build();

	public final ComponentSystemSpecification KLINGON_COMPONENTS_REGULAR_SPECIFICATION
			= DEFAULT_REGULAR_COMPONENT_BUILDER.build();

	public final UnitSpecification COMMANDER_REGULAR_SPECIFICATION
			= DEFAULT_REGULAR_SPECIFICATION_BUILDER.build();

	public final ComponentSystemSpecification COMMANDER_COMPONENTS_REGULAR_SPECIFICATION
			= DEFAULT_REGULAR_COMPONENT_BUILDER.build();

	public final UnitSpecification SUPER_COMMANDER_REGULAR_SPECIFICATION
			= DEFAULT_REGULAR_SPECIFICATION_BUILDER.build();

	public final ComponentSystemSpecification SUPER_COMMANDER_COMPONENTS_REGULAR_SPECIFICATION
			= DEFAULT_REGULAR_COMPONENT_BUILDER.build();

	public  final UnitSpecification ROMULAN_REGULAR_SPECIFICATION
			= DEFAULT_REGULAR_SPECIFICATION_BUILDER.build();

	public final ComponentSystemSpecification ROMULAN_COMPONENTS_REGULAR_SPECIFICATION
			= DEFAULT_REGULAR_COMPONENT_BUILDER.build();


	@Override
	public UnitSpecification getEnterpriseSpecification() {
		return ENTERPRISE_REGULAR_SPECIFICATION;
	}

	@Override
	public ComponentSystemSpecification getEnterpriseComponentSystemSpecification() {
		return ENTERPRISE_COMPONENT_REGULAR_SPECIFICATION;
	}

	@Override
	public UnitSpecification getKlingonSpecification() {
		return KLINGON_REGULAR_SPECIFICATION;
	}

	@Override
	public ComponentSystemSpecification getKlingonComponentSystemSpecification() {
		return KLINGON_COMPONENTS_REGULAR_SPECIFICATION;
	}

	@Override
	public UnitSpecification getCommanderSpecification() {
		return COMMANDER_REGULAR_SPECIFICATION;
	}

	@Override
	public ComponentSystemSpecification getCommanderComponentSystemSpecification() {
		return COMMANDER_COMPONENTS_REGULAR_SPECIFICATION;
	}

	@Override
	public UnitSpecification getSuperCommanderSpecification() {
		return SUPER_COMMANDER_REGULAR_SPECIFICATION;
	}

	@Override
	public ComponentSystemSpecification getSuperCommanderComponentSystemSpecification() {
		return SUPER_COMMANDER_COMPONENTS_REGULAR_SPECIFICATION;
	}

	@Override
	public UnitSpecification getRomulanSpecification() {
		return ROMULAN_REGULAR_SPECIFICATION;
	}

	@Override
	public ComponentSystemSpecification getRomulanComponentSystemSpecification() {
		return ROMULAN_COMPONENTS_REGULAR_SPECIFICATION;
	}
}
