package pl.oakfusion.sst.data.initializer;

import pl.oakfusion.sst.data.world.component.Component;

@FunctionalInterface
public interface ComponentFactory<C extends Component> {
	C createComponent(double maxHealth, double repairRatio);
}
