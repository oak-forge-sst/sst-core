package pl.oakfusion.sst.data.initializer;

import pl.oakfusion.sst.data.world.component.ComponentId;
import pl.oakfusion.sst.data.world.component.Hull;
import pl.oakfusion.sst.data.world.component.SubspaceRadio;
import pl.oakfusion.sst.data.world.component.*;

import java.util.HashMap;
import java.util.Map;

import static pl.oakfusion.sst.data.world.component.ComponentId.*;

//leaving it here for future reference, not currently used.
public class ComponentSystemSpecification {


	private final static double DEFAULT_MAX_HEALTH = 100;
	private final static double DEFAULT_REPAIR_RATIO = 20;
	private final static double FAST_REPAIR_RATIO = 40;
	private final static double SLOW_REPAIR_RATIO = 10;
	private final static double NULL_SPECIFICATION = 0;

	public static final ComponentSystemSpecification ENTERPRISE_COMPONENT_NULL_SPECIFICATION = new ComponentSystemSpecificationBuilder()
			.specifyComponent(SHIELDS, NULL_SPECIFICATION, NULL_SPECIFICATION, Shields::new)
			.specifyComponent(HULL, NULL_SPECIFICATION, NULL_SPECIFICATION, Hull::new)
			.specifyComponent(ENERGY_CONTROLLER, NULL_SPECIFICATION, NULL_SPECIFICATION, EnergyController::new)
			.specifyComponent(LIFE_SUPPORT, NULL_SPECIFICATION, NULL_SPECIFICATION, LifeSupport::new)
			.specifyComponent(SUBSPACE_RADIO, NULL_SPECIFICATION, NULL_SPECIFICATION, SubspaceRadio::new)
			.specifyComponent(COMPUTER, NULL_SPECIFICATION, NULL_SPECIFICATION, Computer::new)
			.specifyComponent(SHORT_RANGE_SENSOR, NULL_SPECIFICATION, NULL_SPECIFICATION, ShortRangeSensor::new)
			.specifyComponent(LONG_RANGE_SENSOR, NULL_SPECIFICATION, NULL_SPECIFICATION, LongRangeSensor::new)
			.specifyComponent(WARP_ENGINES, DEFAULT_MAX_HEALTH, NULL_SPECIFICATION, (double maxHealth, double repairRatio) -> new WarpEngines(maxHealth, repairRatio, 1, true))
			.specifyComponent(TORPEDOES_DEVICE, DEFAULT_MAX_HEALTH, NULL_SPECIFICATION, (double maxHealth, double repairRatio) -> new TorpedoesDevice(maxHealth, repairRatio, 0))
			.specifyComponent(PHASERS_DEVICE, DEFAULT_MAX_HEALTH, NULL_SPECIFICATION, PhasersDevice::new)
			.specifyComponent(DEATH_RAY_COMPONENT, DEFAULT_MAX_HEALTH, NULL_SPECIFICATION, DeathRayComponent::new)
			.build();

	public static final ComponentSystemSpecification ENTERPRISE_COMPONENT_REGULAR_SPECIFICATION = new ComponentSystemSpecificationBuilder()
			.specifyComponent(SHIELDS, DEFAULT_MAX_HEALTH, SLOW_REPAIR_RATIO, Shields::new)
			.specifyComponent(HULL, DEFAULT_MAX_HEALTH, DEFAULT_REPAIR_RATIO, Hull::new)
			.specifyComponent(ENERGY_CONTROLLER, DEFAULT_MAX_HEALTH, FAST_REPAIR_RATIO, EnergyController::new)
			.specifyComponent(LIFE_SUPPORT, DEFAULT_MAX_HEALTH, FAST_REPAIR_RATIO, LifeSupport::new)
			.specifyComponent(SUBSPACE_RADIO, DEFAULT_MAX_HEALTH, FAST_REPAIR_RATIO, SubspaceRadio::new)
			.specifyComponent(COMPUTER, DEFAULT_MAX_HEALTH, SLOW_REPAIR_RATIO, Computer::new)
			.specifyComponent(SHORT_RANGE_SENSOR, DEFAULT_MAX_HEALTH, DEFAULT_REPAIR_RATIO, ShortRangeSensor::new)
			.specifyComponent(LONG_RANGE_SENSOR, DEFAULT_MAX_HEALTH, DEFAULT_REPAIR_RATIO, LongRangeSensor::new)
			.specifyComponent(WARP_ENGINES, DEFAULT_MAX_HEALTH, DEFAULT_REPAIR_RATIO, (double maxHealth, double repairRatio) -> new WarpEngines(maxHealth, repairRatio, 6, true))
			.specifyComponent(TORPEDOES_DEVICE, DEFAULT_MAX_HEALTH, DEFAULT_REPAIR_RATIO, (double maxHealth, double repairRatio) -> new TorpedoesDevice(maxHealth, repairRatio, 10))
			.specifyComponent(PHASERS_DEVICE, DEFAULT_MAX_HEALTH, DEFAULT_REPAIR_RATIO, PhasersDevice::new)
			.specifyComponent(ENERGY_CONTAINER, DEFAULT_MAX_HEALTH, DEFAULT_REPAIR_RATIO, (double maxHealth, double repairRatio) -> new EnergyContainer(5000, 5000, maxHealth, repairRatio))
			.specifyComponent(DEATH_RAY_COMPONENT, DEFAULT_MAX_HEALTH, FAST_REPAIR_RATIO, DeathRayComponent::new)
			.build();


	public static final ComponentSystemSpecification SUPER_COMMANDER_COMPONENT_REGULAR_SPECIFICATION = new ComponentSystemSpecificationBuilder()
			.specifyComponent(SHIELDS, DEFAULT_MAX_HEALTH, SLOW_REPAIR_RATIO, (double maxHealth, double repairRatio) -> new Shields(maxHealth, repairRatio, 0))
			.specifyComponent(WARP_ENGINES, DEFAULT_MAX_HEALTH, DEFAULT_REPAIR_RATIO, (double maxHealth, double repairRatio) -> new WarpEngines(maxHealth, repairRatio, 10, false))
			.specifyComponent(SUBSPACE_RADIO, DEFAULT_MAX_HEALTH, FAST_REPAIR_RATIO, SubspaceRadio::new)
			.specifyComponent(TORPEDOES_DEVICE, DEFAULT_MAX_HEALTH, DEFAULT_REPAIR_RATIO, (double maxHealth, double repairRatio) -> new TorpedoesDevice(maxHealth, repairRatio, 1))
			.specifyComponent(PHASERS_DEVICE, DEFAULT_MAX_HEALTH, DEFAULT_REPAIR_RATIO, PhasersDevice::new)
			.specifyComponent(SHORT_RANGE_SENSOR, DEFAULT_MAX_HEALTH, DEFAULT_REPAIR_RATIO, ShortRangeSensor::new)
			.specifyComponent(TRACTOR_BEAM_DEVICE, DEFAULT_MAX_HEALTH, DEFAULT_REPAIR_RATIO, TractorBeamDevice::new)
			.specifyComponent(ANTI_PHOTON_DEVICE, DEFAULT_MAX_HEALTH, DEFAULT_REPAIR_RATIO, AntiPhotonDevice::new)
			.specifyComponent(HULL, DEFAULT_MAX_HEALTH, DEFAULT_REPAIR_RATIO, Hull::new)
			.specifyComponent(COMPUTER, DEFAULT_MAX_HEALTH, SLOW_REPAIR_RATIO, Computer::new)
			.build();

	public static final ComponentSystemSpecification KLINGON_REGULAR_SPECIFICATION = new ComponentSystemSpecificationBuilder()
			.specifyComponent(HULL, DEFAULT_MAX_HEALTH, DEFAULT_REPAIR_RATIO, Hull::new)
			.specifyComponent(WARP_ENGINES, DEFAULT_MAX_HEALTH, DEFAULT_REPAIR_RATIO, (double maxHealth, double repairRatio) -> new WarpEngines(maxHealth, repairRatio, 10, false))
			.specifyComponent(PHASERS_DEVICE, DEFAULT_MAX_HEALTH, DEFAULT_REPAIR_RATIO, PhasersDevice::new)
			.specifyComponent(SHORT_RANGE_SENSOR, DEFAULT_MAX_HEALTH, DEFAULT_REPAIR_RATIO, ShortRangeSensor::new)
			.build();

	public static final ComponentSystemSpecification COMMANDER_REGULAR_SPECIFICATION = new ComponentSystemSpecificationBuilder()
			.specifyComponent(PHASERS_DEVICE, DEFAULT_MAX_HEALTH, DEFAULT_REPAIR_RATIO, PhasersDevice::new)
			.specifyComponent(WARP_ENGINES, DEFAULT_MAX_HEALTH, DEFAULT_REPAIR_RATIO, (double maxHealth, double repairRatio) -> new WarpEngines(maxHealth, repairRatio, 10, false))
			.specifyComponent(HULL, DEFAULT_MAX_HEALTH, DEFAULT_REPAIR_RATIO, Hull::new)
			.specifyComponent(SHORT_RANGE_SENSOR, DEFAULT_MAX_HEALTH, DEFAULT_REPAIR_RATIO, ShortRangeSensor::new)
			.specifyComponent(TRACTOR_BEAM_DEVICE, DEFAULT_MAX_HEALTH, DEFAULT_REPAIR_RATIO, TractorBeamDevice::new)
			.build();

	private final Map<ComponentId, ComponentSpecification<? extends Component>> specifications;

	public ComponentSystemSpecification(Map<ComponentId, ComponentSpecification<? extends Component>> specifications) {
		this.specifications = specifications;
	}

	public Map<ComponentId, ComponentSpecification<? extends Component>> getSpecifications() {
		return specifications;
	}

	public ComponentSpecification<? extends Component> getSpecificationOf(ComponentId id) {
		return specifications.get(id);
	}

	public Map<ComponentId, Component> build() {
		Map<ComponentId, Component> components = new HashMap<>(specifications.size());
		specifications.forEach((id, spec) -> components.put(id, spec.createComponent()));
		return components;
	}

}
