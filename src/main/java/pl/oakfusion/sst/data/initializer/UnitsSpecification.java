package pl.oakfusion.sst.data.initializer;

import pl.oakfusion.sst.data.world.civilizations.UnitSpecification;


public interface UnitsSpecification {
	UnitSpecification getEnterpriseSpecification();
	ComponentSystemSpecification getEnterpriseComponentSystemSpecification();
	UnitSpecification getKlingonSpecification();
	ComponentSystemSpecification getKlingonComponentSystemSpecification();
	UnitSpecification getCommanderSpecification();
	ComponentSystemSpecification getCommanderComponentSystemSpecification();
	UnitSpecification getSuperCommanderSpecification();
	ComponentSystemSpecification getSuperCommanderComponentSystemSpecification();
	UnitSpecification getRomulanSpecification();
	ComponentSystemSpecification getRomulanComponentSystemSpecification();

}
