package pl.oakfusion.sst.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.oakfusion.data.message.Event;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public abstract class SstEvent extends Event {
	private UUID gameSessionUuid;
	private UUID playerUuid;
	public SstEvent (UUID playerUuid){
		this.playerUuid = playerUuid;
	}
}
