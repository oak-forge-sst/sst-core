package pl.oakfusion.sst.data.parameter;

import lombok.AllArgsConstructor;
import lombok.Getter;
import pl.oakfusion.sst.data.world.gameobject.GameObject;
import pl.oakfusion.sst.data.world.gameobject.BlackHole;
import pl.oakfusion.sst.data.world.gameobject.Planet;
import pl.oakfusion.sst.data.world.gameobject.Star;
import pl.oakfusion.sst.data.world.gameobject.Starbase;
import pl.oakfusion.sst.data.world.civilizations.Unit;

@Getter
@AllArgsConstructor
public enum ObjectType {
	STAR(Star.class),
	PLANET(Planet.class),
	STARBASE(Starbase.class),
	BLACK_HOLE(BlackHole.class),
	UNIT(Unit.class);


	private Class<? extends GameObject> clazz;

}
