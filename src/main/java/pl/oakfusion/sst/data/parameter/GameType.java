package pl.oakfusion.sst.data.parameter;

public enum GameType {
	SINGLEPLAYER,
	MULTIPLAYER
}
