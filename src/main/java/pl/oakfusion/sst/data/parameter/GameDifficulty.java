package pl.oakfusion.sst.data.parameter;

import pl.oakfusion.sst.data.util.AbbreviationFactory;

import java.util.List;

public enum GameDifficulty implements Parameter {
    NOVICE,
    FAIR,
    GOOD,
    EXPERT,
    EMERITUS("em");

    private final List<String> abbreviations;

    GameDifficulty() {
        abbreviations = AbbreviationFactory.getAbbreviations(name());
    }

    GameDifficulty(String abbreviation) {
        abbreviations = AbbreviationFactory.getAbbreviations(name(), abbreviation.toUpperCase());
    }

    @Override
    public boolean accepts(String string) {
        return abbreviations.contains(string);
    }
}
