package pl.oakfusion.sst.data.parameter;

public interface Parameter {
	boolean accepts(String string);
}
