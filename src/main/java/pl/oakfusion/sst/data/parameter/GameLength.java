package pl.oakfusion.sst.data.parameter;

import pl.oakfusion.sst.data.util.AbbreviationFactory;

import java.util.List;

public enum GameLength implements Parameter {
    SHORT,
    MEDIUM,
    LONG;

    private final List<String> abbreviations;

    GameLength() {
        abbreviations = AbbreviationFactory.getAbbreviations(name());
    }

    @Override
    public boolean accepts(String string) {
        return abbreviations.contains(string);
    }
}
