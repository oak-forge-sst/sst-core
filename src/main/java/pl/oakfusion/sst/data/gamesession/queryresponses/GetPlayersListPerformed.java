package pl.oakfusion.sst.data.gamesession.queryresponses;

import pl.oakfusion.sst.data.DTO.PlayerDTO;

import java.util.List;
import java.util.UUID;

public class GetPlayersListPerformed extends GameSessionQueryResponse {
	List<PlayerDTO> playersList;

	public GetPlayersListPerformed(UUID gameSessionUuid, UUID playerUuid, List<PlayerDTO> playersList) {
		super(gameSessionUuid, playerUuid);
		this.playersList = playersList;
	}
}
