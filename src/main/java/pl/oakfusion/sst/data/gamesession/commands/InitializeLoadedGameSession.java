package pl.oakfusion.sst.data.gamesession.commands;

import java.util.UUID;

public class InitializeLoadedGameSession extends InitializeGameSession {
	public InitializeLoadedGameSession(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
