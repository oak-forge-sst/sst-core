package pl.oakfusion.sst.data.gamesession.events;

import pl.oakfusion.sst.data.gamesession.events.GameSessionEvent;

import java.util.UUID;

public class FileNotFound extends GameSessionEvent {
	public FileNotFound(UUID playerUuid){
		super(playerUuid);
	}
}
