package pl.oakfusion.sst.data.gamesession.events;

import java.util.UUID;

public class EndTurnPerformed extends GameSessionEvent {
	public EndTurnPerformed(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
