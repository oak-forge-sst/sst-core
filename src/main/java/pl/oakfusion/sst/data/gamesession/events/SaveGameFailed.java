package pl.oakfusion.sst.data.gamesession.events;

import pl.oakfusion.sst.data.gamesession.events.GameSessionEvent;

import java.util.UUID;

public class SaveGameFailed extends GameSessionEvent {
	public SaveGameFailed(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
