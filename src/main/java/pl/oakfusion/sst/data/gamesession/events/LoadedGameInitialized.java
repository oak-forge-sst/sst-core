package pl.oakfusion.sst.data.gamesession.events;

import pl.oakfusion.sst.data.gamesession.events.GameSessionEvent;

import java.util.UUID;

public class LoadedGameInitialized extends GameSessionEvent {
	public LoadedGameInitialized(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
