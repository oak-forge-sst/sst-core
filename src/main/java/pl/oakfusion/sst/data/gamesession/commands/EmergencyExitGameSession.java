package pl.oakfusion.sst.data.gamesession.commands;

import java.util.UUID;

public class EmergencyExitGameSession extends GameSessionCommand {
	public EmergencyExitGameSession(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
