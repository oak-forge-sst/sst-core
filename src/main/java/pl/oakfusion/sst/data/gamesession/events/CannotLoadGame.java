package pl.oakfusion.sst.data.gamesession.events;

import pl.oakfusion.sst.data.gamesession.events.GameSessionEvent;

import java.util.UUID;

public class CannotLoadGame extends GameSessionEvent {
	public CannotLoadGame(UUID playerUuid){
		super(playerUuid);
	}
}
