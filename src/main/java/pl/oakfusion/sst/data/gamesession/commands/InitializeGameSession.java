package pl.oakfusion.sst.data.gamesession.commands;

import java.util.UUID;

public class InitializeGameSession extends GameSessionCommand {
	public InitializeGameSession(UUID playerUuid){
		super(playerUuid);
	}

	public InitializeGameSession(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
