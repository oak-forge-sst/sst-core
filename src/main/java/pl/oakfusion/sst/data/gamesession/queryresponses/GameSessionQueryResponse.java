package pl.oakfusion.sst.data.gamesession.queryresponses;

import pl.oakfusion.sst.data.SstQueryResponse;

import java.util.UUID;


public abstract class GameSessionQueryResponse extends SstQueryResponse {
	public GameSessionQueryResponse(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
