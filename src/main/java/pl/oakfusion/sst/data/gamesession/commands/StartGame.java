package pl.oakfusion.sst.data.gamesession.commands;

import java.util.UUID;

public class StartGame extends GameSessionCommand{
	public StartGame(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
