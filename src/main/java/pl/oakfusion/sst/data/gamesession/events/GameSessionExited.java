package pl.oakfusion.sst.data.gamesession.events;

import java.util.UUID;

public class GameSessionExited extends GameSessionEvent {
	public GameSessionExited(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
