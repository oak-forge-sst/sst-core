package pl.oakfusion.sst.data.gamesession.commands;

import lombok.Getter;

import java.util.Optional;
import java.util.UUID;

@Getter
public class LoadGameSession extends GameSessionCommand {
	private Optional<String> fileName;
	LoadGameSession(UUID gameSessionUuid, UUID playerUuid, Optional<String> fileName){
		super(gameSessionUuid, playerUuid);
		this.fileName = fileName;
	}
}
