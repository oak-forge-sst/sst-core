package pl.oakfusion.sst.data.gamesession.events;

import java.util.UUID;

public class GameSessionInitialized extends GameSessionEvent {
	public GameSessionInitialized(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
