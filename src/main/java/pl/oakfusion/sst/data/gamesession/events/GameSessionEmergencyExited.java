package pl.oakfusion.sst.data.gamesession.events;

import java.util.UUID;

public class GameSessionEmergencyExited extends GameSessionEvent {
	public GameSessionEmergencyExited(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
