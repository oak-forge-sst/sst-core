package pl.oakfusion.sst.data.gamesession.commands;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.world.player.CivilizationType;

import java.util.UUID;

@Getter
@Setter
public class JoinGameSession extends GameSessionCommand{
	private CivilizationType userCivilization;
	private String name;

	public JoinGameSession(UUID gameSessionUuid, UUID playerUuid, CivilizationType userCivilization, String name) {
		super(gameSessionUuid, playerUuid);
		this.userCivilization = userCivilization;
		this.name = name;
	}
}
