package pl.oakfusion.sst.data.gamesession.events;

import java.util.UUID;

public class GameExited extends GameSessionEvent {
	public GameExited(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
