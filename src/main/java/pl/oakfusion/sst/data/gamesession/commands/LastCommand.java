package pl.oakfusion.sst.data.gamesession.commands;

import pl.oakfusion.data.message.Command;

public class LastCommand {
	private final Command lastCommand;
	private boolean continueCommand = false;
	private boolean commandValidated = false;

	public LastCommand(Command command) {
		lastCommand = command;
	}

	public Command getLastCommand() {
		return lastCommand;
	}

	public boolean isContinueCommand() {
		return continueCommand;
	}

	public boolean isCommandValidated() {
		return commandValidated;
	}

	public LastCommand setContinueCommand(boolean continueCommand) {
		this.continueCommand = continueCommand;
		return this;
	}

	public LastCommand setCommandValidated(boolean commandValidated) {
		this.commandValidated = commandValidated;
		return this;
	}

}
