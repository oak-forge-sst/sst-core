package pl.oakfusion.sst.data.gamesession.events;

import java.util.UUID;

public class StartGamePerformed extends GameSessionEvent{
	public StartGamePerformed(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
