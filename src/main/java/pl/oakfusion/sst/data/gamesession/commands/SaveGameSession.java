package pl.oakfusion.sst.data.gamesession.commands;

import lombok.Getter;

import java.util.UUID;

@Getter
public class SaveGameSession extends GameSessionCommand {
	private  String name;
	private  String description;

	public SaveGameSession(UUID gameSessionUuid, UUID playerUuid, String name, String description) {
		super(gameSessionUuid, playerUuid);
		this.name = name;
		this.description = description;
	}
}
