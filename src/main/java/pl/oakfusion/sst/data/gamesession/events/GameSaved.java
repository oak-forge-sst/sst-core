package pl.oakfusion.sst.data.gamesession.events;

import java.util.UUID;

public class GameSaved extends GameSessionEvent {
	public GameSaved(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
