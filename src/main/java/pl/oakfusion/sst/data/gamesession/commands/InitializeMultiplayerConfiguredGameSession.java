package pl.oakfusion.sst.data.gamesession.commands;

import lombok.Getter;
import pl.oakfusion.sst.data.initializer.WorldSpecification;
import pl.oakfusion.sst.data.world.player.CivilizationType;

import java.util.UUID;

@Getter
public class InitializeMultiplayerConfiguredGameSession extends InitializeGameSession{
	private int maxNumberOfPlayers;
	private WorldSpecification worldSpecification;
	private CivilizationType userCivilization;
	private String name;

	public InitializeMultiplayerConfiguredGameSession(UUID playerUuid, int maxNumberOfPlayers, WorldSpecification worldSpecification, CivilizationType userCivilization, String name) {
		super(playerUuid);
		this.maxNumberOfPlayers = maxNumberOfPlayers;
		this.worldSpecification = worldSpecification;
		this.userCivilization = userCivilization;
		this.name = name;
	}
}
