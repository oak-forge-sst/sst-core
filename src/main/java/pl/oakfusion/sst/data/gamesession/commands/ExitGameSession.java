package pl.oakfusion.sst.data.gamesession.commands;

import java.util.UUID;

public class ExitGameSession extends GameSessionCommand {
	public ExitGameSession(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
