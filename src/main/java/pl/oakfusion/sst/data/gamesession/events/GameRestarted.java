package pl.oakfusion.sst.data.gamesession.events;

import java.util.UUID;

public class GameRestarted extends GameSessionEvent {
	public GameRestarted(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
