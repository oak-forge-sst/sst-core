package pl.oakfusion.sst.data.gamesession.events;

import java.util.UUID;

public class JoinGamePerformed extends GameSessionEvent{
	public JoinGamePerformed(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
