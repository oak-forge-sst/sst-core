package pl.oakfusion.sst.data.gamesession.commands;


import java.util.UUID;

public class ExitGame extends GameSessionCommand {
	public ExitGame(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
