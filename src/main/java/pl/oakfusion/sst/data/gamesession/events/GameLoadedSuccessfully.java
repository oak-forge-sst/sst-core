package pl.oakfusion.sst.data.gamesession.events;

import pl.oakfusion.sst.data.gamesession.events.GameSessionEvent;

import java.util.UUID;


public class GameLoadedSuccessfully extends GameSessionEvent {
	public GameLoadedSuccessfully(UUID gameSessionUuid, UUID playerUuid){
		super(gameSessionUuid, playerUuid);
	}
}
