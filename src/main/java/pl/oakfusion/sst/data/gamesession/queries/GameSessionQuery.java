package pl.oakfusion.sst.data.gamesession.queries;

import pl.oakfusion.sst.data.SstQuery;

import java.util.UUID;

public abstract class GameSessionQuery extends SstQuery {
	public GameSessionQuery(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
