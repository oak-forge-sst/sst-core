package pl.oakfusion.sst.data.gamesession.queries;

import java.util.UUID;

public class GetPlayersList extends GameSessionQuery {
	public GetPlayersList(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
