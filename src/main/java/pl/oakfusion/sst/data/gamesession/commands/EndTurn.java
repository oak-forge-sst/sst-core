package pl.oakfusion.sst.data.gamesession.commands;

import java.util.UUID;

public class EndTurn extends GameSessionCommand {
	public EndTurn(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
