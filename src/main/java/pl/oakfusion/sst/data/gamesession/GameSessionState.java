package pl.oakfusion.sst.data.gamesession;

public enum GameSessionState {
	NOT_RUNNING,
	RUNNING
}
