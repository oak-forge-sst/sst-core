package pl.oakfusion.sst.data.gamesession.commands;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.parameter.GameDifficulty;
import pl.oakfusion.sst.data.parameter.GameLength;
import pl.oakfusion.sst.data.world.player.CivilizationType;

import java.util.UUID;

@Getter
@Setter
public class InitializeMultiplayerRegularGameSession extends InitializeGameSession {
	private GameLength gameLength;
	private int maxNumberOfPlayers;
	private String name;
	private CivilizationType userCivilization;
	private GameDifficulty gameDifficulty;

	InitializeMultiplayerRegularGameSession(UUID playerUuid){
		super(playerUuid);
	}

	public InitializeMultiplayerRegularGameSession(UUID playerUuid, GameLength gameLength, int maxNumberOfPlayers,
												   String name, CivilizationType userCivilization, GameDifficulty gameDifficulty) {
		super(playerUuid);
		this.gameLength = gameLength;
		this.maxNumberOfPlayers = maxNumberOfPlayers;
		this.name = name;
		this.userCivilization = userCivilization;
		this.gameDifficulty = gameDifficulty;
	}
}
