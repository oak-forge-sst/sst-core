package pl.oakfusion.sst.data.gamesession.events;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.gamesession.SavedGameData;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class LoadSavedGames extends GameSessionEvent {
	private List<SavedGameData> gameData;
	public LoadSavedGames(UUID playerUuid, List<SavedGameData> gameData){
		super(playerUuid);
		this.gameData = gameData;
	}
}
