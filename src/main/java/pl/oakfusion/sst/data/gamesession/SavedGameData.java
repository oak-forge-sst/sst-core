package pl.oakfusion.sst.data.gamesession;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;

import java.util.Date;
@Getter
public class SavedGameData {
	private String name;
	private String description;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-DD@HH:mm")
	private Date timeStamp;

	public SavedGameData(String name, String description, Date timeStamp) {
		this.name = name;
		this.description = description;
		this.timeStamp = timeStamp;
	}

	private SavedGameData() { }
}
