package pl.oakfusion.sst.data.gamesession.commands;

import java.util.UUID;

public class RestartGame extends GameSessionCommand {
	public RestartGame(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
