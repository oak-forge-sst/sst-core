package pl.oakfusion.sst.data.gamesession.events;


import pl.oakfusion.sst.data.SstEvent;

import java.util.UUID;

public abstract class GameSessionEvent extends SstEvent {
	public GameSessionEvent(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}

	public GameSessionEvent(UUID playerUuid) {
		super(playerUuid);
	}
}
