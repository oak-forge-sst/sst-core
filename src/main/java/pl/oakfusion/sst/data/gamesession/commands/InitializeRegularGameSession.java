package pl.oakfusion.sst.data.gamesession.commands;

import lombok.Getter;
import pl.oakfusion.sst.data.parameter.GameDifficulty;
import pl.oakfusion.sst.data.parameter.GameLength;
import pl.oakfusion.sst.data.world.player.CivilizationType;

import java.util.UUID;

@Getter
public class InitializeRegularGameSession extends InitializeGameSession {
	private final int maxNumberOfPlayers = 2;
	private GameDifficulty gameDifficulty;
	private GameLength gameLength;
	private CivilizationType userCivilization;
	private CivilizationType opponentCivilization;
	private String name;
	private UUID AIuuid;

	public InitializeRegularGameSession(GameLength gameLength, GameDifficulty gameDifficulty,
										CivilizationType userCivilization, CivilizationType opponentCivilization, String name,
										UUID playerUuid, UUID AIuuid) {
		super(playerUuid);
		this.gameDifficulty = gameDifficulty;
		this.gameLength = gameLength;
		this.userCivilization = userCivilization;
		this.opponentCivilization = opponentCivilization;
		this.name = name;
		this.AIuuid = AIuuid;
	}

	public InitializeRegularGameSession(UUID playerUuid) {
		super(playerUuid);
	}
}
