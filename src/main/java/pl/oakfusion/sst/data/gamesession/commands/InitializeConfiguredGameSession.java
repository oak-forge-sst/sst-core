package pl.oakfusion.sst.data.gamesession.commands;

import lombok.Getter;
import pl.oakfusion.sst.data.initializer.WorldSpecification;
import pl.oakfusion.sst.data.world.player.CivilizationType;

import java.util.UUID;

@Getter
public class InitializeConfiguredGameSession extends InitializeGameSession {

	private final int maxNumberOfPlayers = 2;
    private  WorldSpecification specification;
	private CivilizationType userCivilization;
	private CivilizationType opponentCivilization;
	private String name;
	private UUID AIuuid;

    public InitializeConfiguredGameSession(WorldSpecification specification, CivilizationType userCivilization,
										   CivilizationType opponentCivilization, String name, UUID playerUuid,
										   UUID AIuuid) {
    	super(playerUuid);
        this.specification = specification;
		this.userCivilization = userCivilization;
		this.opponentCivilization = opponentCivilization;
		this.name = name;
		this.AIuuid = AIuuid;
    }

    public InitializeConfiguredGameSession(UUID playerUuid){
    	super(playerUuid);
	}

}
