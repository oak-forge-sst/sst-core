package pl.oakfusion.sst.data.gamesession.commands;

import pl.oakfusion.sst.data.SstCommand;

import java.util.UUID;

public abstract class GameSessionCommand extends SstCommand {
	public GameSessionCommand(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}

	public GameSessionCommand(UUID playerUuid) {
		super(playerUuid);
	}
}
