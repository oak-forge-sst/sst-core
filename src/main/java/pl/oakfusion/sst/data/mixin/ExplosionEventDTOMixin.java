package pl.oakfusion.sst.data.mixin;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import pl.oakfusion.sst.data.DTO.torpedo.ObjectExplosionDTO;
import pl.oakfusion.sst.data.DTO.torpedo.UnitHitDTO;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "ObjectType", include = JsonTypeInfo.As.EXTERNAL_PROPERTY)
@JsonSubTypes(value = {
		@JsonSubTypes.Type(value = UnitHitDTO.class, name = "Unit"),
		@JsonSubTypes.Type(value = ObjectExplosionDTO.class)
})
public abstract class ExplosionEventDTOMixin {
}
