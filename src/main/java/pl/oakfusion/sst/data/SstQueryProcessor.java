package pl.oakfusion.sst.data;

import pl.oakfusion.data.message.QueryResponse;
import pl.oakfusion.sst.core.gamedata.GameSessionData;

import java.util.List;

@FunctionalInterface
public interface SstQueryProcessor<Q extends SstQuery> {
	List<QueryResponse> process(Q query, GameSessionData gameSessionData);
}
