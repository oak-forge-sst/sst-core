package pl.oakfusion.sst.data.validationfailures;

import java.util.List;

public class NoActiveUnit extends ValidationFailure {
	public NoActiveUnit(String errorMessage, List<String> tags) {
		super(errorMessage, tags);
	}
}
