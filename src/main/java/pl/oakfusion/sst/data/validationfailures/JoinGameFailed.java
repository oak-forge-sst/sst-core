package pl.oakfusion.sst.data.validationfailures;

import java.util.List;

public class JoinGameFailed extends ValidationFailure {

	public JoinGameFailed(String errorMessage, List<String> tags) {
		super(errorMessage, tags);
	}
}
