package pl.oakfusion.sst.data.validationfailures;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.world.component.ComponentId;

import java.util.List;
import java.util.UUID;

@Setter
@Getter
public class ComponentLowHealthWarning extends ValidationFailure {
	private ComponentId componentId;
	private UUID unitUuid;

	public ComponentLowHealthWarning(String errorMessage, List<String> tags, ComponentId componentId, UUID unitUuid) {
		super(errorMessage, tags);
		this.componentId = componentId;
		this.unitUuid = unitUuid;
	}
}
