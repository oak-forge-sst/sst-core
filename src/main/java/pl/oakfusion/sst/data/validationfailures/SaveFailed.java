package pl.oakfusion.sst.data.validationfailures;

import java.util.List;

public class SaveFailed extends ValidationFailure {
	public SaveFailed(String errorMessage, List<String> tags) {
		super(errorMessage, tags);
	}
}
