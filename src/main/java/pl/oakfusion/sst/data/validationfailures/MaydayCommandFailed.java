package pl.oakfusion.sst.data.validationfailures;

import java.util.List;

public class MaydayCommandFailed extends ValidationFailure {
	public MaydayCommandFailed(String errorMessage, List<String> tags) {
		super(errorMessage, tags);
	}
}
