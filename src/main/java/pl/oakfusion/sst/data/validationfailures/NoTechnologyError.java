package pl.oakfusion.sst.data.validationfailures;

import lombok.Getter;
import pl.oakfusion.sst.data.validationfailures.ValidationFailure;

import java.util.List;
import java.util.UUID;

@Getter
public class NoTechnologyError extends ValidationFailure {
	private final String errorMessage;
	private UUID unitUuid;

	public NoTechnologyError(String errorMessage, List<String> tags, UUID unitUuid) {
		super(errorMessage, tags);
		this.errorMessage = errorMessage;
		this.unitUuid = unitUuid;
	}

	public NoTechnologyError(String errorMessage, List<String> tags) {
		super(errorMessage, tags);
		this.errorMessage = errorMessage;
	}

	@Override
	public String toString() {
		return "NoTechnologyError{" +
				"message='" + errorMessage + '\'' +
				'}';
	}
}
