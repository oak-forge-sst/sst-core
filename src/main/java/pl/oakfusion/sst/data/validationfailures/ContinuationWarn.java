package pl.oakfusion.sst.data.validationfailures;

import java.util.List;

public class ContinuationWarn extends ValidationFailure {
	public ContinuationWarn(String errorMessage, List<String> tags) {
		super(errorMessage, tags);
	}
}
