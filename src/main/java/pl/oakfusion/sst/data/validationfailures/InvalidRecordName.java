package pl.oakfusion.sst.data.validationfailures;

import java.util.List;

public class InvalidRecordName extends ValidationFailure {
	public InvalidRecordName(String errorMessage, List<String> tags) {
		super(errorMessage, tags);
	}
}
