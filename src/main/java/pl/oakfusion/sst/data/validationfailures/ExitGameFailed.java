package pl.oakfusion.sst.data.validationfailures;

import java.util.List;

public class ExitGameFailed extends ValidationFailure {
	public ExitGameFailed(String errorMessage, List<String> tags) {
		super(errorMessage, tags);
	}
}
