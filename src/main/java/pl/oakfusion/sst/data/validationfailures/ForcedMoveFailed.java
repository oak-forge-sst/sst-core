package pl.oakfusion.sst.data.validationfailures;

import java.util.ArrayList;

public class ForcedMoveFailed extends ValidationFailure {
	public ForcedMoveFailed(String errorMessage, ArrayList<String> tags) {
		super(errorMessage, tags);
	}
}
