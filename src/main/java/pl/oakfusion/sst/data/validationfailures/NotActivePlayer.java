package pl.oakfusion.sst.data.validationfailures;

import java.util.List;

public class NotActivePlayer extends ValidationFailure {
	public NotActivePlayer(String errorMessage, List<String> tags) {
		super(errorMessage, tags);
	}
}
