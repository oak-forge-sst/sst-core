package pl.oakfusion.sst.data.validationfailures;

import lombok.Getter;
import pl.oakfusion.data.message.Error;

import java.util.List;

@Getter
public class ValidationFailure extends Error {
	private final String errorMessage;
	private final List<String> tags;

	public ValidationFailure(String errorMessage, List<String> tags) {
		this.errorMessage = errorMessage;
		this.tags = tags;
	}

	@Override
	public String toString() {
		return "ValidationError{" +
				"errorMessage='" + errorMessage + '\'' +
				", tags=" + tags +
				'}';
	}
}
