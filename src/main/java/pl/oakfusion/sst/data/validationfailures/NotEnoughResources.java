package pl.oakfusion.sst.data.validationfailures;

import java.util.List;

public class NotEnoughResources extends ValidationFailure {
	public NotEnoughResources(String errorMessage, List<String> tags) {
		super(errorMessage, tags);
	}
}
