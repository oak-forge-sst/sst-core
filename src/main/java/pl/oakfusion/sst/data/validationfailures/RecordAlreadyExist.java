package pl.oakfusion.sst.data.validationfailures;

import java.util.List;

public class RecordAlreadyExist extends ValidationFailure {
	public RecordAlreadyExist(String errorMessage, List<String> tags) {
		super(errorMessage, tags);
	}
}

