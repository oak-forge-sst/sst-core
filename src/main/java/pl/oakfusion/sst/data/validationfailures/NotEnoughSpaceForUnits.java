package pl.oakfusion.sst.data.validationfailures;

import java.util.List;

public class NotEnoughSpaceForUnits extends ValidationFailure {
	public NotEnoughSpaceForUnits(String errorMessage, List<String> tags) {
		super(errorMessage, tags);
	}
}
