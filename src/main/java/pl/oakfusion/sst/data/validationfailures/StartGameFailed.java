package pl.oakfusion.sst.data.validationfailures;

import java.util.List;

public class StartGameFailed extends ValidationFailure {

	public StartGameFailed(String errorMessage, List<String> tags) {
		super(errorMessage, tags);
	}
}
