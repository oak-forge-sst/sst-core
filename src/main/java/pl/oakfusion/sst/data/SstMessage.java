package pl.oakfusion.sst.data;

import java.util.UUID;


public interface SstMessage {

	UUID getGameSessionUuid();

	UUID getPlayerUuid();
}
