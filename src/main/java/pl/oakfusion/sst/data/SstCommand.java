package pl.oakfusion.sst.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.oakfusion.data.message.Command;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public abstract class SstCommand extends Command implements SstMessage {
	private UUID gameSessionUuid;
	private UUID playerUuid;
	public SstCommand (UUID playerUuid){
		this.playerUuid = playerUuid;
	}
}
