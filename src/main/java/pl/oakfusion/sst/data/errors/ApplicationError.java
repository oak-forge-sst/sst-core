package pl.oakfusion.sst.data.errors;

import lombok.Getter;

import java.util.UUID;

@Getter
public class ApplicationError extends SstError {
	private String message;

	public ApplicationError(UUID gameSessionUuid, UUID playerUuid, String message) {
		super(gameSessionUuid, playerUuid);
		this.message = message;
	}
}
