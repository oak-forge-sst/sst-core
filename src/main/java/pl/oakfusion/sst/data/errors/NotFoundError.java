package pl.oakfusion.sst.data.errors;

import lombok.Getter;

import java.util.UUID;

@Getter
public class NotFoundError extends SstError {

	private String message = "Data not found";

	public NotFoundError(UUID gameSessionUuid, UUID playerUuid, String message) {
		super(gameSessionUuid, playerUuid);
		this.message = message;
	}

	public NotFoundError(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}

	@Override
	public String toString() {
		return "NotFoundError{" +
				"message='" + message + '\'' +
				'}';
	}
}
