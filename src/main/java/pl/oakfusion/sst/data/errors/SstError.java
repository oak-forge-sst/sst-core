package pl.oakfusion.sst.data.errors;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.oakfusion.data.message.Error;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public abstract class SstError extends Error {
	private UUID gameSessionUuid;
	private UUID playerUuid;
	public SstError (UUID playerUuid){
		this.playerUuid = playerUuid;
	}
}
