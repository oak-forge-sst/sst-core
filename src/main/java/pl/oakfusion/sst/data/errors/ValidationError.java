package pl.oakfusion.sst.data.errors;

import java.util.List;
import java.util.UUID;
import lombok.Getter;
import pl.oakfusion.sst.data.errors.SstError;
import pl.oakfusion.sst.data.validationfailures.ValidationFailure;

@Getter
public class ValidationError extends SstError {
	public final List<ValidationFailure> errorList;

	public ValidationError(UUID gameSessionUuid, UUID playerUuid, List<ValidationFailure> errorList) {
		super(gameSessionUuid, playerUuid);
		this.errorList = errorList;
	}

	@Override
	public String toString() {
		return "ValidationError{" +
				"errorList=" + errorList +
				", timestamp=" + timestamp +
				", authToken='" + authToken + '\'' +
				'}';
	}
}
