package pl.oakfusion.sst.data.messagegenerator;

public enum MessageType {
	WARNING,
	ALERT,
	ERROR,
	NOTIFICATION
}
