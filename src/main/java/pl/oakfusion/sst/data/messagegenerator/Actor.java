package pl.oakfusion.sst.data.messagegenerator;

public enum Actor {
	SPOCK,
	SCOTTY,
	SULU,
	CHEKOV,
	SYSTEM,
}
