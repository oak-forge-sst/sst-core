package pl.oakfusion.sst.data.messagegenerator;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SystemMessage {
	private Actor actor;
	private MessageKey messageKey;

}
