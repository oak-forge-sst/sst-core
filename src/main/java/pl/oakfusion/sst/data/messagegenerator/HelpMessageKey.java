package pl.oakfusion.sst.data.messagegenerator;

public enum HelpMessageKey {
	HELP_ABANDON,
	HELP_CAPTURE,
	HELP_CHART,
	HELP_CLOAK,
	HELP_COMPUTER,
	HELP_DAMAGE,
	HELP_DOCK,
	HELP_EMEXIT,
	HELP_ORBIT,
	HELP_ESTIMATE,
	HELP_FREEZE,
	HELP_HELP,
	HELP_IMPULSE,
	HELP_PROBE,
	HELP_CRYSTAL,
	HELP_LRSCAN,
	HELP_MAYDAY,
	HELP_MINE,
	HELP_MOVE,
	HELP_MSELECT,
	HELP_PHASERS,
	HELP_PLANET,
	HELP_QUIT,
	HELP_REPORT,
	HELP_REST,
	HELP_SAVE,
	HELP_SELECT,
	HELP_DESTRUCT,
	HELP_SHIELDS,
	HELP_SRSCAN,
	HELP_SHUTTLE,
	HELP_STATUS,
	HELP_TORPEDO,
	HELP_BEAM,
	HELP_TRANSPORT,
	HELP_WARP;
};
