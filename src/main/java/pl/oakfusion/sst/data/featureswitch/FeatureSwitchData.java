package pl.oakfusion.sst.data.featureswitch;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FeatureSwitchData {
	public boolean AUDIT_LOG;
}
