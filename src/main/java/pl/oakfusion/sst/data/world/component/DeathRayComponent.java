package pl.oakfusion.sst.data.world.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class DeathRayComponent extends Component {
	private final static int WHEN_DEATHRAY_BROKEN = 30;
	public DeathRayComponent(double maxHealth, double currentHealth, double repairRatio) {
		super(maxHealth, currentHealth, repairRatio);
	}

	public DeathRayComponent(double maxHealth, double repairRatio) {
		super(maxHealth, repairRatio);
	}
	public boolean isBroken() {
		return getCurrentHealth() <WHEN_DEATHRAY_BROKEN;

	}
	public void used(){setCurrentHealth(0);}

}
