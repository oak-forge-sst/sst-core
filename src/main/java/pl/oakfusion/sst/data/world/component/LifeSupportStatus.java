package pl.oakfusion.sst.data.world.component;

public enum LifeSupportStatus {

    ACTIVE,
    DAMAGED
}
