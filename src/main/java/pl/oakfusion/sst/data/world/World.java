package pl.oakfusion.sst.data.world;

import pl.oakfusion.sst.data.DTO.WorldScanDTO;
import pl.oakfusion.sst.data.world.component.Condition;
import pl.oakfusion.sst.data.world.gameobject.GameObject;
import pl.oakfusion.sst.data.parameter.ObjectType;
import pl.oakfusion.sst.data.util.Position;
import pl.oakfusion.sst.data.util.repository.GameObjectsRepository;
import pl.oakfusion.sst.data.util.repository.Grid;
import pl.oakfusion.sst.data.world.civilizations.Unit;
import pl.oakfusion.sst.data.world.gameobject.Rubbish;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkArgument;

public class World {
	private final int widthInQuadrants;
	private final int heightInQuadrants;
	private final int quadrantSize;

	private final int absoluteWidth;
	private final int absoluteHeight;
	private final StarDate starDate;
	private List<Rubbish> rubbishes = new ArrayList<>();

    private final GameObjectsRepository gameObjectsRepository;

    public World(int widthInQuadrants, int heightInQuadrants, int quadrantSize, StarDate starDate) {
        checkArgument(widthInQuadrants > 0, "World width must be positive.");
        checkArgument(heightInQuadrants > 0, "World height must be positive.");
        checkArgument(quadrantSize > 0, "Quadrant size must be positive.");
        this.widthInQuadrants = widthInQuadrants;
        this.heightInQuadrants = heightInQuadrants;
        this.quadrantSize = quadrantSize;
        this.starDate = starDate;
        this.absoluteWidth = widthInQuadrants * quadrantSize;
        this.absoluteHeight = heightInQuadrants * quadrantSize;
        this.gameObjectsRepository = new Grid(absoluteWidth, absoluteHeight, quadrantSize);
    }

    public int getWidthInQuadrants() {
        return widthInQuadrants;
    }

    public int getHeightInQuadrants() {
        return heightInQuadrants;
    }

	public int getAbsoluteWidth() {
		return absoluteWidth;
	}

	public int getAbsoluteHeight() {
		return absoluteHeight;
	}

	public int getQuadrantSize() {
		return quadrantSize;
	}

	public int sectorIndex(int x, int y) {
		return y * absoluteWidth + x;
	}

    public void add(GameObject gameObject) {
		gameObjectsRepository.add(gameObject);
    }

    public void remove(GameObject gameObject) {
        gameObjectsRepository.remove(gameObject);
    }

	public void updatePositionOf(GameObject gameObject) {
		remove(gameObject);
		add(gameObject);
	}

	public boolean checkIsWorld(int x, int y){
		return x>=0 && y>=0 && x<quadrantSize*widthInQuadrants && y<quadrantSize*heightInQuadrants;
	}

    public Optional<GameObject> getGameObject(int x, int y) {
        return gameObjectsRepository.get(x, y);
    }

	public String getCurrentStarDate() {
		return starDate.toString();
	}

	public void rest(double amount) {
		starDate.increment(amount);
	}

    public List<GameObject> getGameObjectsInRange(int x, int y, int range) {
        return gameObjectsRepository.getInRange(x, y, range);
    }

    public List<GameObject> getGameObjectsInLineRange(int startX, int startY, int endX, int endY, int range) {
        return gameObjectsRepository.getInLine(startX, startY, endX, endY, range);
    }

    public List<Position> findShortestPath(Position source, Position target, List<Unit> unitsSafeTMoveThrough){
    	return gameObjectsRepository.findShortestPath(source, target, unitsSafeTMoveThrough);
	}

	public List<Position> findShortestPathWarp(Position source, Position target){
    	return gameObjectsRepository.findPathInEmptyWorld(source, target);
	}

    public WorldScanDTO getWorldScanDTO(int positionX, int positionY, int scanRange,Unit activeUnit) {
        return new WorldScanDTO(widthInQuadrants, heightInQuadrants, quadrantSize, getGameObjectsInRange(positionX, positionY, scanRange).stream().filter(gameObject ->
        	 !(gameObject.getObjectType().equals(ObjectType.UNIT) && ((Unit)gameObject).getCondition().equals(Condition.DOCKED) && !((Unit)gameObject).getUuid().equals(activeUnit.getUuid()))
		).collect(Collectors.toList()));
    }

    public boolean isObjectOfClassInRange(Class<? extends GameObject> type, int x, int y, int range) {
    	return getGameObjectsInRange(x,y,range).stream().anyMatch(o ->  o.getClass() == type);
	}

	public StarDate getStarDate() {
		return starDate;
	}

	public List<GameObject> getAllGameObjects(){
    	return gameObjectsRepository.getGameObjectsInWorld();
	}

	public void addRubbishes(List<Rubbish> rubbishList) {
    	rubbishes.addAll(rubbishList);
	}


	public List<Rubbish> getRubbishes() {
		return rubbishes;
	}
}
