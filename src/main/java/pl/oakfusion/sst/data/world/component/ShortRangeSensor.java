package pl.oakfusion.sst.data.world.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import static java.lang.Math.max;
@NoArgsConstructor
@Getter
@Setter
public class ShortRangeSensor extends Component {
	private int scanRange;

	public ShortRangeSensor(double maxHealth, double repairRatio) {
		this(maxHealth, repairRatio, 10);
	}

	public ShortRangeSensor(double maxHealth, double currentHealth, int scanRange) {
		super(maxHealth, currentHealth);
		this.scanRange = scanRange;

	}

	public int getScanRange() {
		return scanRange;
	}


	public int getViewRange(double levelOfFunctionality) {
		return (int) (max(levelOfFunctionality, calculateHealthFactor()) * scanRange);
	}
}
