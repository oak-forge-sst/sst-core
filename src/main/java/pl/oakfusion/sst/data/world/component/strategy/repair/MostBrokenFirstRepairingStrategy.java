package pl.oakfusion.sst.data.world.component.strategy.repair;

import pl.oakfusion.sst.data.world.component.Component;
import pl.oakfusion.sst.data.world.component.strategy.RepairingStrategy;

import java.util.Collection;
import java.util.Iterator;

import static java.util.Comparator.comparingDouble;

public class MostBrokenFirstRepairingStrategy implements RepairingStrategy {

	@Override
	public void repair(Collection<Component> components, double stardateToRepair) {

		Iterator<Component> sortedComponentsIterator = components.stream()
				.sorted(comparingDouble(Component::calculateHealthFactor))
				.iterator();

		while (stardateToRepair > 0 && sortedComponentsIterator.hasNext()) {
			Component component = sortedComponentsIterator.next();
			stardateToRepair = component.receiveRepairAndGetOverflow(stardateToRepair, getRepairTime(component));
		}
	}

	@Override
	public final double getRepairTime(Component component) {
		return (component.getMaxHealth() - component.getCurrentHealth()) / component.getRepairRatio();
	}
}
