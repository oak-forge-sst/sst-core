package pl.oakfusion.sst.data.world.component;

import lombok.Getter;
import pl.oakfusion.sst.data.world.component.*;

public enum ComponentId {
    SHIELDS(Shields.class),
    HULL(Hull.class),
    ENERGY_CONTROLLER(EnergyController.class),
    LIFE_SUPPORT(LifeSupport.class),
	SUBSPACE_RADIO(SubspaceRadio.class),
	LONG_RANGE_SENSOR(LongRangeSensor.class),
	SHORT_RANGE_SENSOR(ShortRangeSensor.class),
	COMPUTER(Computer.class),
	PHASERS_DEVICE(PhasersDevice.class),
	WARP_ENGINES(WarpEngines.class),
	TORPEDOES_DEVICE(TorpedoesDevice.class),
	TRACTOR_BEAM_DEVICE(TractorBeamDevice.class),
	ANTI_PHOTON_DEVICE(AntiPhotonDevice.class),
	ENERGY_CONTAINER(EnergyContainer.class),
	DEATH_RAY_COMPONENT(DeathRayComponent.class);

    @Getter
    private final Class<? extends Component> clazz;

	ComponentId(Class<? extends Component> clazz) {
		this.clazz = clazz;
	}
}
