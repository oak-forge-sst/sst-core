package pl.oakfusion.sst.data.world.component.strategy.pathFinding;

import com.github.javaparser.utils.Pair;
import pl.oakfusion.sst.data.util.Position;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.min;

public class EmptyWorldPathFinder {

	public List<Position> findPathInEmptyWorld(Position source, Position target) {
		int x1 = source.getX();
		int x2 = target.getX();
		int y1 = source.getY();
		int y2 = target.getY();
		var coords = getCoordsInTheDirectionOfTarget(source, target);
		ArrayList<Position> path = new ArrayList<>();
		while (x1 != x2 || y1 != y2){
			var nextPoint = chooseTheClosestPosition(new Position(x1+coords.a, y1),
					new Position(x1 + coords.a, y1 + coords.b),
					new Position(x1, y1 + coords.b), target);
			path.add(nextPoint);
			x1 = nextPoint.getX();
			y1 = nextPoint.getY();
		}
		return path;
	}

	private Position chooseTheClosestPosition(Position a, Position b, Position c, Position target){
		double adistance = calculateDistance(a, target);
		double bdistance = calculateDistance(b, target);
		double cdistance = calculateDistance(c, target);

		double minimumDistance = min(min(adistance, bdistance), cdistance);
		if (minimumDistance == adistance) return a;
		else if (minimumDistance == bdistance) return b;
		else if (minimumDistance == cdistance) return c;
		else return a;
	}

	private double calculateDistance(Position source, Position target){
		return Math.sqrt(Math.pow(source.getX() - target.getX(), 2) + Math.pow(source.getY() - target.getY(), 2));
	}

	private Pair<Integer, Integer> getCoordsInTheDirectionOfTarget(Position source, Position target){
		int x1 = source.getX();
		int y1 = source.getY();
		int x2 = target.getX();
		int y2 = target.getY();
		int fCoord = 0;
		int sCoord = 0;

		if (x1 == x2){
			fCoord = 0;
			if (y2 > y1) {
				sCoord = 1;
			}
			else {
				sCoord = -1;
			}
		}
		else if (y1 == y2) {
			sCoord = 0;
			if (x1 < x2){
				fCoord = 1;
			}
			else {
				fCoord = -1;
			}
		}

		if (x1 == x2 && y2 == y1){
			fCoord = 0;
			sCoord = 0;
		}
		else if (x1 < x2 && y1 < y2){
			fCoord = 1;
			sCoord = 1;
		}
		else if (x1 < x2 && y1 > y2){
			fCoord = 1;
			sCoord = -1;
		}
		else if (x1 > x2 && y1 < y2){
			fCoord = -1;
			sCoord = 1;
		}
		else if (x1 > x2 && y1 > y2){
			fCoord = -1;
			sCoord = -1;
		}
		return new Pair<Integer, Integer>(fCoord, sCoord);
	}
}
