package pl.oakfusion.sst.data.world.civilizations;

import pl.oakfusion.sst.data.configuration.UnitConfiguration;
import pl.oakfusion.sst.data.util.RandomGenerator;
import pl.oakfusion.sst.data.world.civilizations.Unit;

public interface UnitFromConfigFactory {
	Unit createUnit(UnitConfiguration unitConfiguration, RandomGenerator randomGenerator);
}
