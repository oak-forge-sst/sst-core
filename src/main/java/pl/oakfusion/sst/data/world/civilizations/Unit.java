package pl.oakfusion.sst.data.world.civilizations;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.world.gameobject.MovableGameObject;
import pl.oakfusion.sst.data.parameter.ObjectType;
import pl.oakfusion.sst.data.game.commands.GameCommand;
import pl.oakfusion.sst.data.util.Position;
import pl.oakfusion.sst.data.util.RandomGenerator;
import pl.oakfusion.sst.data.world.component.ComponentId;
import pl.oakfusion.sst.data.world.component.Condition;
import pl.oakfusion.sst.data.world.component.ShieldsStatus;
import pl.oakfusion.sst.data.world.component.CommandToComponentIdChanger;
import pl.oakfusion.sst.data.world.component.ComponentSystem;
import pl.oakfusion.sst.data.world.component.Shields;
import pl.oakfusion.sst.data.world.component.strategy.RepairingStrategy;
import pl.oakfusion.sst.data.world.component.strategy.damage.NormalHitReceivingStrategy;

import java.util.UUID;

@Getter
public abstract class Unit extends MovableGameObject {

	//Ranks should be ordered from lowest to highest rank
	public enum UnitRank{
		CADET, CAPTAIN, GENERAL
	}

	@Getter
	protected UnitRank unitRank;
	protected ComponentSystem componentSystem;
	protected UUID playerUUID;
	protected Condition condition;
	@Setter
	protected UUID uuid;
	@Setter
	protected String unitName;
	@Setter
	protected double actionPoints;


	public Unit(int x, int y, UUID playerUUID,
					  ComponentSystem componentSystem,
					  UnitSpecification unitSpecification, UnitRank unitRank) {
		super(x, y);
		this.playerUUID = playerUUID;
		this.objectType = ObjectType.UNIT;
		this.componentSystem = componentSystem;
		this.actionPoints = unitSpecification.getActionPoints();
		this.condition = unitSpecification.getCondition();
		this.unitRank = unitRank;
	}

	@Override
	public void moveTo(int absX, int absY) {
		changeCondition();
		super.moveTo(absX, absY);
	}

	@Override
	public void moveDelta(int dx, int dy) {
		changeCondition();
		super.moveDelta(dx, dy);
	}

	private void changeCondition() {
		if (condition == Condition.DOCKED) {
			componentSystem.undock();
			condition = Condition.UNKNOWN;
			updateCondition();
		}
	}

	protected void updateCondition() {
		if (componentSystem.getComponentSystemHealthStatus() > 75) {
			condition = Condition.GREEN;
		} else if ((componentSystem.getComponentSystemHealthStatus() <= 75) && (componentSystem.getComponentSystemHealthStatus() > 25)) {
			condition = Condition.YELLOW;
		} else {
			condition = Condition.RED;
		}
	}

	public Position getPosition(){return new Position(x, y);}

	public double getHealth() {
		return componentSystem.getComponentSystemHealthSum();
	}

	public double getHealthWithShields() {
		double healthWithShields = getHealth();
		if (componentSystem.getComponents().containsKey(ComponentId.SHIELDS)) {
			Shields shields = componentSystem.getComponent(ComponentId.SHIELDS);
			if (shields.getShieldsStatus().equals(ShieldsStatus.UP)) {
				healthWithShields += shields.getEnergyContainer().getEnergy();
			}
		}
		return healthWithShields;
	}

	public double getHealthAsFraction() {
		return componentSystem.getComponentSystemHealthSum() / componentSystem.getMaxComponentSystemHealthSum();
	}

	@Override
	public void receiveHit(double dmg) {
		componentSystem.receiveDamage(new NormalHitReceivingStrategy(new RandomGenerator()), dmg);
		updateCondition();
	}

	@Override
	public boolean isObjectAlive() {
		return componentSystem.getHull().getCurrentHealth() != 0;
	}

	public abstract UnitType getType();

	public Condition getCondition() {
		updateCondition();
		return condition;
	}

	public void setCondition(Condition condition) {
		this.condition = condition;
		updateCondition();
	}

	public void decreaseActionPoints(double usedActionPoints, RepairingStrategy repairStrategy) {
		componentSystem.repair(repairStrategy, usedActionPoints);
		this.actionPoints -= usedActionPoints;

	}

	public <C extends GameCommand> boolean canPerformCommand(Class<C> command){
		if (CommandToComponentIdChanger.containsCommand(command)) {
			ComponentId componentId = CommandToComponentIdChanger.getComponentIdByCommand(command).orElseThrow();

			return componentSystem.getComponent(componentId) != null;
		}
		return true;
	}

	public void dock() {
		componentSystem.dock();
		condition = Condition.DOCKED;
	}

}
