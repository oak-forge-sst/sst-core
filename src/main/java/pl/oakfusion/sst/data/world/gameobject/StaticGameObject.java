package pl.oakfusion.sst.data.world.gameobject;

public abstract class StaticGameObject extends GameObject {
    public StaticGameObject(int x, int y) {
        super(x, y);
    }
}
