package pl.oakfusion.sst.data.world.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class TractorBeamDevice extends Component implements StatusComponents {

	public TractorBeamDevice(double maxHealth, double repairRatio) {
		super(maxHealth, repairRatio);
	}
}
