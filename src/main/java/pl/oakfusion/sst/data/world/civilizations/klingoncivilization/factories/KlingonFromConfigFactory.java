package pl.oakfusion.sst.data.world.civilizations.klingoncivilization.factories;

import pl.oakfusion.sst.data.world.civilizations.UnitFromConfigFactory;
import pl.oakfusion.sst.data.configuration.UnitConfiguration;
import pl.oakfusion.sst.data.util.RandomGenerator;
import pl.oakfusion.sst.data.world.civilizations.klingoncivilization.Klingon;
import pl.oakfusion.sst.data.world.civilizations.UnitSpecification;
import pl.oakfusion.sst.data.world.component.ComponentSystem;

public class KlingonFromConfigFactory implements UnitFromConfigFactory {

	@Override
	public Klingon createUnit(UnitConfiguration unitConfiguration, RandomGenerator randomGenerator) {
		UnitSpecification unitSpecification = new UnitSpecification(
				unitConfiguration.getActionPoints(),
				unitConfiguration.getCondition()
		);
		Klingon unit = new Klingon(unitConfiguration.getPositionX(),
				unitConfiguration.getPositionY(),
				unitConfiguration.getPlayerUUID(),
				new ComponentSystem(unitConfiguration.getComponents()),
				unitSpecification);
		unit.setUnitName(unitConfiguration.getName());
		unit.setUuid(unitConfiguration.getUuid());
		return unit;
	}
}
