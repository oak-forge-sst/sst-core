package pl.oakfusion.sst.data.world.civilizations.klingoncivilization;

import pl.oakfusion.sst.data.world.civilizations.Unit;
import pl.oakfusion.sst.data.world.civilizations.UnitType;
import pl.oakfusion.sst.data.world.civilizations.UnitSpecification;
import pl.oakfusion.sst.data.world.component.ComponentSystem;

import java.util.UUID;

import static pl.oakfusion.sst.data.world.civilizations.UnitType.COMMANDER;
public class Commander extends Unit implements KlingonUnit {



	public Commander(int x, int y, UUID playerUUID, ComponentSystem componentSystem, UnitSpecification unitSpecification) {
		super(x, y, playerUUID, componentSystem, unitSpecification, UnitRank.CAPTAIN);
	}

	@Override
	public UnitType getType() {
		return COMMANDER;
	}
}
