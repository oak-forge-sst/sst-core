package pl.oakfusion.sst.data.world.civilizations;

public interface CivilizationFactory {
	Civilization createCivilization();

}
