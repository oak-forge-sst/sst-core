package pl.oakfusion.sst.data.world.civilizations.federationcivilization.factories;

import pl.oakfusion.sst.data.world.civilizations.UnitFromConfigFactory;
import pl.oakfusion.sst.data.configuration.UnitConfiguration;
import pl.oakfusion.sst.data.util.RandomGenerator;
import pl.oakfusion.sst.data.world.civilizations.federationcivilization.Enterprise;
import pl.oakfusion.sst.data.world.civilizations.UnitSpecification;
import pl.oakfusion.sst.data.world.component.ComponentSystem;

public class EnterpriseFromConfigFactory implements UnitFromConfigFactory {

public Enterprise createUnit(UnitConfiguration unitConfiguration, RandomGenerator randomGenerator) {
	UnitSpecification unitSpecification = new UnitSpecification(
			unitConfiguration.getActionPoints(),
			unitConfiguration.getCondition()
	);
	Enterprise unit = new Enterprise(unitConfiguration.getPositionX(),
			unitConfiguration.getPositionY(),
			unitConfiguration.getPlayerUUID(),
			new ComponentSystem(unitConfiguration.getComponents()),
			unitSpecification);
	unit.setUnitName(unitConfiguration.getName());
	unit.setUuid(unitConfiguration.getUuid());
	return unit;
}
}
