package pl.oakfusion.sst.data.world;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Locale;

import static com.google.common.base.Preconditions.checkArgument;
import static java.lang.String.format;
@NoArgsConstructor
@Getter
@Setter

public class StarDate {
    public final static double MIN_STARDATE = 2000.000;
    public final static double MAX_STARDATE = 5000.000;
    private double date;
    private int precision;
    private String formatStr;

    public StarDate(double date, int precision) {
        this.date = date;
        setPrecision(precision);
    }

    private void updatePrecisionFormat() {
        formatStr = format("%%.%df", precision);
    }

    public void increment (double toIncrement){
        checkArgument(toIncrement >= 0, "negative toIncrement: %s", toIncrement);
        date += toIncrement;
    }

    protected void setPrecision(int precision) {
        checkArgument(precision > 0, "negative precision: %s", precision);
        checkArgument(precision < 5, "too big precision: %s", precision);
        this.precision = precision;
        updatePrecisionFormat();
    }

    public String toString() {
        return format(Locale.US,formatStr, date);
    }

}
