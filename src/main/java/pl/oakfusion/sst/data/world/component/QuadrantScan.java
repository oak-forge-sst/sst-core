package pl.oakfusion.sst.data.world.component;

import lombok.Getter;
import pl.oakfusion.sst.data.world.gameobject.GameObject;
import pl.oakfusion.sst.data.initializer.WorldSpecification;
import pl.oakfusion.sst.data.world.gameobject.Starbase;
import pl.oakfusion.sst.data.world.civilizations.Unit;
import pl.oakfusion.sst.data.world.civilizations.federationcivilization.FederationUnit;
import pl.oakfusion.sst.data.world.civilizations.klingoncivilization.KlingonUnit;

import java.util.Collection;
import java.util.LinkedHashSet;

import static java.lang.Math.toIntExact;

@Getter
public class QuadrantScan {

    private Collection<GameObject> scannedObjects;
    private boolean scannedKlingons;
    private boolean scannedStarbases;
    private boolean scannedPlayer;

    public QuadrantScan() {
        this.scannedObjects = new LinkedHashSet<>();
        this.scannedKlingons = false;
        this.scannedStarbases = false;
        this.scannedPlayer = false;
    }


	public int getScannedKlingons(Unit unit) {
		if (scannedKlingons) {
			long klingonsCount = scannedObjects.stream()
					.filter(object -> KlingonUnit.class.isAssignableFrom(object.getClass()))
					.count();

			klingonsCount += scannedObjects.stream()
					.filter(object -> FederationUnit.class.isAssignableFrom(object.getClass()))
					.map(gameObject -> (Unit) gameObject)
					.filter(o -> o instanceof Unit && !o.getUuid().equals(unit.getUuid()))
					.count();

			return toIntExact(klingonsCount * WorldSpecification.OPPONENTUNIT);
		} else {
			return WorldSpecification.EMPTY;
		}
	}

    public int getScannedStarbases() {
        if (scannedStarbases) {
            long starbasesCount = scannedObjects.stream()
                                                .filter(object -> Starbase.class.isAssignableFrom(object.getClass()))
                                                .count();
            return toIntExact(starbasesCount * WorldSpecification.STARSBASE);
        } else {
            return WorldSpecification.EMPTY;
        }
    }

    public int getScannedPlayer(Unit unit) {
        if (scannedPlayer) {

            long starsCount = scannedObjects.stream()
                                            .filter(object -> FederationUnit.class.isAssignableFrom(object.getClass()))
											.map(gameObject -> (Unit) gameObject)
											.filter(o -> o instanceof Unit && o.getUuid().equals(unit.getUuid()))
					                        .count();

            return toIntExact(starsCount * WorldSpecification.PLAYERUNIT);
        } else {
            return WorldSpecification.EMPTY;
        }
    }

    public int getSupernovaThreat() {
        // TODO: 7/19/18 After creation of domain object containing supernova information fill this method
        return WorldSpecification.EMPTY;
    }

    public void setScannedObjects(Collection<GameObject> scannedObjects) {
        this.scannedObjects = scannedObjects;

        if (!scannedKlingons) {
            scannedObjects.stream()
                          .filter(object -> KlingonUnit.class.isAssignableFrom(object.getClass()))
                          .findFirst()
                          .ifPresent(object -> scannedKlingons = true);
			scannedObjects.stream()
					.filter(object -> FederationUnit.class.isAssignableFrom(object.getClass()))
					.findFirst()
					.ifPresent(object -> scannedKlingons = true);

        }
        if (!scannedStarbases) {
            scannedObjects.stream()
                          .filter(object -> Starbase.class.isAssignableFrom(object.getClass()))
                          .findFirst()
                          .ifPresent(object -> scannedStarbases = true);
        }
        if (!scannedPlayer) {
            scannedObjects.stream()
                          .filter(object -> FederationUnit.class.isAssignableFrom(object.getClass()))
                          .findFirst()
                          .ifPresent(object -> scannedPlayer = true);
        }
    }

}
