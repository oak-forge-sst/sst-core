package pl.oakfusion.sst.data.world.component;

import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Getter
public class TorpedoesDevice extends Component implements StatusComponents {

	private int maxTorpedoes;
	private int torpedoes;

	public TorpedoesDevice(double maxHealth, double repairRatio, int torpedoes, int maxTorpedoes) {
		super(maxHealth, repairRatio);
		validateTorpedoesAmount(torpedoes, maxTorpedoes);
		this.maxTorpedoes = maxTorpedoes;
		this.torpedoes = torpedoes;
	}

	public TorpedoesDevice(double maxHealth, double repairRatio, int torpedoes) {
		this(maxHealth, repairRatio, torpedoes, torpedoes);
	}

	public void resetTorpedoes() {
		this.torpedoes = maxTorpedoes;
	}
	public void setMaxTorpedoes(int maxTorpedoes) {
		validateTorpedoesAmount(maxTorpedoes);
		this.maxTorpedoes = maxTorpedoes;
	}

	public void setTorpedoes(int torpedoes) {
		validateTorpedoesAmount(torpedoes);
		this.torpedoes = torpedoes;
	}

	public void validateTorpedoesAmount(int... torpedoesCount) {
		for (int torpedoes : torpedoesCount) {
			if (torpedoes < 0) {
				throw new IllegalArgumentException("Torpedoes or Max Torpedoes can't be less than 0");
			}
		}
	}
}

