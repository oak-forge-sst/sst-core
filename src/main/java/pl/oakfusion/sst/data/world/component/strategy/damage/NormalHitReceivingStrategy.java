package pl.oakfusion.sst.data.world.component.strategy.damage;

import pl.oakfusion.sst.data.util.RandomGenerator;
import pl.oakfusion.sst.data.world.component.strategy.DamageReceivingStrategy;
import pl.oakfusion.sst.data.world.component.Component;
import pl.oakfusion.sst.data.world.component.Shields;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class NormalHitReceivingStrategy implements DamageReceivingStrategy {
	private RandomGenerator randomGenerator;

	public NormalHitReceivingStrategy(RandomGenerator randomGenerator) {
		this.randomGenerator = randomGenerator;
	}

	@Override
	public void applyDamage(Collection<Component> components, double damage) {
		if (componentsSumHealth(new ArrayList<>(components)) <= damage) {
			components.forEach(component -> component.receiveDamageAndGetOverflow(component.getCurrentHealth()));
		} else {
			double currentDamage = damage;
			List<Component> componentList = new ArrayList<>(components);
			int componentsListMaxIndex = componentList.size() - 1;
			while (currentDamage > 0 && !componentList.isEmpty()) {
				double damageForComponent = (currentDamage < 50) ? currentDamage : damage * randomGenerator.nextDoubleInRange(0, 1);
				Component component = componentList.get(randomGenerator.nextIntInRange(0, componentsListMaxIndex));
				currentDamage = currentDamage + component.receiveDamageAndGetOverflow(damageForComponent) - damageForComponent;
				if (component.getCurrentHealth() == 0) {
					componentList.remove(component);
					componentsListMaxIndex--;
				}
			}
		}
	}

	private double componentsSumHealth(List<Component> componentList) {
		return componentList.stream().map(Component::getCurrentHealth).reduce(0.0, Double::sum);
	}

	@Override
	public void applyDamage(Shields shields, Collection<Component> components, double damage) {
		double shieldDamage = (shields.getEnergyContainer().getCapacity() == 0) ? 0 : damage * shields.getEnergyContainer().getEnergy() / shields.getEnergyContainer().getCapacity();
		double componentDamage = damage - shieldDamage;
		componentDamage += shields.getEnergyContainer().damageShields(shieldDamage);

		applyDamage(components, componentDamage);
	}
}
