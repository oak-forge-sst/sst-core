package pl.oakfusion.sst.data.world.component.strategy.pathFinding;

import com.github.javaparser.utils.Pair;
import pl.oakfusion.sst.data.util.Position;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.min;

public class EmptyWorldPathFinderStrategy implements ShortestPathStrategy {
	// idea:
	// imagine source is the new centre of the coordinate system
	// think of quarters of the coordinate system as super vertices
	// I - (1,1), II - (-1,1), III - (-1,-1), IV - (1,-1)
	// every point finds itself inside one of the quarters
	// (corner cases are resolved by weak inequalities)
	// 1. find coords (a,b) of the quarter target is in
	// 2. locally choose the best one of three possible
	// 		cells: (target.x + a, target.y), (target.x, target.y + b),
	// 		(target.x + a, target.y + b) based on their euclidean distance from target
	// 3. repeat until target is reached

	// algorithm works based on a few assumptions: the world is empty,
	// every cell is walkable, the cost of going through a cell is not
	// taken into account, source & target are not outside the grid boundaries
	private List<Position> findPathInEmptyWorld(Position source, Position target) {
		if (source == target){
			return List.of();
		}

		int x1 = source.getX();
		int x2 = target.getX();
		int y1 = source.getY();
		int y2 = target.getY();
		var coords = getCoordsInTheDirectionOfTarget(source, target);
		ArrayList<Position> path = new ArrayList<>();

		while (x1 != x2 || y1 != y2){
			var nextPoint = chooseTheClosestPosition(new Position(x1+coords.a, y1),
					new Position(x1 + coords.a, y1 + coords.b),
					new Position(x1, y1 + coords.b), target);
			path.add(nextPoint);
			x1 = nextPoint.getX();
			y1 = nextPoint.getY();
		}

		return path;
	}

	private Position chooseTheClosestPosition(Position a, Position b, Position c, Position target){
		double aDistance = calculateDistance(a, target);
		double bDistance = calculateDistance(b, target);
		double cDistance = calculateDistance(c, target);

		double minimumDistance = min(min(aDistance, bDistance), cDistance);
		if (minimumDistance == aDistance) return a;
		else if (minimumDistance == bDistance) return b;
		else if (minimumDistance == cDistance) return c;
		else return a;
	}

	private double calculateDistance(Position source, Position target){
		return Math.sqrt(Math.pow(source.getX() - target.getX(), 2)
				+ Math.pow(source.getY() - target.getY(), 2));
	}

	private Pair<Integer, Integer> getCoordsInTheDirectionOfTarget(Position source, Position target){
		int x1 = source.getX();
		int y1 = source.getY();
		int x2 = target.getX();
		int y2 = target.getY();
		int fCoord = 0;
		int sCoord = 0;

		if (x1 == x2){
			fCoord = 0;
			if (y2 > y1) {
				sCoord = 1;
			}
			else {
				sCoord = -1;
			}
		}
		else if (y1 == y2) {
			sCoord = 0;
			if (x1 < x2){
				fCoord = 1;
			}
			else {
				fCoord = -1;
			}
		}

		if (x1 == x2 && y2 == y1){
			fCoord = 0;
			sCoord = 0;
		}
		else if (x1 < x2 && y1 < y2){
			fCoord = 1;
			sCoord = 1;
		}
		else if (x1 < x2 && y1 > y2){
			fCoord = 1;
			sCoord = -1;
		}
		else if (x1 > x2 && y1 < y2){
			fCoord = -1;
			sCoord = 1;
		}
		else if (x1 > x2 && y1 > y2){
			fCoord = -1;
			sCoord = -1;
		}
		return new Pair<>(fCoord, sCoord);
	}

	@Override
	public List<Position> findShortestPath(Integer[][] matrix, Position start, Position end) {
		return findPathInEmptyWorld(start, end);
	}

	private boolean isPointInsideTheWorld(Position point, Integer[][] matrix) {
		return point.getX() >= 0 && point.getX() < matrix[0].length &&
				point.getY() >= 0 && point.getY() < matrix.length;
	}
}
