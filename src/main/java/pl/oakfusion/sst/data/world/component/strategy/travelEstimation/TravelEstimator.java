package pl.oakfusion.sst.data.world.component.strategy.travelEstimation;

import pl.oakfusion.sst.data.game.MoveEstimation;
import pl.oakfusion.sst.data.util.Position;
import pl.oakfusion.sst.data.world.component.ComponentId;
import pl.oakfusion.sst.data.world.component.ShieldsStatus;
import pl.oakfusion.sst.data.world.World;
import pl.oakfusion.sst.data.world.civilizations.Unit;
import pl.oakfusion.sst.data.world.component.Computer;
import pl.oakfusion.sst.data.world.component.EnergyContainer;
import pl.oakfusion.sst.data.world.component.WarpEngines;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import static pl.oakfusion.sst.data.world.component.Computer.calculateActionPoints;

public class TravelEstimator {

	private static final int SHIELDS_INFLUENCE = 2;
	private static final int IMPULSE_WARP_FACTOR = 2;

	public MoveEstimation estimateImpulse(World world, Unit unit, List<Position> path, Position source, Position target) {
		return estimateTravel(world, unit, path, IMPULSE_WARP_FACTOR, path.size());
	}

	public MoveEstimation estimateMove(World world, Unit unit,
									   List<Position> path, Position source,
									   Position target) {
		var warpEngine = ((WarpEngines)unit.getComponentSystem()
									.getComponent(ComponentId.WARP_ENGINES));
		int warpFactor = warpEngine.getWarpFactor();
		int pathLength = path.size();

		return estimateTravel(world, unit, path, warpFactor, pathLength);

	}

	private MoveEstimation estimateTravel(World world, Unit unit,
										 List<Position> path, int warpFactor,
										 int pathLength) {
		ShieldsStatus shieldsStatus =  unit.getComponentSystem().getShields().getShieldsStatus();
		Computer computer = unit.getComponentSystem().getComputer();

		double energyCostToLocation = computer.computeEnergy(warpFactor, pathLength, shieldsStatus);
		double timeCostToLocation = Computer.computeTime(warpFactor, pathLength);
		double actionPointsCostToLocation = calculateActionPoints(timeCostToLocation);

		double energyLeft = ((EnergyContainer) unit.getComponentSystem()
				.getComponent(ComponentId.ENERGY_CONTAINER))
				.getEnergy() - energyCostToLocation;

		double arrivalToLocationStarDate = Double.parseDouble(world.getCurrentStarDate()) + timeCostToLocation;
		boolean isEnoughResources = computer.isEnoughResources(timeCostToLocation, energyCostToLocation, unit);

		return new MoveEstimation(path, warpFactor, timeCostToLocation, energyCostToLocation, energyLeft, (int)actionPointsCostToLocation,
				arrivalToLocationStarDate, isEnoughResources, shieldsStatus);
	}



	public MoveEstimation estimateForcedMove(World world, Unit unit, List<Position> path,
											 Position source, Position target,
											 List<Position> occupiedPositions) {
		int warpFactor = ((WarpEngines)unit.getComponentSystem()
				.getComponent(ComponentId.WARP_ENGINES)).getWarpFactor();

		return estimateForcedTravel(world, unit, path, source, target,
				occupiedPositions, warpFactor);
	}

	public MoveEstimation estimateForcedImpulse(World world, Unit unit, List<Position> path,
												Position source, Position target,
												List<Position> occupiedPositions) {
		return estimateForcedTravel(world, unit, path, source,
				target, occupiedPositions, IMPULSE_WARP_FACTOR);
	}

	private MoveEstimation estimateForcedTravel(World world, Unit unit, List<Position> path,
												Position source, Position target,
												List<Position> occupiedPositions, int warpFactor) {
		int shorterEmergencyPathLengthICELackOfEnergy = path.size();
		Computer computer = unit.getComponentSystem().getComputer();
		ShieldsStatus shieldsStatus = unit.getComponentSystem().getShields().getShieldsStatus();

		//TODO: take action points into account. shorten path as long as both energy and
		// action points are sufficient
		double time = Computer.computeTime(warpFactor, shorterEmergencyPathLengthICELackOfEnergy);
		double energy = computer.computeEnergy(warpFactor, shorterEmergencyPathLengthICELackOfEnergy, shieldsStatus);
		boolean isEnoughResources = computer.isEnoughResources(time, energy, unit);
		boolean collisionInTargetPoint = unitDetectedCollisionInTargetPoint(world, target);

		while (!isEnoughResources || collisionInTargetPoint
				|| unitDetectedPossibleCollisionWithPlayersOtherUnit(occupiedPositions, target)){
			path.remove(path.size()-1);
			if (path.isEmpty()) {
				target = source;
			}
			else {
				target = path.get(path.size()-1);
			}
			shorterEmergencyPathLengthICELackOfEnergy--;
			time = Computer.computeTime(warpFactor, shorterEmergencyPathLengthICELackOfEnergy);
			energy = computer.computeEnergy(warpFactor, shorterEmergencyPathLengthICELackOfEnergy, shieldsStatus);
			isEnoughResources = computer.isEnoughResources(time, energy, unit);
			collisionInTargetPoint = unitDetectedCollisionInTargetPoint(world, target);
		}
		return estimateTravel(world, unit, path, warpFactor, path.size());
	}

	private boolean unitDetectedCollisionInTargetPoint(World world, Position target){
		return world.getGameObject(target.getX(), target.getY()).isPresent();
	}

	private boolean unitDetectedPossibleCollisionWithPlayersOtherUnit(List<Position> occupiedPositions, Position target){
		AtomicBoolean result = new AtomicBoolean(false);
		occupiedPositions.forEach(pos -> {
			if (target.getX() == pos.getX() && target.getY() == pos.getY()){
				result.set(true);
			}
		});
		return result.get();
	}
}
