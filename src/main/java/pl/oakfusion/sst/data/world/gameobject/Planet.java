package pl.oakfusion.sst.data.world.gameobject;

import pl.oakfusion.sst.data.parameter.ObjectType;
import pl.oakfusion.sst.data.world.gameobject.StaticGameObject;

public class Planet extends StaticGameObject {
	public static final int PLANET_FORCE_FIELD_RANGE = 2;
	private boolean isAlive = true;
	public Planet(int x, int y) {
		super(x, y);
		objectType = ObjectType.PLANET;
	}

	@Override
	public void receiveHit(double dmg) {
		isAlive = false;
	}

	@Override
	public boolean isObjectAlive() {
		return isAlive;
	}
}
