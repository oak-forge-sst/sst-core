package pl.oakfusion.sst.data.world.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class PhasersDevice extends Component {
	public PhasersDevice(double maxHealth, double currentHealth, double repairRatio) {
		super(maxHealth, currentHealth, repairRatio);
	}

	public PhasersDevice(double maxHealth, double repairRatio) {
		super(maxHealth, repairRatio);
	}

}
