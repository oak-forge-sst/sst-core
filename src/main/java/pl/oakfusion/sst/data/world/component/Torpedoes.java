package pl.oakfusion.sst.data.world.component;

public class Torpedoes {
	private final int maxTorpedoes;
	private int torpedoes;

	public Torpedoes(int torpedoes, int maxTorpedoes) {
		this.maxTorpedoes = maxTorpedoes;
		this.torpedoes = torpedoes;
	}

	public Torpedoes(int torpedoes) {
		this(torpedoes, torpedoes);
	}

	protected void resetTorpedoes() {this.torpedoes = maxTorpedoes;}

	protected int getTorpedoes() {
		return torpedoes;
	}

	protected void setTorpedoes(int torpedoes) {
		this.torpedoes = torpedoes;
	}
}
