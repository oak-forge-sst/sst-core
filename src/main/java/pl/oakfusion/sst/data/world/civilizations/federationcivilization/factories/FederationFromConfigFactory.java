package pl.oakfusion.sst.data.world.civilizations.federationcivilization.factories;

import pl.oakfusion.sst.data.world.civilizations.Civilization;
import pl.oakfusion.sst.data.world.civilizations.CivilizationFromConfigFactory;
import pl.oakfusion.sst.data.world.civilizations.federationcivilization.Federation;
import pl.oakfusion.sst.data.world.civilizations.federationcivilization.Enterprise;
import pl.oakfusion.sst.data.world.civilizations.Unit;

import java.util.List;

public class FederationFromConfigFactory implements CivilizationFromConfigFactory {
	@Override
	public Civilization createCivilization(List<Unit> unitList) {
		return new Federation((Enterprise) unitList.get(0));
	}
}
