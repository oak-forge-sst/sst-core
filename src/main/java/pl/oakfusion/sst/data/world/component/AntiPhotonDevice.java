package pl.oakfusion.sst.data.world.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class AntiPhotonDevice extends Component implements StatusComponents {

	public AntiPhotonDevice(double currentHealth, double repairRatio) {
		super(currentHealth, repairRatio);
	}
}
