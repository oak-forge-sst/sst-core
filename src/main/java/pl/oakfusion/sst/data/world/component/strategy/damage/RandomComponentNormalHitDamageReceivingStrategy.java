package pl.oakfusion.sst.data.world.component.strategy.damage;

import pl.oakfusion.sst.data.util.RandomGenerator;
import pl.oakfusion.sst.data.world.component.Component;
import pl.oakfusion.sst.data.world.component.Shields;
import pl.oakfusion.sst.data.world.component.strategy.DamageReceivingStrategy;


import java.util.Collection;
import java.util.Iterator;

public class RandomComponentNormalHitDamageReceivingStrategy implements DamageReceivingStrategy {

    private final RandomGenerator random;

    public RandomComponentNormalHitDamageReceivingStrategy(RandomGenerator random) {
        this.random = random;
    }

    @Override
    public void applyDamage(Collection<Component> components, double damage) {
        if (damage > 0) {
            Iterator<Component> shuffledComponentsIterator = random.shuffle(components).iterator();

            while (damage > 0 && shuffledComponentsIterator.hasNext()) {
                damage = shuffledComponentsIterator.next().receiveDamageAndGetOverflow(damage);
            }
        }
    }

    @Override
    public void applyDamage(Shields shields, Collection<Component> components, double damage) {
        damage = shields.getEnergyContainer().damageShields(damage);
        applyDamage(components, damage);
    }
}
