package pl.oakfusion.sst.data.world.civilizations;

public enum UnitType {
	KLINGON, COMMANDER, SUPER_COMMANDER, ROMULAN, ENTERPRISE
}
