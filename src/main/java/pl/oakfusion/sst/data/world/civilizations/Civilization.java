package pl.oakfusion.sst.data.world.civilizations;

import pl.oakfusion.sst.data.world.civilizations.Unit;
import pl.oakfusion.sst.data.world.player.CivilizationType;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public interface Civilization {

	List<Unit> getActiveUnits();

	List<Unit> getUnits();

	default List<Unit> getLivingUnits() {
		return getUnits().stream().filter(Unit::isObjectAlive).collect(Collectors.toList());
	}

	default void setUnitsUuids(){
		List<Unit> units = getUnits();
		units.forEach(unit -> {
			UUID uuidForUnit = UUID.randomUUID();
			unit.setUuid(uuidForUnit);
		});
	}

	CivilizationType getCivilizationType();


	default Optional<Unit> getUnitByName(String name) {
		return getLivingUnits().stream().filter(u -> u.getUnitName().equalsIgnoreCase(name)).findFirst();
	}

	default Optional<Unit> getUnitByUuid(UUID unitUuid) {
		return getLivingUnits().stream().filter(u -> u.getUuid().equals(unitUuid)).findFirst();
	}

	default Optional<Unit> getActiveUnitByUuid(UUID unitUuid) {
		return getActiveUnits().stream().filter(u -> u.getUuid().equals(unitUuid)).findFirst();
	}

	void setActiveUnits(List<Unit> units);
}
