package pl.oakfusion.sst.data.world.gameobject;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.parameter.ObjectType;

import static java.lang.String.format;

@Getter
@Setter
@JsonDeserialize
public class GameObject {

	protected int x;
	protected int y;
	protected ObjectType objectType;

	public GameObject(int x, int y) {
		this.x = x;
		this.y = y;
	}


    public void receiveHit(double dmg) {
	}

	public void receiveEnergyHit(double dmg) {
	}

	public boolean isObjectAlive() {
		return true;
	}


	@Override
	public String toString() {
		return format("%s{%d, %d}", getClass(), x, y);
	}

}
