package pl.oakfusion.sst.data.world.player;

import java.util.UUID;

public class HumanPlayer extends Player {
	public HumanPlayer(CivilizationType civilizationType, String name, UUID uuid, boolean gameMaster) {
		super(civilizationType, name, uuid, gameMaster);
		playerType = PlayerType.HUMAN_PLAYER;
	}
}
