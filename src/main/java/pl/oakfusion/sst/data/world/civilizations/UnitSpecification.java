package pl.oakfusion.sst.data.world.civilizations;

import lombok.Getter;
import pl.oakfusion.sst.data.world.component.Condition;


@Getter
public class UnitSpecification {

	double actionPoints;
	Condition condition;

	public UnitSpecification(double actionPoints, Condition condition) {
		this.actionPoints = actionPoints;
		this.condition = condition;
	}
}
