package pl.oakfusion.sst.data.world.civilizations.federationcivilization;

import pl.oakfusion.sst.data.world.civilizations.Unit;
import pl.oakfusion.sst.data.world.civilizations.UnitSpecification;
import pl.oakfusion.sst.data.world.component.ComponentSystem;

import java.util.UUID;

public abstract class FederationUnit extends Unit {
	public FederationUnit(int x, int y, UUID playerUUID, ComponentSystem componentSystem, UnitSpecification unitSpecification, UnitRank unitRank) {
		super(x, y, playerUUID, componentSystem, unitSpecification, unitRank);
	}
}
