package pl.oakfusion.sst.data.world.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class EnergyContainer extends Component implements StatusComponents {

	private double capacity;
	private double energy;

	public EnergyContainer(double capacity, double energy, double maxHealth, double repairRatio) {
		super(maxHealth, repairRatio);
		this.capacity = capacity;
		this.energy = energy;
	}

	public EnergyContainer(double capacity, double maxHealth, double repairRatio) {
		this(capacity, capacity, maxHealth, repairRatio);
	}

	public void setEnergyToMax() {
		setEnergy(capacity);
	}

	public final double damageShields(double damage) {

		double nextEnergy = energy - damage;
		if (nextEnergy > 0) {
			energy = nextEnergy;
			return 0;
		} else {
			energy = 0;
			return -nextEnergy;
		}
	}
}
