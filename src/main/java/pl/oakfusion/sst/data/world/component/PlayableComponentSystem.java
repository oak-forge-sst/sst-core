package pl.oakfusion.sst.data.world.component;

import pl.oakfusion.sst.data.world.component.strategy.DamageReceivingStrategy;
import pl.oakfusion.sst.data.world.component.strategy.RepairingStrategy;

public interface PlayableComponentSystem {
	//default components
	Computer getComputer();
	Hull getHull();
	Shields getShields();
	EnergyController getEnergyController();
	LifeSupport getLifeSupport();
	SubspaceRadio getSubspaceRadio();
	LongRangeSensor getLongRangeSensor();
	ShortRangeSensor getShortRangeSensor();
	WarpEngines getWarpEngines();
	PhasersDevice getPhasersDevice();
	TorpedoesDevice getTorpedoesDevice();
	EnergyContainer getEnergyContainer();
	DeathRayComponent getDeathRayComponent();

	//info about whole system status
	void receiveDamage(DamageReceivingStrategy damageReceivingStrategy, double damage);
	void repair(RepairingStrategy repairingStrategy, double repair);
	double getComponentSystemHealthStatus();
	double getComponentSystemHealthSum();
	double getMaxComponentSystemHealthSum();
}
