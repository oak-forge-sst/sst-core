package pl.oakfusion.sst.data.world.civilizations.klingoncivilization;

import pl.oakfusion.sst.data.world.civilizations.Civilization;
import pl.oakfusion.sst.data.world.civilizations.Unit;
import pl.oakfusion.sst.data.world.player.CivilizationType;

import java.util.ArrayList;
import java.util.List;

public class KlingonCivilization implements Civilization {

	KlingonStrategy klingonStrategy;
	List<Klingon> klingonList;
	List<Commander> commandersList;
	List<SuperCommander> superCommanderList;
	List<Unit> activeUnits =  new ArrayList<>();


	public List<Commander> getCommanderslist() {
		return commandersList;
	}


	ArrayList<Unit> units = new ArrayList<>();


	public KlingonCivilization(KlingonStrategy klingonStrategy, List<Klingon> klingonList, List<Commander> commandersList, List<SuperCommander> superCommanderList) {
		this.klingonStrategy = klingonStrategy;
		this.klingonList = klingonList;
		this.commandersList = commandersList;
		this.superCommanderList = superCommanderList;

		if (!superCommanderList.isEmpty()) {
			activeUnits.add(superCommanderList.get(0));
		} else if (!commandersList.isEmpty()) {
			activeUnits.add(commandersList.get(0));
		} else {
			activeUnits.add(klingonList.get(0));
		}

		setUnits();
		setUnitsUuids();
	}


	private void setUnits() {
		units.addAll(klingonList);
		units.addAll(commandersList);
		units.addAll(superCommanderList);
	}


	public List<SuperCommander> getSuperCommanderList() {
		return superCommanderList;
	}


	@Override
	public List<Unit> getActiveUnits() {
		return activeUnits;
	}

	@Override
	public List<Unit> getUnits() {
		return units;
	}


	@Override
	public CivilizationType getCivilizationType() {
		return CivilizationType.KLINGON_EMPIRE;
	}

	public List<Klingon> getKlingonList() {
		return klingonList;
	}

	public void setActiveUnits(List<Unit> unit) {
		activeUnits = unit;
	}


}
