package pl.oakfusion.sst.data.world.component.strategy.pathFinding;

import lombok.AllArgsConstructor;
import pl.oakfusion.sst.data.util.Position;

import java.util.*;
import java.util.stream.Collectors;

// implementation of A* algorithm
public class AStarShortestPathStrategy implements ShortestPathStrategy {

	@AllArgsConstructor
	private class Node implements Comparable<Node> {
		public Node parent;
		public int x;
		public int y;
		public double g;
		public double h;

		public int compareTo(Node o){
			return (int)((this.g+this.h) - (o.g + o.h));
		}
	}

	private List<Node> open = new ArrayList<>();
	private List<Node> closed = new ArrayList<>();
	private List<Node> path = new ArrayList<>();
	private Integer[][] maze;
	private Node now;
	private int xstart;
	private int ystart;
	private int xend;
	private int yend;

	public List<Position> findShortestPath(Integer[][] matrix, Position start, Position end) {
		xstart = start.getX();
		ystart = start.getY();
		xend = end.getX();
		yend = end.getY();
		maze = matrix;
		now = new Node(null, xstart, ystart, 0, 0);

		closed.add(now);
		addNeighborsToOpenList();

		while (now.x != xend || now.y != yend){
			if (open.isEmpty()){
				return List.of();
			}

			now = open.get(0);
			open.remove(0);
			closed.add(now);
			addNeighborsToOpenList();

		}

		while (now.x != xstart || now.y != ystart){
			path.add(0, now);
			now = now.parent;
		}

		return path.stream().map(item -> new Position(item.x, item.y)).collect(Collectors.toList());
	}


	private void addNeighborsToOpenList(){
		Node node;
		List<Integer> possWays = List.of(-1, 0, 1);

		for (var x : possWays){
			for (var y : possWays){
				node = new Node(now, now.x + x, now.y + y, now.g, computeDistance(x,y));

				var nextY = now.y + y;
				var nextX = now.x +x;
				if ( (x != 0 || y != 0) && isPointInsideTheWorld(x, y)
						&& maze[nextX][nextY] != -1
						&& !findNeighborInList(open, node) && !findNeighborInList(closed, node)) //if not already done
				{
					node.g = node.parent.g + 1; //+1 cost
					node.g += maze[nextX][nextY]; //add movement cost for this square
					open.add(node);
				}
			}
		}
		Collections.sort(open);
	}

	private double computeDistance(int dx, int dy) {
		return Math.abs(this.now.x + dx - this.xend) + Math.abs(this.now.y + dy - this.yend);
	}

	private boolean findNeighborInList(List<Node> array, Node node) {
		return array.stream().anyMatch((n) -> (n.x == node.x && n.y == node.y));
	}

	private boolean isPointInsideTheWorld(int row, int col) {
		return this.now.x + row >= 0 && this.now.x + row < this.maze[0].length
				&& this.now.y + col >= 0 && this.now.y + col < this.maze.length;
	}

	private void printMatrix(Integer[][] matrix){
		for (Integer[] integers : maze) {
			for (int j = 0; j < maze[0].length; j++) {
				System.out.print(integers[j] + " ");
			}
			System.out.println();
		}
	}
}


