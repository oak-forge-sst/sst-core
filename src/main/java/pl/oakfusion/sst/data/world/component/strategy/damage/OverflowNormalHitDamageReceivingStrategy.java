package pl.oakfusion.sst.data.world.component.strategy.damage;

import pl.oakfusion.sst.data.util.RandomGenerator;
import pl.oakfusion.sst.data.world.component.Component;
import pl.oakfusion.sst.data.world.component.Shields;
import pl.oakfusion.sst.data.world.component.strategy.DamageReceivingStrategy;


import java.util.Collection;
import java.util.Iterator;
import java.util.stream.Collectors;

public class OverflowNormalHitDamageReceivingStrategy implements DamageReceivingStrategy {

    private final RandomGenerator random;

    public OverflowNormalHitDamageReceivingStrategy(RandomGenerator random) {
        this.random = random;
    }

    @Override
    public void applyDamage(Collection<Component> components, double damage) {
        if (damage > 0) {
        	Collection<Component> componentsNotIncludingShields = components.stream().filter(component -> !Shields.class.isAssignableFrom(component.getClass())).collect(Collectors.toList());
            Iterator<Component> shuffledComponentsIterator = random.shuffle(componentsNotIncludingShields).iterator();

            while (damage > 0 && shuffledComponentsIterator.hasNext()) {
                damage = shuffledComponentsIterator.next().receiveDamageAndGetOverflow(damage);
            }
        }
    }

    @Override
    public void applyDamage(Shields shields, Collection<Component> components, double damage) {
        damage = shields.getEnergyContainer().damageShields(damage);
        applyDamage(components, damage);
    }
}
