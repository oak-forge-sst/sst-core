package pl.oakfusion.sst.data.world.player;

public enum PlayerType {
	AI_PLAYER,
	HUMAN_PLAYER
}
