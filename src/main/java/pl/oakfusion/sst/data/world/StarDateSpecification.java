package pl.oakfusion.sst.data.world;

public class StarDateSpecification {
	public static final double ACTION_POINTS = 10;
	public static final double STARDATES_IN_ONE_TURN = 0.3;
	public static final double UPDATE_STARDATES_AFTER_TURN = 10;
}
