package pl.oakfusion.sst.data.world.component.strategy.pathFinding;

import pl.oakfusion.sst.data.Strategy;
import pl.oakfusion.sst.data.util.Position;
import pl.oakfusion.sst.data.util.repository.Node;

import java.util.*;
import java.util.stream.Collectors;

public class DijkstraShortestPathStrategy implements ShortestPathStrategy {

	/*
	* Currently not needed in application, leaving for future reference.
	 * This algorithm does not take into account the cost of going through the cell,
	 * it is pretty much 0-1 - either you are allowed to go or you are not.
	 * It has been working on boolean [][] matrix, changed to Integer[][] due to
	 * ShortestPathStrategy contract
	 * */
	public List<Position> shortestPath(Integer[][] map, Position start, Position end)
	{

		int[][] distances = new int[map.length][];
		for (int i = 0; i < map.length; i++) {
			distances[i] = new int[map[i].length];
			Arrays.fill(distances[i], Integer.MAX_VALUE);
		}

		int distance = 0;
		List<Position> currentCells = Arrays.asList(start);

		while (distances[end.getX()][ end.getY()] == Integer.MAX_VALUE
				&& !currentCells.isEmpty() ){
			List<Position> nextCells = new ArrayList<>();

			for (Position cell : currentCells) {
				if (distances[cell.getX()][cell.getY()] == Integer.MAX_VALUE
						&& map[cell.getX()][cell.getY()] != -1) {
					distances[cell.getX()][cell.getY()] = distance;
					addNeighbors(cell, nextCells,
							map.length, map[0].length);
				}
			}

			currentCells = nextCells;
			distance++;


		}

		Stack<Position> path = new Stack<>();

		if (distances[end.getX()][end.getY()] < Integer.MAX_VALUE) {
			for (int d = distances[end.getX()][end.getY()]-1; d >= 0; d--) {
				end = getNeighbor(end, d, distances);
				path.push(end);
			}
		}
		Collections.reverse(path);
		return new ArrayList<>(path);
	}

	private void addNeighbors(Position cell, List<Position> list,
									 int maxRow, int maxCol) {
		int[][] ds = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}, {1,1}, {-1,-1}, {-1,1}, {1,-1}};
		for (int[] d : ds) {
			int row = cell.getX() + d[0];
			int col = cell.getY() + d[1];
			if (isValid(row, col, maxRow, maxCol))
				list.add(new Position(row, col));
		}
	}

	private Position getNeighbor(Position cell, int distance, int[][] distances) {
		int[][] ds = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}, {1,1}, {-1,-1}, {-1,1}, {1,-1}};
		for (int[] d : ds) {
			int row = cell.getX() + d[0];
			int col = cell.getY() + d[1];
			if (isValid(row, col, distances.length, distances[0].length)
					&& distances[row][col] == distance)
				return new Position(row, col);
		}
		return null;
	}

	private boolean isValid(int row, int col, int maxRow, int maxCol) {
		return row >= 0 && row < maxRow && col >= 0 && col < maxCol;
	}

	@Override
	public List<Position> findShortestPath(Integer[][] matrix, Position start, Position end) {
		return shortestPath(matrix, start, end);
	}
}

