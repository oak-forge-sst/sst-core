package pl.oakfusion.sst.data.world.component;

import pl.oakfusion.sst.data.game.commands.GameCommand;
import pl.oakfusion.sst.data.game.commands.deathRay.DeathRay;
import pl.oakfusion.sst.data.game.commands.mayday.Mayday;
import pl.oakfusion.sst.data.game.commands.move.Move;
import pl.oakfusion.sst.data.game.commands.phasers.AskForOpponentsInRange;
import pl.oakfusion.sst.data.game.commands.phasers.Phasers;
import pl.oakfusion.sst.data.game.commands.shields.ChangeShieldsStatus;
import pl.oakfusion.sst.data.game.commands.shields.ShieldsCommand;
import pl.oakfusion.sst.data.game.commands.shields.TransferShieldsEnergy;
import pl.oakfusion.sst.data.game.commands.torpedo.Torpedo;
import pl.oakfusion.sst.data.game.commands.tractorbeam.TractorBeam;
import pl.oakfusion.sst.data.game.commands.warp.ChangeWarpFactor;

import java.util.Map;
import java.util.Optional;

import static java.util.Map.entry;

public class CommandToComponentIdChanger {

	private static final Map<Class<? extends GameCommand>, ComponentId> commandToComponentIdMap = Map.ofEntries(
			entry(ChangeShieldsStatus.class, 		ComponentId.SHIELDS),
			entry(ShieldsCommand.class, 			ComponentId.SHIELDS),
			entry(TransferShieldsEnergy.class, 		ComponentId.SHIELDS),
			entry(Torpedo.class, 					ComponentId.TORPEDOES_DEVICE),
			entry(TractorBeam.class, 				ComponentId.TRACTOR_BEAM_DEVICE),
			entry(ChangeWarpFactor.class, 			ComponentId.WARP_ENGINES),
			entry(Move.class,						ComponentId.WARP_ENGINES),
			entry(AskForOpponentsInRange.class, 	ComponentId.PHASERS_DEVICE),
			entry(Phasers.class, 					ComponentId.PHASERS_DEVICE),
			entry(DeathRay.class, 					ComponentId.DEATH_RAY_COMPONENT),
			entry(Mayday.class,						ComponentId.SUBSPACE_RADIO)
	);

	public static <C extends GameCommand> Optional<ComponentId> getComponentIdByCommand(Class<C> command){
		return Optional.ofNullable(commandToComponentIdMap.get(command));
	}

	public static <C extends GameCommand> boolean containsCommand(Class<C> command){
		return commandToComponentIdMap.containsKey(command);
	}
}
