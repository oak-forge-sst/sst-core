package pl.oakfusion.sst.data.world.component;

import lombok.NoArgsConstructor;
import pl.oakfusion.sst.data.world.component.Component;
import pl.oakfusion.sst.data.world.component.StatusComponents;
@NoArgsConstructor
public class Hull extends Component implements StatusComponents {

    public Hull(double maxHealth, double repairRatio) {
        super(maxHealth, repairRatio);
    }
}
