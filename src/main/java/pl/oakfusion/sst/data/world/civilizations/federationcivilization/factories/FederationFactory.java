package pl.oakfusion.sst.data.world.civilizations.federationcivilization.factories;

import pl.oakfusion.sst.core.generator.GameObjectSpawner;
import pl.oakfusion.sst.data.world.civilizations.Civilization;
import pl.oakfusion.sst.data.world.civilizations.AbstractUnitFactory;
import pl.oakfusion.sst.data.world.civilizations.CivilizationFactory;
import pl.oakfusion.sst.data.world.civilizations.federationcivilization.Federation;

import java.util.UUID;

public class FederationFactory implements CivilizationFactory {
	private final GameObjectSpawner gameObjectSpawner;
	private final UUID playerUuid;
	private final AbstractUnitFactory unitFactory;

	public FederationFactory(UUID playerUuid, GameObjectSpawner gameObjectSpawner, AbstractUnitFactory unitFactory) {
		this.playerUuid = playerUuid;
		this.gameObjectSpawner = gameObjectSpawner;
		this.unitFactory = unitFactory;
	}

	@Override
	public Civilization createCivilization() {
		int UNITS_FORCE_FIELD_RANGE = 0;
		return new Federation(gameObjectSpawner
				.spawnObjectInWorld(unitFactory.createEnterprise(playerUuid), UNITS_FORCE_FIELD_RANGE));
	}

}
