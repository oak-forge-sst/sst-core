package pl.oakfusion.sst.data.world.gameobject;

import pl.oakfusion.sst.data.parameter.ObjectType;
import pl.oakfusion.sst.data.world.gameobject.StaticGameObject;

public class BlackHole extends StaticGameObject {
	public static final int BLACK_HOLE_FORCE_FIELD_RANGE = 2;
    public BlackHole(int x, int y) {
        super(x, y);
        objectType = ObjectType.BLACK_HOLE;
    }
}
