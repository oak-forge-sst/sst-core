package pl.oakfusion.sst.data.world.civilizations.klingoncivilization.factories;

import lombok.Getter;
import pl.oakfusion.sst.data.initializer.WorldGeneratorCache;
import pl.oakfusion.sst.data.util.RandomGenerator;
import pl.oakfusion.sst.data.world.civilizations.klingoncivilization.Commander;
import pl.oakfusion.sst.data.world.player.KlingonsNames;

import java.util.List;
import java.util.UUID;

@Getter

public class KlingonFactory {
	private List<Commander> commanders;
	private final int quantity;
	private final WorldGeneratorCache worldGeneratorCache;
	private final RandomGenerator randomGenerator;
	private UUID playerUuid;
	KlingonsNames klingonsNames = new KlingonsNames();
	List<String> names = klingonsNames.newName();
	int nextName;


	public KlingonFactory(WorldGeneratorCache worldGeneratorCache, int quantity, List<Commander> commanders, RandomGenerator randomGenerator, UUID playerUuid) {
		this.commanders = commanders;
		this.quantity = quantity;
		this.worldGeneratorCache = worldGeneratorCache;
		this.randomGenerator = randomGenerator;
		this.playerUuid = playerUuid;

	}

	/*public List<Klingon> spawnObjects() {
		List<Klingon> klingonList = new ArrayList<>();
		int klingonsLeft = quantity;

		for (Commander commander : commanders) {
			List<Position> emptyAroundCommander = worldGeneratorCache.getEmptySurroundingInRange(
					commander.getX(), commander.getY(), KlingonUnitsSpecification.KLINGONS_FORCE_FIELD, 7);
			int klingonsPerCommanderQuantity = Math.min(Math.min(3, klingonsLeft), emptyAroundCommander.size());

			for (int i = 0; i < klingonsPerCommanderQuantity; i++) {
				int index = randomGenerator.nextInt(emptyAroundCommander.size());
				Position klingonPosition = emptyAroundCommander.get(index);
				emptyAroundCommander.remove(index);
				nextName++;
				Klingon klingon = new Klingon(klingonPosition.getX(), klingonPosition.getY(), playerUuid, randomGenerator, new UnitSpecification(UnitSpecification.KLINGON_MESSAGES, ComponentSystemSpecification.KLINGON_REGULAR_SPECIFICATION));
				worldGeneratorCache.put(klingonPosition.getX(), klingonPosition.getY(), KlingonUnitsSpecification.KLINGONS_FORCE_FIELD, klingon);
				klingonList.add(klingon);
			}
			klingonsLeft -= klingonsPerCommanderQuantity;

		}

		if (klingonsLeft != 0) {
			nextName++;
			klingonList.addAll(new RandomSpawnGameObjectFactory<>(klingonsLeft, worldGeneratorCache, randomGenerator, (x, y) -> new Klingon(x, y, playerUuid, randomGenerator, new UnitSpecification(UnitSpecification.KLINGON_MESSAGES, ComponentSystemSpecification.KLINGON_REGULAR_SPECIFICATION))).spawnObjects());
		}
		return klingonList;
	}*/

}
