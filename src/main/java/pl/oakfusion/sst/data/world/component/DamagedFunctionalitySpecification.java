package pl.oakfusion.sst.data.world.component;

public class DamagedFunctionalitySpecification {
	public static double DEFAULT_LEVEL_OF_FUNCTIONALITY = 50;
	public static double DEFAULT_PENALTY_FOR_COMPONENT = 0.1;
}
