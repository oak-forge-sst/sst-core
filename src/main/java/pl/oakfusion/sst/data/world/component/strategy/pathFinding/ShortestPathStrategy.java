package pl.oakfusion.sst.data.world.component.strategy.pathFinding;

import pl.oakfusion.sst.data.util.Position;

import java.util.List;

/**
 * Different strategies of finding the shortest path on a grid.
 * */
//-1 = not allowed, other than -1: cost of travel
public interface ShortestPathStrategy {
	/**
	 * Return a list of positions representing the shortest path on a grid.
	 * A path is the shortest one if if the cost of going through it is the lowest
	 * In case the path does not exist or the source equals the target, returns an empty list.
	 * @param matrix 	an array of integers - (-1) is reserved for blockade,
	 *               	numbers greater than (0) represent costs of going through a cell
	 * @param start 	starting point (x,y)
	 * @param end 	   	finishing point (x,y)
	 * @return 			a list of Position object excluding starting and including
	 * 					finishing Position
	 * */
	List<Position> findShortestPath(Integer[][] matrix, Position start, Position end);
}
