package pl.oakfusion.sst.data.world.component;

public enum ComponentCondition {

	FUNCTIONAL,
	DAMAGED,
	NOT_FUNCTIONAL
}

