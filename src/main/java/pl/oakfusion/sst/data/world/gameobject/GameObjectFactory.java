package pl.oakfusion.sst.data.world.gameobject;

@FunctionalInterface
public interface GameObjectFactory<G extends GameObject> {
	G create(int x, int y);
}
