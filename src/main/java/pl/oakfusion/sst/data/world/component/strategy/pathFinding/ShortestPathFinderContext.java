package pl.oakfusion.sst.data.world.component.strategy.pathFinding;

import pl.oakfusion.sst.data.util.Position;

import java.util.List;

public class ShortestPathFinderContext {
	private ShortestPathStrategy strategy;

	public void setStrategy(ShortestPathStrategy strategy){
		this.strategy = strategy;
	}

	public List<Position> executeStrategy(Integer[][] matrix, Position source, Position end){
		return strategy.findShortestPath(matrix, source, end);
	}
}
