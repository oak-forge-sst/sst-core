package pl.oakfusion.sst.data.world.component;

import lombok.NoArgsConstructor;
import pl.oakfusion.sst.data.world.component.Component;
@NoArgsConstructor
public class SubspaceRadio extends Component {

	public SubspaceRadio(double maxHealth, double repairRatio) {
		super(maxHealth, repairRatio);
	}

	public boolean isBroken() {
		return getCurrentHealth() == 0;
	}
}
