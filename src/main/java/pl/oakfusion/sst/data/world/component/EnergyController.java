package pl.oakfusion.sst.data.world.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import static java.lang.Math.min;
import static pl.oakfusion.sst.data.util.MathUtils.min;
@NoArgsConstructor
@Getter
@Setter
public class EnergyController extends Component implements StatusComponents {
	public EnergyController(double maxHealth, double repairRatio) {
		super(maxHealth, repairRatio);
	}

	public double transferEnergy(EnergyContainer source, EnergyContainer destination, double energy) {
		double DESTROYED_ENERGY_LOSS_FACTOR = calculateHealthFactor();
		double energyToTransfer = min(energy, source.getEnergy(), destination.getCapacity() - destination.getEnergy()).orElse(energy);
		energyToTransfer = energyToTransfer * DESTROYED_ENERGY_LOSS_FACTOR;
		source.setEnergy(source.getEnergy() - energyToTransfer);
		destination.setEnergy(min(destination.getCapacity(), destination.getEnergy() + energyToTransfer));
		return energyToTransfer;
	}
}
