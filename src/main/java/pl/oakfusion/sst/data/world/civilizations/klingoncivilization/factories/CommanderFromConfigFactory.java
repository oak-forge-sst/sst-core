package pl.oakfusion.sst.data.world.civilizations.klingoncivilization.factories;

import pl.oakfusion.sst.data.world.civilizations.UnitFromConfigFactory;
import pl.oakfusion.sst.data.configuration.UnitConfiguration;
import pl.oakfusion.sst.data.util.RandomGenerator;
import pl.oakfusion.sst.data.world.civilizations.klingoncivilization.Commander;
import pl.oakfusion.sst.data.world.civilizations.UnitSpecification;
import pl.oakfusion.sst.data.world.component.ComponentSystem;

public class CommanderFromConfigFactory implements UnitFromConfigFactory {

	@Override
	public Commander createUnit(UnitConfiguration unitConfiguration, RandomGenerator randomGenerator) {
		UnitSpecification unitSpecification = new UnitSpecification(
				unitConfiguration.getActionPoints(),
				unitConfiguration.getCondition()
		);
		Commander commander = new Commander(unitConfiguration.getPositionX(),
				unitConfiguration.getPositionY(),
				unitConfiguration.getPlayerUUID(),
				new ComponentSystem(unitConfiguration.getComponents()),
				unitSpecification);
		commander.setUnitName(unitConfiguration.getName());
		commander.setUuid(unitConfiguration.getUuid());
		return commander;
	}
}
