package pl.oakfusion.sst.data.world.player;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.world.civilizations.Civilization;

import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor

public abstract class Player {
	private UUID uuid;
	private boolean gameMaster = false;
	private Civilization civilization;
	private CivilizationType civilizationType;
	private String playerName;
	protected PlayerType playerType;
	private boolean usedTractorBeam;
	private boolean alive;
	private int turn = 0;
	private int score;

	public void incrementPlayerTurn() {
		turn++;
	}

	public void incrementScore(int score){
		this.score += score;
	}

	public Player(CivilizationType civilizationType, String name, UUID uuid, boolean gameMaster) {
		this(civilizationType, name, uuid);
		this.gameMaster = gameMaster;
	}

	public Player(CivilizationType civilizationType, String name, UUID uuid) {
		this.uuid = uuid;
		this.civilizationType = civilizationType;
		playerName = name;
		alive = true;
	}
//TODO: 2021-05-04 change method, remove setting active units, make setting components as not used
	/*public void setUpPlayerForNewTurn() {
		usedTractorBeam = false;
		if (!civilization.getActiveUnits().get(0).isObjectAlive()) {
			setActiveUnit();
		}
		getCivilization().getUnits().forEach(unit -> {
			unit.setActionPoints(StarDateSpecification.ACTION_POINTS);
		});
	}

	private void setActiveUnit() {
		if (!getCivilization().getLivingUnits().isEmpty()) {
			civilization.setActiveUnits(List.of(getCivilization().getLivingUnits().get(0)));
		}
	}*/

}
