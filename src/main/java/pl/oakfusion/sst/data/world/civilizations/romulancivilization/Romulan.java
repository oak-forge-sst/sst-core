package pl.oakfusion.sst.data.world.civilizations.romulancivilization;


import pl.oakfusion.sst.data.world.civilizations.Unit;
import pl.oakfusion.sst.data.world.civilizations.UnitType;
import pl.oakfusion.sst.data.world.civilizations.UnitSpecification;
import pl.oakfusion.sst.data.world.component.ComponentSystem;

import java.util.UUID;

import static pl.oakfusion.sst.data.world.civilizations.UnitType.ROMULAN;

public class Romulan extends Unit {



	public Romulan(int x, int y, UUID playerUUID, ComponentSystem componentSystem, UnitSpecification unitSpecification) {
		super(x, y, playerUUID, componentSystem, unitSpecification, UnitRank.GENERAL);
	}

	public void setUnitRank(UnitRank unitRank){
		this.unitRank = unitRank;
	}

	@Override
	public UnitType getType() {
		return ROMULAN;
	}
}
