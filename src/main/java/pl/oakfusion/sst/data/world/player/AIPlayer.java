package pl.oakfusion.sst.data.world.player;

import java.util.UUID;

public class AIPlayer extends Player {

	public AIPlayer(CivilizationType civilizationType, String name, UUID uuid) {
		super(civilizationType, name, uuid);
		playerType = PlayerType.AI_PLAYER;
	}
}
