package pl.oakfusion.sst.data.world.player;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class FederationsNames extends UnitName {
	private String name;

}


