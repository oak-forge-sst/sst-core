package pl.oakfusion.sst.data.world.component;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import pl.oakfusion.sst.data.util.RandomGenerator;

public abstract class DefaultMixIn {
	@JsonCreator
	public DefaultMixIn() {
	}
	@JsonIgnore
	public abstract boolean isComputerDown();
	@JsonIgnore
	public abstract int getMaxAmountTorpedoesToShot();
	@JsonIgnore
	public abstract boolean isBroken();
	@JsonIgnore
	@JsonProperty("randomGenerator")
	protected RandomGenerator randomGenerator;
}
