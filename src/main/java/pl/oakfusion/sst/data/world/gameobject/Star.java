package pl.oakfusion.sst.data.world.gameobject;

import pl.oakfusion.sst.data.parameter.ObjectType;
import pl.oakfusion.sst.data.world.gameobject.StaticGameObject;

public class Star extends StaticGameObject {
	public static final int STAR_FORCE_FIELD_RANGE = 1;

	private boolean isAlive = true;
    public Star(int x, int y) {
        super(x, y);
        objectType = ObjectType.STAR;
    }
	@Override
	public void receiveHit(double dmg) { isAlive = false; }

	@Override
	public boolean isObjectAlive() { return isAlive; }

}
