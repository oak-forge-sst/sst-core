package pl.oakfusion.sst.data.world.civilizations.klingoncivilization.factories;

import pl.oakfusion.sst.core.generator.GameObjectSpawner;
import pl.oakfusion.sst.data.world.civilizations.Civilization;
import pl.oakfusion.sst.data.world.civilizations.AbstractUnitFactory;
import pl.oakfusion.sst.data.world.civilizations.CivilizationFactory;
import pl.oakfusion.sst.data.world.civilizations.Unit;
import pl.oakfusion.sst.data.world.civilizations.klingoncivilization.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class KlingonCivilizationFactory implements CivilizationFactory {
	private final KlingonSpecification klingonSpecification;
	private final GameObjectSpawner gameObjectSpawner;
	private final UUID playerUuid;
	private int UNITS_FORCE_FIELD_RANGE = 0;
	private final AbstractUnitFactory unitFactory;
	public String name;

	public KlingonCivilizationFactory(KlingonSpecification klingonSpecification, AbstractUnitFactory unitFactory,
									  UUID playerUuid, GameObjectSpawner gameObjectSpawner) {
		this.klingonSpecification = klingonSpecification;
		this.playerUuid = playerUuid;
		this.gameObjectSpawner = gameObjectSpawner;
		this.unitFactory = unitFactory;
	}
	@Override
	public Civilization createCivilization() {

		List<SuperCommander> superCommanderList = new ArrayList<>();

		for(int i = 0; i < klingonSpecification.getSuperCommanderQuantity(); i++){
			superCommanderList.add(gameObjectSpawner.spawnObjectInWorld(unitFactory.createSuperCommander(playerUuid), UNITS_FORCE_FIELD_RANGE));
		}

		List<Commander> commanderList = new ArrayList<>();

		for(int i = 0; i < klingonSpecification.getCommanderQuantity(); i++){
			commanderList.add(gameObjectSpawner.spawnObjectInWorld(unitFactory.createCommander(playerUuid), UNITS_FORCE_FIELD_RANGE));
		}

		List<Klingon> klingonList = new ArrayList<>();

		for(int i = 0; i < klingonSpecification.getKlingonQuantity(); i++){
			klingonList.add(gameObjectSpawner.spawnObjectInWorld(unitFactory.createKlingon(playerUuid), UNITS_FORCE_FIELD_RANGE));
		}

		List<Unit> units = Stream.of(klingonList, commanderList, superCommanderList).flatMap(Collection::stream).collect(Collectors.toList());
		for (Unit unit : units) {
			UnitsKlingonsNamesFactory.setName(unit);
		}
		return new KlingonCivilization(new KlingonStrategy(), klingonList, commanderList, superCommanderList);
	}


}
