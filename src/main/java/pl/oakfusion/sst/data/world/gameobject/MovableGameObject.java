package pl.oakfusion.sst.data.world.gameobject;

public abstract class MovableGameObject extends GameObject {
	public MovableGameObject(int x, int y) {
		super(x, y);
	}


	public void moveTo(int absX, int absY) {
		x = absX;
		y = absY;
	}

	public void moveDelta(int dx, int dy) {
		x += dx;
		y += dy;
	}
}
