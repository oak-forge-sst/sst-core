package pl.oakfusion.sst.data.world.component;


import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import pl.oakfusion.sst.data.exceptions.NotFoundException;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ComponentSystemDeserializer extends StdDeserializer<Map<ComponentId, Component>> {

	protected ComponentSystemDeserializer() {
		super(Map.class);
	}

	@Override
	public Map<ComponentId, Component> deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
		Map<ComponentId, Component> componentList = new HashMap<>();
		TreeNode node = p.readValueAsTree();
		Iterator<String> fieldNames = node.fieldNames();
		while(fieldNames.hasNext()) {
			String name = fieldNames.next();
			TreeNode componentNode = node.get(name);
			ComponentId componentId = Arrays.stream(ComponentId.values()).filter(id -> id.name().equals(name)).findFirst().orElseThrow(NotFoundException::new);
			try {
				componentList.put(componentId, p.getCodec().treeToValue(componentNode, componentId.getClazz()));
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
		}
		return componentList;
	}
}
