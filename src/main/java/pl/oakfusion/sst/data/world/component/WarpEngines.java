package pl.oakfusion.sst.data.world.component;

import lombok.Setter;

import static com.google.common.base.Preconditions.checkArgument;
import static java.lang.Math.max;

public class WarpEngines extends Component implements StatusComponents{
	public static int MAX_WARP_FACTOR = 10;
	public static int MIN_WARP_FACTOR = 1;
	/**Factor defining speed of movement with warp engines, possible values 1-10
	 */
	private int warpFactor;
	@Setter
	private boolean ifWarpFactorMutable;

	public WarpEngines(double maxHealth, double repairRatio, int warpFactor, boolean ifWarpFactorMutable) {
		super(maxHealth, repairRatio);
		checkArgument(warpFactor >= MIN_WARP_FACTOR && warpFactor <= MAX_WARP_FACTOR, "warp factor not in range [1,10]");
		this.warpFactor = warpFactor;
		this.ifWarpFactorMutable = ifWarpFactorMutable;
	}


	public int getMaxWarpFactor(double levelOfFunctionality) {
		return (int) Math.floor(max(calculateHealthFactor(), levelOfFunctionality)*MAX_WARP_FACTOR);
	}

	public int getWarpFactor() {
		return warpFactor;
	}

	public boolean isIfWarpFactorMutable() {
		return ifWarpFactorMutable;
	}

	public void setWarpFactor(int warpFactor) {
		if(isIfWarpFactorMutable()) {
			this.warpFactor = warpFactor;
		}
	}
}
