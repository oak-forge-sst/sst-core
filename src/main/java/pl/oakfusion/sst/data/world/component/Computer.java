package pl.oakfusion.sst.data.world.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.oakfusion.sst.data.world.gameobject.GameObject;
import pl.oakfusion.sst.data.game.MoveEstimation;
import pl.oakfusion.sst.data.util.Position;
import pl.oakfusion.sst.data.world.*;
import pl.oakfusion.sst.data.world.civilizations.Unit;
import pl.oakfusion.sst.data.world.component.strategy.travelEstimation.TravelEstimator;
import pl.oakfusion.sst.data.world.gameobject.Starbase;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class Computer extends Component {

	private static final int SHIELDS_INFLUENCE = 2;

	public Computer(double maxHealth, double repairRatio) {
		super(maxHealth, repairRatio);
	}

	public static double computeDistance(int x, int y) {
		return Math.sqrt(x * x + y * y);
	}

	public static double computeDistance(int x, int y, int eX, int eY) {
		int vectorX = x - eX;
		int vectorY = y - eY;
		return computeDistance(vectorX, vectorY);
	}

	public static double calculateActionPoints(double usedStarDates) {
		return usedStarDates * StarDateSpecification.ACTION_POINTS / StarDateSpecification.STARDATES_IN_ONE_TURN;
	}
	public static double computeStarDate(double actualActionPoints) {
		return (StarDateSpecification.STARDATES_IN_ONE_TURN * actualActionPoints) / StarDateSpecification.ACTION_POINTS;
	}

	public static double computeTime(int warpFactor, double distance) {
		if (warpFactor < WarpEngines.MIN_WARP_FACTOR || warpFactor > WarpEngines.MAX_WARP_FACTOR || distance < 0) {
			return -1;
		}
		double timeCostPerSector = calculateTimeCostPerSector(warpFactor);
		return timeCostPerSector * distance;
	}

	private static double calculateTimeCostPerSector(int warpFactor){
		double timeCostPerSector;
		switch (warpFactor) {
			case 1:
			case 2: {
				timeCostPerSector = StarDateSpecification.STARDATES_IN_ONE_TURN / warpFactor;
				break;
			}
			case 3:
			case 4: {
				timeCostPerSector = StarDateSpecification.STARDATES_IN_ONE_TURN / (warpFactor + 1);
				break;
			}
			default: {
				timeCostPerSector = 1.0 / (warpFactor * warpFactor);
				break;
			}
		}
		return timeCostPerSector;
	}

	public static double calculateDistanceUnitCanTravelWithLimitedActionPoints(int warpFactor, double actionPoints){
		return actionPoints * StarDateSpecification.STARDATES_IN_ONE_TURN /
				(StarDateSpecification.ACTION_POINTS * calculateTimeCostPerSector(warpFactor));
	}

	public static double calculateDistanceUnitCanTravelWithLimitedEnergy(int warpFactor, double energy){
		return energy * 10 / (warpFactor * warpFactor * warpFactor);
	}

	public double computeEnergy(int warpFactor, double distance, ShieldsStatus shieldsStatus) {
		if (warpFactor < WarpEngines.MIN_WARP_FACTOR || warpFactor > WarpEngines.MAX_WARP_FACTOR || distance < 0) {
			return -1;
		}
		double energyCostToLocation = (warpFactor * warpFactor * warpFactor) / 10.0 * distance;
		if (shieldsStatus.equals(ShieldsStatus.UP)) {
			energyCostToLocation *= SHIELDS_INFLUENCE;
		}
		return energyCostToLocation;
	}

	public boolean isEnoughResources(double timeCost, double energyCost, Unit e) {
		LifeSupport lifeSupport = e.getComponentSystem().getComponent(ComponentId.LIFE_SUPPORT);
		EnergyContainer energyContainer = e.getComponentSystem().getComponent(ComponentId.ENERGY_CONTAINER);
		if (lifeSupport.getLifeSupportStatus().equals(LifeSupportStatus.DAMAGED)) {
			return ((energyContainer.getEnergy() - energyCost >= 0) && (lifeSupport.getRemainingTime() - timeCost) >= 0);
		} else {
			return energyContainer.getEnergy() - energyCost >= 0;
		}
	}

	public double getDistanceToClosestStarbase(World world, int eX, int eY) {
		Starbase starbase = getClosestStarbase(world, eX, eY);
		return starbase == null ? Double.MAX_VALUE : computeDistance(eX, eY, starbase.getX(), starbase.getY());
	}

	public Starbase getClosestStarbase(World world, int eX, int eY) {
		double shortestDistance = Double.MAX_VALUE;
		Starbase starbase = null;
		List<GameObject> gameObjects = world.getGameObjectsInRange(1, 1, world.getAbsoluteHeight() + world.getAbsoluteWidth());
		for (GameObject object : gameObjects) {
			if (object.getClass().equals(Starbase.class)) {
				double distance = computeDistance(eX, eY, object.getX(), object.getY());

				if (shortestDistance > distance) {
					shortestDistance = distance;
					starbase = (Starbase) object;
				}
			}
		}

		return starbase;
	}

	public boolean isComputerDown() {
		return this.getComponentCondition().equals(ComponentCondition.DAMAGED);
	}

	public MoveEstimation estimateMove(World world, Unit unit, List<Position> path, Position source, Position target) {
		TravelEstimator estimator = new TravelEstimator();
		return estimator.estimateMove(world, unit, path, source, target);
	}

	public MoveEstimation estimateImpulseMove(World world, Unit unit, List<Position> path, Position source, Position target) {
		TravelEstimator estimator = new TravelEstimator();
		return estimator.estimateImpulse(world, unit, path, source, target);
	}

	public MoveEstimation estimateForcedMove(World world, Unit unit, List<Position> path,
											 Position source, Position target,
											 List<Position> occupiedPositions){
		TravelEstimator estimator = new TravelEstimator();
		return estimator.estimateForcedMove(world, unit, path, source, target, occupiedPositions);
	}

	public MoveEstimation estimateForcedImpulse(World world, Unit unit, List<Position> path,
												Position source, Position target,
												List<Position> occupiedPositions) {
		TravelEstimator estimator = new TravelEstimator();
		return estimator.estimateForcedImpulse(world, unit, path, source, target, occupiedPositions);
	}
}
