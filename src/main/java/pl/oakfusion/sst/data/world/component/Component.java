package pl.oakfusion.sst.data.world.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public abstract class Component {

	private static final double DEFAULT_REPAIR_RATIO = 15;
	public static final int PERCENT_MULTIPLIER = 100;
	private double maxHealth;
	@Getter
	@Setter
	private double currentHealth;
	private double repairRatio;
	private ComponentCondition componentCondition;
	private Object GameWarningEvent;

	protected Component(double maxHealth, double currentHealth, double repairRatio) {
		this.maxHealth = maxHealth;
		this.currentHealth = currentHealth;
		this.repairRatio = repairRatio;
		componentCondition = ComponentCondition.FUNCTIONAL;
	}


	public Component(double maxHealth, double repairRatio) {
		this(maxHealth, maxHealth, repairRatio);
	}

	public Component(double maxHealth) {
		this(maxHealth, maxHealth, DEFAULT_REPAIR_RATIO);
	}


	// TODO
	public final double receiveDamageAndGetOverflow(double damage) {
		double healthAfterDamageTaken = currentHealth - damage;
		boolean isEnterpriseAlive = healthAfterDamageTaken > 0;

		if (isEnterpriseAlive) {
			currentHealth = healthAfterDamageTaken;
			updateComponentCondition();
			return 0;
		} else {
			currentHealth = 0;
			updateComponentCondition();
			return -healthAfterDamageTaken;
		}
	}

	public final double receiveRepairAndGetOverflow(double stardatesToRepair, double repairTime) {
		double missingHealth = maxHealth - currentHealth;

		if (stardatesToRepair < repairTime) {
			currentHealth += (stardatesToRepair / repairTime) * missingHealth;
			updateComponentCondition();
			return 0;
		} else {
			currentHealth = maxHealth;
			updateComponentCondition();
			return stardatesToRepair - repairTime;
		}
	}

	public void fullRepair() {
		currentHealth = maxHealth;
		updateComponentCondition();
	}

	public final double getMaxHealth() {
		return maxHealth;
	}

	public final double getRepairRatio() {
		return repairRatio;
	}

	public final double calculateRepairTime() {
		return (maxHealth - currentHealth) / repairRatio;
	}

	public double calculateHealthPercentage() {
		return (currentHealth / maxHealth) * PERCENT_MULTIPLIER;
	}

	public double calculateHealthFactor() {
		return (currentHealth / maxHealth);
	}

	public final ComponentCondition getComponentCondition() {
		updateComponentCondition();
		return componentCondition;
	}

	private void updateComponentCondition() {
		if (calculateHealthPercentage() < DamagedFunctionalitySpecification.DEFAULT_LEVEL_OF_FUNCTIONALITY) {
			componentCondition = ComponentCondition.DAMAGED;
		} else {
			componentCondition = ComponentCondition.FUNCTIONAL;
		}
	}

	public final void setRepairRatio(double repairRatio) {
		this.repairRatio = repairRatio;
	}

	public final void resetRepairRatio() {
		setRepairRatio(DEFAULT_REPAIR_RATIO);
	}

}
