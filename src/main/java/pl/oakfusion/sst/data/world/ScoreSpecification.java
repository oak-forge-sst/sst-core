package pl.oakfusion.sst.data.world;


import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
public class ScoreSpecification {
	public Map<String, Integer> specification = new HashMap<>();

	public void setSpecification() {
		specification.put("MAYDAY", -45);
		specification.put("KLINGON", 10);
		specification.put("COMMANDER", 50);
		specification.put("SUPER_COMMANDER", 200);
		specification.put("ROMULAN", 20);
		specification.put("NOVICE", 100);
		specification.put("FAIR", 200);
		specification.put("GOOD", 300);
		specification.put("EXPERT", 400);
		specification.put("EMERITUS", 500);
		specification.put("WON", 500);
		specification.put("PLANET_INHABITED", -300);
		specification.put("PLANET_UNINHABITED", -10);
		specification.put("DIED", -200);
		specification.put("BASE", -100);
		specification.put("STAR", -5);
		specification.put("ENTERPRISE", 500);
		specification.put("CASUALTY", -1);
	}

}
