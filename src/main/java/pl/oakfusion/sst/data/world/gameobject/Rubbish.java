package pl.oakfusion.sst.data.world.gameobject;

import pl.oakfusion.sst.data.world.gameobject.StaticGameObject;

public class Rubbish extends StaticGameObject {
	private double timeForDelete = 0.9;
	private boolean isAlive = true;
	public Rubbish(int x, int y) {
		super(x, y);
	}

	@Override
	public void receiveHit(double dmg) { isAlive = false; }

	@Override
	public boolean isObjectAlive() { return isAlive; }

	public void deletTime(double time){
		timeForDelete -=time;
	}

	public double getTimeForDelete() {
		return timeForDelete;
	}
}
