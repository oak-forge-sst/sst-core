package pl.oakfusion.sst.data.world.civilizations.klingoncivilization.factories;

import pl.oakfusion.sst.data.world.civilizations.Civilization;
import pl.oakfusion.sst.data.world.civilizations.CivilizationFromConfigFactory;
import pl.oakfusion.sst.data.world.civilizations.Unit;
import pl.oakfusion.sst.data.world.civilizations.UnitType;
import pl.oakfusion.sst.data.world.civilizations.klingoncivilization.*;

import java.util.List;
import java.util.stream.Collectors;

public class KlingonCivilizationFromConfigFactory implements CivilizationFromConfigFactory {
	@Override
	public Civilization createCivilization(List<Unit> unitList) {

		return new KlingonCivilization(new KlingonStrategy(),
				unitList.stream().filter(u -> u.getType().equals(UnitType.KLINGON)).map(u -> (Klingon) u).collect(Collectors.toList()),
				unitList.stream().filter(u -> u.getType().equals(UnitType.COMMANDER)).map(u -> (Commander) u).collect(Collectors.toList()),
				unitList.stream().filter(u -> u.getType().equals(UnitType.SUPER_COMMANDER)).map(u -> (SuperCommander) u).collect(Collectors.toList()));
	}
}
