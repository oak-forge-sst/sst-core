package pl.oakfusion.sst.data.world.component;

public enum Condition {
    GREEN,
    YELLOW,
    RED,
    DOCKED,
	UNKNOWN
}
