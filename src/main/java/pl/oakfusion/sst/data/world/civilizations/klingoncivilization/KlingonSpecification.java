package pl.oakfusion.sst.data.world.civilizations.klingoncivilization;

import lombok.Builder;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
public class KlingonSpecification {
	private int klingonQuantity;
	private int commanderQuantity;
	private int superCommanderQuantity;

	public KlingonSpecification(int klingonQuantity, int commanderQuantity, int superCommanderQuantity) {
		this.klingonQuantity = klingonQuantity;
		this.commanderQuantity = commanderQuantity;
		this.superCommanderQuantity = superCommanderQuantity;
	}

	public int getQuantityKlingonUnits() {
		return klingonQuantity + commanderQuantity + superCommanderQuantity;
	}

	public int getKlingonQuantity() {
		return klingonQuantity;
	}

	public int getCommanderQuantity() {
		return commanderQuantity;
	}

	public int getSuperCommanderQuantity() {
		return superCommanderQuantity;
	}

}
