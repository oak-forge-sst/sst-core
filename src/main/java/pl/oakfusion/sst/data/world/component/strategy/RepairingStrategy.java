package pl.oakfusion.sst.data.world.component.strategy;

import pl.oakfusion.sst.data.world.component.Component;

import java.util.Collection;

public interface RepairingStrategy {

    void repair(Collection<Component> components, double repair);

    double getRepairTime(Component component);
}
