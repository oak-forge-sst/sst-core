package pl.oakfusion.sst.data.world.component;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LifeSupport extends Component implements StatusComponents {

	public static final int MAXIMUM_HEALTH_PERCENTAGE = 100;
	private LifeSupportStatus lifeSupportStatus;
	private double remainingTime;

	public LifeSupport() {
		this.lifeSupportStatus = LifeSupportStatus.ACTIVE;
	}

	public LifeSupport(final double maxHealth, final double repairRatio) {
		super(maxHealth, repairRatio);
		this.lifeSupportStatus = LifeSupportStatus.ACTIVE;
	}

	public LifeSupport(final double maxHealth, final double currentHealth, final double repairRatio) {
		super(maxHealth, currentHealth, repairRatio);
		this.lifeSupportStatus = LifeSupportStatus.ACTIVE;
	}

	//TODO Do obgadania
	protected void lifeReserve(final int crew) {
		if (crew == 0){
			throw  new IllegalArgumentException("Crew can't be equal to 0");
		}
		double container = 10000;
		double maxResourceLevel = 1000;
		double lifeSupportCondition = calculateHealthFactor();
		double currentResourceLevel = lifeSupportCondition * maxResourceLevel;
		double selfSufficiency = currentResourceLevel / crew;

		if (selfSufficiency >= 1) {
			remainingTime = (int) (container / (crew * selfSufficiency - crew));
		}
	}

	//TODO Uwzględnienie upływu czasu
	protected void checkStatus() {
		if (calculateHealthPercentage() == MAXIMUM_HEALTH_PERCENTAGE) {
			lifeSupportStatus = LifeSupportStatus.ACTIVE;
		} else {
			lifeSupportStatus = LifeSupportStatus.DAMAGED;
		}
	}
}
