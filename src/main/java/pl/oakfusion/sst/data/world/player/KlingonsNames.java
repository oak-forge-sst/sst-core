package pl.oakfusion.sst.data.world.player;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Slf4j
public class KlingonsNames extends UnitName {

	FileReader fr = null;
	private String name;

	List<String> names = new ArrayList<>();

	public List<String> newName() {
		try {
			ClassLoader classLoader = this.getClass().getClassLoader();
			InputStream i = classLoader.getResourceAsStream("KlingonsName.txt");
			BufferedReader r = new BufferedReader(new InputStreamReader(i));
			String l;
			while ((l = r.readLine()) != null) {
				names.add(l);
			}
			i.close();
		} catch (IOException | NullPointerException e) {
			log.warn("Class KlingonsNames could not found file in the resources!");
			e.printStackTrace();
		}

		return names;
	}

	public String getName() {
		return name;
	}

}








