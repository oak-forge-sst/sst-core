package pl.oakfusion.sst.data.world.gameobject;

import pl.oakfusion.sst.data.world.gameobject.GameObject;

public interface ObjectsFromConfigFactory {
	GameObject createObject(int x, int y);
}
