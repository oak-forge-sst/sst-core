package pl.oakfusion.sst.data.world.civilizations.klingoncivilization.factories;

import pl.oakfusion.sst.data.world.civilizations.Unit;
import pl.oakfusion.sst.data.world.player.KlingonsNames;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class UnitsKlingonsNamesFactory {
	private static List<String> names;

	public static void init() {
		names = new KlingonsNames().newName();
	}

	public static void setName(Unit unit) {
		if (names == null || names.isEmpty()) {
			init();
		}
		unit.setUnitName(names.get(0));
		names.remove(0);
	}
}
