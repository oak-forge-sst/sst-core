package pl.oakfusion.sst.data.world.civilizations;

import pl.oakfusion.sst.data.world.civilizations.Civilization;
import pl.oakfusion.sst.data.world.civilizations.Unit;

import java.util.List;

public interface CivilizationFromConfigFactory {
	 Civilization createCivilization(List<Unit> unitList);
}
