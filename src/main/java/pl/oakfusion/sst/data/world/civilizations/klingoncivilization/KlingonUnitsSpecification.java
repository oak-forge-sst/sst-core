package pl.oakfusion.sst.data.world.civilizations.klingoncivilization;

import pl.oakfusion.sst.data.world.civilizations.UnitType;

import java.util.HashMap;

public class KlingonUnitsSpecification {

	public void setKlingonSpecification() {
		setUnitsHealth();
		setUnitsDamage();
	}

	//KLINGON UNITS HEALTH
	private static HashMap<UnitType, Double> unitsHealth = new HashMap<>();
	private static HashMap<UnitType, Double> unitsDamage = new HashMap<>();

	private static void setUnitsHealth() {
		unitsHealth.put(UnitType.KLINGON, 350.0);
		unitsHealth.put(UnitType.COMMANDER, 650.0);
		unitsHealth.put(UnitType.SUPER_COMMANDER, 1250.0);
	}

	private static void setUnitsDamage() {
		unitsDamage.put(UnitType.KLINGON, 300.0);
		unitsDamage.put(UnitType.COMMANDER, 400.0);
		unitsDamage.put(UnitType.SUPER_COMMANDER, 600.0);
	}


	//
	public static double getUnitHealth(UnitType unitType) {
		if(unitsHealth.isEmpty()) {
			setUnitsHealth();
		}
		return unitsHealth.get(unitType);
	}

	public static double getUnitDamage(UnitType unitType) {
		if(unitsDamage.isEmpty()) {
			setUnitsDamage();
		}
		return unitsDamage.get(unitType);
	}


}
