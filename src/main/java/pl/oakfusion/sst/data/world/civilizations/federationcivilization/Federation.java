package pl.oakfusion.sst.data.world.civilizations.federationcivilization;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.oakfusion.sst.data.world.civilizations.Civilization;
import pl.oakfusion.sst.data.world.civilizations.Unit;
import pl.oakfusion.sst.data.world.player.CivilizationType;
import pl.oakfusion.sst.data.world.player.FederationsNames;
import pl.oakfusion.sst.data.world.player.UnitName;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class Federation implements Civilization {

	private Enterprise enterprise;
	private FederationsNames federationsNames;
	private List<Unit> activeUnits = new ArrayList<>();
	private List<Unit> units = new ArrayList<>();


	public Federation(Enterprise enterprise) {

		this.enterprise = enterprise;

		if(enterprise != null){
			units.add(enterprise);
		}

		setUnitsUuids();
	}

	public Enterprise getEnterprise() {
		return enterprise;
	}

	@Override
	public List<Unit> getActiveUnits() {
		return activeUnits;
	}

	@Override
	public List<Unit> getUnits() {
		return List.of(enterprise);
	}

	@Override
	public CivilizationType getCivilizationType() {
		return CivilizationType.FEDERATION;
	}

	@Override
	public void setActiveUnits(List<Unit> units) {
		this.activeUnits = units;
	}


	public List<UnitName> getUnitName() {
		return List.of();
	}

}

