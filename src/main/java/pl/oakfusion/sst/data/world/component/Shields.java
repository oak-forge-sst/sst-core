package pl.oakfusion.sst.data.world.component;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.game.commands.shields.ChangeShieldsStatus;
import pl.oakfusion.sst.data.game.events.GameEvent;
import pl.oakfusion.sst.data.game.events.shields.ShieldStatusChangeFailed;
import pl.oakfusion.sst.data.game.events.shields.ShieldsLowered;
import pl.oakfusion.sst.data.game.events.shields.ShieldsRisen;

import static pl.oakfusion.sst.data.world.component.ShieldsStatus.UP;

@Getter
@Setter
public class Shields extends Component implements StatusComponents {

	public static final int SHIELDS_HEALTH_LEVEL_OF_FUNCTIONALITY = 25;
	private EnergyContainer energyContainer;
	private ShieldsStatus shieldsStatus;


	public Shields(final double maxHealth, final double repairRatio) {
		this(maxHealth, maxHealth, repairRatio, 2500, 2500, UP);
	}

	public Shields(final double maxHealth, final double repairRatio, final double energyCapacity) {
		this(maxHealth, maxHealth, repairRatio, energyCapacity, energyCapacity, UP);
	}

	public Shields(final double maxHealth, final double currentHealth, final double repairRatio, final double energyCapacity, final double energy, final ShieldsStatus shieldsStatus) {
		super(maxHealth, currentHealth, repairRatio);
		this.energyContainer = new EnergyContainer(energyCapacity, energy, 0);
		this.shieldsStatus = shieldsStatus;
	}

	public GameEvent changeShieldStatus(final ChangeShieldsStatus command) {
		if (getShieldsStatus() != command.getShieldsStatus() && calculateHealthPercentage() > SHIELDS_HEALTH_LEVEL_OF_FUNCTIONALITY) {
			setShieldsStatus(command.getShieldsStatus());
			if (getShieldsStatus() == UP) {
				return new ShieldsRisen(command.getGameSessionUuid(), command.getPlayerUuid());
			} else {
				return new ShieldsLowered(command.getGameSessionUuid(), command.getPlayerUuid());
			}
		} else {
			return new ShieldStatusChangeFailed(command.getGameSessionUuid(), command.getPlayerUuid(), getShieldsStatus());
		}
	}
}
