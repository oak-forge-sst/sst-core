package pl.oakfusion.sst.data.world.civilizations.federationcivilization;

import pl.oakfusion.sst.data.world.civilizations.UnitType;
import pl.oakfusion.sst.data.world.civilizations.UnitSpecification;
import pl.oakfusion.sst.data.world.component.ComponentSystem;

import java.util.UUID;

public class Enterprise extends FederationUnit {


	public Enterprise(int x, int y, UUID playerUUID, ComponentSystem componentSystem, UnitSpecification unitSpecification) {
		super(x, y, playerUUID, componentSystem, unitSpecification, UnitRank.GENERAL);
	}

	public void setUnitRank(UnitRank unitRank){
		this.unitRank = unitRank;
	}

	@Override
	public UnitType getType() {
		return UnitType.ENTERPRISE;
	}
}
