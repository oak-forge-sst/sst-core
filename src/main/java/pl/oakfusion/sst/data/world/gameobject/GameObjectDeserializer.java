package pl.oakfusion.sst.data.world.gameobject;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import pl.oakfusion.sst.data.parameter.ObjectType;

import java.io.IOException;

public class GameObjectDeserializer extends StdDeserializer<GameObject> {

	public GameObjectDeserializer() {
		this(null);
	}

	public GameObjectDeserializer(Class<?> vc) {
		super(vc);
	}

	@Override
	public GameObject deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
		TreeNode node = p.readValueAsTree();
		for(ObjectType type : ObjectType.values()) {
			if(node.get("objectType").toString().equalsIgnoreCase("\"" + type.name() + "\"")) {
				return p.getCodec().treeToValue(node, type.getClazz());
			}
		}
		return null;
	}
}
