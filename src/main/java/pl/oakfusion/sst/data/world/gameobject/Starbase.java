package pl.oakfusion.sst.data.world.gameobject;

import pl.oakfusion.sst.data.parameter.ObjectType;
import pl.oakfusion.sst.data.world.gameobject.StaticGameObject;

public class Starbase extends StaticGameObject {
	public static final int STARBASE_FORCE_FIELD_RANGE = 3;

	private boolean isAlive = true;
	public Starbase(int x, int y) {
		super(x, y);
		objectType = ObjectType.STARBASE;
	}

	@Override
	public void receiveHit(double dmg) {
		isAlive = false;
	}

	@Override
	public boolean isObjectAlive() {
		return isAlive;
	}
}
