package pl.oakfusion.sst.data.world.civilizations.klingoncivilization.factories;

import pl.oakfusion.sst.data.parameter.GameDifficulty;
import pl.oakfusion.sst.data.parameter.GameLength;
import pl.oakfusion.sst.data.world.civilizations.klingoncivilization.KlingonSpecification;
import pl.oakfusion.sst.data.world.player.PlayerType;

public class KlingonSpecificationFactory {

	public KlingonSpecificationFactory() {
	}

	public static KlingonSpecification createAIPlayerSpecification(GameDifficulty gameDifficulty, GameLength gameLength) {
		int multiplier = getMultiplierForLength(gameLength);
		return KlingonSpecification.builder()
				.klingonQuantity(10 * multiplier)
				.commanderQuantity(getCommanderQuantity(gameDifficulty, PlayerType.AI_PLAYER) * multiplier)
				.superCommanderQuantity(getSuperCommanderQuantity(gameDifficulty, PlayerType.AI_PLAYER))
				.build();
	}

	public static KlingonSpecification createHumanPlayerSpecification(GameDifficulty gameDifficulty, GameLength gameLength) {
		int multiplier = getMultiplierForLength(gameLength);
		return KlingonSpecification.builder()
				.klingonQuantity(10 * multiplier)
				.commanderQuantity(getCommanderQuantity(gameDifficulty, PlayerType.HUMAN_PLAYER) * multiplier)
				.superCommanderQuantity(getSuperCommanderQuantity(gameDifficulty, PlayerType.HUMAN_PLAYER))
				.build();
	}


	private static int getCommanderQuantity(GameDifficulty gameDifficulty, PlayerType playerType){
		if(playerType == PlayerType.AI_PLAYER) {
			switch (gameDifficulty) {
				case NOVICE:
					return 0;
				case FAIR:
					return 1;
				case GOOD:
					return 2;
				case EXPERT:
					return 3;
				case EMERITUS:
					return 4;
				default:
					return 0;
			}
		}else if(playerType == PlayerType.HUMAN_PLAYER){
			switch (gameDifficulty) {
				case NOVICE:
					return 4;
				case FAIR:
					return 3;
				case GOOD:
					return 2;
				case EXPERT:
					return 1;
				case EMERITUS:
					return 0;
				default:
					return 0;
			}
		}else{
			return 0;
		}
	}

	private static int getSuperCommanderQuantity(GameDifficulty gameDifficulty, PlayerType playerType){
		if(playerType == PlayerType.AI_PLAYER) {
			switch (gameDifficulty) {
				case NOVICE:
				case FAIR:
				case GOOD:
					return 0;
				case EXPERT:
				case EMERITUS:
					return 1;
				default:
					return 0;
			}
		}else if(playerType == PlayerType.HUMAN_PLAYER){
			switch (gameDifficulty) {
				case NOVICE:
				case FAIR:
				case GOOD:
					return 1;
				case EXPERT:
				case EMERITUS:
					return 0;
				default:
					return 0;
			}
		}else{
			return 0;
		}
	}

	private static int getMultiplierForLength (GameLength gameLength) {
		switch (gameLength) {
			case LONG: return 3;
			case MEDIUM: return 2;
			case SHORT: return 1;
		}
		return 1;
	}
}
