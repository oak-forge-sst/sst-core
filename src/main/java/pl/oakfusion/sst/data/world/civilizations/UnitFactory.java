package pl.oakfusion.sst.data.world.civilizations;

import pl.oakfusion.sst.data.world.gameobject.GameObjectFactory;
import pl.oakfusion.sst.data.initializer.*;
import pl.oakfusion.sst.data.parameter.GameDifficulty;
import pl.oakfusion.sst.data.world.civilizations.federationcivilization.Enterprise;
import pl.oakfusion.sst.data.world.civilizations.romulancivilization.Romulan;
import pl.oakfusion.sst.data.world.civilizations.klingoncivilization.Commander;
import pl.oakfusion.sst.data.world.civilizations.klingoncivilization.Klingon;
import pl.oakfusion.sst.data.world.civilizations.klingoncivilization.SuperCommander;
import pl.oakfusion.sst.data.world.component.ComponentSystem;

import java.util.Map;
import java.util.UUID;


public class UnitFactory implements AbstractUnitFactory {

	UnitsSpecification unitsSpecification;
	Map<GameDifficulty, UnitsSpecification> gameDifficultyUnitsSpecificationMap =
			Map.of(
					GameDifficulty.GOOD,     new GoodLevelUnitsSpecification(),
					GameDifficulty.NOVICE,   new NoviceLevelUnitsSpecification(),
					GameDifficulty.FAIR,     new FairLevelUnitsSpecification(),
					GameDifficulty.EXPERT,   new ExpertLevelUnitsSpecification(),
					GameDifficulty.EMERITUS, new EmeritusLevelUnitsSpecification()
			);
	public UnitFactory(GameDifficulty gameDifficulty){

		//Right now every UnitsSpecification other than GoodLevelSpecification
		//extends GoodLevelSpecification
		//TODO: implement specifications for different levels
		unitsSpecification = gameDifficultyUnitsSpecificationMap.get(gameDifficulty);
	}

	@Override
	public GameObjectFactory<Enterprise> createEnterprise(UUID playerUUID) {
		ComponentSystem componentSystem = new ComponentSystem(
				unitsSpecification.getEnterpriseComponentSystemSpecification()
						.build());

		return (x,y) -> new Enterprise(x, y, playerUUID, componentSystem,
				unitsSpecification.getEnterpriseSpecification());
	}

	@Override
	public GameObjectFactory<Klingon> createKlingon(UUID playerUUID) {
		ComponentSystem componentSystem = new ComponentSystem(
				unitsSpecification.getCommanderComponentSystemSpecification()
						.build());

		return (x,y) -> new Klingon(x, y, playerUUID, componentSystem,
				unitsSpecification.getKlingonSpecification());
	}

	@Override
	public GameObjectFactory<Commander> createCommander(UUID playerUUID) {
		ComponentSystem componentSystem = new ComponentSystem(
				unitsSpecification.getCommanderComponentSystemSpecification()
						.build());

		return (x,y) -> new Commander(x, y, playerUUID, componentSystem,
				unitsSpecification.getCommanderSpecification());
	}
	@Override
	public GameObjectFactory<SuperCommander> createSuperCommander(UUID playerUUID) {
		ComponentSystem componentSystem = new ComponentSystem(
				unitsSpecification.getSuperCommanderComponentSystemSpecification()
						.build());

		return (x,y) -> new SuperCommander(x, y, playerUUID, componentSystem,
				unitsSpecification.getSuperCommanderSpecification());
	}
	@Override
	public GameObjectFactory<Romulan> createRomulan(UUID playerUUID) {
		ComponentSystem componentSystem = new ComponentSystem(
				unitsSpecification.getRomulanComponentSystemSpecification()
						.build());

		return (x,y) -> new Romulan(x, y, playerUUID, componentSystem,
				unitsSpecification.getRomulanSpecification());
	}

}
