package pl.oakfusion.sst.data.world.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.oakfusion.sst.data.world.gameobject.GameObject;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static java.lang.Math.floor;
import static java.lang.Math.max;
@NoArgsConstructor
@Getter
@Setter
public class LongRangeSensor extends Component {

	private int scanRange;
	private  Map<Integer, QuadrantScan> scannedQuadrants;


	public LongRangeSensor(double maxHealth, double repairRatio) {
		this(maxHealth, repairRatio, 2);
	}

	public LongRangeSensor(double maxHealth, double repairRatio, int scanRange) {
		this(maxHealth, repairRatio, scanRange, new HashMap<>());
	}

	 public LongRangeSensor(double maxHealth, double repairRatio, int scanRange, Map<Integer, QuadrantScan> scannedQuadrants) {
		super(maxHealth, repairRatio);
		this.scanRange = scanRange;
		this.scannedQuadrants = scannedQuadrants;
	}

	public void setScannedObjects(Collection<GameObject> objects, int quadrantX, int quadrantY, int quadrantSize) {
		scannedQuadrants.computeIfAbsent(getQuadrantKey(quadrantX, quadrantY, quadrantSize), k -> new QuadrantScan())
				.setScannedObjects(objects);
	}


	public QuadrantScan[][] getScannedQuadrants(int worldWidthInQuadrants, int worldHeightInQuadrants, int quadrantSize) {
		QuadrantScan[][] scans = new QuadrantScan[worldWidthInQuadrants][worldHeightInQuadrants];
		for (int x = 0; x < worldWidthInQuadrants; x++) {
			for (int y = 0; y < worldHeightInQuadrants; y++) {
				scans[x][y] = scannedQuadrants.computeIfAbsent(getQuadrantKey(x, y, quadrantSize), k -> new QuadrantScan());
			}
		}
		return scans;
	}

	protected static int getQuadrantKey(int quadrantX, int quadrantY, int quadrantSize) {
		return quadrantY * quadrantSize + quadrantX;
	}
	public int getViewRange(double levelOfFunctionality) {
		return (int)floor(max(levelOfFunctionality, calculateHealthFactor())*scanRange);
	}
}
