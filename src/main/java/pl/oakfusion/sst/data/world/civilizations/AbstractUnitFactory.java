package pl.oakfusion.sst.data.world.civilizations;

import pl.oakfusion.sst.data.world.gameobject.GameObjectFactory;
import pl.oakfusion.sst.data.world.civilizations.federationcivilization.Enterprise;
import pl.oakfusion.sst.data.world.civilizations.romulancivilization.Romulan;
import pl.oakfusion.sst.data.world.civilizations.klingoncivilization.Commander;
import pl.oakfusion.sst.data.world.civilizations.klingoncivilization.Klingon;
import pl.oakfusion.sst.data.world.civilizations.klingoncivilization.SuperCommander;

import java.util.UUID;

/**
* Creates different types of units.
 * Units should not be created with coordinates due to
 * World generation and Object placement. Therefore AbstractUnitFactory
 * returns a function that takes in x,y coordinates and returns
 * a Unit.
 *
* */
public interface AbstractUnitFactory {
	GameObjectFactory<Enterprise> createEnterprise(UUID playerUUID);
	GameObjectFactory<Klingon> createKlingon(UUID playerUUID);
	GameObjectFactory<Romulan> createRomulan(UUID playerUUID);
	GameObjectFactory<Commander> createCommander(UUID playerUUID);
	GameObjectFactory<SuperCommander> createSuperCommander(UUID playerUUID);

}
