package pl.oakfusion.sst.data.world.component;

import pl.oakfusion.sst.data.world.component.strategy.DamageReceivingStrategy;
import pl.oakfusion.sst.data.world.component.strategy.RepairingStrategy;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ComponentSystem implements PlayableComponentSystem {

	public static double DOCKED_REPAIR_RATIO = 30;

	protected Map<ComponentId, Component> components;

	public ComponentSystem(Map<ComponentId, Component> components) {
		this.components = components;
	}

	public <C extends Component> C getComponent(ComponentId id) {
		return (C) components.get(id);
	}

	public void receiveDamage(DamageReceivingStrategy strategy, double damage) {
		if (getShields().getShieldsStatus() == ShieldsStatus.UP) {
			strategy.applyDamage(getShields(), components.values(), damage);
		} else {
			strategy.applyDamage(components.values(), damage);
		}
	}

	public void repair(RepairingStrategy strategy, double repair) {
		List<Component> components = new ArrayList<>(this.components.values());
		strategy.repair(components, repair);
	}

	public double getComponentSystemHealthStatus() {
		double componentsHealthSum = 0;
		for (Component element : components.values()) {
			componentsHealthSum += element.calculateHealthPercentage();
		}
		return (componentsHealthSum / components.size());
	}

	public double getComponentSystemHealthSum() {
		return components
				.values().stream()
				.map(Component::getCurrentHealth)
				.reduce(0.0, Double::sum);
	}

	public double getMaxComponentSystemHealthSum() {
		return components.values().stream()
				.map(Component::getMaxHealth)
				.reduce(0.0, Double::sum);
	}

	public void dock(){
		components.values().forEach(c -> c.setRepairRatio(DOCKED_REPAIR_RATIO));
		getShields().fullRepair();
		getLifeSupport().fullRepair();
		getEnergyContainer().setEnergyToMax();
		getTorpedoesDevice().resetTorpedoes();
	}

	public void undock(){
		components.values().forEach(Component::resetRepairRatio);
	}

	public Map<ComponentId, Component> getComponents() {
		return components;
	}

	public void addComponents(ComponentId componentId, Component component){
		components.put(componentId, component);
	}

	/*
	* Default Components every Unit possesses
	*
	* */
	@Override
	public Computer getComputer() {
		return getComponent(ComponentId.COMPUTER);
	}

	@Override
	public Hull getHull() {
		return getComponent(ComponentId.HULL);
	}

	@Override
	public Shields getShields() {
		return getComponent(ComponentId.SHIELDS);
	}

	@Override
	public EnergyController getEnergyController() {
		return getComponent(ComponentId.ENERGY_CONTROLLER);
	}

	@Override
	public LifeSupport getLifeSupport() {
		return getComponent(ComponentId.LIFE_SUPPORT);
	}

	@Override
	public SubspaceRadio getSubspaceRadio() {
		return getComponent(ComponentId.SUBSPACE_RADIO);
	}

	@Override
	public LongRangeSensor getLongRangeSensor() {
		return getComponent(ComponentId.LONG_RANGE_SENSOR);
	}

	@Override
	public ShortRangeSensor getShortRangeSensor() {
		return getComponent(ComponentId.SHORT_RANGE_SENSOR);
	}

	public WarpEngines getWarpEngines() {return getComponent(ComponentId.WARP_ENGINES);}

	@Override
	public PhasersDevice getPhasersDevice() {
		return getComponent(ComponentId.PHASERS_DEVICE);
	}

	@Override
	public TorpedoesDevice getTorpedoesDevice() {
		return getComponent(ComponentId.TORPEDOES_DEVICE);
	}

	@Override
	public EnergyContainer getEnergyContainer(){
		return getComponent(ComponentId.ENERGY_CONTAINER);
	}

	@Override
	public DeathRayComponent getDeathRayComponent() {
		return getComponent(ComponentId.DEATH_RAY_COMPONENT);
	}


}
