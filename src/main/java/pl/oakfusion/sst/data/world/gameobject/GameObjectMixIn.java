package pl.oakfusion.sst.data.world.gameobject;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public abstract class GameObjectMixIn {
	@JsonCreator
	public GameObjectMixIn(@JsonProperty("x") int x, @JsonProperty("y") int y) {}
	@JsonIgnore
	public abstract boolean isAlive();
}
