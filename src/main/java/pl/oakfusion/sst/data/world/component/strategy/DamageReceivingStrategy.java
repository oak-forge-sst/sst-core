package pl.oakfusion.sst.data.world.component.strategy;

import pl.oakfusion.sst.data.world.component.Component;
import pl.oakfusion.sst.data.world.component.Shields;

import java.util.Collection;

public interface DamageReceivingStrategy {

    void applyDamage(Collection<Component> components, double damage);

    void applyDamage(Shields shields, Collection<Component> components, double damage);
}
