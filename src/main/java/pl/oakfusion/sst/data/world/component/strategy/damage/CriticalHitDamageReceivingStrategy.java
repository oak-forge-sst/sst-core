package pl.oakfusion.sst.data.world.component.strategy.damage;

import pl.oakfusion.sst.data.util.RandomGenerator;
import pl.oakfusion.sst.data.world.component.Component;
import pl.oakfusion.sst.data.world.component.Shields;
import pl.oakfusion.sst.data.world.component.strategy.DamageReceivingStrategy;


import java.util.Collection;
import java.util.Iterator;

public class CriticalHitDamageReceivingStrategy implements DamageReceivingStrategy {

    private static final double MIN_SHIELD_DAMAGE_RATIO = 0.2;
    private static final double MAX_SHIELD_DAMAGE_RATIO = 0.8;

    private final RandomGenerator random;

    public CriticalHitDamageReceivingStrategy(RandomGenerator random) {
        this.random = random;
    }

    @Override
    public void applyDamage(Collection<Component> components, double damage) {
        if (damage > 0) {
            Iterator<Component> shuffledComponentsIterator = random.shuffle(components).iterator();

            while (damage > 0 && shuffledComponentsIterator.hasNext()) {
                Component component = shuffledComponentsIterator.next();
                damage = component.receiveDamageAndGetOverflow(damage);
            }
        }
    }

    @Override
    public void applyDamage(Shields shields, Collection<Component> components, double damage) {

        double shieldsDamageRatio = random.nextDoubleInRange(MIN_SHIELD_DAMAGE_RATIO, MAX_SHIELD_DAMAGE_RATIO);
        double shieldDamage = damage * shieldsDamageRatio;
        double componentDamage = damage - shieldDamage;

        damage = shields.getEnergyContainer().damageShields(shieldDamage) + componentDamage;

        applyDamage(components, damage);
    }
}
