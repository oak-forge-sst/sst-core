package pl.oakfusion.sst.data.world.player;

import pl.oakfusion.sst.data.parameter.Parameter;
import pl.oakfusion.sst.data.util.AbbreviationFactory;

import java.util.List;

public enum CivilizationType implements Parameter {
	FEDERATION,
	KLINGON_EMPIRE;

	private final List<java.lang.String> abbreviations;

	CivilizationType() {
		abbreviations = AbbreviationFactory.getAbbreviations(name());
	}

	@Override
	public boolean accepts(String string) {
		return abbreviations.contains(string);
	}
}
