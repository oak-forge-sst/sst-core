package pl.oakfusion.sst.data.world;

import pl.oakfusion.sst.data.world.gameobject.GameObject;

import java.util.LinkedList;
import java.util.List;


public class Quadrant {

	private final List<GameObject> staticObjects;
	private final List<GameObject> dynamicObjects;

	public Quadrant() {
		dynamicObjects = new LinkedList<>();
		staticObjects = new LinkedList<>();
	}

	protected void addStaticObject(GameObject go) {
		staticObjects.add(go);
	}

	protected void removeStaticObject(GameObject go) {
		staticObjects.remove(go);
	}

	protected List<GameObject> getStaticObjects() {
		return staticObjects;
	}

	protected void addDynamicObject(GameObject go) {
		dynamicObjects.add(go);
	}

	protected List<GameObject> getDynamicObjects() {
		return dynamicObjects;
	}

	protected void removeDynamicObject(GameObject go) {
		dynamicObjects.remove(go);
	}

}
