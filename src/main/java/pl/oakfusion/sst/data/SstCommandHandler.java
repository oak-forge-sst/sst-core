package pl.oakfusion.sst.data;

import pl.oakfusion.data.message.Event;
import pl.oakfusion.sst.core.gamedata.GameSessionData;
import pl.oakfusion.sst.data.exceptions.NotFoundException;

import java.util.List;

@FunctionalInterface
public interface SstCommandHandler<C extends SstCommand> {
	List<Event> handle(C command, GameSessionData gameSessionData) throws NotFoundException;
}
