package pl.oakfusion.sst.data.DTO.damageReportDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class DamageReportDTO {
	private ArrayList<ComponentDTO> componentsList = new ArrayList<>();
	private ArrayList<UnitDTO> unitList = new ArrayList<>();


	public void addComponentToList(ComponentDTO componentDTO) {
		componentsList.add(componentDTO);
	}

	public void addUnitToList(UnitDTO unitDTO) {
		unitList.add(unitDTO);
	}


}


