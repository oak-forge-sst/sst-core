package pl.oakfusion.sst.data.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.oakfusion.data.message.Event;

import java.util.Optional;
@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class DeadPlayersDTO {

		private String deadPlayer;
		private Class<? extends Event> causeOfDeath;
		private Optional<String> killer;
		private int turn;


		public DeadPlayersDTO(String deadPlayer, Class<? extends Event> causeOfDeath) {
			this.deadPlayer = deadPlayer;
			this.causeOfDeath = causeOfDeath;
			this.killer = Optional.empty();
		}
	}


