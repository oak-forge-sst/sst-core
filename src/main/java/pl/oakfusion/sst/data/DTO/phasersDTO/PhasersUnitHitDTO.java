package pl.oakfusion.sst.data.DTO.phasersDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.oakfusion.sst.data.util.Position;
import pl.oakfusion.sst.data.world.civilizations.UnitType;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PhasersUnitHitDTO {
	private double damage;
	private UnitType unitType;
	private boolean alive;
	private Position position;

}
