package pl.oakfusion.sst.data.DTO.score;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.LinkedList;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ScoreDTO {
	private int sumScore;
	private LinkedList<OneScoreDTO> score;

	public ScoreDTO(LinkedList<OneScoreDTO> score) {
		this.score = score;
	}

	public void addScore(OneScoreDTO oneScoreDTO) {
		score.add(oneScoreDTO);
	}

	public void incrementSumScore(int score) {
		sumScore += score;
	}
}
