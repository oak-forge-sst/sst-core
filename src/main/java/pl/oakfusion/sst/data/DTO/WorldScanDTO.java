package pl.oakfusion.sst.data.DTO;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.oakfusion.sst.data.DTO.shortRangeScanDTO.SRSObject;
import pl.oakfusion.sst.data.world.gameobject.GameObject;
import pl.oakfusion.sst.data.world.civilizations.Unit;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class WorldScanDTO {
	private int widthInQuadrants;
	private int heightInQuadrants;
	private int quadrantSize;
	@JsonIgnore	private List<GameObject> gameObjects;
	private List<SRSObject> shortRangeScanObjects = new ArrayList<>();


	public int getAbsoluteWidth() {
		return widthInQuadrants * quadrantSize;
	}

	public int getAbsoluteHeight() {
		return heightInQuadrants * quadrantSize;
	}

	public WorldScanDTO(int widthInQuadrants, int heightInQuadrants, int quadrantSize, List<GameObject> gameObjects) {
		this.widthInQuadrants = widthInQuadrants;
		this.heightInQuadrants = heightInQuadrants;
		this.quadrantSize = quadrantSize;
		gameObjects.forEach(gameObject -> {
			if (gameObject instanceof Unit) {
				shortRangeScanObjects.add(new SRSObject(gameObject.getX(), gameObject.getY(), ((Unit) gameObject).getType().name()));
			} else {
				shortRangeScanObjects.add(new SRSObject(gameObject.getX(), gameObject.getY(), gameObject.getObjectType().name()));
			}
		});
	}

}
