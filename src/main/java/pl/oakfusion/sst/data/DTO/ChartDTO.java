package pl.oakfusion.sst.data.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@Setter
@Getter
@NoArgsConstructor
public class ChartDTO {
	private int worldSizeX;
	private int worldSizeY;
	private int enterpriseQuadrantX;
	private int enterpriseQuadrantY;

	private int[][] quadrantChunks;
	private int scanRange;

}
