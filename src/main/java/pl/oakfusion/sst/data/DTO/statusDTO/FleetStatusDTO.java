package pl.oakfusion.sst.data.DTO.statusDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.oakfusion.sst.data.world.civilizations.UnitType;

import java.util.UUID;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class FleetStatusDTO {
	private int x;
	private int y;
	private double health;
	private UnitType type;
	private double actionPoints;
	private String unitName;
	private UUID unitID;


}
