package pl.oakfusion.sst.data.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.oakfusion.sst.data.parameter.ObjectType;
import pl.oakfusion.sst.data.world.civilizations.UnitType;
import pl.oakfusion.sst.data.world.player.Player;

@AllArgsConstructor
@Getter
@NoArgsConstructor
public class DamagedObjects {
	private Player attacker;
	private Player defender;
	private boolean alive;
	private UnitType defenderUnitType;
	private ObjectType destroyedObject;
	private int x;
	private int y;
	private UnitType attackerUnitType;
	private String name;
	private int turn;

	public DamagedObjects(Player attacker, UnitType attackerUnitType, ObjectType destroyedObject) {
		this.attacker = attacker;
		this.attackerUnitType = attackerUnitType;
		this.destroyedObject = destroyedObject;
	}

	public DamagedObjects(Player attacker, UnitType attackerUnitType, UnitType defenderUnitType) {
		this.attacker = attacker;
		this.attackerUnitType = attackerUnitType;
		this.defenderUnitType = defenderUnitType;
	}


	public DamagedObjects(Player attacker, Player defender, boolean alive, UnitType defenderUnitType, int x, int y, UnitType attackerUnitType, String name, int turn) {
		this.attacker = attacker;
		this.defender = defender;
		this.alive = alive;
		this.defenderUnitType = defenderUnitType;
		this.x = x;
		this.y = y;
		this.name = name;
		this.attackerUnitType = attackerUnitType;
		this.turn = turn;
	}

	//Used when object is destroyed by explosion
	public DamagedObjects(Player defender, boolean alive, UnitType defenderUnitType, int x, int y, String name, int turn) {
		this.defender = defender;
		this.alive = alive;
		this.defenderUnitType = defenderUnitType;
		this.x = x;
		this.y = y;
		this.name = name;
		this.turn = turn;
	}
}
