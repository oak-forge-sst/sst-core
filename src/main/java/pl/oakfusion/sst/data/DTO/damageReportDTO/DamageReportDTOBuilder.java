package pl.oakfusion.sst.data.DTO.damageReportDTO;

import pl.oakfusion.sst.data.DTO.damageReportDTO.ComponentDTO;
import pl.oakfusion.sst.data.DTO.damageReportDTO.DamageReportDTO;
import pl.oakfusion.sst.data.DTO.damageReportDTO.UnitDTO;
import pl.oakfusion.sst.data.util.Precision;
import pl.oakfusion.sst.data.world.component.ComponentCondition;
import pl.oakfusion.sst.data.world.component.ComponentId;
import pl.oakfusion.sst.data.world.civilizations.Civilization;
import pl.oakfusion.sst.data.world.civilizations.Unit;
import pl.oakfusion.sst.data.world.civilizations.UnitType;
import pl.oakfusion.sst.data.world.component.Component;
import pl.oakfusion.sst.data.world.player.Player;

import java.util.List;
import java.util.Map;
import java.util.UUID;

public class DamageReportDTOBuilder {

	public static final int MAXIMUM_HEALTH_PERCENTAGE = 100;
	private Precision precision = new Precision();
	private DamageReportDTO damageReportDTO = new DamageReportDTO();
	private Map<ComponentId, Component> components;
	private Civilization civilization;

	public DamageReportDTOBuilder(Player activePlayer, UUID unitUuid) {
		this.civilization = activePlayer.getCivilization();
		components = civilization.getActiveUnits().stream().
				filter(unit -> unit.getUuid().equals(unitUuid)).findFirst().orElseThrow().getComponentSystem().getComponents();

		setComponentsList(components);
		setUnits(civilization.getLivingUnits());
	}

	private void setComponentsList(Map<ComponentId, Component> components) {
		components.entrySet().stream()
				.filter(componentsValues -> componentsValues.getValue().calculateHealthPercentage() != MAXIMUM_HEALTH_PERCENTAGE)
				.forEach(componentsValues -> setComponentDTO(
						componentsValues.getKey().name(),
						componentsValues.getValue().getComponentCondition(),
						componentsValues.getValue().getCurrentHealth(),
						componentsValues.getValue().calculateRepairTime()));
	}

	private void setUnits(List<? extends Unit> units) {
		units.stream().filter(unit -> unit.getHealthAsFraction() != 1)
				.forEach(unit -> setUnitDTO(
						unit.getX(),
						unit.getY(),
						unit.getType(),
						unit.getHealth()
				));
	}

	private void setComponentDTO(String name, ComponentCondition componentCondition, double health,
								 double repairTime) {
		repairTime = precision.setPrecision(repairTime, 2);
		health = precision.setPrecision(health, 2);
		ComponentDTO componentDTO = new ComponentDTO(name, health, repairTime, componentCondition);
		damageReportDTO.addComponentToList(componentDTO);
	}

	private void setUnitDTO(int x, int y, UnitType unitType, double health) {
		health = precision.setPrecision(health, 2);
		UnitDTO unitDTO = new UnitDTO(x, y, unitType, health);
		damageReportDTO.addUnitToList(unitDTO);
	}

	public DamageReportDTO getDamageReportDTO() {
		return damageReportDTO;
	}
}
