package pl.oakfusion.sst.data.DTO.phasersDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@Setter
@Getter
@NoArgsConstructor
public class PhasersDTO {
	private List<PhasersUnitHitDTO> opponentsUnitsDamaged;
	private double energyWasted;

}
