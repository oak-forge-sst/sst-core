package pl.oakfusion.sst.data.DTO.torpedo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.oakfusion.sst.data.util.Position;
import pl.oakfusion.sst.data.world.civilizations.UnitType;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class UnitHitDTO extends ExplosionEventDTO {
	private Position position;
	private UnitType unitType;
	private boolean alive;

}
