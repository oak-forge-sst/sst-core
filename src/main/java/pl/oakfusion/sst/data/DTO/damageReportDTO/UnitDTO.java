package pl.oakfusion.sst.data.DTO.damageReportDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.oakfusion.sst.data.world.civilizations.UnitType;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UnitDTO {
	private int x;
	private int y;
	private UnitType unitType;
	private double health;

}
