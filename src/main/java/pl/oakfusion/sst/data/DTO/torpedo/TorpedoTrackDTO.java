package pl.oakfusion.sst.data.DTO.torpedo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.oakfusion.sst.data.util.Position;

import java.util.List;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class TorpedoTrackDTO {
	private Position position;
	private List<ExplosionEventDTO> eventsList;
}


