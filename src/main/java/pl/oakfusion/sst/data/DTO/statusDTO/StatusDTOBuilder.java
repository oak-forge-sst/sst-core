package pl.oakfusion.sst.data.DTO.statusDTO;

import pl.oakfusion.sst.data.world.civilizations.Civilization;
import pl.oakfusion.sst.data.messagegenerator.MessageKey;
import pl.oakfusion.sst.data.util.Precision;
import pl.oakfusion.sst.data.world.civilizations.Unit;
import pl.oakfusion.sst.data.world.civilizations.UnitType;
import pl.oakfusion.sst.data.world.player.Player;

import java.util.List;
import java.util.UUID;

public class StatusDTOBuilder {
	private Precision precision = new Precision();
	private Civilization civilization;
	private List<Civilization> opponentsCivilizations;
	private Player activePlayer;
	private String starDate;
	private StatusDTO statusDTO = new StatusDTO();
	private int score;
	private int quadrantSize;


	public StatusDTOBuilder(Player activePlayer, String starDate, List<Civilization> opponentsCivilizations, int quadrantSize) {
		this.activePlayer = activePlayer;
		this.starDate = starDate;
		this.opponentsCivilizations = opponentsCivilizations;
		this.quadrantSize = quadrantSize;

		civilization = activePlayer.getCivilization();
		setPlayerStatusDTO(activePlayer, opponentsCivilizations);
	}

	private void setPlayerStatusDTO(Player activePlayer, List<Civilization> opponentsCivilizations) {
		setFleet(activePlayer.getCivilization().getLivingUnits());
		addStatusValues(activePlayer);
		setOpponnets(opponentsCivilizations);


	}

	private void addStatusValues(Player activePlayer) {
		setPlayerName(activePlayer);
		setStardate();
		setCivilizationType(activePlayer);
		setScore(activePlayer);
	}



	private void setStardate() {
		statusDTO.addUnitStatusDTOS(String.valueOf(MessageKey.STARDATE), starDate);
	}

	private void setScore(Player activePlayer) {
		statusDTO.addUnitStatusDTOS(String.valueOf(MessageKey.TOTAL_SCORE), activePlayer.getScore());
	}


	private void setCivilizationType(Player activePlayer) {
		statusDTO.addUnitStatusDTOS(String.valueOf(MessageKey.CIVILIZATION_TYPE),
				activePlayer.getCivilization().getCivilizationType());
	}

	private void setPlayerName(Player activePlayer) {
		statusDTO.addUnitStatusDTOS(String.valueOf(MessageKey.PLAYER_NAME),
				activePlayer.getPlayerName());
	}

	private void setOpponnets(List<Civilization> opponentsCivilizations) {
		int opponentsCounter = 0;
		for (Civilization opponentCivilization : opponentsCivilizations) {
			opponentsCounter += (int) opponentCivilization.getUnits().stream().filter(Unit::isObjectAlive).count();
		}
		statusDTO.addUnitStatusDTOS(String.valueOf(MessageKey.OPPONENTS_LEFT), opponentsCounter);
	}


	private void setFleet(List<? extends Unit> units) {
		units.forEach(unit -> setFleetDTO(
				unit.getX(),
				unit.getY(),
				unit.getHealth(),
				unit.getType(),
				unit.getActionPoints(),
				unit.getUnitName(),
				unit.getUuid()
		));
	}

	private void setFleetDTO(int x, int y, double health, UnitType unitType, double actionPoints, String unitName, UUID unitID) {
		health = precision.setPrecision(health, 2);
		actionPoints = precision.setPrecision(actionPoints, 2);
		FleetStatusDTO fleetDTO = new FleetStatusDTO(x, y, health, unitType, actionPoints, unitName, unitID);
		statusDTO.addFleetStatusDTOS(fleetDTO);
	}

	public StatusDTO getStatusDTO() {
		return statusDTO;
	}
}
