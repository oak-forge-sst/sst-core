package pl.oakfusion.sst.data.DTO.statusDTO;

import pl.oakfusion.sst.data.world.component.ComponentCondition;
import pl.oakfusion.sst.data.world.component.ComponentId;
import pl.oakfusion.sst.data.world.component.ShieldsStatus;

public class StatusComponentDTO {
	private ComponentId componentId;
	private double health;
	private int torpedoes;
	private ShieldsStatus shieldsStatus;
	private double energy;
	private int warp;
	private ComponentCondition componentCondition;

	public ComponentCondition getComponentCondition() {
		return componentCondition;
	}

	public void setComponentCondition(ComponentCondition componentCondition) {
		this.componentCondition = componentCondition;
	}

	public double getEnergy() {
		return energy;
	}

	public void setEnergy(double energy) {
		this.energy = energy;
	}

	public int getWarp() {
		return warp;
	}

	public void setWarp(int warp) {
		this.warp = warp;
	}


	public ComponentId getComponentId() {
		return componentId;
	}

	public void setComponentId(ComponentId componentId) {
		this.componentId = componentId;
	}

	public double getHealth() {
		return health;
	}

	public void setHealth(double health) {
		this.health = health;
	}

	public int getTorpedoes() {
		return torpedoes;
	}

	public void setTorpedoes(int torpedoes) {
		this.torpedoes = torpedoes;
	}

	public ShieldsStatus getShieldsStatus() {
		return shieldsStatus;
	}

	public void setShieldsStatus(ShieldsStatus shieldsStatus) {
		this.shieldsStatus = shieldsStatus;
	}
}
