package pl.oakfusion.sst.data.DTO.shortRangeScanDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.oakfusion.sst.data.DTO.ChartDTO;
import pl.oakfusion.sst.data.DTO.WorldScanDTO;
import pl.oakfusion.sst.data.DTO.phasersDTO.OpponentUnitDTO;
import pl.oakfusion.sst.data.DTO.statusDTO.StatusDTO;

import java.util.List;
import java.util.Optional;
@AllArgsConstructor
@Getter
@SuppressWarnings("OptionalUsedAsFieldOrParameterType")
@Setter
@NoArgsConstructor
public class ShortRangeScanDTO {


	private WorldScanDTO worldScan;
	private int unitX;
	private int unitY;
	private int shortRangeScanRange;
	private Optional<ChartDTO> optionalChart;
	private Optional<StatusDTO> optionalStatus;
	private int viewRange;
	private List<OpponentUnitDTO> surroundingUnitsInfo;
}
