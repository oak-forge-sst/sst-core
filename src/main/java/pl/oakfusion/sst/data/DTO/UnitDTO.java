package pl.oakfusion.sst.data.DTO;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.oakfusion.sst.data.world.component.Condition;
import pl.oakfusion.sst.data.world.civilizations.Unit;
import pl.oakfusion.sst.data.world.component.ComponentSystem;

@AllArgsConstructor
@Setter
@Getter
@NoArgsConstructor
public class UnitDTO {

	private int x;
	private int y;
	private ComponentSystem componentSystem;
	private Condition condition;

	public UnitDTO(Unit unit) {
		this(
				unit.getX(),
				unit.getY(),
				unit.getComponentSystem(),
				unit.getCondition()
		);
	}

}
