package pl.oakfusion.sst.data.DTO.phasersDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.oakfusion.sst.data.world.component.Condition;
import pl.oakfusion.sst.data.world.civilizations.Unit;
import pl.oakfusion.sst.data.world.civilizations.UnitType;

import java.util.UUID;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class OpponentUnitDTO {
	private UnitType unitType;
	private String name;
	private String playerName;
	private UUID playerUuid;
	private int x;
	private int y;
	private Condition condition;

	public OpponentUnitDTO(Unit unit, String playerName) {
		name = unit.getUnitName();
		this.playerName = playerName;
		playerUuid = unit.getUuid();
		x = unit.getX();
		y = unit.getY();
		unitType = unit.getType();
		condition=unit.getCondition();
	}

	@Override
	public String toString() {
		return getUnitType() + " " + getName() + " " + getX() + " " + getY() + " " + getPlayerName();
	}
}
