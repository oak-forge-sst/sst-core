package pl.oakfusion.sst.data.DTO.shortRangeScanDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class SRSObject {
	private int x;
	private int y;
	private String type;
}
