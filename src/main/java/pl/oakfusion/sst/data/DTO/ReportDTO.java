package pl.oakfusion.sst.data.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.oakfusion.sst.data.messagegenerator.MessageKey;
import pl.oakfusion.sst.data.util.Position;
import pl.oakfusion.sst.data.world.civilizations.UnitType;

@Getter
@AllArgsConstructor
@Setter
@NoArgsConstructor
public class ReportDTO {
	private String name;
	private UnitType playerObjectType;
	private Position position;
	private MessageKey event;
	private UnitType AttackerUnitType;
	private String AttackerName;
}
