package pl.oakfusion.sst.data.DTO;

import pl.oakfusion.sst.data.world.component.LifeSupport;
import pl.oakfusion.sst.data.world.component.LifeSupportStatus;

public class LifeSupportDTO {

    private LifeSupportStatus lifeSupportStatus;
    private double reserve;

    public LifeSupportDTO(LifeSupportStatus lifeSupportStatus, LifeSupport lifeSupport) {
        this.lifeSupportStatus = lifeSupportStatus;
//        this.reserve = lifeSupport.getReserve();
    }

    public double getReserveInStarDate() {
        return reserve;
    }

    public LifeSupportStatus getLifeSupportStatus() {
        return lifeSupportStatus;
    }

    public String toString() {
        if (lifeSupportStatus == LifeSupportStatus.ACTIVE){
            return getLifeSupportStatus().toString();
        } else {
            return getLifeSupportStatus().toString() + "\t" + getReserveInStarDate();
        }
    }
}
