package pl.oakfusion.sst.data.DTO.score;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class OneScoreDTO {
	private String event;
	private int eventInvoke;
	private int score;

	public void incrementScore(int score) {
		this.score += score;
	}

	public void incrementEventInvoke() {
		eventInvoke++;
	}
}
