package pl.oakfusion.sst.data.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.oakfusion.sst.data.world.player.CivilizationType;
import pl.oakfusion.sst.data.world.player.Player;

@Getter
@AllArgsConstructor
@Setter
@NoArgsConstructor
public class PlayerDTO {
	private CivilizationType civilizationType;
	private String playerName;
	public PlayerDTO (Player player){
		this.civilizationType = player.getCivilizationType();
		this.playerName = player.getPlayerName();
	}
}
