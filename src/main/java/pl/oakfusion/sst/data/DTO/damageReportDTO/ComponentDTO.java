package pl.oakfusion.sst.data.DTO.damageReportDTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.oakfusion.sst.data.world.component.ComponentCondition;

@AllArgsConstructor
@Setter
@Getter
@NoArgsConstructor
public class ComponentDTO  {
	private String componentName;
	private Double health;
	private Double repairTime;
	private ComponentCondition componentCondition;

}
