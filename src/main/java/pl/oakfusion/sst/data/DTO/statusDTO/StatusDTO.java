package pl.oakfusion.sst.data.DTO.statusDTO;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;

@Setter
@Getter
public class StatusDTO {

	private HashMap<String, String> unitStatusDTOS = new HashMap<>();
	private ArrayList<FleetStatusDTO> fleetStatusDTOS = new ArrayList<>();

	public void addUnitStatusDTOS(String key, Object value) {
		unitStatusDTOS.put(key, value.toString());
	}

	public void addFleetStatusDTOS(FleetStatusDTO fleetStatusDTO) {
		this.fleetStatusDTOS.add(fleetStatusDTO);
	}
}

