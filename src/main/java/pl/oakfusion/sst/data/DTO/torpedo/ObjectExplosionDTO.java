package pl.oakfusion.sst.data.DTO.torpedo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.oakfusion.sst.data.parameter.ObjectType;
import pl.oakfusion.sst.data.util.Position;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ObjectExplosionDTO extends ExplosionEventDTO {
	private Position position;
	private ObjectType objectType;
	private boolean alive;

}
