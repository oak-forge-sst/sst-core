package pl.oakfusion.sst.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.oakfusion.data.message.QueryResponse;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public abstract class SstQueryResponse extends QueryResponse {
	private UUID gameSessionUuid;
	private UUID playerUuid;
	public SstQueryResponse (UUID playerUuid){
		this.playerUuid = playerUuid;
	}
}
