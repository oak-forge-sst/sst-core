package pl.oakfusion.sst.data;

import pl.oakfusion.data.message.Command;
import pl.oakfusion.sst.core.gamedata.GameSessionData;

import java.util.List;

@FunctionalInterface
public interface SstEventHandler<E extends SstEvent> {
	List<Command> handle(E event, GameSessionData gameSessionData);
}
