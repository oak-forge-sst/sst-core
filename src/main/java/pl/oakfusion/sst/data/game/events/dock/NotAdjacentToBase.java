package pl.oakfusion.sst.data.game.events.dock;

import java.util.UUID;

public class NotAdjacentToBase extends DockEvent {
	public NotAdjacentToBase(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
