package pl.oakfusion.sst.data.game.commands;


import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.SstCommand;

import java.util.UUID;

@Getter
@Setter
public abstract class GameCommand extends SstCommand {
	boolean ignoreComponentLowHealth = false;

	public GameCommand(UUID gameSessionUuid, UUID playerUuid, boolean ignoreComponentLowHealth) {
		super(gameSessionUuid, playerUuid);
		this.ignoreComponentLowHealth = ignoreComponentLowHealth;
	}

	public GameCommand(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
