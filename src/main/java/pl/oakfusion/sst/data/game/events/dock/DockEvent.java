package pl.oakfusion.sst.data.game.events.dock;

import pl.oakfusion.sst.data.game.events.GameEvent;

import java.util.UUID;

public abstract class DockEvent extends GameEvent {
	public DockEvent(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
