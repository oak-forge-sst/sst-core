package pl.oakfusion.sst.data.game.events.dock;

import java.util.UUID;

public class DockPerformed extends DockEvent {
	public DockPerformed(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
