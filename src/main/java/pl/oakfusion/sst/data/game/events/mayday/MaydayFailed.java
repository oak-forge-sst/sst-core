package pl.oakfusion.sst.data.game.events.mayday;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.DTO.DeadPlayersDTO;
import pl.oakfusion.sst.data.game.GameEventDead;
import pl.oakfusion.sst.data.game.events.GameWarningEvent;

import java.util.Collections;
import java.util.List;
import java.util.UUID;


@Getter
@Setter
public class MaydayFailed extends GameEventDead {
	private DeadPlayersDTO deadPlayerDTO;

	@JsonIgnore
	@Override
	public List<DeadPlayersDTO> getDeadPlayersDTO() {
		return Collections.singletonList(deadPlayerDTO);
	}

	public MaydayFailed(UUID gameSessionUuid, UUID playerUuid, DeadPlayersDTO deadPlayerDTO) {
		super(gameSessionUuid, playerUuid);
		this.deadPlayerDTO = deadPlayerDTO;
	}
}
