package pl.oakfusion.sst.data.game.events.phasers;

import pl.oakfusion.sst.data.game.events.GameEvent;

import java.util.UUID;

public class EnergyNotSpecifiedForPhasers extends GameEvent {
	public EnergyNotSpecifiedForPhasers(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
