package pl.oakfusion.sst.data.game.events.torpedoes;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.DTO.DeadPlayersDTO;
import pl.oakfusion.sst.data.DTO.torpedo.TorpedoTrackDTO;
import pl.oakfusion.sst.data.game.GameEventDead;
import pl.oakfusion.sst.data.game.events.GameEvent;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class TorpedoPerformed extends GameEventDead {
	private List<TorpedoTrackDTO> torpedoesTargetList;
	private int torpedoesAmount;
	private List<DeadPlayersDTO> deadPlayersDTO;

	public TorpedoPerformed(UUID gameSessionUuid, UUID playerUuid, List<TorpedoTrackDTO> torpedoesTargetList, int torpedoesAmount, List<DeadPlayersDTO> deadPlayersDTO) {
		super(gameSessionUuid, playerUuid);
		this.torpedoesTargetList = torpedoesTargetList;
		this.torpedoesAmount = torpedoesAmount;
		this.deadPlayersDTO = deadPlayersDTO;
	}
}
