package pl.oakfusion.sst.data.game.commands.impulse;

import lombok.Getter;
import pl.oakfusion.sst.data.game.commands.GameCommand;

import java.util.UUID;

@Getter
public class Impulse extends GameCommand {
	private int x;
	private int y;
	boolean isInAutoMode;
	boolean isForced;

	public Impulse(UUID gameSessionUuid, UUID playerUuid, int x, int y,
				   boolean isInAutoMode, boolean isForced) {
		super(gameSessionUuid, playerUuid);
		this.x = x;
		this.y = y;
		this.isForced = isForced;
		this.isInAutoMode = isInAutoMode;
	}




}
