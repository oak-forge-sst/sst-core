package pl.oakfusion.sst.data.game.queryresponses.help;

import pl.oakfusion.sst.data.game.queryresponses.GameQueryResponse;

import java.util.UUID;


public class HelpQueryResponse extends GameQueryResponse {
	public HelpQueryResponse(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
