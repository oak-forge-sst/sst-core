package pl.oakfusion.sst.data.game.queryresponses.damagereport;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.DTO.damageReportDTO.DamageReportDTO;
import pl.oakfusion.sst.data.game.queryresponses.GameQueryResponse;

import java.util.UUID;

@Getter
@Setter
public class DamageReportPerformed extends GameQueryResponse {
	private DamageReportDTO	 damageReportDTO;

	public DamageReportPerformed(UUID gameSessionUuid, UUID playerUuid, DamageReportDTO damageReportDTO) {
		super(gameSessionUuid, playerUuid);
		this.damageReportDTO = damageReportDTO;
	}
}
