package pl.oakfusion.sst.data.game.commands.dock;

import pl.oakfusion.sst.data.game.commands.GameCommand;

import java.util.UUID;

public class Dock extends GameCommand {
	public Dock(UUID gameSessionUuid, UUID playerUuid){
		super(gameSessionUuid, playerUuid);
	}
}
