package pl.oakfusion.sst.data.game.events.tractorbeam;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.util.Position;

import java.util.UUID;

@Setter
@Getter
public class TractorBeamPerformed extends TractorBeamEvent {
	private Position position;

	public TractorBeamPerformed(UUID gameSessionUuid, UUID playerUuid, Position position) {
		super(gameSessionUuid, playerUuid);
		this.position = position;
	}
}
