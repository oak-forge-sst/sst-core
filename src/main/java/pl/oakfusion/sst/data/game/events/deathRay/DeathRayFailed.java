package pl.oakfusion.sst.data.game.events.deathRay;

import pl.oakfusion.sst.data.game.events.GameEvent;

import java.util.UUID;

public class DeathRayFailed extends GameEvent {
	public DeathRayFailed(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
