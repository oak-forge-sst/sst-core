package pl.oakfusion.sst.data.game.events.energy;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.game.events.GameEvent;

import java.util.UUID;

@Getter
@Setter
public class EnergyTransferredToShields extends GameEvent {
	private double energy;

	public EnergyTransferredToShields(UUID gameSessionUuid, UUID playerUuid, double energy) {
		super(gameSessionUuid, playerUuid);
		this.energy = energy;
	}
}
