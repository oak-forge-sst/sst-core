package pl.oakfusion.sst.data.game.commands.select;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Setter
@Getter
public class MultiSelectIndices extends MultiSelect {
	private List<Integer> indices;

	public MultiSelectIndices(UUID gameSessionUuid, UUID playerUuid, List<Integer> indices) {
		super(gameSessionUuid, playerUuid);
		this.indices = indices;
	}
}
