package pl.oakfusion.sst.data.game.events.rest;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.game.events.GameEvent;

import java.util.UUID;

@Setter
@Getter
public class RestPerformed extends GameEvent {
	private String starDate;

	public RestPerformed(UUID gameSessionUuid, UUID playerUuid, String starDate) {
		super(gameSessionUuid, playerUuid);
		this.starDate = starDate;
	}
}
