package pl.oakfusion.sst.data.game.commands.move;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.core.game.FleetDestinationStrategy;
import pl.oakfusion.sst.data.game.commands.GameCommand;

import java.util.UUID;


@Getter
@Setter
public class Move extends GameCommand {
	private boolean isAutoMode;
	private int x;
	private int y;
	private boolean forceMode;
	private FleetDestinationStrategy.FleetFormation fleetFormation = FleetDestinationStrategy.FleetFormation.RANDOM;

	public Move(UUID gameSessionUuid, UUID playerUuid, boolean isAutoMode, int x, int y, boolean forceMode) {
		super(gameSessionUuid, playerUuid);
		this.isAutoMode = isAutoMode;
		this.x = x;
		this.y = y;
		this.forceMode = forceMode;
	}

	public Move(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
