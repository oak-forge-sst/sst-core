package pl.oakfusion.sst.data.game.commands;

import java.util.UUID;

public class Cancel extends GameCommand {
	public Cancel(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
