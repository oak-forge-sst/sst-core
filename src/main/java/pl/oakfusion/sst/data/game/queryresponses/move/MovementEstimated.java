package pl.oakfusion.sst.data.game.queryresponses.move;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.game.MoveEstimation;
import pl.oakfusion.sst.data.game.queryresponses.GameQueryResponse;

import java.util.UUID;

@Getter
@Setter
public class MovementEstimated extends GameQueryResponse {
	private boolean autoMode;
	private int warpFactor;
	private MoveEstimation estimation;

	public MovementEstimated(UUID gameSessionUuid, UUID playerUuid, boolean autoMode, int warpFactor, MoveEstimation estimation) {
		super(gameSessionUuid, playerUuid);
		this.autoMode = autoMode;
		this.warpFactor = warpFactor;
		this.estimation = estimation;
	}
}

