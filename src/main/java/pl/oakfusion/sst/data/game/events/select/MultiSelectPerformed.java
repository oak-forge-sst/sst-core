package pl.oakfusion.sst.data.game.events.select;

import pl.oakfusion.sst.data.gamesession.events.GameSessionEvent;

import java.util.UUID;

public class MultiSelectPerformed extends GameSessionEvent {
	public MultiSelectPerformed(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
