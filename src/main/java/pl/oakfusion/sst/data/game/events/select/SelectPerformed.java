package pl.oakfusion.sst.data.game.events.select;

import pl.oakfusion.sst.data.game.events.GameEvent;

import java.util.UUID;

public class SelectPerformed extends GameEvent {
	public SelectPerformed(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
