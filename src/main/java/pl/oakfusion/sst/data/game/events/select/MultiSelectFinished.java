package pl.oakfusion.sst.data.game.events.select;

import pl.oakfusion.sst.data.gamesession.events.GameSessionEvent;

import java.util.UUID;

public class MultiSelectFinished extends GameSessionEvent {
	public MultiSelectFinished(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
