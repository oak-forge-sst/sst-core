package pl.oakfusion.sst.data.game.events.select;

import pl.oakfusion.sst.data.game.events.GameEvent;

import java.util.UUID;

public class MultiSelectChangeState extends GameEvent {
	public MultiSelectChangeState(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
