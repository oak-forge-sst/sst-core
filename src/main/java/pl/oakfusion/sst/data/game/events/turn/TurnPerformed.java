package pl.oakfusion.sst.data.game.events.turn;

import pl.oakfusion.sst.data.game.events.GameWarningEvent;

import java.util.UUID;

public class TurnPerformed extends GameWarningEvent {
	public TurnPerformed(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
