package pl.oakfusion.sst.data.game.commands.torpedo;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.game.commands.GameCommand;
import pl.oakfusion.sst.data.util.Position;

import java.util.List;
import java.util.UUID;

@Setter
@Getter
public class Torpedo extends GameCommand {
	private int torpedoAmountIntendedToShoot;
	private List<Position> target;

	public Torpedo(UUID gameSessionUuid, UUID playerUuid, int torpedoAmountIntendedToShoot, List<Position> target) {
		super(gameSessionUuid, playerUuid);
		this.torpedoAmountIntendedToShoot = torpedoAmountIntendedToShoot;
		this.target = target;
	}
}
