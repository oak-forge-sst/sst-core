package pl.oakfusion.sst.data.game;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.util.Position;
import pl.oakfusion.sst.data.world.component.ShieldsStatus;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
public class MoveEstimation {
	private List<Position> path;
	private final int warpFactor;
	private final double timeCostToLocation;
	private final double energyCostToLocation;
	private final double energyLeft;
	private final int actionPointsCostToLocation;
	private final double starDateOfArrivalToLocation;
	private final boolean isEnoughResources;
	private final ShieldsStatus shieldsStatus;
}

