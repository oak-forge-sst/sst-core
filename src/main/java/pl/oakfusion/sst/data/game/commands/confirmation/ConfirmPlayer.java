package pl.oakfusion.sst.data.game.commands.confirmation;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.game.commands.GameCommand;

import java.util.UUID;

@Getter
@Setter
public class ConfirmPlayer extends GameCommand {
	private String foundPlayer;
	public ConfirmPlayer (UUID gameSessionUuid, UUID playerUuid){
		super(gameSessionUuid, playerUuid);
	}
}
