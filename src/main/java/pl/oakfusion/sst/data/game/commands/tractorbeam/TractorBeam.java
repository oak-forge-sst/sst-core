package pl.oakfusion.sst.data.game.commands.tractorbeam;

import pl.oakfusion.sst.data.game.commands.GameCommand;

import java.util.UUID;

public class TractorBeam extends GameCommand {
	public TractorBeam(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
