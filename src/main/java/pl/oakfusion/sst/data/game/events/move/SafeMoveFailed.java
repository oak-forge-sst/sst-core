package pl.oakfusion.sst.data.game.events.move;

import pl.oakfusion.sst.data.validationfailures.ValidationFailure;

import java.util.List;

public class SafeMoveFailed extends ValidationFailure {

	public SafeMoveFailed(String errorMessage, List<String> tags) {
		super(errorMessage, tags);
	}
}
