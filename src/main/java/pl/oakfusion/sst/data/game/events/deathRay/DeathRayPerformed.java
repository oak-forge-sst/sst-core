package pl.oakfusion.sst.data.game.events.deathRay;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.DTO.DeadPlayersDTO;
import pl.oakfusion.sst.data.DTO.phasersDTO.PhasersUnitHitDTO;
import pl.oakfusion.sst.data.game.GameEventDead;
import pl.oakfusion.sst.data.game.events.GameEvent;

import java.util.List;
import java.util.UUID;

@Setter
@Getter
public class DeathRayPerformed extends GameEventDead {
	private List<PhasersUnitHitDTO> phasersUnitHitDTOS;
	private List<DeadPlayersDTO> deadPlayersDTO;

	public DeathRayPerformed(UUID gameSessionUuid, UUID playerUuid, List<PhasersUnitHitDTO> phasersUnitHitDTOS, List<DeadPlayersDTO> deadPlayersDTO) {
		super(gameSessionUuid, playerUuid);
		this.phasersUnitHitDTOS = phasersUnitHitDTOS;
		this.deadPlayersDTO = deadPlayersDTO;
	}
}
