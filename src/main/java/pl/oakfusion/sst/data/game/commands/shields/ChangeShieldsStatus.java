package pl.oakfusion.sst.data.game.commands.shields;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.world.component.ShieldsStatus;

import java.util.UUID;

@Getter
@Setter
public class ChangeShieldsStatus extends ShieldsCommand {
	private ShieldsStatus shieldsStatus;

	public ChangeShieldsStatus(UUID gameSessionUuid, UUID playerUuid, ShieldsStatus shieldsStatus) {
		super(gameSessionUuid, playerUuid);
		this.shieldsStatus = shieldsStatus;
	}
}
