package pl.oakfusion.sst.data.game.queries.shortrangescan;

import lombok.Getter;
import pl.oakfusion.sst.data.game.queries.GameQuery;

import java.io.Serializable;
import java.util.UUID;

@Getter
public class ShortRangeScan extends GameQuery implements Serializable {
	private boolean chartRequested;
	private boolean statusRequested;
	private boolean unitsInfoRequested;

	public ShortRangeScan(UUID gameSessionUuid, UUID playerUuid, boolean chartRequested, boolean statusRequested, boolean unitsInfoRequested) {
		super(gameSessionUuid, playerUuid);
		this.chartRequested = chartRequested;
		this.statusRequested = statusRequested;
		this.unitsInfoRequested = unitsInfoRequested;
	}
}
