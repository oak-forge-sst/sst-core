package pl.oakfusion.sst.data.game.commands.mayday;

import pl.oakfusion.sst.data.game.commands.GameCommand;

import java.util.UUID;

public class Mayday extends GameCommand {
	public Mayday(UUID gameSessionUuid, UUID playerUuid){
		super(gameSessionUuid, playerUuid);
	}
}
