package pl.oakfusion.sst.data.game.events.impulse;

import pl.oakfusion.sst.data.game.events.GameEvent;

import java.util.UUID;

public class ImpulsePerformed extends GameEvent {
	private boolean isAutoMode;
	private int x;
	private int y;
	private boolean isForced;


	public ImpulsePerformed(UUID gameSessionUuid, UUID playerUuid, int x, int y, boolean isInAutoMode, boolean isForced) {
		super(gameSessionUuid, playerUuid);
		this.isAutoMode = isInAutoMode;
		this.isForced = isForced;
		this.x = x;
		this.y = y;
	}
}
