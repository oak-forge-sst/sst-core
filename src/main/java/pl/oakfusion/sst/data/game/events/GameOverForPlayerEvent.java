package pl.oakfusion.sst.data.game.events;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.DTO.score.ScoreDTO;

import java.util.UUID;

@Getter
@Setter
public class GameOverForPlayerEvent extends GameWarningEvent {
	private ScoreDTO scoreDTO;
	private boolean lastPlayer;
	private String playersName;

	public GameOverForPlayerEvent(UUID gameSessionUuid, UUID playerUuid, ScoreDTO scoreDTO, boolean lastPlayer, String playersName) {
		super(gameSessionUuid, playerUuid);
		this.scoreDTO = scoreDTO;
		this.lastPlayer = lastPlayer;
		this.playersName = playersName;
	}
}
