package pl.oakfusion.sst.data.game.commands.warp;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.game.commands.GameCommand;

import java.util.UUID;

@Setter
@Getter
public class ChangeWarpFactor extends GameCommand {
	private int warpFactor;

	public ChangeWarpFactor(UUID gameSessionUuid, UUID playerUuid, int warpFactor) {
		super(gameSessionUuid, playerUuid);
		this.warpFactor = warpFactor;
	}
}
