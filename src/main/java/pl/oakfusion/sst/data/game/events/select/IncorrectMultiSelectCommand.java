package pl.oakfusion.sst.data.game.events.select;

import pl.oakfusion.sst.data.game.events.GameEvent;

import java.util.UUID;

public class IncorrectMultiSelectCommand extends GameEvent {
	public IncorrectMultiSelectCommand(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
