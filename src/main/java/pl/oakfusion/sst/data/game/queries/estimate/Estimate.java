package pl.oakfusion.sst.data.game.queries.estimate;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.game.queries.GameQuery;

import java.util.UUID;

@Getter
@Setter
public class Estimate extends GameQuery {
	private boolean autoMode;
	private int warpFactor;
	private int x;
	private int y;

	public Estimate(UUID gameSessionUuid, UUID playerUuid, boolean autoMode, int warpFactor, int x, int y) {
		super(gameSessionUuid, playerUuid);
		this.autoMode = autoMode;
		this.warpFactor = warpFactor;
		this.x = x;
		this.y = y;
	}
}
