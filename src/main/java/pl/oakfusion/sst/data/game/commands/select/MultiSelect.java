package pl.oakfusion.sst.data.game.commands.select;

import pl.oakfusion.sst.data.game.commands.GameCommand;

import java.util.UUID;

public abstract class MultiSelect extends GameCommand {
	public MultiSelect(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
