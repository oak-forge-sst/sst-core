package pl.oakfusion.sst.data.game.events.phasers;

import pl.oakfusion.sst.data.game.events.GameEvent;

import java.util.UUID;

public class PhasersFailed extends GameEvent {
	public PhasersFailed(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
