package pl.oakfusion.sst.data.game.queries.chart;

import pl.oakfusion.sst.data.game.queries.GameQuery;

import java.util.UUID;


public class Chart extends GameQuery {
	public Chart(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
