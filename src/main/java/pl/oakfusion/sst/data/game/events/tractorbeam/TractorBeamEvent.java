package pl.oakfusion.sst.data.game.events.tractorbeam;

import pl.oakfusion.sst.data.game.events.GameEvent;

import java.util.UUID;

public abstract class TractorBeamEvent extends GameEvent {
	public TractorBeamEvent(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
