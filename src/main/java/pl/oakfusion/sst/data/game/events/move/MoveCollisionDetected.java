package pl.oakfusion.sst.data.game.events.move;

import pl.oakfusion.sst.data.validationfailures.ValidationFailure;

import java.util.List;

public class MoveCollisionDetected extends ValidationFailure {

	public MoveCollisionDetected(String errorMessage, List<String> tags) {
		super(errorMessage, tags);
	}
}
