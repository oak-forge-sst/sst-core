package pl.oakfusion.sst.data.game.queries.damagereport;

import pl.oakfusion.sst.data.game.queries.GameQuery;

import java.util.UUID;

public class DamageReport extends GameQuery {
	public DamageReport(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
