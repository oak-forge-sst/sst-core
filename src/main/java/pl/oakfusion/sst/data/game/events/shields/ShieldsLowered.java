package pl.oakfusion.sst.data.game.events.shields;

import pl.oakfusion.sst.data.game.events.GameEvent;

import java.util.UUID;

public class ShieldsLowered extends GameEvent {
	public ShieldsLowered(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
