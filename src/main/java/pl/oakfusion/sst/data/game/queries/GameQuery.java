package pl.oakfusion.sst.data.game.queries;

import pl.oakfusion.sst.data.SstQuery;

import java.util.UUID;


public abstract class GameQuery extends SstQuery {
	public GameQuery(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
