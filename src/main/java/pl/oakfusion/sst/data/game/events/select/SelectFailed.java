package pl.oakfusion.sst.data.game.events.select;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.game.events.GameEvent;

import java.util.UUID;

@Setter
@Getter
public class SelectFailed extends GameEvent {
	private String name;

	public SelectFailed(UUID gameSessionUuid, UUID playerUuid, String name) {
		super(gameSessionUuid, playerUuid);
		this.name = name;
	}
}
