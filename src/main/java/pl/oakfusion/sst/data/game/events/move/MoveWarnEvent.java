package pl.oakfusion.sst.data.game.events.move;

import pl.oakfusion.sst.data.game.events.GameWarningEvent;

import java.util.UUID;

public class MoveWarnEvent extends GameWarningEvent {
	public MoveWarnEvent(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
