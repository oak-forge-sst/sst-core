package pl.oakfusion.sst.data.game.commands.rest;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.game.commands.GameCommand;

import java.util.UUID;

@Setter
@Getter
public class Rest extends GameCommand {
	private double actionPoints;

	public Rest(UUID gameSessionUuid, UUID playerUuid, double actionPoints) {
		super(gameSessionUuid, playerUuid);
		this.actionPoints = actionPoints;
	}
}
