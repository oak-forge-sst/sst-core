package pl.oakfusion.sst.data.game.commands.confirmation;

import lombok.Getter;
import pl.oakfusion.sst.data.game.commands.GameCommand;

import java.util.UUID;

@Getter
public abstract class Confirm extends GameCommand {
	public Confirm (UUID gameSessionUuid, UUID playerUuid){
		super(gameSessionUuid, playerUuid);
	}
}
