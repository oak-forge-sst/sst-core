package pl.oakfusion.sst.data.game.commands.select;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.game.commands.GameCommand;

import java.util.UUID;

@Getter
@Setter
public class Select extends GameCommand {
	private String name;
	private int index;
	private boolean byIndex;

	public Select(String name, UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
		this.name = name;
		this.index = -1;
		byIndex = false;
	}

	public Select(int index, UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
		this.name = "";
		this.index = index;
		byIndex = true;
	}
}
