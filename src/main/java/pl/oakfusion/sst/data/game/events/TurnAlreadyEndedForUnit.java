package pl.oakfusion.sst.data.game.events;

import java.util.UUID;

public class TurnAlreadyEndedForUnit extends GameEvent {
	public TurnAlreadyEndedForUnit(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
