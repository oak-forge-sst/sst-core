package pl.oakfusion.sst.data.game.queryresponses;


import pl.oakfusion.sst.data.SstQueryResponse;

import java.util.UUID;

public abstract class GameQueryResponse extends SstQueryResponse {
	public GameQueryResponse(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}

	public GameQueryResponse(UUID playerUuid) {
		super(playerUuid);
	}
}
