package pl.oakfusion.sst.data.game.events.mayday;

import pl.oakfusion.sst.data.game.events.GameEvent;

import java.util.UUID;

public class RadioBroken extends GameEvent {
	public RadioBroken(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
