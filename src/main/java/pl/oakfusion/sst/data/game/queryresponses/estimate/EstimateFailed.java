package pl.oakfusion.sst.data.game.queryresponses.estimate;

import pl.oakfusion.sst.data.game.queryresponses.GameQueryResponse;

import java.util.UUID;

public class EstimateFailed extends GameQueryResponse {
	public EstimateFailed(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}

