package pl.oakfusion.sst.data.game.events.mayday;


import pl.oakfusion.sst.data.game.events.GameEvent;

import java.util.UUID;

public class NoStarbasesAvailable extends GameEvent {
	public NoStarbasesAvailable(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
