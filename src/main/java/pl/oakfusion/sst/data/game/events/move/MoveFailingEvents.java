package pl.oakfusion.sst.data.game.events.move;

import pl.oakfusion.sst.data.game.events.GameEvent;

import java.util.UUID;

public class MoveFailingEvents extends GameEvent {
	public MoveFailingEvents(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
