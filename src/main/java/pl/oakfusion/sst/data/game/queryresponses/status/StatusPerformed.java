package pl.oakfusion.sst.data.game.queryresponses.status;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.DTO.statusDTO.StatusDTO;
import pl.oakfusion.sst.data.game.queryresponses.GameQueryResponse;

import java.util.UUID;

@Getter
@Setter
public class StatusPerformed extends GameQueryResponse {
	private StatusDTO statusDTO;

	public StatusPerformed(UUID gameSessionUuid, UUID playerUuid, StatusDTO statusDTO) {
		super(gameSessionUuid, playerUuid);
		this.statusDTO = statusDTO;
	}
}
