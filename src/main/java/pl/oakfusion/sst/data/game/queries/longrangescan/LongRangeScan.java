package pl.oakfusion.sst.data.game.queries.longrangescan;

import pl.oakfusion.sst.data.game.queries.GameQuery;

import java.util.UUID;

public class LongRangeScan extends GameQuery {
	public LongRangeScan(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
