package pl.oakfusion.sst.data.game.queryresponses.help;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Setter
@Getter
public class HelpPerformed extends HelpQueryResponse {
	private String commandParam;
	private String content;

	public HelpPerformed(UUID gameSessionUuid, UUID playerUuid, String commandParam, String content) {
		super(gameSessionUuid, playerUuid);
		this.commandParam = commandParam;
		this.content = content;
	}
}
