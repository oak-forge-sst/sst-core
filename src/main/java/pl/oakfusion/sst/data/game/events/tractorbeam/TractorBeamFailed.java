package pl.oakfusion.sst.data.game.events.tractorbeam;

import java.util.UUID;

public class TractorBeamFailed extends TractorBeamEvent {
	public TractorBeamFailed(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
