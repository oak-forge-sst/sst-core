package pl.oakfusion.sst.data.game.events.energy;

import pl.oakfusion.sst.data.game.events.GameEvent;

import java.util.UUID;

public class EnergyTransferFailed extends GameEvent {
	public EnergyTransferFailed(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
