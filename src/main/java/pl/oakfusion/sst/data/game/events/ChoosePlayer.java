package pl.oakfusion.sst.data.game.events;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Setter
@Getter
public class ChoosePlayer extends GameWarningEvent {
	private List<String> players;

	public ChoosePlayer(UUID gameSessionUuid, UUID playerUuid, List<String> players) {
		super(gameSessionUuid, playerUuid);
		this.players = players;
	}
}
