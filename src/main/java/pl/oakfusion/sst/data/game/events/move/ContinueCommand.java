package pl.oakfusion.sst.data.game.events.move;

import pl.oakfusion.sst.data.game.commands.confirmation.Confirm;

import java.util.UUID;

public class ContinueCommand extends Confirm {
	public ContinueCommand(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
