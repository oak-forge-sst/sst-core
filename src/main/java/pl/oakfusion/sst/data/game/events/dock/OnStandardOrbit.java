package pl.oakfusion.sst.data.game.events.dock;

import java.util.UUID;

public class OnStandardOrbit extends DockEvent {
	public OnStandardOrbit(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
