package pl.oakfusion.sst.data.game.events.phasers;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.DTO.DeadPlayersDTO;
import pl.oakfusion.sst.data.DTO.phasersDTO.PhasersDTO;
import pl.oakfusion.sst.data.game.GameEventDead;
import pl.oakfusion.sst.data.game.events.GameEvent;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class PhasersPerformed extends GameEventDead {
	private PhasersDTO phasersDTO;
	private List<DeadPlayersDTO> deadPlayersDTO;

	public PhasersPerformed(UUID gameSessionUuid, UUID playerUuid, PhasersDTO phasersDTO, List<DeadPlayersDTO> deadPlayersDTO) {
		super(gameSessionUuid, playerUuid);
		this.phasersDTO = phasersDTO;
		this.deadPlayersDTO = deadPlayersDTO;
	}
}
