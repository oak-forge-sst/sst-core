package pl.oakfusion.sst.data.game.events;

import pl.oakfusion.sst.data.SstEvent;

import java.util.UUID;


public abstract class GameEvent extends SstEvent {
	public GameEvent(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}

