package pl.oakfusion.sst.data.game.commands.confirmation;

import java.util.UUID;

public class AcceptCommand extends Confirm {
	public AcceptCommand(UUID gameSessionUuid, UUID playerUuid){
		super(gameSessionUuid, playerUuid);
	}
}
