package pl.oakfusion.sst.data.game.events;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.world.component.ComponentId;

import java.util.UUID;

@Setter
@Getter
public class ComponentCommandFailed extends GameEvent {
	private ComponentId componentId;
	private UUID unitUuid;

	public ComponentCommandFailed(UUID gameSessionUuid, UUID playerUuid, ComponentId componentId, UUID unitUuid) {
		super(gameSessionUuid, playerUuid);
		this.componentId = componentId;
		this.unitUuid = unitUuid;
	}

}
