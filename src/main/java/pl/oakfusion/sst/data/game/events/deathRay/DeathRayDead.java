package pl.oakfusion.sst.data.game.events.deathRay;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.DTO.DeadPlayersDTO;
import pl.oakfusion.sst.data.game.GameEventDead;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Setter
@Getter
public class DeathRayDead extends GameEventDead {
	private DeadPlayersDTO deadPlayerDTO;

	@JsonIgnore
	@Override
	public List<DeadPlayersDTO> getDeadPlayersDTO() {
		return Collections.singletonList(deadPlayerDTO);
	}

	public DeathRayDead(UUID gameSessionUuid, UUID playerUuid, DeadPlayersDTO deadPlayerDTO) {
		super(gameSessionUuid, playerUuid);
		this.deadPlayerDTO = deadPlayerDTO;
	}
}
