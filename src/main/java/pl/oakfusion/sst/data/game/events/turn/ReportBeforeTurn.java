package pl.oakfusion.sst.data.game.events.turn;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.DTO.DeadPlayersDTO;
import pl.oakfusion.sst.data.DTO.ReportDTO;
import pl.oakfusion.sst.data.game.events.GameWarningEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class ReportBeforeTurn extends GameWarningEvent {
	private List<ReportDTO> report = new ArrayList<>();
	private List<DeadPlayersDTO> reportAboutDeadPlayers = new ArrayList<>();
	private String name;

	public void addElementToReport(ReportDTO reportDTO) {
		report.add(reportDTO);
	}

	public void addElementToDeadPlayers(DeadPlayersDTO deadPlayersDTO) {
		reportAboutDeadPlayers.add(deadPlayersDTO);
	}

	public ReportBeforeTurn(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
