package pl.oakfusion.sst.data.game.events.shields;

import pl.oakfusion.sst.data.game.events.GameEvent;

import java.util.UUID;

public class ShieldsRisen extends GameEvent {
	public ShieldsRisen(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
