package pl.oakfusion.sst.data.game.queryresponses.score;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.DTO.score.ScoreDTO;
import pl.oakfusion.sst.data.game.queryresponses.GameQueryResponse;

import java.util.UUID;

@Setter
@Getter
public class ScorePerformed extends GameQueryResponse {
	private ScoreDTO scoreDTO;

	public ScorePerformed(UUID gameSessionUuid, UUID playerUuid, ScoreDTO scoreDTO) {
		super(gameSessionUuid, playerUuid);
		this.scoreDTO = scoreDTO;
	}
}
