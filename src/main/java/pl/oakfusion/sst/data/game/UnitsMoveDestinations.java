package pl.oakfusion.sst.data.game;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.oakfusion.sst.data.util.Position;
import pl.oakfusion.sst.data.world.civilizations.Unit;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UnitsMoveDestinations {
	private List<UnitMoveDestination> destinationsList = new ArrayList<>();

	public boolean isEmpty(){
		return destinationsList.isEmpty();
	}

	public void addDestination(UnitMoveDestination unitMoveDestination){
		if(!containsDestination(unitMoveDestination.getDestination()) && !containsUnit(unitMoveDestination.getUnit())){
			destinationsList.add(unitMoveDestination);
		}
	}

	public void addAll(UnitsMoveDestinations unitsMoveDestinations){
		unitsMoveDestinations.getDestinationsList().forEach(this::addDestination);
	}

	public Optional<UnitMoveDestination> getDestinationByUnit(Unit unit){
		AtomicReference<UnitMoveDestination> destination = new AtomicReference<>(null);
		destinationsList.stream().filter(unitMoveDestination -> unitMoveDestination.getUnit().getUuid().equals(unit.getUuid())).findAny()
				.ifPresent(destination::set);
		return Optional.ofNullable(destination.get());
	}

	public boolean canAllUnitsReachDestinations(){
		AtomicBoolean result = new AtomicBoolean(true);
		if(destinationsList.isEmpty()){
			result.set(false);
		}
		destinationsList.forEach(unitMoveDestination -> {
			if(!unitMoveDestination.isAbleToReachDestination()){
				result.set(false);
			}
		});
		return result.get();
	}

	public boolean containsDestination(Position destination){
		AtomicBoolean result = new AtomicBoolean(false);
		destinationsList.forEach(unitMoveDestination -> {
			if(unitMoveDestination.getDestination().getX() == destination.getX() && unitMoveDestination.getDestination().getY() == destination.getY()){
				result.set(true);
			}
		});
		return result.get();
	}

	public boolean containsUnit(Unit unit){
		AtomicBoolean result = new AtomicBoolean(false);
		destinationsList.forEach(unitMoveDestination -> {
			if(unitMoveDestination.getUnit().getUuid().equals(unit.getUuid())){
				result.set(true);
			}
		});
		return result.get();
	}
}
