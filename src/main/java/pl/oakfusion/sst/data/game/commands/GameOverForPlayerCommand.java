package pl.oakfusion.sst.data.game.commands;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class GameOverForPlayerCommand extends GameCommand {
	private boolean lastPlayer;

	public GameOverForPlayerCommand(UUID gameSessionUuid, UUID playerUuid, boolean lastPlayer) {
		super(gameSessionUuid, playerUuid);
		this.lastPlayer = lastPlayer;
	}
}
