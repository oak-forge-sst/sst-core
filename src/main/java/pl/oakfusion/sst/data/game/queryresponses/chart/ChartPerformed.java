package pl.oakfusion.sst.data.game.queryresponses.chart;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.DTO.ChartDTO;
import pl.oakfusion.sst.data.game.queryresponses.GameQueryResponse;

import java.util.UUID;

@Setter
@Getter
public class ChartPerformed extends GameQueryResponse {
	private  ChartDTO chart;

	public ChartPerformed(UUID gameSessionUuid, UUID playerUuid, ChartDTO chart) {
		super(gameSessionUuid, playerUuid);
		this.chart = chart;
	}
}
