package pl.oakfusion.sst.data.game.commands.select;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Getter
@Setter

public class MultiSelectNames extends MultiSelect {
	private List<String> names;

	public MultiSelectNames(UUID gameSessionUuid, UUID playerUuid, List<String> names) {
		super(gameSessionUuid, playerUuid);
		this.names = names;
	}
}
