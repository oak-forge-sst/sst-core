package pl.oakfusion.sst.data.game.events.move;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.game.UnitsMoveDestinations;
import pl.oakfusion.sst.data.game.events.GameEvent;

import java.util.UUID;

@Setter
@Getter
public class MovePerformed extends GameEvent {
	private boolean isAutoMode;
	private boolean isForceMode;
	private UnitsMoveDestinations unitsMoveDestinations;

	public MovePerformed(UUID gameSessionUuid, UUID playerUuid, boolean isAutoMode, boolean isForceMode, UnitsMoveDestinations unitsMoveDestinations) {
		super(gameSessionUuid, playerUuid);
		this.isAutoMode = isAutoMode;
		this.isForceMode = isForceMode;
		this.unitsMoveDestinations = unitsMoveDestinations;
	}

}
