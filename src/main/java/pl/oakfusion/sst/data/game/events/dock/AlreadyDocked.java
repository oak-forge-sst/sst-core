package pl.oakfusion.sst.data.game.events.dock;

import java.util.UUID;

public class AlreadyDocked extends DockEvent {
	public AlreadyDocked(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
