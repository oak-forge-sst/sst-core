package pl.oakfusion.sst.data.game.commands.phasers;

import java.util.UUID;

public class AskForOpponentsInRange extends PhaserCommand {
	public AskForOpponentsInRange(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
