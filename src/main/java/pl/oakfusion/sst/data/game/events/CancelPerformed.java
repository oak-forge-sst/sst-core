package pl.oakfusion.sst.data.game.events;

import java.util.UUID;

public class CancelPerformed extends GameEvent {
	public CancelPerformed(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
