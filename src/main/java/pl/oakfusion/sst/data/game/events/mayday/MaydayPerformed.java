package pl.oakfusion.sst.data.game.events.mayday;

import java.util.UUID;

public class MaydayPerformed extends MaydayEvent {

	private String content;

	public MaydayPerformed(UUID gameSessionUuid, UUID playerUuid, String content) {
		super(gameSessionUuid, playerUuid);
		this.content = content;
	}

	public MaydayPerformed(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}

	public String getContent() {
		return content;
	}
}
