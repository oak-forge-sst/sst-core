package pl.oakfusion.sst.data.game.queries.score;

import pl.oakfusion.sst.data.game.queries.GameQuery;

import java.util.UUID;

public class Score extends GameQuery {
	public Score(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
