package pl.oakfusion.sst.data.game.events.warp;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.game.events.GameEvent;
import pl.oakfusion.sst.data.messagegenerator.SystemMessage;

import java.util.UUID;

@Setter
@Getter
public class WarpFactorChanged extends GameEvent {

	private int warp;
	private SystemMessage systemMessage;

	public WarpFactorChanged(UUID gameSessionUuid, UUID playerUuid, int warp, SystemMessage systemMessage) {
		super(gameSessionUuid, playerUuid);
		this.warp = warp;
		this.systemMessage = systemMessage;
	}
}
