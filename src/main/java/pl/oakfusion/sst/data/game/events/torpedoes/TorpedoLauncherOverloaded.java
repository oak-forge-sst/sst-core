package pl.oakfusion.sst.data.game.events.torpedoes;

import pl.oakfusion.sst.data.validationfailures.ValidationFailure;

import java.util.List;

public class TorpedoLauncherOverloaded extends ValidationFailure {

	public TorpedoLauncherOverloaded(String errorMessage, List<String> tags) {
		super(errorMessage, tags);

	}
}
