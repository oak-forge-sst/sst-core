package pl.oakfusion.sst.data.game.events.phasers;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.DTO.phasersDTO.OpponentUnitDTO;
import pl.oakfusion.sst.data.game.events.GameWarningEvent;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class OpponentsInRangeInfoEvent extends GameWarningEvent {
	private List<OpponentUnitDTO> opponentUnitDTOList;

	public OpponentsInRangeInfoEvent(UUID gameSessionUuid, UUID playerUuid, List<OpponentUnitDTO> opponentUnitDTOList) {
		super(gameSessionUuid, playerUuid);
		this.opponentUnitDTOList = opponentUnitDTOList;
	}
}
