package pl.oakfusion.sst.data.game.commands.phasers;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.DTO.phasersDTO.OpponentUnitDTO;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class Phasers extends PhaserCommand {
	private List<OpponentUnitDTO> units;
	private List<Integer> phaserFactor;
	private boolean auto;

	public Phasers(UUID gameSessionUuid, UUID playerUuid, List<OpponentUnitDTO> units, List<Integer> phaserFactor, boolean auto) {
		super(gameSessionUuid, playerUuid);
		this.units = units;
		this.phaserFactor = phaserFactor;
		this.auto = auto;
	}
}
