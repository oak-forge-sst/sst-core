package pl.oakfusion.sst.data.game.events.select;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.game.events.GameEvent;

import java.util.List;
import java.util.UUID;

@Setter
@Getter
public class MultiSelectIncorrectNamesFail extends GameEvent {
	private List<String> nonexistentNames;

	public MultiSelectIncorrectNamesFail(UUID gameSessionUuid, UUID playerUuid, List<String> nonexistentNames) {
		super(gameSessionUuid, playerUuid);
		this.nonexistentNames = nonexistentNames;
	}
}
