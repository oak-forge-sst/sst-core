package pl.oakfusion.sst.data.game.commands.confirmation;


import java.util.UUID;

public class DeclineCommand extends Confirm {
	public DeclineCommand (UUID gameSessionUuid, UUID playerUuid){
		super(gameSessionUuid, playerUuid);
	}
}
