package pl.oakfusion.sst.data.game.queries.help;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.game.queries.GameQuery;

import java.util.UUID;

@Getter
@Setter
public class Help extends GameQuery {
	private String commandParam;

	public Help(UUID gameSessionUuid, UUID playerUuid, String commandParam) {
		super(gameSessionUuid, playerUuid);
		this.commandParam = commandParam;
	}
}
