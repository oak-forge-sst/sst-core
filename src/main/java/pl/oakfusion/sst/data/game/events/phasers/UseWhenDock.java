package pl.oakfusion.sst.data.game.events.phasers;

import pl.oakfusion.sst.data.game.events.GameEvent;

import java.util.UUID;

public class UseWhenDock extends GameEvent {
	public UseWhenDock(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
