package pl.oakfusion.sst.data.game.queries.status;

import pl.oakfusion.sst.data.game.queries.GameQuery;

import java.util.UUID;

public class Status extends GameQuery {
	public Status(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
