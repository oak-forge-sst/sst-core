package pl.oakfusion.sst.data.game.commands.shields;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class TransferShieldsEnergy extends ShieldsCommand {
	private double shieldsEnergy;

	public TransferShieldsEnergy(UUID gameSessionUuid, UUID playerUuid, double shieldsEnergy) {
		super(gameSessionUuid, playerUuid);
		this.shieldsEnergy = shieldsEnergy;
	}
}
