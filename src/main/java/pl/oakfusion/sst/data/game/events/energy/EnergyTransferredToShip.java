package pl.oakfusion.sst.data.game.events.energy;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.game.events.GameEvent;

import java.util.UUID;

@Setter
@Getter
public class EnergyTransferredToShip extends GameEvent {
    private double energy;

	public EnergyTransferredToShip(UUID gameSessionUuid, UUID playerUuid, double energy) {
		super(gameSessionUuid, playerUuid);
		this.energy = energy;
	}
}
