package pl.oakfusion.sst.data.game.events.warp;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.game.events.GameEvent;
import pl.oakfusion.sst.data.messagegenerator.SystemMessage;

import java.util.UUID;

@Getter
@Setter
public class ChangeWarpFactorFailed extends GameEvent {

	private int warp;
	private SystemMessage systemMessage;
	private boolean isAboveMax;

	public ChangeWarpFactorFailed(UUID gameSessionUuid, UUID playerUuid, int warp, SystemMessage systemMessage) {
		super(gameSessionUuid, playerUuid);
		this.warp = warp;
		this.systemMessage = systemMessage;
		this.isAboveMax = false;
	}
}
