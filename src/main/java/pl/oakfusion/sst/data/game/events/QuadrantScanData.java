package pl.oakfusion.sst.data.game.events;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Optional;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class QuadrantScanData {

	private Integer klingons;
	private Integer starbases;
	private Integer stars;
	private Integer supernovaThreat;

}
