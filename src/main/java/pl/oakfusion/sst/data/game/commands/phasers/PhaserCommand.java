package pl.oakfusion.sst.data.game.commands.phasers;

import pl.oakfusion.sst.data.game.commands.GameCommand;

import java.util.UUID;

public abstract class PhaserCommand extends GameCommand {
	public PhaserCommand(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
