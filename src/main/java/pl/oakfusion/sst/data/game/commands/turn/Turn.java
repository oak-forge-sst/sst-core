package pl.oakfusion.sst.data.game.commands.turn;

import pl.oakfusion.sst.data.game.commands.GameCommand;

import java.util.UUID;

public class Turn extends GameCommand {
	public Turn(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
