package pl.oakfusion.sst.data.game.events.torpedoes;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.validationfailures.ValidationFailure;

import java.util.List;


@Setter
@Getter
public class TorpedoesLessThanIntendedToShot extends ValidationFailure {
	private int haveTorpedo;

	public TorpedoesLessThanIntendedToShot(String errorMessage, List<String> tags, int haveTorpedo) {
		super(errorMessage, tags);
		this.haveTorpedo = haveTorpedo;
	}
}
