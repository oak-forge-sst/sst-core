package pl.oakfusion.sst.data.game.queryresponses.shortrangescan;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.DTO.shortRangeScanDTO.ShortRangeScanDTO;
import pl.oakfusion.sst.data.game.queryresponses.GameQueryResponse;

import java.util.UUID;

@Setter
@Getter
public class ShortRangeScanPerformed extends GameQueryResponse {
	private ShortRangeScanDTO shortRangeScan;

	public ShortRangeScanPerformed(UUID gameSessionUuid, UUID playerUuid, ShortRangeScanDTO shortRangeScan) {
		super(gameSessionUuid, playerUuid);
		this.shortRangeScan = shortRangeScan;
	}
}

