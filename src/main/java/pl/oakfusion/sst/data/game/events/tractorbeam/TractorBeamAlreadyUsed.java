package pl.oakfusion.sst.data.game.events.tractorbeam;

import java.util.UUID;

public class TractorBeamAlreadyUsed extends TractorBeamEvent {
	public TractorBeamAlreadyUsed(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}
}
