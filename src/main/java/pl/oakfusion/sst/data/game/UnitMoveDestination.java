package pl.oakfusion.sst.data.game;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.game.MoveEstimation;
import pl.oakfusion.sst.data.util.Position;
import pl.oakfusion.sst.data.world.civilizations.Unit;

@Getter
@Setter
@AllArgsConstructor
public class UnitMoveDestination {
	private Unit unit;
	private Position destination;
	private MoveEstimation estimation;
	private boolean ableToReachDestination;
}
