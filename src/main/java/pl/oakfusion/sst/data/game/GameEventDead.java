package pl.oakfusion.sst.data.game;

import pl.oakfusion.sst.data.DTO.DeadPlayersDTO;
import pl.oakfusion.sst.data.game.events.GameEvent;
import pl.oakfusion.sst.data.game.events.GameWarningEvent;

import java.util.List;
import java.util.UUID;

public abstract class GameEventDead extends GameWarningEvent {
	public GameEventDead(UUID gameSessionUuid, UUID playerUuid) {
		super(gameSessionUuid, playerUuid);
	}

	public abstract List<DeadPlayersDTO> getDeadPlayersDTO();
}
