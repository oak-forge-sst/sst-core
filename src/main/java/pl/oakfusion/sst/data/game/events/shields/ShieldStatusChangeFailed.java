package pl.oakfusion.sst.data.game.events.shields;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.game.events.GameEvent;
import pl.oakfusion.sst.data.world.component.ShieldsStatus;

import java.util.UUID;

@Setter
@Getter
public class ShieldStatusChangeFailed extends GameEvent {

	private ShieldsStatus status;

	public ShieldStatusChangeFailed(UUID gameSessionUuid, UUID playerUuid, ShieldsStatus status) {
		super(gameSessionUuid, playerUuid);
		this.status = status;
	}
}

