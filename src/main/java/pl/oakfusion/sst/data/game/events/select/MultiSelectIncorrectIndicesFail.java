package pl.oakfusion.sst.data.game.events.select;

import lombok.Getter;
import lombok.Setter;
import pl.oakfusion.sst.data.game.events.GameEvent;

import java.util.List;
import java.util.UUID;

@Setter
@Getter
public class MultiSelectIncorrectIndicesFail extends GameEvent {
	private List<Integer> nonexistentIndices;

	public MultiSelectIncorrectIndicesFail(UUID gameSessionUuid, UUID playerUuid, List<Integer> nonexistentIndices) {
		super(gameSessionUuid, playerUuid);
		this.nonexistentIndices = nonexistentIndices;
	}
}
