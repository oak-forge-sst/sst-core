package pl.oakfusion.sst.data.configuration;

import lombok.*;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class WorldSpecificationConfiguration {
	private int widthInQuadrants;
	private int heightInQuadrants;
	private int quadrantSize;

}
