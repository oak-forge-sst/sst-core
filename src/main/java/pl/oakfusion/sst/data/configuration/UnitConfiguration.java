package pl.oakfusion.sst.data.configuration;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;
import pl.oakfusion.sst.data.world.component.ComponentId;
import pl.oakfusion.sst.data.world.component.Condition;
import pl.oakfusion.sst.data.world.civilizations.UnitType;
import pl.oakfusion.sst.data.world.component.Component;
import pl.oakfusion.sst.data.world.component.ComponentSystemDeserializer;

import java.util.Map;
import java.util.UUID;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UnitConfiguration {
	private int positionX;
	private int positionY;
	private double actionPoints;
	private Condition condition;
	private UUID uuid;
	private UUID playerUUID;
	private UnitType unitType;
@JsonDeserialize(using = ComponentSystemDeserializer.class)
	private Map<ComponentId, Component> components;
	private String name;
}
