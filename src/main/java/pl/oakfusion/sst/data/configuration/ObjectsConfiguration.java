package pl.oakfusion.sst.data.configuration;

import lombok.*;
import pl.oakfusion.sst.data.parameter.ObjectType;
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ObjectsConfiguration {
	private int positionX;
	private int positionY;
	private ObjectType objectType;
}
