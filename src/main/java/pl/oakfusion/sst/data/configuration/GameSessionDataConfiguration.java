package pl.oakfusion.sst.data.configuration;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.oakfusion.sst.data.world.gameobject.GameObject;
import pl.oakfusion.sst.data.parameter.GameType;
import pl.oakfusion.sst.data.world.StarDate;

import java.util.List;
import java.util.UUID;

@Builder
@AllArgsConstructor
@Getter
@NoArgsConstructor
public class GameSessionDataConfiguration {
private	List<UnitConfiguration> unitConfigurations;
private	WorldSpecificationConfiguration worldSpecificationConfiguration;
private	StarDate stardate;
private	List<GameObject> objectsConfiguration;
private	List<PlayerConfiguration> playerConfigurationList;
private UUID activePlayer;
private GameType gameType;

}
