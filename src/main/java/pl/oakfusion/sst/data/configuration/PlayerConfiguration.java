package pl.oakfusion.sst.data.configuration;

import lombok.*;
import pl.oakfusion.sst.data.world.player.CivilizationType;
import pl.oakfusion.sst.data.world.player.PlayerType;

import java.util.UUID;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PlayerConfiguration {
	UUID uuid;
	CivilizationType civilizationType;
	String name;
	PlayerType playerType;
	boolean gameMaster;
}
