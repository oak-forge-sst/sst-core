package pl.oakfusion.sst.data.util;

public class Types {
    public static boolean isInteger(String string) {
        return string.matches("-?\\d+");
    }

    public static boolean isDouble(String string) {
        return string.matches("-?\\d+(.\\d+)?");
    }

}
