package pl.oakfusion.sst.data.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Precision {

	public double setPrecision(double value,int precision) {
		value = BigDecimal.valueOf(value).setScale(precision, RoundingMode.HALF_UP).doubleValue();
		return value;
	}
}
