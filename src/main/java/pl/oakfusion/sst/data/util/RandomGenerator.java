package pl.oakfusion.sst.data.util;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import static java.util.Collections.emptyList;

public class RandomGenerator {

    private final Random random;

    public RandomGenerator() {
        this(new Random());
    }

    public RandomGenerator(long seed) {
        this(new Random(seed));
    }

    public RandomGenerator(Random random) {
        this.random = random;
    }

    public int nextIntInRange(int min, int max) {

        return random.nextInt((max - min) + 1) + min;
    }

    public int nextInt(int bound) {
        return random.nextInt(bound);
    }

    public double nextDoubleInRange(double min, double max) {
        return min + (max - min) * random.nextDouble();
    }

    public <T> Collection<T> shuffle(Collection<T> list) {

        if (list == null){
            return emptyList();
        }

        List<T> temporaryComponent = new LinkedList<>(list);
        List<T> randomOrderComponentList = new LinkedList<>();
        int i;

        while(! temporaryComponent.isEmpty() ) {
            i = random.nextInt(temporaryComponent.size()) ;
            randomOrderComponentList.add(temporaryComponent.get(i));
            temporaryComponent.remove(i);
        }

        return randomOrderComponentList;
    }

}
