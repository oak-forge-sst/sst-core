package pl.oakfusion.sst.data.util.repository;


import pl.oakfusion.sst.data.world.gameobject.GameObject;
import pl.oakfusion.sst.data.util.Position;
import pl.oakfusion.sst.data.world.civilizations.Unit;

import java.util.List;
import java.util.Optional;

public interface GameObjectsRepository {

	static GameObjectsRepository unmodifiableRepository(GameObjectsRepository gameObjectsRepository) {
		return new UnmodifiableGameObjectsRepository(gameObjectsRepository);
	}

	Optional<GameObject> get(int x, int y);

	void add(GameObject gameObject);

	void remove(GameObject gameObject);

	void remove(int x, int y);

	List<GameObject> getGameObjectsInWorld();

	List<GameObject> getInRange(int x, int y, int range);

	List<GameObject> getInLine(int startX, int startY, int endX, int endY, int range);

	List<Position> findShortestPath(Position source, Position target, List<Unit> unitsSafeToMoveThrough);

	List<Position> findPathInEmptyWorld(Position source, Position target);

}
