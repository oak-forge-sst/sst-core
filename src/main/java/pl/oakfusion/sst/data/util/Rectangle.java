package pl.oakfusion.sst.data.util;

import static com.google.common.base.Preconditions.checkArgument;

public class Rectangle {

    private final int x;
    private final int y;
    private final int width;
    private final int height;

    public Rectangle() {
        this(0, 0, 0, 0);
    }

    public Rectangle(int x, int y, int width, int height) {
        checkArgument(x >= 0, "negative x position: %s", x);
        checkArgument(y >= 0, "negative y position: %s", y);
        checkArgument(width >= 0, "negative width: %s", width);
        checkArgument(height >= 0, "negative height: %s", height);
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public String toString() {
        return "Rectangle{" +
                "x=" + x +
                ", y=" + y +
                ", width=" + width +
                ", height=" + height +
                '}';
    }
}
