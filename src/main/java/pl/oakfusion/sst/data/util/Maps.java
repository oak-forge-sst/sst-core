package pl.oakfusion.sst.data.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Maps {
    public static <K, V> Map<K, V> reduceJoin(Map<List<K>, V> map) {
        return map.entrySet()
                  .stream()
                  .map(e -> e.getKey()
                             .stream()
                             .distinct()
                             .collect(Collectors.toMap(Function.identity(), c -> e.getValue())))
                  .reduce(Maps::join)
                  .orElse(new HashMap<>());
    }

    private static <K, V> Map<K, V> join(Map<K, V> map1, Map<K, V> map2) {
        map1.putAll(map2);
        return map1;
    }
}
