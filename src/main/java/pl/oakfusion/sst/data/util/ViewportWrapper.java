package pl.oakfusion.sst.data.util;

public class ViewportWrapper {

    private final Viewport viewport;
    private final int offsetX;
    private final int offsetY;

    public ViewportWrapper(Viewport viewport, int offsetX, int offsetY) {
        this.viewport = viewport;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
    }

    public int getOffsetX() {
        return offsetX;
    }

    public int getOffsetY() {
        return offsetY;
    }

    public Viewport getViewport() {
        return viewport;
    }

}
