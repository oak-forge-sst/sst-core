package pl.oakfusion.sst.data.util;

import static com.google.common.base.Preconditions.checkArgument;

public class SpacedViewport extends Viewport {

    private final int columnSpacing;
    private final int rowSpacing;

    public SpacedViewport(int width, int height, int spacing) {
        this(width, height, spacing, spacing);
    }

    public SpacedViewport(int width, int height, int columnSpacing, int rowSpacing) {
        this(width, height, DEFAULT_FILL_CHARACTER, columnSpacing, rowSpacing);
    }

    public SpacedViewport(int width, int height, char fillCharacter, int spacing) {
        this(width, height, fillCharacter, spacing, spacing);
    }

    public SpacedViewport(int width, int height, char fillCharacter, int columnSpacing, int rowSpacing) {
        super(width + (width * columnSpacing) - columnSpacing, height + (height * rowSpacing) - rowSpacing, fillCharacter);
        checkArgument(columnSpacing >= 0);
        checkArgument(rowSpacing >= 0);
        this.columnSpacing = columnSpacing;
        this.rowSpacing = rowSpacing;
    }

    @Override
    public char get(int x, int y) {
        return getAbsolute(x * (1 + columnSpacing), y * (1 + rowSpacing));
    }

    @Override
    public void set(int x, int y, char character) {
        setAbsolute(x * (1 + columnSpacing), y * (1 + rowSpacing), character);
    }
    public void setMiddleSpacing(int x, int y, char character) {
        setAbsolute(x * (1 + columnSpacing) - (1 + columnSpacing) / 2, y * (1 + rowSpacing) - (1 + rowSpacing) / 2, character);
    }
    @Override
    public void setColumn(int column, char fillCharacter) {
        super.setColumn(column * (1 + columnSpacing), fillCharacter);
    }

    @Override
    public void setRow(int row, char fillCharacter) {
        super.setRow(row * (1 + rowSpacing), fillCharacter);
    }
    public void setColumnPlusSpacing(int column, int startY, int endY, char fillCharacter) {

        for (int y = startY; y <= endY; y++) {
            setAbsolute(column, y, fillCharacter);
        }


    }
    public void setRowPlusSpacing(int row, int startX, int endX,  char fillCharacter) {

        for (int x = startX; x <= endX; x++) {
            setAbsolute(x, row, fillCharacter);
        }

    }

}
