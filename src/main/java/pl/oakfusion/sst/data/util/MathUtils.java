package pl.oakfusion.sst.data.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;
import java.util.Optional;

public class MathUtils {

    @SafeVarargs
    public static <T extends Comparable<T>> Optional<T> max(T... numbers) {
        return Arrays.stream(numbers).reduce(MathUtils::max);
    }

    private static <T extends Comparable<T>> T max(T t1, T t2) {
        int comparisonResult = t1.compareTo(t2);
        if (comparisonResult > 0) {
            return t1;
        }
        if (comparisonResult < 0) {
            return t2;
        }
        return t1;
    }

    @SafeVarargs
    public static <T extends Comparable<T>> Optional<T> min(T... numbers) {
        return Arrays.stream(numbers).reduce(MathUtils::min);
    }

    private static <T extends Comparable<T>> T min(T t1, T t2) {
        int comparisonResult = t1.compareTo(t2);
        if (comparisonResult > 0) {
            return t2;
        }
        if (comparisonResult < 0) {
            return t1;
        }
        return t1;
    }

    public static Character getDigit(int digit) {
        return Character.forDigit(digit, 10);
    }

    public static int getDigitCount(int number) {
        return (int) (Math.log10(number) + 1);
    }

    public static double round(double number, int precision) {
		return Double.parseDouble(new BigDecimal(String.valueOf(number)).setScale(precision, RoundingMode.HALF_EVEN).toString());
	}
}
