package pl.oakfusion.sst.data.util;

import java.util.Arrays;

import static com.google.common.base.Preconditions.checkArgument;
import static java.lang.Math.max;

public class Viewport {

    protected final static char DEFAULT_FILL_CHARACTER = ' ';

    private final int width;
    private final int height;
    private final char[] view;

    public Viewport(int width, int height) {
        this(width, height, DEFAULT_FILL_CHARACTER);
    }

    public Viewport(int width, int height, char fillCharacter) {
        checkArgument(width > 0, "negative or 0 width value: %s", width);
        checkArgument(height > 0, "negative or 0 height value: %s", height);
        this.width = width;
        this.height = height;
        this.view = new char[width * height];
        fillView(fillCharacter);
    }

    public Viewport(char[][] characters) {
        this(characters, DEFAULT_FILL_CHARACTER);
    }

    public Viewport(char[][] characters, char fillCharacter) {
        checkArgument(characters != null, "input characters array cannot be null");
        this.width = Arrays.stream(characters).mapToInt(chars -> chars.length).max().orElse(0);
        this.height = characters.length;
        this.view = new char[width * height];
        fillView(characters, fillCharacter);
    }

    private void fillView(char[][] characters, char fillCharacter) {
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                char[] line = characters[y];
                if (x < line.length) {
                    set(x, y, line[x]);
                } else {
                    set(x, y, fillCharacter);
                }
            }
        }
    }

    private void fillView(char fillCharacter) {
        for (int i = 0; i < view.length; i++) {
            view[i] = fillCharacter;
        }
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public char get(int x, int y) {
        return getAbsolute(x, y);
    }

    protected final char getAbsolute(int x, int y) {
        return view[x + y * width];
    }

    public void set(int x, int y, char character) {
        setAbsolute(x, y, character);
    }

    public final void setAbsolute(int x, int y, char character) {
		view[x + y * width] = character;
    }

    public void setRow(int row, char fillCharacter) {
        for (int x = 0; x < width; x++) {
            setAbsolute(x, row, fillCharacter);
        }
    }

    public void setColumn(int column, char fillCharacter) {
        for (int y = 0; y < height; y++) {
            setAbsolute(column, y, fillCharacter);
        }
    }

    public Viewport joinAboveOf(Viewport belowView, int spacing) {
        checkArgument(spacing >= 0, "negative value of spacing: %s", spacing);
        Viewport joinedView = new Viewport(max(this.width, belowView.width), this.height + spacing + belowView.height);

        for (int y = 0; y < this.height; y++) {
            for (int x = 0; x < this.width; x++) {
                joinedView.setAbsolute(x, y, getAbsolute(x, y));
            }
        }

        for (int y = 0; y < belowView.height; y++) {
            for (int x = 0; x < belowView.width; x++) {
                joinedView.setAbsolute(x, y + this.height + spacing, belowView.getAbsolute(x, y));
            }
        }

        return joinedView;
    }

    public Viewport joinAboveOf(Viewport belowView) {
        return joinAboveOf(belowView, 0);
    }

    public Viewport joinBelowOf(Viewport aboveView, int spacing) {
        return aboveView.joinAboveOf(this, spacing);
    }

    public Viewport joinBelowOf(Viewport aboveView) {
        return joinBelowOf(aboveView, 0);
    }

    public Viewport joinLeftOf(Viewport rightView, int spacing) {
        checkArgument(spacing >= 0, "negative value of spacing: %s", spacing);
        Viewport joinedView = new Viewport(this.width + spacing + rightView.width, max(this.height, rightView.height));

        for (int y = 0; y < this.height; y++) {
            for (int x = 0; x < this.width; x++) {
                joinedView.setAbsolute(x, y, getAbsolute(x, y));
            }
        }

        for (int y = 0; y < rightView.height; y++) {
            for (int x = 0; x < rightView.width; x++) {
                joinedView.setAbsolute(x + this.width + spacing, y, rightView.getAbsolute(x, y));
            }
        }

        return joinedView;
    }

    public Viewport joinLeftOf(Viewport rightView) {
        return joinLeftOf(rightView, 0);
    }

    public Viewport joinRightOf(Viewport leftView, int spacing) {
        return leftView.joinLeftOf(this, spacing);
    }

    public Viewport joinRightOf(Viewport leftView) {
        return leftView.joinLeftOf(this, 0);
    }

    public Viewport extendBelow(int extensionLength) {
        return new Viewport(this.width, extensionLength).joinBelowOf(this);
    }

    public Viewport extendAbove(int extensionLength) {
        return new Viewport(this.width, extensionLength).joinAboveOf(this);
    }

    public Viewport extendRight(int extensionLength) {
        return new Viewport(extensionLength, this.height).joinRightOf(this);
    }

    public Viewport extendLeft(int extensionLength) {
        return new Viewport(extensionLength, this.height).joinLeftOf(this);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder().append(view[0]);
        for (int charIndex = 1; charIndex < view.length; charIndex++) {
            if (charIndex % width == 0) {
                builder.append('\n');
            }
            builder.append(view[charIndex]);
        }
        return String.valueOf(builder);
    }
}
