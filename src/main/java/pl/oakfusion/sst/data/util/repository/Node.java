package pl.oakfusion.sst.data.util.repository;

import lombok.Getter;
import pl.oakfusion.sst.data.world.gameobject.GameObject;

import java.util.LinkedList;
import java.util.List;

@Getter
public class Node {

	private final List<GameObject> objects = new LinkedList<>();

	public boolean isEmpty(){
		return objects.isEmpty();
	}
}
