package pl.oakfusion.sst.data.util;

import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;

public class AbbreviationFactory {

    public static List<String> getAbbreviations(String name, String shortestAbbreviation) {

        if (!name.startsWith(shortestAbbreviation)) {
            return emptyList();
        }

        if (shortestAbbreviation.equals("")) {
            return getSingleAbbreviation(name);
        }

        List<String> abbreviations = new ArrayList<>(singletonList(shortestAbbreviation.toUpperCase()));

        name.chars()
            .skip(shortestAbbreviation.length())
            .mapToObj(i -> (char) i)
            .forEach(character -> abbreviations.add((abbreviations.get(abbreviations.size() - 1) + character).toUpperCase()));

        return abbreviations;
    }

    public static List<String> getAbbreviations(String name) {
        return getAbbreviations(name, String.valueOf(name.charAt(0)));
    }

    private static List<String> getSingleAbbreviation(String name) {
        return singletonList(name.toUpperCase());
    }
}
