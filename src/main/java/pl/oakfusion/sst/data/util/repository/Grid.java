package pl.oakfusion.sst.data.util.repository;

import pl.oakfusion.sst.data.world.gameobject.GameObject;
import pl.oakfusion.sst.data.util.Position;
import pl.oakfusion.sst.data.world.component.strategy.pathFinding.*;
import pl.oakfusion.sst.data.world.civilizations.Unit;

import java.util.*;
import java.util.stream.Collectors;

import static java.lang.Math.*;
import static java.util.Optional.empty;
import static java.util.Optional.of;

public class Grid implements GameObjectsRepository {

	private final int worldHeight;
	private final int worldWidth;
	private final int widthInQuadrants;
	private final int heightInQuadrants;
	private final int sizeOfQuadrant;

	private final Node[][] nodes;

	public Grid(int width, int height, int quadrantSize) {
		this.worldWidth = width;
		this.worldHeight = height;
		this.sizeOfQuadrant = quadrantSize;
		this.widthInQuadrants = (int) ceil((double) width / sizeOfQuadrant);
		this.heightInQuadrants = (int) ceil((double) height / sizeOfQuadrant);
		this.nodes = new Node[heightInQuadrants][widthInQuadrants]; //Node = 1 QUADRANT
		initializeNodes();
	}

	private void initializeNodes() {
		for (int y = 0; y < heightInQuadrants; y++) {
			for (int x = 0; x < widthInQuadrants; x++) {
				nodes[y][x] = new Node();
			}
		}
	}

	private Node getNode(int x, int y) {
		int nodeY = y / sizeOfQuadrant;
		int nodeX = x / sizeOfQuadrant;
		return nodes[nodeY][nodeX];
	}

	private Node getNode(GameObject object) {
		return getNode(object.getX(), object.getY());
	}

	@Override
	public Optional<GameObject> get(int x, int y) {
		Node node = getNode(x, y);
		for (GameObject object : node.getObjects()) {
			if (object.getX() == x && object.getY() == y) {
				return of(object);
			}
		}
		return empty();
	}

	@Override
	public void add(GameObject object) {
		if (object.getX() >= worldWidth || object.getY() >= worldHeight) {
			throw new IndexOutOfBoundsException(String.format("Object cant be added outside of the world: %s, %s, WorldSize: %s, %s", object.getX(), object.getY(), worldWidth, worldHeight));
		}
		Node node = getNode(object);
		for (GameObject nodeObject : node.getObjects()) {
			if (object.getX() == nodeObject.getX() && object.getY() == nodeObject.getY()) {
				throw new SpaceOccupiedException(nodeObject);
			}
		}
		node.getObjects().add(object);
	}

	@Override
	public void remove(GameObject object) {
		Node node = getNode(object);
		node.getObjects().remove(object);
	}

	@Override
	public void remove(int x, int y) {
		Node node = getNode(x, y);
		for (GameObject object : node.getObjects()) {
			if (object.getX() == x && object.getY() == y) {
				node.getObjects().remove(object);
				break;
			}
		}
	}

	@Override
	public List<GameObject> getInRange(int x, int y, int range) {
		return getNodesInRange(x, y, range)
				.stream()
				.map(Node::getObjects)
				.flatMap(Collection::stream)
				.filter(object -> isGameObjectInRange(object, x, y, range))
				.collect(Collectors.toList());
	}


	private boolean isGameObjectInRange(GameObject object, int x, int y, int range) {
		return object.getX() >= (x - range) && object.getX() <= (x + range) &&
				object.getY() >= (y - range) && object.getY() <= (y + range);
	}

	private Collection<Node> getNodesInRange(int x, int y, int range) {

		Set<Node> nodes = new HashSet<>();

		int startX = x - range;
		if (startX < 0) {
			startX = 0;
		}
		int endX = x + range;
		if (endX >= worldWidth) {
			endX = worldWidth - 1;
		}
		int startY = y - range;
		if (startY < 0) {
			startY = 0;
		}
		int endY = y + range;
		if (endY >= worldHeight) {
			endY = worldHeight - 1;
		}

		int startNodeX = startX / sizeOfQuadrant;
		int endNodeX = endX / sizeOfQuadrant;
		int startNodeY = startY / sizeOfQuadrant;
		int endNodeY = endY / sizeOfQuadrant;

		for (int nodeY = startNodeY; nodeY <= endNodeY; nodeY++) {
			nodes.addAll(Arrays.asList(this.nodes[nodeY]).subList(startNodeX, endNodeX + 1));
		}

		return nodes;
	}

	@Override
	public List<GameObject> getInLine(int startX, int startY, int endX, int endY, int range) {
		var path = getMostAccuratePath(startX, startY, endX, endY);
		return	path
				.stream()
				.map((p -> getNodesInRange(p.getX(), p.getY(), range)))
				.flatMap(Collection::stream)
				.collect(Collectors.toSet())
				.stream()
				.map(Node::getObjects)
				.flatMap(Collection::stream)
				.filter(object -> isGameObjectInLineRange(object, startX, startY, endX, endY, range))
				.collect(Collectors.toList());
	}

	static List<Position> getMostAccuratePath(int startX, int startY, int endX, int endY) {
		// TODO: 8/8/18 Rounding problem ex. 0.999999999999 rounds to 0, should round to 1
		List<Position> path = new LinkedList<>();

		int deltaX = (endX - startX);
		int deltaY = (endY - startY);

		if (deltaX == 0 && deltaY == 0) {
			path.add(new Position(startX, startY));
		} else {

			boolean horizontalDirection = (abs(deltaX) - abs(deltaY)) > 0;

			if (horizontalDirection) {

				int direction = deltaX / abs(deltaX);

				for (int i = 0; i <= abs(deltaX); i++) {

					int directionalStep = i * direction;

					int x = startX + directionalStep;
					int y = (deltaY == 0) ? startY : startY + (int) floor((double) directionalStep / deltaX * deltaY);

					path.add(new Position(x, y));
				}
			} else {

				int direction = deltaY / abs(deltaY);

				for (int i = 0; i <= abs(deltaY); i++) {

					int directionalStep = i * direction;

					int x = (deltaX == 0) ? startX : startX + (int) floor((double) directionalStep / deltaY * deltaX);
					int y = startY + directionalStep;

					path.add(new Position(x, y));
				}
			}
		}

		return path;
	}

	private boolean isGameObjectInLineRange(GameObject object, int startX, int startY, int endX, int endY, int range) {
		return checkIfPointInRange(startX, startY, endX, endY, object.getX(), object.getY(), range);
	}

	boolean checkIfPointInRange(int startX, int startY, int endX, int endY, int x0, int y0, int range) {
		for (Position point : getMostAccuratePath(startX, startY, endX, endY)) {
			if ((abs(x0 - point.getX()) <= range) && (abs(y0 - point.getY()) <= range)) {
				return true;
			}
		}
		return false;
	}

	public List<GameObject> getGameObjectsInWorld() {
		return Arrays.stream(nodes).flatMap(Arrays::stream)
				.flatMap(e -> e.getObjects().stream())
				.collect(Collectors.toList());

	}

	@Override
	public List<Position> findShortestPath(Position source, Position target, List<Unit> unitsSafeToMoveThrough){
		if(isPositionOutOfBoundsOfGrid(source) || isPositionOutOfBoundsOfGrid(target)){
			throw new IndexOutOfBoundsException("One or more coordinates out of world range");
		}
		List<GameObject> objectsSafeToMoveThrough = new ArrayList<>(unitsSafeToMoveThrough);
		var matrix = createMatrixOfAvailablePlaces(objectsSafeToMoveThrough);
		ShortestPathFinderContext finder = new ShortestPathFinderContext();
		finder.setStrategy(new AStarShortestPathStrategy());
		return finder.executeStrategy(matrix, source, target);
	}


	@Override
	public List<Position> findPathInEmptyWorld(Position source, Position target) {
		if(isPositionOutOfBoundsOfGrid(source) || isPositionOutOfBoundsOfGrid(target)){
			throw new IndexOutOfBoundsException("One or more coordinates out of world range");
		}
		ShortestPathFinderContext finder = new ShortestPathFinderContext();
		finder.setStrategy(new AStarShortestPathStrategy());
		var matrix = createEmptyMatrix();
		return finder.executeStrategy(matrix, source, target);
	}


	private Integer[][] createMatrixOfAvailablePlaces(List<GameObject> objectsSafeToMoveThrough){
		Integer[][] matrix = new Integer[worldWidth][worldHeight];
		for (int i = 0; i < worldWidth; i++){
			for (int j = 0; j < worldHeight; j++){
				matrix[i][j] = 1;
			}
		}
		if (objectsSafeToMoveThrough == null) {
			objectsSafeToMoveThrough = List.of();
		}
		for (int i = 0; i < widthInQuadrants; i++){
			for (int j = 0; j < heightInQuadrants; j++){
				for (var gameObject : nodes[i][j].getObjects()){
					if (!objectsSafeToMoveThrough.contains(gameObject)){
						matrix[gameObject.getX()][ gameObject.getY()] = -1;
					}
				}
			}
		}
		return matrix;
	}

	private Integer[][] createEmptyMatrix() {
		Integer[][] matrix = new Integer[worldWidth][worldHeight];
		for (int i = 0; i < worldWidth; i++){
			for (int j = 0; j < worldHeight; j++){
				matrix[i][j] = 1;
			}
		}
		return matrix;
	}

	private boolean isPositionOutOfBoundsOfGrid(Position position){
		return position.getY() < 0 || position.getY() >= worldHeight || position.getX() < 0 || position.getX() >= worldWidth;
	}
}

