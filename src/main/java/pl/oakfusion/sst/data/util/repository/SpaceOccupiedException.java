package pl.oakfusion.sst.data.util.repository;

import pl.oakfusion.sst.data.world.gameobject.GameObject;

public class SpaceOccupiedException extends RuntimeException {
    public SpaceOccupiedException(GameObject gameObject) {
        super(String.format("This position: %s, %s is already occupied by another object", gameObject.getX(), gameObject.getY()));
    }
}
