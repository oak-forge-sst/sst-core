package pl.oakfusion.sst.data.util.repository;

import pl.oakfusion.sst.data.world.gameobject.GameObject;
import pl.oakfusion.sst.data.util.Position;
import pl.oakfusion.sst.data.world.civilizations.Unit;

import java.util.List;
import java.util.Optional;

import static java.util.Collections.unmodifiableList;

public final class UnmodifiableRepository implements GameObjectsRepository {

    private final GameObjectsRepository repository;

    UnmodifiableRepository(GameObjectsRepository repository) {
        this.repository = repository;
    }

    @Override
    public Optional<GameObject> get(int x, int y) {
        return repository.get(x, y);
    }

    @Override
    public void add(GameObject gameObject) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void remove(GameObject gameObject) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void remove(int x, int y) {
        throw new UnsupportedOperationException();
    }

	@Override
	public List<GameObject> getGameObjectsInWorld() {
		return repository.getGameObjectsInWorld();
	}

	@Override
    public List<GameObject> getInRange(int x, int y, int range) {
        return unmodifiableList(repository.getInRange(x, y, range));
    }


    @Override
    public List<GameObject> getInLine(int startX, int startY, int endX, int endY, int range) {
        return unmodifiableList(repository.getInLine(startX, startY, endX, endY, range));
    }

	@Override
	public List<Position> findShortestPath(Position source, Position target, List<Unit> unitsSafeToMoveThrough) {
		return unmodifiableList(repository.findShortestPath(source, target, unitsSafeToMoveThrough));
	}

	@Override
	public List<Position> findPathInEmptyWorld(Position source, Position target) {
		return unmodifiableList(repository.findPathInEmptyWorld(source, target));

	}
}
