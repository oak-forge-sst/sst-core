package pl.oakfusion.sst.data.exceptions;

import lombok.Getter;
import pl.oakfusion.sst.data.validationfailures.ValidationFailure;

import java.util.List;

@Getter
public class ValidationException extends Exception{
	List<ValidationFailure> validationFailures;

	public ValidationException(String message, List<ValidationFailure> validationFailures) {
		super(message);
		this.validationFailures = validationFailures;
	}

	public ValidationException(List<ValidationFailure> validationFailures) {
		this.validationFailures = validationFailures;
	}
}
