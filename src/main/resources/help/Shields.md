#Help Shields

Mnemonic:

    SHIELDS

Shortest abbreviation:

    SH

Full commands:

    SHIELDS UP
    SHIELDS DOWN
    SHIELDS TRANSFER <amount of energy to transfer>

Your deflector shields are a defensive device to protect you from Klingon attacks (and nearby novas). As the shields
  protect you, they gradually weaken. A shield strength of 75%, for example, means that the next time a Klingon hits you, your shields will deflect 75% of the hit, and let 25% get through to hurt you.

It costs 50 units of energy to raise shields, nothing to lower them. You may move with your shields up; this costs
  nothing under impulse power, but doubles the energy required for warp drive.

Each time you raise or lower your shields, the Klingons have another chance to attack. Since shields do not raise and
lower instantaneously, the hits you receive will be intermediate between what they would be if the shields were completely up or completely down.
