#Help Move
Mnemonic:

    MOVE

Shortest abbreviation:

    M

Full command:

       MOVE MANUAL
       MOVE AUTOMATIC

This command is the usual way to move from one place to another within the galaxy. You move under warp drive, \n\
  according to the current warp factor (see “WARP FACTOR”).

Example command looks like:

    M 2 -2 - Manual move which move your ship 2 points right and 2 up.
    M A 2 2 - Auto move move your ship to position 2 2 on the map.
You can use only one argument, it means that u can't move with manual and auto argument

There is one argument which can be use with manual or auto argument, it's FORCE, and can move your ship
even if computer stops you.
