#Help Phasers

Mnemonic:

    PHASERS

Shortest abbreviation:

    P

Full commands:

    PHASERS - For klingon it attack with full power nearest unit.
    PHASERS AUTOMATIC <AMOUNT TO FIRE> - attack nearest opponent unit with specify energy(only for ENTERPRISE).
    PHASERS MANUAL <NO> <AMOUNT 1> <AMOUNT 2>...<AMOUNT N> - attack nearest units with specify energy
    <NO> - number of units|| <Amount n> - specify energy for unit.

    PHASERS UNITS - Specify amount energy to attack for every unit in range
    Phasers are energy weapon which gives you opportunity to attack enemies.

The farther away opponent units are, the less phaser energy will reach them.
