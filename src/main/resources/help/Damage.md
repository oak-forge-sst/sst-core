#Help Damage
## !!! Not yet implemented !!!

Mnemonic:

    DAMAGES

Shortest abbreviation:

    DA

At any time you may ask for a damage report to find out what devices are damaged and how long it will take to repair
them. Naturally, repairs proceed faster at a starbase.
