#Help Estimate

Mnemonic:

    ESTIMATE
Shortest abbreviation:

    ES

This command allows using the ship's computer (if functional) to calculate travel times and energy usage.
