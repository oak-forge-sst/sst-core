#Help Saved Game

Mnemonic:

    SAVE

Shortest abbreviation:

    SA
  Full commands:

    SELECT <NAME>
    SAVE <NAME> <DESCRIPTION>

Choose your saved game by id/name.
