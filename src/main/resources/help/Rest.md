#Help Rest

Mnemonic:

    REST
Shortest abbreviation:

    R

Full command:

    REST <NUMBER-OF-ACTION-POINTS>

This command simply allows the specified number of action points to go by. This is useful if you have suffered damages
  and wish to wait until repairs are made before you go back into battle.

It is not generally advisable to rest while you are under attack by Klingons or Romulans.
