#Help
Mnemonic:

    Help

Full command:

    HELP <command>

This command show information about commands.
