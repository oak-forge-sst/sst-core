#Help Multi Select

Mnemonic:

    MSELECT

Shortest abbreviation:

    MS

Full commands:

    MSELECT <NAME1> <NAME2>...
    MSELECT -i <ID1> <ID2> <ID2>...

MultiSelect gives you opportunity to select few units, but you cant use every
command with few selected units
