#Help Emexit

Mnemonic:

    EMEXIT

Shortest abbreviation:

    E
This command provides a quick way to exit from the game when you observe a Klingon battle cruiser approaching your
