#Help Tracktor Beam

Mnemonic:

    BEAM

Shortest abbreviation:

    B

It pull enemy ship to immediate area. It only need to specify which
player unit we want to grab. This technology is only available for Commander and
SupperCommanders. Beam pull enemy unit with the highest full hp.
