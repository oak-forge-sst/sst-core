#Help Torpedo

Mnemonic:

    TORPEDO

Shortest abbreviation:

    TO

Full commands:

    TORPEDO <NUMBER> <TARG1> <TARG2> <TARG3>

Example command with attack 2 torpedo looks like
TO 2 10 10 11 11 - where "TO" is abbreviation, "2" is number of torpedoes
and "10 10" "11 11" are position objects which we want to attack

Torpedo are weapon which gives opportunity to attack enemies and objects.
Enterprise have 10 torpedo's which reload every time when it's docked.

SuperCommander have only one torpedo per turn
