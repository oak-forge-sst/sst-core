#Help Warp

Mnemonic:

    WARP

Shortest abbreviation:

    W

Full command:

    WARP <number>

Your warp factor controls the speed of your starship. The larger the warp factor, the faster you go and the more
  energy you use.

Your minimum warp factor is 1.0 and your maximum warp factor is 10.0
At exactly warp 10 there is some probability of entering a so-called “time warp” and being thrown foward or backward
  in time. The farther you go at warp 10, the greater is the probability of entering the time warp.

