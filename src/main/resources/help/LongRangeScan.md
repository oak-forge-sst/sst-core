#Long Range Scan

Mnemonic:

    LRSCAN

Shortest abbreviation:

    L

A long-range scan gives you general information about where you are and what is around you

    Thousands digit: 1000 indicates a supernova (only)
    Hundreds digit:	number of Klingons present
    Tens digit:	number of starbases present
    Ones digit:	number of stars present
