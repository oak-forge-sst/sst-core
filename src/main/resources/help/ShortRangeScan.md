#Short-Range Scan

Mnemonic:

    SRSCAN

Shortest abbreviation:

    S

Full commands:

                SRSCAN;

                SRSCAN NO;

                SRSCAN CHART;

If you are using the screen-oriented interface, this command is suppressed; instead,
a short-range scan will always be present on the screen.<br><br>
The short-range scan gives you a considerable amount of information about
the quadrant your starship is in. A short-range scan is best described by an example below:

        1 2 3 4 5 6 7 8 9 10
      1  * . . . . R . . . .  Stardate      2516.3
      2  . . . E . . . . . .  Condition     RED
      3  . . . . . * . B . .  Position      5 - 1, 2 - 4
      4  . . . S . . . . . .  Life Support  DAMAGED, Reserves = 2.30
      5  . . . . . . . K . .  Warp Factor   5.0
      6  . K .   . . . . * .  Energy        2176.24
      7  . . . . . P . . . .  Torpedoes     3
      8  . . . . * . . . . .  Shields       UP, 42% 1050.0 units
      9  . * . . * . . . C .  Klingons Left 12
     10  . . . . . . . . . .  Time Left     3.72

     * - Star;
     R - Romulan;
     E - Enterprise;
     B - Starbase;
     S - Super-commander(Kligon);
     K - Kligons;
     P - An uninhabited planet(if it were inhabited, it would display as a '@');
     C - Klingon Commander;

Short-range scans are free. That is, they use up no energy and no time. If you are in battle,
doing a short-range scan does not give the enemies another chance to hit you.
You can safely do a short-range scan anytime you like.

If your short-range sensors are damaged, this command will only show the contents of adjacent sectors.

Adition Function:

    The information on the right is assorted status information. You can get this alone with the
    STATUS command. The status information will be absent if you type “N” after SRSCAN. O
    therwise status information will be presented.

    If you type “C” after SRSCAN, you will be given a short-range scan and a Star Chart.



