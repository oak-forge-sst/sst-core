#Help Self-Destruct
## !!! Not yet implemented !!!

Mnemonic:

    DESTRUCT

You may self-destruct, thus killing yourself and ending the game. If there are nearby Klingons, you may take a few of them with you (the more energy you have left, the bigger the bang). It is possible to win this way, if you kill off your last adversaries with the blast.

In order to self-destruct you must remember the password you typed in at the beginning of the game.
