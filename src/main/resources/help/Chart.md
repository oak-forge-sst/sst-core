#Help Chart

Mnemonic:

    CHART

Shortest abbreviation:

    C

The chart looks like an 8 by 8 array of numbers.
These numbers are interpreted exactly as they are on a long-range
scan.

A period (.) in place of a digit means you do not know that information yet. For example, ... means you know
nothing about the quadrant, while .1. menas you know it contains a base, but an unknown number of Klingons and stars.
