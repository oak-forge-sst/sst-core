#Help Planet Report

Mnemonic:

    PLANETS

Shortest abbreviation:

    PL

Mr. Spock presents you a list of the available information on planets in the galaxy which are potential
dilithium sources. Since planets do not show up on long-range scans, the only way to obtain this information
is with the “SENSORS” command.

