#Help Select

Mnemonic:

    SELECT

Shortest abbreviation:

    SE

Full commands:

    SELECT <ID>
    SELECT <NAME>

Select is for choosing your unit. You can do it by using name or ID
