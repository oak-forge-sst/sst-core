#All Help Commands

HELP_SRSCAN=Mnemonic: SRSCAN\n\
Shortest abbreviation:  S\n\
Full commands:  SRSCAN\n\
Scan arguments(OPTIONAL): NO, CHART, UNITS\n\
SRSCAN NO - shows only galaxy without status\n\
SRSCAN CHART - shows chart under galaxy scan\n\
SRSCAN U - shows data about units in range\n\
The short-range scan gives you a information about closest part of galaxy our starship is in.\n\
This command also gives information about status of our ship. For additional information's like\n\
data about enemy ships in your range or chart, you need to use arguments, so full command can looks lIke\n\
SRSCAN NO U  - scan without status but with information about units in range. Arguments are optional

##
HELP_STATUS=Mnemonic:  STATUS\n\
Shortest abbreviation: ST\n\
This command gives you information about the current state of your starship

##
HELP_LRSCAN=Mnemonic:  LRSCAN\n\
Shortest abbreviation:  L\n\
A long-range scan gives you general information about where you are and what is around you\n\
Thousands digit: 1000 indicates a supernova (only)\n\
Hundreds digit:	number of Klingons present\n\
Tens digit:	number of starbases present\n\
Ones digit:	number of stars present


##
HELP_CHART=Mnemonic:  CHART\n\
Shortest abbreviation:  C\n\
The chart looks like an 8 by 8 array of numbers. These numbers are interpreted exactly as they are on a long-range \n\
scan. A period (.) in place of a digit means you do not know that information yet. For example, ... means you know\n\
nothing about the quadrant, while .1. menas you know it contains a base, but an unknown number of Klingons and stars.

##
HELP_DAMAGES=Mnemonic:  DAMAGES\n\
Shortest abbreviation:  DA\n\
At any time you may ask for a damage report to find out what devices are damaged and how long it will take to repair \n\
  them. Naturally, repairs proceed faster at a starbase.

##
HELP_MOVE=Mnemonic:  MOVE\
Shortest abbreviation:  M\n\
Full command:  MOVE MANUAL <displacement>\n\
MOVE AUTOMATIC <destination>\n\
This command is the usual way to move from one place to another within the galaxy. You move under warp drive, \n\
  according to the current warp factor (see “WARP FACTOR”).\n\
Example command looks like:\n\
M 2 -2 - Manual move which move your ship 2 points right and 2 up.\n\
M A 2 2 - Auto move move your ship to position 2 2 on the map\n\
You can use only one argument, it means that u can't move with manual and auto argument\n\
  There is one argument which can be use with manual or auto argument, it's FORCE, and can move your ship\n\
even if computer stops you.

##
HELP_WARP=Mnemonic:  WARP\n\
Shortest abbreviation:  W\n\
Full command:  WARP <number>\n\
Your warp factor controls the speed of your starship. The larger the warp factor, the faster you go and the more \n\
  energy you use.\n\
Your minimum warp factor is 1.0 and your maximum warp factor is 10.0 \n\
At exactly warp 10 there is some probability of entering a so-called “time warp” and being thrown foward or backward \n\
  in time. The farther you go at warp 10, the greater is the probability of entering the \time warp.


##
HELP_SHIELDS=Mnemonic:  SHIELDS\n\
Shortest abbreviation:  SH\n\
Full commands:  SHIELDS UP\n\
SHIELDS DOWN\n\
SHIELDS TRANSFER <amount of energy to transfer>\n\
Your deflector shields are a defensive device to protect you from Klingon attacks (and nearby novas). As the shields \n\
  protect you, they gradually weaken. A shield strength of 75%, for example, means that the next time a Klingon hits you, your shields will deflect 75% of the hit, and let 25% get through to hurt you.\n\
\n\
It costs 50 units of energy to raise shields, nothing to lower them. You may move with your shields up; this costs \n\
  nothing under impulse power, but doubles the energy required for warp drive.\n\
\n\
Each time you raise or lower your shields, the Klingons have another chance to attack. Since shields do not raise and\n\
lower instantaneously, the hits you receive will be intermediate between what they would be if the shields were completely up or completely down.


##
HELP_ESTIMATE=Mnemonic:  ESTIMATE\n\
Shortest abbreviation: ES\n\
This command allows using the ship's computer (if functional) to calculate travel times and energy usage.


##
HELP_DOCK=Mnemonic:  DOCK\n\
Shortest abbreviation:  D\n\
You may dock your starship whenever you are in one of the eight sector positions immediately adjacent to a starbase. \n\
  When you dock, your starship is resupplied with energy, shield energy photon torpedoes, and life support reserves. Repairs also proceed faster at starbase, so if some of your devices are damaged, you may wish to stay at base (by using the “REST” command) until they are fixed. If your ship has more than its normal maximum energy (which can happen if you've loaded crystals) the ship's energy is not changed.\n\
\n\
You may not dock while in standard orbit around a planet.\n\
\n\
Starbases have their own deflector shields, so you are completely safe from phaser attack while docked. You are also \n\
  safe from long-range tractor beams.\n\
\n\
Starbases also have both short and long range sensors, which you can use if yours are broken. There's also a subspace\n\
   radio to get information about happenings in the galaxy. Mr. Spock will update the star chart if your ask for it while docked and your own radio is dead.



##
HELP_REST=Mnemonic:  REST\n\
Shortest abbreviation:  R\n\
Full command:  REST <NUMBER-OF-ACTION-POINTS>\n\
This command simply allows the specified number of action points to go by. This is useful if you have suffered damages \n\
  and wish to wait until repairs are made before you go back into battle.\n\
\n\
It is not generally advisable to rest while you are under attack by Klingons or Romulans.


##
HELP_MAYDAY=Mnemonic:  MAYDAY\n\
Shortest abbreviation:  R\n\
When you get into serious trouble, you may call a starbase for help. Starbases have a device called a “long-range \n\
  transporter beam” which they can use to teleport you to base. This works by dematerializing your starship at its current\n\
  position and re-materializing it adjacent to the nearest starbase. Teleportation is instantaneous, and starbase supplies the \n\
  required energy—all you have to do is let them know (via subspace radio) that you need to be rescued.\n\
\n\
This command should be employed only when absolutely necessary. In the first place, calling for help is an admission on your\n\
   part that you got yourself into something you cannot get yourself out of, and you are heavily penalized for this in the final \n\
  scoring. Secondly, the long-range transporter beam is not reliable—starbase can always manage to dematerialize your starship, \n\
  but (depending on distance) may or may not be able to re-materialize you again. The long-range transporter beam has no absolute\n\
   maximum range; if you are in the same quadrant as a starbase, you have a good chance (about 90%) of re-materializing successfully.\n\
   your chances drop to roughly 50-50 at just over 3 quadrants.


##
HELP_QUIT=Mnemonic:  QUIT\n\
(no abbreviation)\n\
Immediately cancel the current game; no conclusion is reached. You will be given an opportunity to start a new game or to\n\
   leave the Star Trek.


##
HELP_EMEXIT=Mnemonic:  EMEXIT\n\
Shortest abbreviation:  E\n\
This command provides a quick way to exit from the game when you observe a Klingon battle cruiser approaching your

##
HELP_HELP=Mnemonic:  HELP\n\
Full command:  HELP <command>\n\
This command show information about commands.

##
HELP_MISSING_COMMAND=Help on what command?
##
HELP_PHASERS=Mnemonic: PHASERS\n\
  Shortest abbreviation: P\n\
  Full commands: \n\
  PHASERS - For klingon it attack with full power nearest unit\n\
  PHASERS AUTOMATIC <AMOUNT TO FIRE> - attack nearest opponent unit with specify energy(only for ENTERPRISE) \n\
  PHASERS MANUAL <NO> <AMOUNT 1> <AMOUNT 2>...<AMOUNT N> - attack nearest units with specify energy  \n\
  <NO> - number of units|| <Amount n> - specify energy for unit\n\
  PHASERS UNITS - Specify amount energy to attack for every unit in range \n\
Phasers are energy weapon which gives you opportunity to attack enemies.\n\
  The farther away opponent units are, the less phaser energy will reach them.
HELP_TORPEDO=Mnemonic: TORPEDO\n\
  Shortest abbreviation: TO\n\
  Full commands: \n\
  TORPEDO <NUMBER> <TARG1> <TARG2> <TARG3>\n\
  Example command with attack 2 torpedo looks like\n\
  TO 2 10 10 11 11 - where "TO" is abbreviation, "2" is number of torpedoes\n\
and "10 10" "11 11" are position objects which we want to attack\n\
  Torpedo are weapon which gives opportunity to attack enemies and objects.\n\
Enterprise have 10 torpedo's which reload every time when it's docked.\n\
  SuperCommander have only one torpedo per turn
HELP_TRACKTOR_BEAM=Mnemonic: Beam\n\
  Shortest abbreviation: B\n\
  It pull enemy ship to immediate area. It only need to specify which \n\
  player unit we want to grab. This technology is only available for Commander and \n\
SupperCommanders. Beam pull enemy unit with the highest full hp.
HELP_SELECT=Mnemonic: SELECT\n\
  Shortest abbreviation: SE\n\
  Full commands: \nSELECT <ID> \n\
  SELECT <NAME>\n\
  Select is for choosing your unit. You can do it by using name or ID
HELP_MULTI_SELECT=Mnemonic: MSELECT\n\
  Shortest abbreviation: MS\n\
  Full commands: \n\
  MSELECT <NAME1> <NAME2>... \n\
  MSELECT -i <ID1> <ID2> <ID2>...\n\
  MultiSelect gives you opportunity to select few units, but you cant use every\n\
  command with few selected units
HELP_SAVED_GAME=Choose your saved game by id/name:
HELP_SAVE_GAME=Mnemonic: SAVE\n\
  Shortest abbreviation: SA\n\
  Full commands: \n\
  SELECT <NAME>\n\
  SAVE <NAME> <DESCRIPTION>
