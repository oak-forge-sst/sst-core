#System turowy

Gra będzie oparta na systemie turowym.

Każda jednostka ma określoną liczbę stardatów do wykorzystania podczas tury oraz możliwość wykonania ataków.

###Stardate
Podczas tury mija x stardate'ów. Mogą one być wykorzystane do wykonania ruchu, odpoczynku lub kopania kryształów.

Upływ czasu powoduje odpowiednio regenerację jedostek.

Jeżeli gracz chce dotrzeć do celu zbyt oddalonego na dotarcie tam w ciągu jednej tury można zadać zadanie na kilka tur z możliwością przerwania go.
#####Klingoni
Mogą wykorzystywać stardate'y do poruszania się lub niszczenia obiektów (planet, baz). 
###Atak
#####Enterprise
Jednostka podczas tury może zaatakować jeden raz.
Jeden atak to wystrzelenie n torped lub użycie pewnej ilość energii w formie ataku phaserami (obydwie rzeczy są z góry ograniczone).

Straty zadane przez gracza wykonują się od razu. 
###Inne
#####Akcje nie zajmujące czasu, które można wykonywać dowolną liczbę razy
* skaner
* dock
* orbit
#####Akcje kończące turę
* mayday
* deathray
* tracktor beam
#####Akcje działające od początku następnej tury
* cloak on/off
* shields up/down
