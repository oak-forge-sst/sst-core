- Wprowadzić większy nacisk na korzystanie z narzędzi z których korzystamy w projekcie.
Korzystać z możliwie najnowszych wersji narzędzi.
- Dbać o to żeby publiczny interfejs klasy był dobrze udokumentowany
- Unikać hardcodowania nazw pakietów/klas w kodzie. Unikać hardcodowania stałych w metodach
- Trzymać się konwencji.
