#GameplayProcess
It is process which is responsible for 
1.  Change turn,
2. Create report before turn,
3. Ending game ,
4. Keep data about dead players

##Events ending turn
We have 2 events ending turns
* TurnPerformed
* GameOverForPlayerEvent

After one of these event's function "endTurn" is invoked.

##Event's ending game
We have two event's ending game
* DeathRayDead
* MaydayFailed

which change player and invoke GameOverForPlayerCommand

##endTurn function
This is main function in this class. It works in few simple steps:
* filter players which are alive,
* set index of player who is next in queue,
* check, if deadPlayers are present,
* set active player
* create report which is information about what was happened to him since last turn. It also contain data about dead players,
* when it is last player in list, time is incremented,
* check if it's last player stand, and if it's true, post GameOverForPlayerCommand 
