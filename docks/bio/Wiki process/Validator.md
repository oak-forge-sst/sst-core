#Validation
Validation aggregator and Validation decorator were made to make easy send confirmation and 
choosePlayer back to Client. It givers us opportunity to handle warns, like:
* Continue - ask player, is he want to continue command from last turn
* Accept/Decline - ask players is he want to continue command, even when it can cost him penalty(when components damaged)
* Choose player - ask player, which player they want to choose(Beam is excellent example)

It also post failures when it's something wrong, and post commands further to aggregates when it's everything good.


