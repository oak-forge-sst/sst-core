#Scoring
The main mechanism for calculate scoring for each player is ScoringHandler. It contain a lot of
lists which keep information's affecting score, like events and damaged objects.

##Scoring from events
Scoring form events is calculated, when some event's will come to ScoringHandler and will be handle. \
Great example is Mayday. When player call for help, his score will be decremented.

##Scoring from damaged objects
In gameSessionData we have data about every object which was damaged.\
 When player invoke "Score" command, the function "getScoreDTO" will filter only objects which was
 destroyed by this player and add it to list "damagedObjects".
##FinalScore
FinalScore it's list which is posted with ScorePerformed. This list include data from scoreFromEvents,
and damagedObjects. Before "Score" command invoke method "getScoreDTO", finalScore is always cleaned.
