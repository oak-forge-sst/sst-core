 <h2>Short-range scan</h2>
  <h4>Mnemonic:  SRSCAN</h4>
  <h4>Shortest abbreviation:  S</h4>
 <p>The short-range scan gives you a considerable amount of information about the location in galaxy where your starship is in.
 Short-range scans are free. That is, they use up no energy and no time. If you are in battle, doing a short-range scan does not give the enemies another chance to hit you. You can safely do a short-range scan anytime you like.
 
 </p>
 <h4> Short range scan params</h4>
 <ol>
 <li> - chart (shortest abbreviation - c):<br> displays chart</li>
 <li> - no (shortest abbreviation - n):<br> displays short range scan without status</li>
 <li> - units (shortest abbreviation - u):<br> displays enemy units with their basic description:<br>
 ID/UNIT TYPE/ UNIT NAME / POSITION X/ POSITION Y/ UNIT OWNER NAME
 </li>
 </ol>
 <h3>Events Flow</h3>
 <h4>Default params</h4>
 <ol>
 <li>chartRequested = false</li>
 <li>statusRequested = true</li>
 <li>unitInfoReuquested = false</li>
 </ol>
 <ol>
<li> <b>Normal use case</b>
 <p>(Command) <b>ShortRangeScan</b>(boolean chartRequested,boolean statusRequested,boolean unitInfoRequested)<b> -></b>
  
  (Event) <b>ShortRangeScanPerformed</b>(ShortRangeScanDTO shortRangeScan)</p></li>
 
 
 <li><b>When unit of this type cannot use the command </b>
 <p>(Command) <b>ShortRangeScan</b>(boolean chartRequested,boolean statusRequested,boolean unitInfoRequested) <b>-></b>
 
 (Event)<b> NoTechnology</b></p></li>
 <li><b>When short-range sensor is damaged and you accept after warning</b>
 <p>(Command) <b>ShortRangeScan</b>(boolean chartRequested,boolean statusRequested,boolean unitInfoRequested) <b>-></b>
 
 (Event) <b>ComponentLowHealthWarning -></b>
 
 (Confirm) <b>AcceptCommand</b> <b>-></b>
 
 (Command) <b>ShortRangeScan</b>(boolean chartRequested,boolean statusRequested,boolean unitInfoRequested)</p></li>
 <li><b>When short-range sensor is damaged and you decline after warning</b>
 <p>(Command)<b>ShortRangeScan</b>(chartRequested, statusRequested, unitInfoRequested) <b>-></b>
 
 (Event) <b>ComponentLowHealthWarning -></b>
 
 (Confirm) <b>DeclineCommand</b></p></li>
 </ol>
 <h3>Low component health scenarios</h3>
 <ol>
 <li>Short-range scan will be displayed with restricted range</li>
 <li>Short-range scan command will fail when below certain level of functionality and drawn number will be lower than propability of failure</li>
 
 </ol>

