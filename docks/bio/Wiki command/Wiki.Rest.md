 <h2>Rest</h2>
  <h4>Mnemonic:  REST</h4>
  <h4>Shortest abbreviation:  R</h4>
 <p>TThis command simply allows the specified number of stardates to go by. This is useful if you have suffered damages and wish to wait until repairs are made before you go back into battle.
    
   It is not generally advisable to rest while you are under attack by Klingons or Romulans.
 </p>
 <h4> Rest params</h4>
<\NUMBER-OF-STARDATES\>
 <h3>Events Flow</h3>
 
<li> <b>Normal use case</b>
 <p>(Command) <b>Rest</b><b> -></b>
  
  (Event) <b>RestPerformed</b>(String starDate)</p>




