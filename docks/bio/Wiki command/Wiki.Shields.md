 <h2>Deflector Shields</h2>
  <h4>Mnemonic:  SHIELDS</h4>
  <h4>Shortest abbreviation:  SH</h4>
 <p>Your deflector shields are a defensive device to protect you from  attacks (and nearby novas). As the shields protect you, they gradually weaken. A shield strength of 75%, for example, means with next attack,  your shields will deflect 75% of the hit, and let 25% get through to hurt you.
    
 You may move with your shields up; this costs nothing under impulse power, but doubles the energy required for warp drive.
 
 Each time you raise or lower your shields, the enemy have another chance to attack. Since shields do not raise and lower instantaneously, the hits you receive will be intermediate between what they would be if the shields were completely up or completely down.
 
 
 You may transfer energy beteen the ship's energy (given as “Energy” in the status) and the shields. The word “TRANSFER” may be abbreviated “T”. The ammount of energy to transfer is the number of units of energy you wish to take from the ship's energy and put into the shields. If you specify an negative number, energy is drained from the shields to the ship. Transfering energy constitutes a turn. If you transfer energy to the shields while you are under attack, they will be at the new energy level when you are next hit.
 
 Enemy torpedoes hitting your ship explode on your shields (if they are up) and have essentially the same effect as phaser hits.
 </p>
 <h4> Shields</h4>
 <ol>
 <li> - up (shortest abbreviation - u):<br> Shields Up</li>
 <li> - down (shortest abbreviation - d):<br> Shields Down</li>
 <li> -transfer(shortest abbreviation - t) <\amount_of_energy_to_transfer> :<br> Transfers energy to shields</li>
 </ol>
 <h3>Events Flow</h3>
 <h4>Default params</h4>
    >>NO DEFAULT PARAM
 <ol>
<li> <b>Normal use case when shields up</b>
 <p>(Command) <b>ChangeShieldsStatus</b>ChangeShieldsStatus(ShieldsStatus shieldsStatus)<b> -></b>
  
  (Event) <b>ShieldsLowered</b></p></li>
 <li> <b>Normal use case when shields down</b>
 <p>(Command) <b>ChangeShieldsStatus</b>ChangeShieldsStatus(ShieldsStatus shieldsStatus)<b> -></b>
  
  (Event) <b>ShieldsRisen</b></p></li>
 <li> <b>Normal use case when shields already down/up</b>
 <p>(Command) <b>ChangeShieldsStatus</b>ChangeShieldsStatus(ShieldsStatus shieldsStatus)<b> -></b>
  
  (Event) <b>ShieldStatusChangeFailed</b></p></li>
 
 
 <li><b>When unit of this type cannot use the command </b>
 <p>(Command) <b>Shields</b>(ShieldsStatus shieldsStatus) <b>-></b>
 
 (Event)<b> NoTechnology</b></p></li>
 
