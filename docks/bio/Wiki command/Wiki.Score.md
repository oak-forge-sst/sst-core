 <h2>Score</h2>
  <h4>Mnemonic:  SCORE</h4>
  <h4>Shortest abbreviation:  SC</h4>
 <p>This command displays current scoring for the player.
 </p>
 <h4> Score params</h4>
NO PARAMS
 <h3>Events Flow</h3>
 
<li> <b>Normal use case</b>
 <p>(Command) <b>Score</b><b> -></b>
  
  (Event) <b>ScorePerformed</b>(String commandParam)</p>




