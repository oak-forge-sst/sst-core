 <h2>Damage Report</h2>
  <h4>Mnemonic:  DAMAGES</h4>
  <h4>Shortest abbreviation:  DA</h4>
 <p>At any time you may ask for a damage report to find out what devices and units in fleet are damaged and how long it will take to repair components. Naturally, repairs proceed faster at a base.
     
   If you suffer damages while moving, it is possible that a subsequent damage report will not show any damage. This happens if the time spent on the move exceeds the repair time, since in this case the damaged devices were fixed en route.
     
   Damage reports are free. They use no energy or time, and can be done safely even in the midst of battle.
 </p>
 <h4> Damage Report params:</h4>
<p>NO PARAMS</p>
 <h3>Events Flow</h3>
 <h5>Normal use case</h5>
 <p>DamageReport -> DamageReportPerformed</p>
 <h5>When unit of this type cannot use the command </h5>
 <p>DamageReport -> NoTechnology</p>
