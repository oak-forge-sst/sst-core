 <h2>Long-range Scan</h2>
 <h4>Mnemonic:  LRSCAN</h4>
 <h4>Shortest abbreviation:  L</h4>
 <p>A long-range scan gives you general information about where you are and what is around you.
 Romulans possess a “cloaking device” which prevents their detection by long-range scan. Because of this fact, Starfleet Command is never sure how many Romulans are “out there”. When you kill the last Klingon, the remaining Romulans surrender to the Federation.
 
 Planets are also undetectable by long-range scan. The only way to detect a planet is to find it in your current quadrant with the short-range sensors.
 
 Since you are in column 1, there are no quadrants to your left. The minus ones indicate the negative energy barrier at the edge of the galaxy, which you are not permitted to cross.
 
 Long-range scans are free. They use up no energy or time.
 </p>
 <h4> Long-range scan params</h4>
<p>NO PARAMS</p>
 <h3>Events Flow</h3>
 <ol>
 <li><b>Normal use case</b>
 <p>LongRangeScan -> ChartPerformed(ChartDTO chartDTO)</p></li>
 <li><b>When unit of this type cannot use the command </b>
 <p>LongRangeScan -> NoTechnology</p></li>
 <li><b>When long-range sensor is damaged and you accept after warning</b>
 <p>LongRangeScan -> ComponentLowHealthWarning -> AcceptCommand -> LongRangeScan</p></li>
 <li><b>When short-range sensor is damaged and you decline after warning</b>
 <p>LongRangeScan -> ComponentLowHealthWarning -> DeclineCommand</p></li>
</ol>
 <h3>Low component health scenarios</h3>
 <ol>
 <li>Long-range scan will be displayed with restricted range</li>
 <li>Long-range scan command will fail when below certain level of functionality and drawn number will be lower than propability of failure</li>
 
 </ol>

