 <h2>Move</h2>
  <h4>Mnemonic:  MOVE</h4>
  <h4>Shortest abbreviation:  M</h4>
  <h4>Full commands:</h4> 
  <h5>MOVE \<displacement> \<FORCE></h5> 
  <h5>MOVE \<AUTOMATIC> \<destination> \<FORCE></h5>
 <p>This command is the usual way to move from one place to another within the galaxy. You move under warp drive, according to the current warp factor (see “WARP FACTOR”).
    
   There are two command modes for movement: MANUAL and AUTOMATIC. The manual mode requires the following format:
    
   MOVE MANUAL <deltax> <deltay>
   <deltax> and <deltay> are the horizontal and vertical displacements for your starship, in quadrants
 
 </p>
 <h4> Move params</h4>
 <ol>
 <li> - automatic (shortest abbreviation - a):<br> Automatic mode utilizes the ship's “battle computer.” If the computer is damaged, manual movement must be used.</li>
 <li> - force (shortest abbreviation -f): Force parameter forces starship to move even if action is potentialy dangerous 
 
 </li>
 </ol>
 <h3>Events Flow</h3>
 <ol>
 <li><b>Normal use case</b>
 <p>Move(boolean isAutoMode, int x, int y, boolean forceMode) -> MovePerformed(boolean isAutoMode, int x, int y)</p></li>
 <li><b>When unit of this type cannot use the command </b>
 <p>Move(boolean isAutoMode, int x, int y, boolean forceMode) -> NoTechnology</p></li>
 <li><b>When warp engines are damaged and you accept after warning</b>
 <p>Move(boolean isAutoMode, int x, int y, boolean forceMode) -> ComponentLowHealthWarning -> AcceptCommand -> Move(boolean isAutoMode, int x, int y, boolean forceMode)</p></li>
 <li><b>When warp engines are damaged and you decline after warning</b>
 <p>Move(boolean isAutoMode, int x, int y, boolean forceMode) -> ComponentLowHealthWarning -> DeclineCommand</p></li>
 </ol>
  </ol>
  <h3>Possible Scenarios</h3>
  <ol><li>
  Move(with force mode ON and not safe travel)->SafeMoveFailed 
  </li>
  <li>Move(with force mode ON and not enough resources) -> ForcedMoveFailed
  
  </li>
  <li>
  Move(when computer is down and automatic mode is ON) -> AutoMoveFailed
  </li>
  <li>Move(when user tries to move in the occupied place) -> MoveCollisionDetected</li>
  <li>Move(when user tries to move outside of world boundaries) -> MoveOutOfBorderDetected </li>
  </ol>
 <h3>Low component health scenarios</h3>
 <ol>
 <li>Move command will fail when below certain level of functionality and drawn number will be lower than propability of failure</li>
 <li>When Move command fails user recive penalty damaging warp engines component</li>

</ol>
