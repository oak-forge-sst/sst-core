 <h2>Warp Factor</h2>
  <h4>Mnemonic:  WARP</h4>
  <h4>Shortest abbreviation:  W</h4>
 <p>Your warp factor controls the speed of your starship. The larger the warp factor, the faster you go and the more energy you use.
    
  Your minimum warp factor is 1 and your maximum warp factor is 10. At speeds above warp 6 there is some danger of causing damage to your warp engines; this damage is larger at higher warp factors and also depends on how far you go at that warp factor.
 </p>
 <h4> Short range scan params</h4>
 <\NUMBER\>
 <h3>Events Flow</h3>

<li> <b>Normal use case</b>
 <p>(Command) <b>Warp</b>(int changeWarpFactor)<b> -></b>
  
  (Event) <b>WarpFactorChanged</b>(int warp, SystemMessage SystemMessage)</p></li>
 
 <li><b>When user wants set warp greater than 10 or lesser than 0 or when tries to set higher warp factor than damaged warp engines allows</b>
<p>(Command) <b>Warp</b>(int changeWarpFactor)<b> -></b>
  
  (Event) <b>ChangeWarpFactorFailed </b>(int warp, SystemMessage SystemMessage, boolean isAboveMax)</p>
 </li>
 
 <li><b>When unit of this type cannot use the command </b>
 <p>(Command) <b>ChangeWarpFactor</b>(int changeWarpFactor) <b>-></b>
 
 (Event)<b> NoTechnology </b>
  </p> 
  </li>
   <li><b>When Component is damaged </b>
   <p>(Command) <b>ChangeWarpFactor</b>(int changeWarpFactor) <b>-></b>
   
   (Event)<b>  ComponentCommandFailed </b>
   </p> 
   </li>
 </ol>
  <li><b>When short-range sensor is damaged and you accept after warning</b>
  <p>(Command) <b>ChangeWarpFactor</b>(int changeWarpFactor) <b>-></b>
  
  (Event) <b>ComponentLowHealthWarning -></b>
  
  (Confirm) <b>AcceptCommand</b> <b>-></b>
  
  (Command) <b>ChangeWarpFactor</b>(int changeWarpFactor)</p></li>
   <li><b>When short-range sensor is damaged and you decline after warning</b>
   <p>(Command)<b>ChangeWarpFactor</b>(int changeWarpFactor) <b>-></b>
   
   (Event) <b>ComponentLowHealthWarning -></b>
   
   (Confirm) <b>DeclineCommand</b>
   </p></li>
   </ol>
  <h3>Low component health scenarios</h3>
  <ol>
  <li>Warp factor max level will be restricted</li>
  <li>There is possibility of getting damaged</li>
  
  </ol>


