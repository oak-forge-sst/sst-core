 <h2>Turn change</h2>
  <h4>Mnemonic:  TURN</h4>
  <h4>Shortest abbreviation:  TU</h4>
 <p>This command let you end your turn as civilization </p>
 <h4> Turn params</h4>
<ol>
<li>- yes (shortest abbreviation: y)</li>
<li>- no  (shortest abbreviation: n)</li>
</ol>
 <h3>Events Flow</h3>
 
<li> <b>Normal use case</b>
 <p>(Command) <b>Turn </b><b> -></b>
  
  (GameWarningEvent) <b>TurnPerformed</b>(String starDate)</p>
  
  <b> -></b>(ApplicationCommand)<b>EndTurn</b><b> -></b>
  
  (GameWarningEvent) <b>ReportBeforeTurn </b><b> -></b>
  
  
  (ApplicationEvent)<b>EndTurnPerformed</b>
 </li>
  




