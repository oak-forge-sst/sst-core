 <h2>Torpedo 
  <h4>Mnemonic:  TORPEDO</h4>
  <h4>Shortest abbreviation:  TO</h4>
 <p>
 Photon torpedoes are projectile weapons—you either hit what you aim at, or you don't. There are no “partial hits”.
 </p>
 <h4> Torpedo params</h4>
 - <\NUMBER\>  <\TARG1\> <\TARG2\> <\TARG3\>
 <h3>Events Flow</h3>

<li> <b>Normal use case</b>
 <p>(Command) <b>Torpedo(int number, List target)</b><b> -></b>
  
  (Event) <b>TorpedoPerformed</b></p></li>
 
 
 <li><b>When unit of this type cannot use the command </b>
 <p>(Command) <b>Torpedo(int number, List target)</b> <b>-></b>
 
 (Event)<b> NoTechnology</b></p></li>
 <li><b>When torpedo device is damaged and you accept after warning</b>
 <p>(Command) <b>Torpedo(int number, List target)</b> <b>-></b>
 
 (Event) <b>ComponentLowHealthWarning -></b>
 
 (Confirm) <b>AcceptCommand</b> <b>-></b>
 
 (Command) <b>Torpedo(int number, List target)</b></p></li>
 <li><b>When torpedo device is damaged and you decline after warning</b>
 <p>(Command)<b>Torpedo(int number, List target)</b><b>-></b>
 
 (Event) <b>ComponentLowHealthWarning -></b>
 
 (Confirm) <b>DeclineCommand</b>
 </p></li>
 </ol>
 <h3>Low component health scenarios</h3>
 <ol>
 <li>Torpedo device will have restricted amount of torpedoes to shoot lineary to damages taken</li>
 <li>Torpedo command will fail when below certain level of functionality and drawn number will be lower than propability of failure</li>
 
 </ol>

