 <h2>Star Chart</h2>
  <h4>Mnemonic:  CHART</h4>
  <h4>Shortest abbreviation:  C</h4>
 <p>The chart looks like an 8 by 8 array of numbers. These numbers are interpreted exactly as they are on a long-range scan. A period (.) in place of a digit means you do not know that information yet. For example, ... means you know nothing about the quadrant, while .1. menas you know it contains a base, but an unknown number of Klingons and stars.
    
  Looking at the star chart is a free operation. It costs neither time nor energy, and can be done safely whether in or out of battle.
 </p>
 <h4> Chart params</h4>
<p>NO PARAMS</p>
 <h3>Events Flow</h3>
 <h5>Normal use case</h5>
 <p>Chart -> ChartPerformed</p>
 <h5>When unit of this type cannot use the command </h5>
 <p>Chart -> NoTechnology</p>
