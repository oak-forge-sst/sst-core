 <h2>Status Report</h2>
  <h4>Mnemonic:  STATUS</h4>
  <h4>Shortest abbreviation:  ST</h4>
 <p>This command gives you information about the current state of your starship and fleet.
 
 Status information is free—it uses no time or energy, and if you are in battle, the enemies are not given another chance to hit you.
 
 Status information can also be obtained by doing a short-range scan. See the SRSCAN command for details.
 </p>
 <h4> Status Report params</h4>
 <p>NO PARAMS</p>
 <h3>Events Flow</h3>
 <ol>
 <li><b>Normal use case</b>
 <p><b>Status -> StatusPerformed</b>(StatusDTO)</p></li>
 <li><b>When unit of this type cannot use the command </b>
 <p><b>Status -> NoTechnology</b></p></li>
 </ol>

