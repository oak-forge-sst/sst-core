 <h2>Ask for Help</h2>
  <h4>Mnemonic:  HELP</h4>
  <h4>Shortest abbreviation:  H</h4>
 <p>This command reads the appropriate section from the properties files, providing the file is in the project resourcesBundle.
 </p>
 <h4> Help params</h4>
NO PARAMS
 <h3>Events Flow</h3>
 
<li> <b>Normal use case</b>
 <p>(Command) <b>Help(String commandParam)</b><b> -></b>
  
  (Event) <b>HelpPerformed</b>HelpPerformed(String commandParam)</p>




