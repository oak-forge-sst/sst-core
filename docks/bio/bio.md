# Tales of Super Star Trek Devs

## Mentor
* [Michał Kapłon](here_be_link_to_some_social_stuff)

## Devs
* [Patryk Szydłowski](here_be_link_to_some_social_stuff)
* [Patryk Hajzer](here_be_link_to_some_social_stuff)
* [Gabriel Wiewiórka](here_be_link_to_some_social_stuff)
* [Bartek Jakobsche](here_be_link_to_some_social_stuff)
* [Jakub Bachusz](here_be_link_to_some_social_stuff)

## Sprints stories
* [Sprint 8](./sprint-8/sprint.md)
* [Sprint 9](./sprint-9/sprint.md)