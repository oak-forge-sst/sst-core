# Początek przygody z Dev Stories

### Oficjalnie się zaczęło!

Przez następny tydzień razem z całym [team'em](../../bio.md#Devs)
postaramy się produkować się wpisy dla Dev Stories.
Pomysł wyszedł od naszego mentora [Michała](../../bio.md#Mentor).
Gdy tylko o nim usłyszałem, od razu chciałem się za to zabrać!
Oczywiście nie może to konkurować z produkowaniem kodu, ma być to miły dodatek i wgląd na historię projektu
(niczym w Event Sourcing'u :laughing:).

Z tego powodu uzgodniliśmy (przy wielkiej rozkminie podczas meczu ping-ponga), że codziennie każdy spróbuje napisać niedługi wyrywek o tym co było dla niego najważniejsze tego dnia.
Wiadomo, jeżeli danego dnia zrobiliśmy jedynie kolejnego pojo'sa z wygenerowanymi przez IDE getter'ami i setter'ami to nie ma o czym pisać :wink:.

Same teksty nie będą dotyczyć jedynie rzeczy ściśle związanych z projektem (sam mam m.in. pomysł o wpisie nt. markdown'u, żeby cały team mógł z tego skorzystać :smiley:).

Jeżeli pomysł sprawdzi się w przeciągu następnego tygodnia, to w dniu planowania kolejnego sprint'u pokażemy efekty Michałowi (pozdro Michał :sunglasses:) i zdecydujemy, czy zostanie to z nami na przyszłość.

Do tego dnia, nasze wpisy będą przybywać!

---
Patryk Szydłowski

