    # Sprint 8

[full bio](../bio.md)

## Dev stories

* ### By Dev

    * #### Patryk Szydłowski

        * [Początek przygody z Dev Stories](22-08-2018/dev-stories-0.md)

    * #### Patryk Hajzer

        * [SST-57](22-08-2018/22-08-2018-Patryk-H.md)

    * #### Gabriel Wiewiórka

        * Oops no stuff yet

    * #### Bartek Jakobsche

        * Oops no stuff yet

    * #### Jakub Bachusz

        * Oops no stuff yet

* ### By Date

    * #### 22-08-2018

        * [Początek przygody z Dev Stories](22-08-2018/dev-stories-0.md)

        * [SST-57](22-08-2018/22-08-2018-Patryk-H.md)

    * #### 22-08-2018
